/**
 * 
 */
package src.baloot.utils;

import src.baloot.extensions.BalootExtension;

/**
 * @author MSKumar
 * Commands Class contains all the string commands
 */
public class Commands {
	
	// Testing slack
	/* Game Types */	
	public static String SAN = "san";
	public static String HOKOM = "hokom";
	public static String PASS = "pass";
	public static String ASHKAL = "ashkal";
	public static String OPEN_DOUBLE = "openDouble";
	public static String CLOSED_DOUBLE = "closedDouble";
	public static String BALOOT = "baloot";
	
	public static String NOT_SELECTED = "NotSelected";
	
	/* Application Instance*/
	public static BalootExtension appInstance;	
	
	/* Commands */
	
	public static String SINGLE_PLAYER_GAME = "singlePlayerGame";
	
	public static String USER_JOINED = "userJoined";
	
	public static String WAITING_PLAYER_JOINED = "waitingPlayerJoined";
	
	public static String WAITING_PLAYER_LEFT = "waitingPlayerLeft";
	
	public static String JOIN_LEAVE = "joinLeave";
	
	public static String TRY_TO_JOIN_SECOND_TABLE = "tryToJoinSecondTable";
	
	public static String HOST_LEFT = "hostLeft";
	
	public static String NOT_HAVEING_ENOUGH_GAMES = "notHavingEnoughGames";
	
	public static String QUICK_START_GAME = "quickStartGame";
	
	public static String QUICK_START_PRIVATE_TABLE = "quickStartPrivateTable";
	
	public static String DISTRIBUTE_CARDS = "distributeCards";
	
	public static String DISTRIBUTE_REMAINING_CARDS = "distributeRemainingCards";
	
	public static String DISCARD = "discard";
	
	public static String TRICK_COMPLETED = "trickCompleted";
	
	public static String ROUND_COMPLETED = "roundCompleted";
	
	public static String GAME_COMPLETED = "gameCompleted";
	
	public static String Bonus = "bonus";
	
	public static String DISCARD_BONUS = "discardBonus";
	
	public static final String BUDDYLIST_REQUEST = "buddyListRequest";
	
	public static final String BUDDYLIST_RESPONSE = "buddyListResponse";
		
	public static String BID = "bid";
	
	public static String TURN = "turn";
	
	public static String LEAVE_TABLE = "leaveTable";
	
	public static String HIGHEST_CARD = "highestCard";
	
	public static String HOKOM_SELECTED_IN_SECOND_ROUND = "hokomSelectedInSecondRound";
	
	public static String START = "start"; 
	
	public static String PLAY_AI = "playAI";
	
	public static String SELECT_HOKOM_SUIT = "selectHokomSuit";
	
	public static String START_NEXT_ROUND = "startNextRound";
	
	public static final String GET_LOBBY_ROOM = "getLobbyRoom";
	
	public static final String GET_LOBBY = "getLobby";
	
	public static final String UPDATE_LOBBY = "updateLobby";
	
	public static final String ADD_LOBBY = "addLobby";
	
	public static final String REMOVE_LOBBY = "removeLobby";
	
	public static final String ONLINE_USERS_COUNT = "onlineUsersCount";
	
	public static final String CHANGE_PLAYER = "changePlayer";
	
	public static final String REPLACE_AI_WITH_USER = "replaceAiWithUser";
	
	public static final String SINK_DATA = "sinkData";
	
	public static final String RECONNECTION_COMPLETED = "reconnectionCompleted";
	
	public static final String GET_USER_STATUS = "getUserStatus";
	
	public static final String GET_FACE_BOOK_USER_DETAILS = "getFaceBookUserDetails";
	
	public static final String GET_USER_WON_GAMES_COUNT = "getUserWonGamesCount";
	
	public static final String GET_DEVICE_TYPE = "getDeviceType";
	
	public static final String SET_EMAIL_VISIBLE = "setEmailVisible";
	
	public static final String SOUND_STATUS = "soundStatus";
	
	public static final String CREATE_PRIVATE_TABLE = "createPrivateTable";
	
	public static final String ROOM_FULL = "roomFull";
	
	public static final String JOIN_GAME_ROOM = "joinGameRoom";
	
	public static final String OCCUPY_SEAT = "occupySeat";
	
	public static final String RESOURCE_BUSY_TRY_AGAIN = "resourceBusyTryAgain";
	
	public static final String YOU_ALREADY_OCCUPIED = "youAlreadyOccupied";
	
	public static final String START_PRIVATE_TABLE  = "startPrivateTable";
	
	public static final String INVITE_PLAYER = "invitePlayer";
	
	public static final String HOST_KICK_USER = "hostKickUser";	
	
	public static final String REMATCH = "rematch";	
	
	public static final String REMATCH_FOR_SPECTATOR = "rematchForSpectator";	
	
	public static final String CLOSE_ROOM = "closeRoom";	
	
	public static final String CHANGE_PLAYER_TO_SPECTATOR = "changePlayerToSpectator";
	
	public static final String WON_MEDAL = "wonMedal";
	
	public static final String FORCE_LOGIN = "forceLogin";
	
	public static final String PLAYER_CHANGED_AS_AI = "playerChangedAsAI";
	
	public static final String CHAT = "chat";
	
	public static final String CHANGE_PASSWORD = "changePassword";
	
	public static final String CHANGE_PHOTO_URL = "changePhotoUrl";
	
	public static final String TRANSFER_GAMES = "transferGames";
	
	public static final String DEVICE_TYPE = "deviceType";
	
	public static final String USER_PING = "userPing";
	
	public static final String SERVER_PING = "serverPing";
	
	public static final String CLIENT_LOG = "clientLog";
	
	
	public static final String GET_USER_DETAILS = "getUserDetails";	
	
	public static final String GET_OTHER_USER_DETAILS = "getOtherUserDetails";
	
	public static final String GET_ALL_USERS_DETAILS = "getAllUsersDetails";
	
	public static final String GET_TOP_PLAYERS_LIST = "getTopPlayersList";
	
	public static final String DUPLICATE_USER_EXCEPTION = "duplicateUserException";
	
	public static final String DUPLICATE_SPECTATOR_EXCEPTION = "duplicateSpectatorException";
	
	public static final String AI_SOUND = "AISound";
	
	public static final String DOUBLE = "Double";
	
	
	
	/* Medal Types */
	
	public static Integer MEDAL_1 = 1;
	public static Integer MEDAL_2 = 2;
	public static Integer MEDAL_3 = 3;
	public static Integer MEDAL_4 = 4;
	public static Integer MEDAL_5 = 5;
	public static Integer MEDAL_6 = 6;
	public static Integer MEDAL_7 = 7;
	public static Integer MEDAL_8 = 8;
	public static Integer MEDAL_9 = 9;
	public static Integer MEDAL_10 = 10;
	public static Integer MEDAL_11 = 11;
	public static Integer MEDAL_12 = 12;
	public static Integer MEDAL_13 = 13;
	public static Integer MEDAL_14 = 14;
	public static Integer MEDAL_15 = 15;
	public static Integer MEDAL_16 = 16;
	public static Integer MEDAL_17 = 17;
	public static Integer MEDAL_18 = 18;
	public static Integer MEDAL_19 = 19;
	public static Integer MEDAL_20 = 20;
	public static Integer MEDAL_21 = 21;
	public static Integer MEDAL_22 = 22;
	public static Integer MEDAL_23 = 23;
	public static Integer MEDAL_24 = 24;
	public static Integer MEDAL_25 = 25;
	public static Integer MEDAL_26 = 26;
	public static Integer MEDAL_27 = 27;	
	public static Integer MEDAL_28 = 28;
	public static Integer MEDAL_29 = 29;
	public static Integer MEDAL_30 = 30;
	public static Integer MEDAL_31 = 31;
	public static Integer MEDAL_32 = 32;
	public static Integer MEDAL_33 = 33;
	public static Integer MEDAL_34 = 34;
	public static Integer MEDAL_35 = 35;
	public static Integer MEDAL_36 = 36;
	public static Integer MEDAL_37 = 37;
	public static Integer MEDAL_38 = 38;
	public static Integer MEDAL_39 = 39;
	public static Integer MEDAL_40 = 40;
	public static Integer MEDAL_41 = 41;
	public static Integer MEDAL_42 = 42;
	public static Integer MEDAL_43 = 43;
	public static Integer MEDAL_44 = 44;
	public static Integer MEDAL_45 = 45;
	public static Integer MEDAL_46 = 46;
	public static Integer MEDAL_47 = 47;
	public static Integer MEDAL_48 = 48;
	public static Integer MEDAL_49 = 49;
	public static Integer MEDAL_50 = 50;
	
	public static final Integer MEDAL_51 = 51;
	public static final Integer MEDAL_52 = 52;
	public static final Integer MEDAL_53 = 53;
	
	public static final Integer MEDAL_54 = 54; // silver medal.
	public static final Integer MEDAL_55 = 55;
	public static final Integer MEDAL_56 = 56;	
}
