/**
 * 
 */
package src.baloot.utils;

import java.sql.Connection;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerBean;
import src.baloot.beans.TableBean;
import src.baloot.constants.Constant;
import src.baloot.exce.Validations;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.Zone;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class SQLProxy {

	Connection con = null;

	public SQLProxy(Zone zone) {
		
		try {
			con = zone.getDBManager().getConnection();
			Apps.sqlLog("SQL Connection Success ");
		} catch (Exception e) {
			Apps.sqlErr("SQLProxy SQL Connection Failed :" + e.toString(), null);
		}
		updateUsersPlayingGames();
		updateGameStatus();//RDV3287035
	}
	
	
	private void updateUsersPlayingGames()
	{
		String sqlUpdate = "update users set IsPlaying ='false'";
		Apps.sqlLog(" Sql Update :"+ sqlUpdate);
		
		try {			
				PreparedStatement pstmt = con.prepareStatement(sqlUpdate);					
				pstmt.executeUpdate();			
				pstmt.close();
			
		} catch (Exception e) {
			Apps.sqlErr("updateUsersPlayingGame :" + e.toString()+" Query "+sqlUpdate, null);
		}	
		
		sqlUpdate = "update guest_users set IsPlaying = 'false'";
		
		try {			
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);					
			pstmt.executeUpdate();			
			pstmt.close();
		
	} catch (Exception e) {
		Apps.sqlErr("Guest_updateUsersPlayingGame :" + e.toString()+" Query "+sqlUpdate, null);
	}
	}
	
	private void updateGameStatus()
	{
		String sqlUpdate = "update gamestatus set Status='Incomplete' where Status='Running'";
		Apps.sqlLog(" Sql Update :"+ sqlUpdate);
		
		try {			
				PreparedStatement pstmt = con.prepareStatement(sqlUpdate);					
				pstmt.executeUpdate();			
				pstmt.close();
			
		} catch (Exception e) {
			Apps.sqlErr("updateUsersPlayingGames" + e.toString()+" Query "+sqlUpdate, null);
		}		
	}

	public void getPublicTables() 
	{
		String sql = "Select * from public_tables;";
		Apps.sqlLog("Sql " + sql);
		
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet res = stmt.executeQuery();

			if (res.next()) {
				do {
					String tableid = res.getString("table_id");
					TableBean grb = new TableBean();
					grb.setTableid(tableid);
					grb.setTableType("public");
					Commands.appInstance.publicTables.add(grb);
				} while (res.next());
			}	
			
			res.close();
			stmt.close();
		} catch (Exception exception) {
			Apps.sqlErr("getPublicTables" + exception.toString()+" Query "+sql, null);
		}
	}

	public void gameStarted(GameBean gameBean) {
		
		String curTime = Apps.getDateString();
		String status = Constant.RUNNING;
		
		String sqlInsert = "Insert INTO gamestatus(TableID, GameID, Status, started_players, start_time, table_type, end_time) VALUES (?, ?, ?, ?, ?, ?, ?)";
		Apps.sqlLog("gameStarted "+ sqlInsert);

		try {
				PreparedStatement pstmt = con.prepareStatement(sqlInsert);
				
				pstmt.setString(1, gameBean.tableId);
				pstmt.setString(2, gameBean.gameId);
				pstmt.setString(3, status);
				pstmt.setString(4, gameBean._players.toString());
				pstmt.setString(5, curTime);
				pstmt.setString(6, gameBean.tableType);
				pstmt.setString(7, curTime);
				
				pstmt.executeUpdate();
				pstmt.close();
			
			} catch (Exception e) {
				
				Apps.sqlErr(" gameStarted : " + e.toString()+" Query "+sqlInsert, gameBean);
			}

		for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(!pb._isAI && !pb.isFakePlayer)
			{
				Integer userID = Apps.getPlayerId(pb._playerId);
				updateMiddleJoinedUserprofile(gameBean, gameBean.tableId, gameBean.gameId, userID, pb._playerId, gameBean.tableType);
			}
		}
	}


	public void updateMiddleJoinedUserprofile(GameBean gameBean, String tableId, String gameId, Integer userID, String player, String table_type) 
	{
	
		if(Apps.isGuestUser(player))
		{
			String sqlUpdate = "update guest_users set IsPlaying = 'true', TableID = ?  where ip_address = ? and game_date = ?;";		
			Apps.sqlLog(" Guest_SqlUpdate :"+ sqlUpdate);
			
			try {		
				
				 tableId = Apps.getBase64EncodedText(tableId);
					// tableId = gameBean.tableId;
				 
					PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
					
					pstmt.setString(1, tableId );
					pstmt.setString(2, player);			
					pstmt.setString(3, Apps.getDayString());	
					
					pstmt.executeUpdate();
					
					pstmt.close();		
				
			} catch (Exception e) {
				Apps.sqlErr("Guest_updateMiddleJoinedUserprofile :" + e.toString()+" Query "+sqlUpdate, gameBean);
			}
		}
		else
		{
			String sqlUpdate = "update users set IsPlaying = 'true', TableID = ?, GameID = ? where UserID = ?;";		
			Apps.sqlLog(" Sql Update :"+ sqlUpdate);
			
			try {		
					PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
					
					 tableId = Apps.getBase64EncodedText(tableId);
					// tableId = gameBean.tableId;
					pstmt.setString(1, tableId );
					pstmt.setString(2, gameId);			
					pstmt.setInt(3, userID);	
					
					pstmt.executeUpdate();
					
					pstmt.close();		
				
			} catch (Exception e) {
				Apps.sqlErr("updateMiddleJoinedUserprofile :" + e.toString()+" Query "+sqlUpdate, gameBean);
			}
		}
		
		
		insertUserGameHistory(gameBean, player, tableId, gameId, table_type);
	}
	
	public void updatePlayingStatusTrue(GameBean gameBean, String player)
	{
		
		
		Integer userID = Apps.getPlayerId(player);		
		
		if(Apps.isGuestUser(player))
		{
			String sqlUpdate = "update guest_users set IsPlaying = 'true', TableID = ? where ip_address = ? and game_date = ?";
			Apps.sqlLog(" Guest_updatePlayingStatusTrue SqlUpdate :"+ sqlUpdate);
			
			try {		
				PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
				
				//String tableId = gameBean.tableId;
				String tableId = Apps.getBase64EncodedText(gameBean.tableId);		
				
				pstmt.setString(1, tableId);
				pstmt.setString(2, player);			
				pstmt.setString(3, Apps.getDayString());	
				
				pstmt.executeUpdate();
				
				pstmt.close();		
			
			} catch (Exception e) {
				Apps.sqlErr("Guest_updatePlayingStatus :" + e.toString()+" Query "+sqlUpdate, gameBean);
			}
		}
		else
		{
			String sqlUpdate = "update users set IsPlaying = 'true', TableID = ?, GameID = ? where UserID = ?;";		
			Apps.sqlLog(" updatePlayingStatusTrue Sql Update :"+ sqlUpdate);
			
			try {		
					PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
					
					String tableId = Apps.getBase64EncodedText(gameBean.tableId);
					//String tableId = gameBean.tableId;
					
					pstmt.setString(1, tableId);
					pstmt.setString(2, gameBean.gameId);			
					pstmt.setInt(3, userID);	
					
					pstmt.executeUpdate();
					
					pstmt.close();		
				
			} catch (Exception e) {
				Apps.sqlErr("updatePlayingStatus :" + e.toString()+" Query "+sqlUpdate, gameBean);
			}
			
			
			/*String sql = "insert into ramana values(?,?,?)";
			
			try {		
				PreparedStatement pstmt = con.prepareStatement(sql);
				
				pstmt.setString(1, gameBean.tableId );
				pstmt.setString(2, gameBean.gameId);			
				pstmt.setString(3, player);	
				
				//pstmt.executeUpdate();
				
				pstmt.close();		
			
		      } catch (Exception e) {
			     Apps.sqlErr("updatePlayingStatus :" + e.toString()+" Query "+sqlUpdate, gameBean);
		    }*/
		}		
	}
	
	public void updatePlayingStatusFalse(GameBean gameBean, String player)
	{
		Integer userID = Apps.getPlayerId(player);
		
		if(Apps.isGuestUser(player))
		{
			String sqlUpdate = "update guest_users set IsPlaying = 'false', TableID = ? where ip_address = ? and game_date = ?";
			
			try {		
				PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
				
				String tableId = Apps.getBase64EncodedText(gameBean.tableId);
				//String tableId = gameBean.tableId;
				
				pstmt.setString(1, tableId );
				pstmt.setString(2, player);			
				pstmt.setString(3, Apps.getDayString());	
				
				pstmt.executeUpdate();
				
				pstmt.close();		
			
			} catch (Exception e) {
				Apps.sqlErr("updatePlayingStatus :" + e.toString()+" Query "+sqlUpdate, gameBean);
			}
		}
		else
		{
			String sqlUpdate = "update users set IsPlaying = 'false', TableID = ?, GameID = ? where UserID = ?;";		
			Apps.sqlLog(" Sql Update :"+ sqlUpdate);
			
			try {		
				String tableId = Apps.getBase64EncodedText(gameBean.tableId);
				//String tableId = gameBean.tableId;
				
					PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
					
					pstmt.setString(1, tableId );
					pstmt.setString(2, gameBean.gameId);			
					pstmt.setInt(3, userID);	
					
					pstmt.executeUpdate();
					
					pstmt.close();		
				
			} catch (Exception e) {
				Apps.sqlErr("updatePlayingStatus :" + e.toString()+" Query "+sqlUpdate, gameBean);
			}
		}		
	}

	// Game Completed or User out of the game
	public void updateUserProfile(GameBean gameBean, String player, Integer points, String tableid, String gameid) {		
				
		String status = "";
		
		if(points == 0)
		{
			status = Constant.LOST;
		}			
		else{
			status = Constant.WON;
		}
			
		
		updateUserGameHistory(gameBean, player, status, gameid);
		
		try
		{
			Commands.appInstance.medalsBsn.updateUserProfile(player, points, tableid, gameBean);
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		//updateUserBeltTypeAndRank(player, status);
	}

	public void insertUserGameHistory(GameBean gameBean, String user_name, String tableId, String game_id, String table_type)
	{
		String sqlInsert = "insert into user_game_history(user_id, user_name, TableId, GameID, start_time, table_type, Status) values (?, ?, ?, ?, ?, ?, ?)";
		Apps.sqlLog("insertUserGameHistory "+sqlInsert);
		
		String start_time = Apps.getDateString();
		Integer user_id = getUserID(user_name);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlInsert);
			
			pstmt.setInt(1, user_id);	
			pstmt.setString(2, user_name);
			pstmt.setString(3, tableId);
			pstmt.setString(4, game_id);
			pstmt.setString(5, start_time);			
			pstmt.setString(6, table_type);
			pstmt.setString(7, "playing");
			
		    pstmt.executeUpdate();	
		    
		    pstmt.close();
		}
		 catch(Exception e){
			 Apps.sqlErr("insertUserGameHistory :"+ e.toString()+" Query "+sqlInsert, gameBean);
		}
	}
	
	public void updateUserGameHistory(GameBean gameBean, String player, String status, String gameId)
	{
		 String sqlUpdate = "update user_game_history set  end_time = ?, Status = ? where user_id = ? and GameID = ?;";
		 
		 String end_time = Apps.getDateString();
		 Integer user_id = getUserID(player);
		 		 
		 try
			{
				PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
				
				pstmt.setString(1, end_time);
				pstmt.setString(2, status);
				pstmt.setInt(3, user_id);
				pstmt.setString(4, gameId);
						
			    pstmt.executeUpdate();				    
			    pstmt.close();			  
			}
			 catch(Exception e){
				 Apps.sqlErr("updateUserGameHistory "+ e.toString()+" Query "+sqlUpdate, null);
			}
	}

	public void insertStatement1(String sqlInsert) 
	{
		Apps.sqlLog("insertStatement  "+sqlInsert);
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlInsert);
			
		    pstmt.executeUpdate();	
		    
		    pstmt.close();
		}
		 catch(Exception e){
			 Apps.sqlErr("insertStatement :"+ e.toString()+" Query "+sqlInsert, null);
		}
	}

	private void updateStatement(String sqlUpdate) 
	{
		Apps.sqlLog("Update Statement :"+ sqlUpdate);
		try {
				PreparedStatement stmtUpdate = con.prepareStatement(sqlUpdate);
				stmtUpdate.executeUpdate();
				
				stmtUpdate.close();
				
			} catch (Exception e) {
				Apps.sqlErr("updateStatement  "+ e.toString()+" Query "+sqlUpdate, null);
			}
	}

	// Update Game status
	public void gameCompletedUpdateGameStatus(GameBean gameBean, String gameid) 
	{
		String curTime = Apps.getDateString();
		
		String sqlUpdate = "update gamestatus set Status = 'Completed', end_time = ? where GameID = ?";
		Apps.sqlLog(" gameCompletedUpdateGameStatus "+sqlUpdate);
		
		try {
				PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
				
				pstmt.setString(1, curTime);
				pstmt.setString(2, gameid);
				
				pstmt.executeUpdate();
				pstmt.close();
				
		} catch (Exception e) {
			Apps.sqlErr("gameCompletedUpdateGameStatus "+ e.toString()+" Query "+sqlUpdate, gameBean);
		}
	}


	public void createPrivateTable(String owner, String created_date)
	{
		String sqlInsert = "Insert INTO private_tables(owner, table_id, created_date) VALUES (?, ?, ?)";
		Apps.sqlLog("createPrivateTable " + sqlInsert);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlInsert);
			
			pstmt.setString(1, owner);	
			pstmt.setString(2, owner);
			pstmt.setString(3, created_date);
			
		    pstmt.executeUpdate();	
		    
		    pstmt.close();
		}
		 catch(Exception e){
			 Apps.sqlErr("createPrivateTable :"+ e.toString()+" Query "+sqlInsert, null);
		}		
	}

	public void closePrivateTable(String created_date) 
	{
		String curTime = Apps.getDateString();
		String sqlUpdate = "update private_tables set closed_date = ? where created_date = ?";

		Apps.sqlLog(" closePrivateTable " +sqlUpdate);
		try {
				PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
				
				pstmt.setString(1, curTime);
				pstmt.setString(2, created_date);
				
				pstmt.executeUpdate();
				pstmt.close();
		} catch (Exception e) {
			Apps.sqlErr("closePrivateTable :"+ e.toString()+" Query "+sqlUpdate, null);
		}
	}

	public void setFirstLogin(String player, Integer userId, String ip) 
	{
		String selectStatement="";
		Long lastlogin = null;
		
		Apps.sqlLog("setFirstLogin");
		String sqlUpdate = "";		
		
		if (userId != null) 
		{				
			selectStatement = "select * from user_data where user_id = ?";
			Apps.sqlLog(selectStatement);
			try {
				
				PreparedStatement pst = con.prepareStatement(selectStatement);
				pst.setInt(1,userId);			
				ResultSet resSet = pst.executeQuery();					
				
				while (resSet.next()) 
				{		
					//System.out.println(" setFirstLogin "+resSet.getString("day_first_login"));
					
					long current_time = System.currentTimeMillis();
					long oneday = 24 * 60 * 60 * 1000;
					
					//System.out.println(" current_time "+current_time);
					//System.out.println("  oneday      "+oneday);
					
					Apps.sqlLog("First Login"	+ resSet.getString("day_first_login"));
					if (resSet.getString("day_first_login") == null) 
					{
						
						// Update new
						sqlUpdate = "update user_data set day_first_login="
								+ current_time
								+ ", noof_signin_fordays_inarow =" + 1
								+ " where user_id=" + userId + ";";
						Apps.sqlLog(sqlUpdate);
						
						updateStatement(sqlUpdate);
					} 
					else 
					{			
						lastlogin = Long.parseLong(resSet.getString("day_first_login"));
						
						//System.out.println(" lastlogin + oneday "+(lastlogin + oneday));
						//System.out.println(" current_time       "+current_time);
						// Update
						if (lastlogin + oneday < current_time)  
						{
							// Check for Days Changed 
							long elapsedTime = current_time - (lastlogin + oneday);
							int daysLogin = resSet.getInt("noof_signin_fordays_inarow");

							//System.out.println(" elapsedTime       "+elapsedTime);
							//System.out.println("  oneday           "+oneday);
							
							//System.out.println("  Before daysLogin           "+daysLogin);
							
							if (elapsedTime > oneday) 
							{
								daysLogin = 0;
							} 
							else 
							{
								daysLogin++;
							}
							
							System.out.println("  Finally daysLogin           "+daysLogin);
							
							// Edit
							sqlUpdate = "update user_data set day_first_login="
									+ current_time + ", noof_wins_inaday=" + 0
									+ ", noof_signin_fordays_inarow ="
									+ daysLogin + " where user_id=" + userId+ ";";
									
							
							Apps.sqlLog(sqlUpdate);
							Commands.appInstance.proxy.updateStatement(sqlUpdate);
							
							Commands.appInstance.medalsBsn.noofSignInForDaysInaRow(player, "Lobby");
						}
					}
				}
			
			resSet.close();
			pst.close();
				
			} catch (Exception e) {
				Apps.sqlErr("setFirstLogin "+ e.toString()+" Query "+selectStatement, null);
			}
		}
		
		updateFreeGamesCount(userId, ip);
	}
	
	public void updateFreeGamesCount(Integer UserID, String ip)
	{
		String lastUpdate = null;
		
		if(UserID != null)
		{
			String sqlSelect = "select last_updated from free_games where UserID = ?"; 
			
			try
			{
				PreparedStatement pstmt = con.prepareStatement(sqlSelect);
				
				pstmt.setInt(1, UserID);
				ResultSet rs = pstmt.executeQuery();
			
				while(rs.next())
				{
					lastUpdate = rs.getString("last_updated");
				}
				
				rs.close();
				pstmt.close();
				
			}catch(Exception e){
				Apps.sqlErr("updateFreeGamesCount  sqlSelect :"+ e.toString()+" Query "+sqlSelect, null);
			}
			
			long oneday = 24 * 60 * 60;
			long elapsedTime = 0;			
			
			Date date = Apps.getDateFromString(lastUpdate);

			elapsedTime = Apps.getTimeDifference(date);
			
			//System.out.println(" elapsed Time "+ elapsedTime);
			if(elapsedTime > oneday)
			{
				String sqlFreeUpdate = "update free_games set no_of_free_games = ?, last_updated = ? where UserID = ?";
				
				try
				{
					PreparedStatement pstmt = con.prepareStatement(sqlFreeUpdate);
					
					pstmt.setInt(1, Constant.FREE_GAMES_COUNT);
					pstmt.setString(2, Apps.getDateString());
					pstmt.setInt(3, UserID);
					pstmt.executeUpdate();
				
					pstmt.close();
					
				}catch(Exception e){
					Apps.sqlErr("updateFreeGamesCount  sqlFreeUpdate :"+ e.toString()+" Query "+sqlFreeUpdate, null);
				}
			}
		}
	}
	
	public String getTableId1(String gameId)
	{
		//AppMethods.sqlLog("getPlayerUserID "+ player);
		
		String tableId = "";
		String sql = "select TableID from gamestatus where GameID = ?";
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1,gameId);			
			ResultSet res = pstmt.executeQuery();			   
		    while (res.next()) 
			{
		    	tableId = res.getString("TableID");
			}
		    
			res.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("getTableId:"+ e.toString()+" Query "+sql, null);
		}		
		return tableId;
	}
	
	public String getGender(int userId)
	{
		String sqlSelect = "select Gender from users where UserID = ?";
		Apps.sqlLog("getGender :"+sqlSelect);
		
		String gender = null;
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlSelect);
			pstmt.setInt(1, userId);
			
		    ResultSet rs = pstmt.executeQuery();			   
		    while (rs.next()) 
			{
		    	gender = rs.getString("Gender");				
			}	
		    
			rs.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("getGender  :"+ e.toString()+" Query "+sqlSelect, null);
		}
		
		return gender;
		
	}
	
	public int getUserID(String userName)
	{
		int userID = -1;
		String sqlSelect = "select * from users";
		Apps.sqlLog("getUserID :"+sqlSelect);
		
		
		if(Apps.isGuestUser(userName))
		{
			return -1;
		}
		else
		{
			try
			{
				PreparedStatement pstmt = con.prepareStatement(sqlSelect);
				
			    ResultSet rs = pstmt.executeQuery();			   
			    while (rs.next()) 
				{
			    	String dbName = rs.getString("Username");
					
					if(userName.equalsIgnoreCase(dbName))
					{
						userID = rs.getInt("UserID");
						break;
					}
				}	
			    
				rs.close();
				pstmt.close();
				
			}catch(Exception e){
				Apps.sqlErr("getUserID  :"+ e.toString()+" Query "+sqlSelect, null);
			}
		}
		
		
		/*if(userID == null)
		{
			String sqlArabicSelect = "select * from users";
			
			try
			{
				PreparedStatement pstmt = con.prepareStatement(sqlArabicSelect);
				
				ResultSet rs = pstmt.executeQuery();
			
				while(rs.next())
				{
					String dbName = rs.getString("Username");
				
					if(userName.equals(dbName))
					{
						userID = rs.getInt("UserID");
						break;
					}
				}
				pstmt.close();
				
			}catch(Exception e){
				Apps.sqlErr("getUserID_for_arabic_user sqlSelect :"+ e.toString()+" Query "+sqlArabicSelect, null);
			}
		}*/
		
		return userID;
	}
	
	public String getStoredDatabaseUserName(String loginUserName)
	{
		String databaseUsername = null;
		
		String sqlSelect = "select * from users";
		Apps.sqlLog("getUserID :"+sqlSelect);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlSelect);
			
		    ResultSet rs = pstmt.executeQuery();			   
		    while (rs.next()) 
			{
		    	databaseUsername = rs.getString("Username");
				
				if(loginUserName.equalsIgnoreCase(databaseUsername))
				{
					return databaseUsername;
				}
			}	
		    
			rs.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("getUserID  :"+ e.toString()+" Query "+sqlSelect, null);
		}
		
		return databaseUsername;
	}
	
	
	public String getPassword(Integer userID)
	{
		String password = null;
		String sqlSelect = "select Password from users where UserID = ?";
		Apps.sqlLog("getPassword :"+sqlSelect);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlSelect);
			pstmt.setInt(1,userID);
			
		    ResultSet rs = pstmt.executeQuery();			   
		    while (rs.next()) 
			{
		    	password = rs.getString("Password");
			}	
		    
			rs.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("getPassword  :"+ e.toString()+" Query "+sqlSelect, null);
		}	
		
		return password;
	}
	
	public boolean isEmailActivated(Integer userID)
	{
		boolean isActivated = false;
		String sqlSelect = "select Activation from users where UserID = ?";
		Apps.sqlLog("isEmailActivated :"+sqlSelect);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlSelect);
			pstmt.setInt(1,userID);
			
		    ResultSet rs = pstmt.executeQuery();			   
		    while (rs.next()) 
			{
		    	if(Constant.ACTIVATED.equals(rs.getString("Activation")))
		    	{
		    		isActivated = true;
		    	}
			}	
		    
			rs.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("isEmailActivated  :"+ e.toString()+" Query "+sqlSelect, null);
		}	
		
		return isActivated;
	}
	
	public ISFSObject getCurrentTalbeId(String userName)
	{
		Integer userID = Apps.getPlayerId(userName);
		String tableId = "null";
		
		if(Apps.isGuestUser(userName))
		{
			String sqlSelect = "select TableID from guest_users where ip_address = ? and game_date = ? ";
			Apps.sqlLog("Guest_getCurrentTalbeId :"+sqlSelect);
			
			try
			{
				PreparedStatement pstmt = con.prepareStatement(sqlSelect);
				pstmt.setString(1, userName);
				pstmt.setString(2, Apps.getDayString());
				
			    ResultSet rs = pstmt.executeQuery();			   
			    while (rs.next()) 
				{
			    	tableId = rs.getString("TableID");
				}	
			    
				rs.close();
				pstmt.close();
				
			}catch(Exception e){
				Apps.sqlErr(" Guest_getCurrentTalbeId  :"+ e.toString()+" Query "+sqlSelect, null);
			}
		}
		else
		{
			String sqlSelect = "select *from users where UserID = ?";
			Apps.sqlLog("getCurrentTalbeId :"+sqlSelect);
			
			try
			{
				PreparedStatement pstmt = con.prepareStatement(sqlSelect);
				pstmt.setInt(1, userID);
				
			    ResultSet rs = pstmt.executeQuery();			   
			    while (rs.next()) 
				{
			    	tableId = rs.getString("TableID");
				}	
			    
				rs.close();
				pstmt.close();
				
			}catch(Exception e){
				Apps.sqlErr("getCurrentTalbeId  :"+ e.toString()+" Query "+sqlSelect, null);
			}
		}
		
		
		
		/*String select = "select *from ramana";
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(select);			
			
		    ResultSet rs = pstmt.executeQuery();			   
		    while (rs.next()) 
			{		    	
		    	//System.out.println(" name "+rs.getString("name")+" dept "+rs.getString("dept")+"  email "+rs.getString("email"));
			}	
		    
			rs.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("getCurrentTalbeId  :"+ e.toString()+" Query "+select, null);
		}
		*/
		
		
		ISFSObject sfso = new SFSObject();
		
		tableId = Apps.getBase64DecodedText(tableId);
		
		sfso.putUtfString("tableId", tableId);		
		
		return sfso;
	}
	
	
	public ISFSObject getUserDetails(String player, ISFSObject sfso)
	{
		Integer  userID = Apps.getPlayerId(player);
		
		if(Apps.isGuestUser(player))
		{
			String sqlSelect = "select * from guest_users where ip_address = ? and game_date = ?";
			Apps.sqlLog("Guest_getUserDetails :"+sqlSelect);
			
			try
			{
				PreparedStatement pstmt = con.prepareStatement(sqlSelect);
				pstmt.setString(1, player);
				pstmt.setString(2, Apps.getDayString());
				
			    ResultSet rs = pstmt.executeQuery();			   
			    while (rs.next()) 
				{			    	
			    	sfso.putUtfString("userName", player);
			    	sfso.putUtfString("isPlaying", rs.getString("IsPlaying"));
			    	sfso.putBool("isGuestUser", true);
			    	
			    	// adding dummy values.
			    	String guestUser = "GuestUser";
			    	
			    	sfso.putInt("userId", -100);
			    	sfso.putUtfString("password", guestUser);
			    	sfso.putUtfString("email", guestUser);
			    	sfso.putUtfString("gender", guestUser);
			    	sfso.putUtfString("profilePictureUrl", guestUser);
			    	sfso.putInt("totalNoofGamesPlayed", 0);
			    	sfso.putInt("totalNoofWins", 0);
			    	sfso.putInt("totalNoofLost", 0);
			    	sfso.putInt("noofGames", 0);
			    	sfso.putInt("noofFreeGames", 0);
			    	sfso.putIntArray("medalsList", new ArrayList<Integer>());
			    	sfso.putInt("emailVisible", 0);
			    	sfso.putInt("status", 0);
			    	//sfso.putUtfString("beltType", rs.getString("belt_type"));
			    	//sfso.putInt("userRank", rs.getInt("user_rank"));
			    	sfso.putInt("points", 0);
			    	sfso.putUtfString("gameSound", guestUser);
			    	sfso.putUtfString("backGroundSound", guestUser);
				}	
			    
				rs.close();
				pstmt.close();
				
			}catch(Exception e){
				Apps.sqlErr("Guest_getUserDetails  :"+ e.toString()+" Query "+sqlSelect, null);
			}
		}
		else
		{
			String sqlSelect = "select * from users where UserID = ?";
			Apps.sqlLog("getUserDetails :"+sqlSelect);
			
			try
			{
				PreparedStatement pstmt = con.prepareStatement(sqlSelect);
				pstmt.setInt(1,userID);
				
			    ResultSet rs = pstmt.executeQuery();			   
			    while (rs.next()) 
				{
			    	int userId = rs.getInt("UserID");
			    	
			    	sfso.putBool("isGuestUser", false);
			    	
			    	
			    	sfso.putInt("userId", userId);
			    	sfso.putUtfString("userName", rs.getString("Username"));
			    	sfso.putUtfString("password", rs.getString("Password"));
			    	sfso.putUtfString("email", rs.getString("EMail"));
			    	sfso.putUtfString("gender", rs.getString("Gender"));
			    	sfso.putUtfString("profilePictureUrl", rs.getString("ProfilePictureUrl"));
			    	sfso.putInt("totalNoofGamesPlayed", rs.getInt("TotalNoofgamesplayed"));
			    	sfso.putInt("totalNoofWins", rs.getInt("Totalnoofwins"));
			    	sfso.putInt("totalNoofLost", rs.getInt("Totalnooflost"));
			    	sfso.putUtfString("isPlaying", rs.getString("IsPlaying"));
			    	sfso.putInt("noofGames", rs.getInt("no_of_games"));
			    	sfso.putInt("noofFreeGames", getNoofFreeGames(rs.getInt("UserID")));
			    	sfso.putIntArray("medalsList", getUserMedalsList(userId));
			    	sfso.putInt("emailVisible", rs.getInt("email_visible"));
			    	sfso.putInt("status", Apps.getStatus(rs.getString("Username")));
			    	//sfso.putUtfString("beltType", rs.getString("belt_type"));
			    	//sfso.putInt("userRank", rs.getInt("user_rank"));
			    	sfso.putInt("points", rs.getInt("Points"));
			    	sfso.putUtfString("gameSound", rs.getString("game_sound"));
			    	sfso.putUtfString("backGroundSound", rs.getString("back_ground_sound"));	    	
			    	
				}	
			    
				rs.close();
				pstmt.close();
				
			}catch(Exception e){
				Apps.sqlErr("getUserDetails  :"+ e.toString()+" Query "+sqlSelect, null);
			}
		}
			
		
		return sfso;
	}
	
	public int getNoofFreeGames(Integer userID)
	{
		String sqlSelect = "select * from free_games where UserID = ?";
		Apps.sqlLog("getNoofFreeGames :"+sqlSelect);
		int noofGames = 0;
		
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlSelect);
			pstmt.setInt(1,userID);
			
		    ResultSet rs = pstmt.executeQuery();			   
		    while (rs.next()) 
			{
		    	noofGames = rs.getInt("no_of_free_games");
			}	
		    
			rs.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("getNoofFreeGames  :"+ e.toString()+" Query "+sqlSelect, null);
		}
		
		return noofGames;
	}
	
	public boolean isPlayingGame(String player)
	{
		Integer userID = Apps.getPlayerId(player);
		
		boolean isPlaying = false;
		
		if(Apps.isGuestUser(player))
		{
			String sqlSelect = "select IsPlaying from guest_users where ip_address = ? and game_date =?";
			Apps.sqlLog(" guest_isPlayingGame :"+sqlSelect);
			
			try
			{
				PreparedStatement pstmt = con.prepareStatement(sqlSelect);
				pstmt.setString(1, player);
				pstmt.setString(2, Apps.getDayString());
				
			    ResultSet rs = pstmt.executeQuery();			   
			    while (rs.next()) 
				{
			    	if(Constant.TRUE.equals(rs.getString("IsPlaying")))
			    	{
			    		isPlaying = true;
			    	}
				}	
			    
				rs.close();
				pstmt.close();
				
			}catch(Exception e){
				Apps.sqlErr(" Guest isPlayingGame  :"+ e.toString()+" Query "+sqlSelect, null);
			}
		}
		else
		{
			String sqlSelect = "select IsPlaying from users where UserID = ?";
			Apps.sqlLog("isPlayingGame :"+sqlSelect);
			
			try
			{
				PreparedStatement pstmt = con.prepareStatement(sqlSelect);
				pstmt.setInt(1, userID);
				
			    ResultSet rs = pstmt.executeQuery();			   
			    while (rs.next()) 
				{
			    	if(Constant.TRUE.equals(rs.getString("IsPlaying")))
			    	{
			    		isPlaying = true;
			    	}
				}	
			    
				rs.close();
				pstmt.close();
				
			}catch(Exception e){
				Apps.sqlErr("isPlayingGame  :"+ e.toString()+" Query "+sqlSelect, null);
			}
		}
		
		
		return isPlaying;		
	}
	
	
	
	public void updatePassword(int userID, String password)
	{
		String sqlUpdate = "update users set Password = ? where UserID = ? ";
		Apps.sqlLog("updatePassword :"+sqlUpdate);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setString(1, password);
			pstmt.setInt(2, userID);
			
		    pstmt.executeUpdate();			   
		    
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("updatePassword  :"+ e.toString()+" Query "+sqlUpdate, null);
		}
	}
	
	public void updatePhotoUrl(Integer userID, String photoUrl)
	{
		String sqlUpdate = "update users set ProfilePictureUrl = ? where UserID = ? ";
		Apps.sqlLog("updatePhotoUrl :"+sqlUpdate);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setString(1, photoUrl);
			pstmt.setInt(2, userID);
			
		    pstmt.executeUpdate();			   
		    
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("updatePhotoUrl  :"+ e.toString()+" Query "+sqlUpdate, null);
		}
	}
	
	public void transferGames(Integer sender, Integer reciever, int gamesCount)
	{
		String sqlUpdate = "update users set users.no_of_games = users.no_of_games - ?  where UserID = ? ";
		Apps.sqlLog("transferGames :"+sqlUpdate);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setInt(1, gamesCount);
			pstmt.setInt(2, sender);
			
		    pstmt.executeUpdate();			   
		    
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("transferGamesdForSender  :"+ e.toString()+" Query "+sqlUpdate, null);
		}
		
		sqlUpdate = "update users set users.no_of_games = users.no_of_games + ?  where UserID = ? ";
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setInt(1, gamesCount);
			pstmt.setInt(2, reciever);
			
		    pstmt.executeUpdate();			   
		    
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("transferGamesdForReciever  :"+ e.toString()+" Query "+sqlUpdate, null);
		}
	}
	
	public ArrayList<Integer> getUserMedalsList(int userId)
	{
		String sqlSelect = "select medal_id from user_medals where user_id = ? ";
		Apps.sqlLog("getUserMedalsList :"+sqlSelect);
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlSelect);
			pstmt.setInt(1, userId);
			
		    ResultSet rs = pstmt.executeQuery();			   
		    while (rs.next()) 
			{
		    	list.add(rs.getInt("medal_id"));
			}	
		    
			rs.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("getUserMedalsList  :"+ e.toString()+" Query "+sqlSelect, null);
		}
		
		return list;
	}
	
	public ArrayList<String> getAllUsersList()
	{
		String sqlSelect = "select Username from users order by Username";
		Apps.sqlLog("getAllUsersList :"+sqlSelect);
		ArrayList<String> list = new ArrayList<String>();
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlSelect);
			
		    ResultSet rs = pstmt.executeQuery();			   
		    while (rs.next()) 
			{
		    	list.add(rs.getString("Username"));
			}	
		    
			rs.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("getAllUsersList  :"+ e.toString()+" Query "+sqlSelect, null);
		}
		
		Collections.sort(list, String.CASE_INSENSITIVE_ORDER);
		return list;
	}
	
	public ISFSArray getTopPlayersList()
	{
		String sqlSelect = "select Username,Totalnoofwins from users where Totalnoofwins > 0  order by Totalnoofwins desc";
		Apps.sqlLog("getTopPlayersList :"+sqlSelect);
		ISFSArray sfsArr = new SFSArray();
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlSelect);
			
		    ResultSet rs = pstmt.executeQuery();			   
		    while (rs.next()) 
			{
		    	
		    	ISFSObject sfso = new SFSObject();
		    	sfso.putUtfString("userName", rs.getString("Username"));
		    	sfso.putInt("noofWins", rs.getInt("Totalnoofwins"));
		    	sfso.putInt("status", Apps.getStatus(rs.getString("Username")));
		    	
		    	sfsArr.addSFSObject(sfso);
			}	
		    
			rs.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("getTopPlayersList  :"+ e.toString()+" Query "+sqlSelect, null);
		}
		
		return sfsArr;
	}
	
	public ISFSArray getPlayersWonGamesList(ArrayList<String> list)
	{
		String sqlSelect = "select Username,Totalnoofwins, belt_type, user_rank, Points from users where UserID = ?";
		Apps.sqlLog("getPlayersWonGamesList :"+sqlSelect);
		ISFSArray sfsArr = new SFSArray();
		
		for(String user : list)
		{
			
			if(Apps.isGuestUser(user))
			{

		    	ISFSObject sfso = new SFSObject();
		    	sfso.putUtfString("userName", user);
		    	sfso.putInt("noofWins", 0);
		    	sfso.putUtfString("beltType", "White");
		    	sfso.putInt("userRank", 0);
		    	sfso.putInt("points", 0);
		    	
		    	sfsArr.addSFSObject(sfso);
			}
			else
			{
				try
				{
					int userID = Apps.getPlayerId(user);
					PreparedStatement pstmt = con.prepareStatement(sqlSelect);
					pstmt.setInt(1, userID);
					
				    ResultSet rs = pstmt.executeQuery();			   
				    while (rs.next()) 
					{
				    	
				    	ISFSObject sfso = new SFSObject();
				    	sfso.putUtfString("userName", rs.getString("Username"));
				    	sfso.putInt("noofWins", rs.getInt("Totalnoofwins"));
				    	sfso.putUtfString("beltType", rs.getString("belt_type"));
				    	sfso.putInt("userRank", rs.getInt("user_rank"));
				    	sfso.putInt("points", rs.getInt("Points"));
				    	
				    	sfsArr.addSFSObject(sfso);
					}	
				    
					rs.close();
					pstmt.close();
					
				}catch(Exception e){
					Apps.sqlErr("getPlayersWonGamesList  :"+ e.toString()+" Query "+sqlSelect, null);
				}
			}
			
		}
		
		return sfsArr;
	}
	
	public void gameCompleted(GameBean gameBean)
	{
		String sqlInsert = "insert into game_logs(game_id, table_id, game_type, game_start_time, game_end_time, total_game_running_time, players,"
							+ "winners, loosers, disconnection_users, leave_table_users, rematch_users, disconnection_users_count, "
							+ "leavetable_users_count, rematch_users_count, total_rounds, total_rtred_count,"
							+ "total_fpsred_count, total_refresh_count, server_version, zone_name) "
							+ "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		Apps.sqlLog("gameCompleted :"+sqlInsert);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlInsert);
			
			pstmt.setString(1, gameBean.gameId);
			pstmt.setString(2, gameBean.tableId);
			pstmt.setString(3, gameBean.tableType);
			pstmt.setString(4, gameBean.getGameStartedTimeString());
			pstmt.setString(5, Apps.getDateString());
			pstmt.setLong(6, gameBean.getTotalGameRunningTime());
			pstmt.setString(7, gameBean._players.toString());
			pstmt.setString(8, gameBean.winners.toString());
			pstmt.setString(9, gameBean.loosers.toString());
			pstmt.setString(10, gameBean.disconnectUsers.toString());
			pstmt.setString(11, gameBean.leaveTablesUsers.toString());
			pstmt.setString(12, gameBean.rematchPlayers.toString());
			pstmt.setInt(13, gameBean.disconnectUsers.size());
			pstmt.setInt(14, gameBean.leaveTablesUsers.size());
			pstmt.setInt(15, gameBean.rematchPlayers.size());
			pstmt.setInt(16, gameBean.getRoundCount());
			pstmt.setInt(17, 0);
			pstmt.setInt(18, 0);
			pstmt.setInt(19, 0);
			pstmt.setString(20, Commands.appInstance.gameCV.getServerBuildVersion());
			pstmt.setString(21, Apps.getZone());
			
			pstmt.executeUpdate();
		
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("gameCompleted  :"+ e.toString()+" Query "+sqlInsert, null);
		}
	}
	
	public int getPlayerTotalGamesCount(String player)
	{
		int freeGamesCount = 0;
		int paidGamesCount = 0;
		int userID = getUserID(player); 
		
		String sqlFreeSelect = "select no_of_free_games from free_games where UserID = ?";
		
		Apps.sqlLog("getPlayerTotoaGamesCount sqlFreeSelect :"+sqlFreeSelect);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlFreeSelect);
			
			pstmt.setInt(1, userID);			
			ResultSet rs = pstmt.executeQuery();
		
			while(rs.next())
			{
				freeGamesCount = rs.getInt("no_of_free_games");
			}
			
			rs.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("getPlayerTotoaGamesCount  sqlFreeSelect :"+ e.toString()+" Query "+sqlFreeSelect, null);
		}
		
		String sqlPaidSelect = "select no_of_games from users where UserID = ?";
		
		Apps.sqlLog("getPlayerTotoaGamesCount sqlPaidSelect :"+sqlPaidSelect);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlPaidSelect);
			
			pstmt.setInt(1, userID);			
			ResultSet rs = pstmt.executeQuery();
		
			while(rs.next())
			{
				paidGamesCount = rs.getInt("no_of_games");
			}
			
			rs.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("isHavingEnoughGamesToPlay  sqlPaidSelect :"+ e.toString()+" Query "+sqlPaidSelect, null);
		}
		
		return freeGamesCount+paidGamesCount;
	}
	
/*	public boolean isHavingEnoughGamesToPlay(GameBean gameBean, String player, boolean isPrivateTable)
	{
		int reduceCount = 1;
		int freeGamesCount = 0;
		int paidGamesCount = 0;
		int userID = getUserID(player);
		boolean isHavingEnoughGames = false;
		// boolean isHavingEnoughGames = true; // for testing
		
		if(isPrivateTable)
		{
			reduceCount = 3;
		}
		
		String sqlFreeSelect = "select no_of_free_games from free_games where UserID = ?";
		
		Apps.sqlLog("isHavingEnoughGamesToPlay sqlFreeSelect :"+sqlFreeSelect);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlFreeSelect);
			
			pstmt.setInt(1, userID);			
			ResultSet rs = pstmt.executeQuery();
		
			while(rs.next())
			{
				freeGamesCount = rs.getInt("no_of_free_games");
			}
			
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("isHavingEnoughGamesToPlay  sqlFreeSelect :"+ e.toString()+" Query "+sqlFreeSelect, gameBean);
		}
		
		String sqlPaidSelect = "select no_of_games from users where UserID = ?";
		
		Apps.sqlLog("isHavingEnoughGamesToPlay sqlPaidSelect :"+sqlPaidSelect);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlPaidSelect);
			
			pstmt.setInt(1, userID);			
			ResultSet rs = pstmt.executeQuery();
		
			while(rs.next())
			{
				paidGamesCount = rs.getInt("no_of_games");
			}
			
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("isHavingEnoughGamesToPlay  sqlPaidSelect :"+ e.toString()+" Query "+sqlPaidSelect, gameBean);
		}
		
		if(gameBean != null)
		{
			gameBean.getGameLogInstance().gamesCountBefore(gameBean, player, reduceCount, freeGamesCount, paidGamesCount);
		}
		
		if((freeGamesCount + paidGamesCount) >= reduceCount)
		{
			isHavingEnoughGames = true;
			reduceGameCount(gameBean, player, reduceCount, freeGamesCount, paidGamesCount);
		}
		else
		{
			Validations.raiseNotHavingEnoughGamesToPlayException(gameBean, player, reduceCount, freeGamesCount, paidGamesCount, isPrivateTable);
		}
		
		return isHavingEnoughGames;
	}*/
	
	public boolean isHavingEnoughGamesToPlay(GameBean gameBean, String player, boolean isPrivateTable, boolean isDeductGames)
	{
		// boolean isHavingEnoughGames = false;
		int gamesCount = 0;
		
		if(Apps.isGuestUser(player))
		{
			gamesCount = getIPBasedFreeGamesCount(gameBean, player);
			
			if(gamesCount >= 4)
			{
				return false;
				
				//return true;// this is only for testing purpose.
			}
			else
			{
				if(isDeductGames)
				{					
					updateIPBasedFreeGamesCount(gameBean, player);
				}
				
				return true;				
			}			
		}
		else if(Apps.isFakePlayer(player))
		{
			if(isHavingEnoughGames(gameBean, player, isPrivateTable, isDeductGames))
			{
				// do nothing.
			}
			else
			{
				updateFakePlayerGamesCount(player);
			}
			
			return true;
		}
		else
		{
			return isHavingEnoughGames(gameBean, player, isPrivateTable, isDeductGames);
		}
			
		//return isHavingEnoughGames;
	}
	
	public boolean isHavingEnoughGames(GameBean gameBean, String player, boolean isPrivateTable, boolean isDeductGames)
	{
		int reduceCount = 1;
		int freeGamesCount = 0;
		int paidGamesCount = 0;
		int userID = getUserID(player);
		boolean isHavingEnoughGames = false;
		// boolean isHavingEnoughGames = true; // for testing
		
		if(isPrivateTable)
		{
			reduceCount = 3;
		}
		
		String sqlFreeSelect = "select no_of_free_games from free_games where UserID = ?";
		
		Apps.sqlLog("isHavingEnoughGamesToPlay sqlFreeSelect :"+sqlFreeSelect);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlFreeSelect);
			
			pstmt.setInt(1, userID);			
			ResultSet rs = pstmt.executeQuery();
		
			while(rs.next())
			{
				freeGamesCount = rs.getInt("no_of_free_games");
			}
			
			rs.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("isHavingEnoughGamesToPlay  sqlFreeSelect :"+ e.toString()+" Query "+sqlFreeSelect, gameBean);
		}
		
		String sqlPaidSelect = "select no_of_games from users where UserID = ?";
		
		Apps.sqlLog("isHavingEnoughGamesToPlay sqlPaidSelect :"+sqlPaidSelect);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlPaidSelect);
			
			pstmt.setInt(1, userID);			
			ResultSet rs = pstmt.executeQuery();
		
			while(rs.next())
			{
				paidGamesCount = rs.getInt("no_of_games");
			}
			
			rs.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("isHavingEnoughGamesToPlay  sqlPaidSelect :"+ e.toString()+" Query "+sqlPaidSelect, gameBean);
		}
		
		if(gameBean != null)
		{
			gameBean.getGameLogInstance().gamesCountBefore(gameBean, player, reduceCount, freeGamesCount, paidGamesCount);
		}
		
		// here deduct the free games count from ip based games.
		//freeGamesCount = freeGamesCount - getIPBasedFreeGamesCount(gameBean, player);
		
		int ipBasedAvailabelFreeGamesCount = 4 - getIPBasedFreeGamesCount(gameBean, player);
		int freeGamesWithIp = 0;
		
		if(ipBasedAvailabelFreeGamesCount > 0)
		{
			if(ipBasedAvailabelFreeGamesCount >= freeGamesCount)
			{
				freeGamesWithIp = freeGamesCount;
			}
			else
			{
				freeGamesWithIp = ipBasedAvailabelFreeGamesCount;
			}			
		}
		else
		{
			freeGamesWithIp = 0;
		}
		
		//freeGamesWithIp = freeGamesCount - getIPBasedFreeGamesCount(gameBean, player);
			
		
		if((freeGamesWithIp + paidGamesCount) >= reduceCount)
		{
			isHavingEnoughGames = true;
			if(isDeductGames)
			{
			  reduceGameCount(gameBean, player, reduceCount, freeGamesCount, freeGamesWithIp, paidGamesCount);
			}
		}
		else
		{
			Validations.raiseNotHavingEnoughGamesToPlayException(gameBean, player, reduceCount, freeGamesCount, freeGamesWithIp, paidGamesCount, isPrivateTable);
			
			if(isDeductGames)
			{
				/*
				 * 1. user have free_games_count > 0 and paid games count == 0 . so we join the user into the game
				 * 2. But at the time starting game user is in disconnection state. so at this time
				 *    we get ipbased free games count as 0(ie total 4 games over).
				 * 3. At this time we reduce the free games count directly.				     
				 */
				User user = Apps.getUserByName(player);
				if(freeGamesWithIp == 0 && paidGamesCount == 0 && freeGamesCount > 0 && user == null)
				{
					Validations.userObjNotAvailableAtTheTimeofUpdatingGames(gameBean, player, user, 
							reduceCount, ipBasedAvailabelFreeGamesCount, freeGamesWithIp,
							paidGamesCount, isPrivateTable);
					
					if(freeGamesCount >= reduceCount)
					{
						freeGamesCount = freeGamesCount - reduceCount;
						
						String sqlFreeUpdate = "update free_games set no_of_free_games = ? where UserID = ?";
						
						try
						{
							PreparedStatement pstmt = con.prepareStatement(sqlFreeUpdate);
							
							pstmt.setInt(1, freeGamesCount);
							pstmt.setInt(2, userID);
							pstmt.executeUpdate();
						
							pstmt.close();
							
						}catch(Exception e){
							Apps.sqlErr("reduceGameCount_inSpecial_condition  sqlFreeUpdate :"+ e.toString()+" Query "+sqlFreeUpdate, gameBean);
						}
					}			
					
				}
			}
		}
		
		return isHavingEnoughGames;
	}
	
	public void reduceGameCount(GameBean gameBean, String player, int reduceCount, int freeGamesCount, int freeGamesWithIp, int paidGamesCount)
	{
		int userID = getUserID(player);
			
		if(freeGamesWithIp >= reduceCount)
		{
			freeGamesCount = freeGamesCount - reduceCount;
			
			// just for updating free games count in ip
			updateIPBasedFreeGamesCount(gameBean, player);
		}
		else
		{
			if(freeGamesWithIp > 0)
			{
				//reduceCount = reduceCount - freeGamesCount;
				//freeGamesCount = 0;
				
				reduceCount = reduceCount - freeGamesWithIp;
				freeGamesCount = freeGamesCount - freeGamesWithIp;
				
				paidGamesCount = paidGamesCount - reduceCount;
				
				
				// just for updating free games count in ip
				updateIPBasedFreeGamesCount(gameBean, player);
			}			
			else
			{
				paidGamesCount = paidGamesCount - reduceCount;
			}
		}
		
		if(gameBean != null)
		{
			gameBean.getGameLogInstance().gamesCountAfter(gameBean, player, reduceCount, freeGamesCount, paidGamesCount);
		}
		String sqlFreeUpdate = "update free_games set no_of_free_games = ? where UserID = ?";
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlFreeUpdate);
			
			pstmt.setInt(1, freeGamesCount);
			pstmt.setInt(2, userID);
			pstmt.executeUpdate();
		
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("reduceGameCount  sqlFreeUpdate :"+ e.toString()+" Query "+sqlFreeUpdate, gameBean);
		}
		
		String sqlPaidUpdate = "update users set no_of_games = ? where UserID = ?";
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlPaidUpdate);
			
			pstmt.setInt(1, paidGamesCount);
			pstmt.setInt(2, userID);
			pstmt.executeUpdate();
		
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("reduceGameCount  sqlPaidUpdate :"+ e.toString()+" Query "+sqlPaidUpdate, gameBean);
		}
	}
	
	public void updateIPBasedFreeGamesCount(GameBean gameBean, String player)
	{
		String ip = null;
		
		if(Apps.isGuestUser(player))
		{
			ip = player;
		}
		else
		{
			User user = Apps.getUserByName(player);
			
			if(user != null)
			{
				ip = user.getSession().getAddress();
			}
			else
			{
				/*
				 * Here user is in disconnection state at the time of game starting so user obj is not available
				 * so we are unable to get the ip address at this time. 
				 */
				Validations.userObjNotAvailableInupdateIPBasedFreeGamesCount(gameBean, player, user);				
			}
		}
		
		String tableId = "private";
		String date = Apps.getDayString();
		
		
		if(gameBean != null)
		{
			tableId = gameBean.tableId;
		}
		
		int gamesCount = 0;
		gamesCount = getIPBasedFreeGamesCount(gameBean, player);
		
		gamesCount++;		
		
		if(gamesCount > 0 && gamesCount < 5)
		{
			String sqlFreeUpdate = "update guest_users set no_of_games = ?, TableID = ? where ip_address = ? and game_date = ?";
			Apps.showLog("updateIPBasedFreeGamesCount     "+sqlFreeUpdate);
			try
			{
				PreparedStatement pstmt = con.prepareStatement(sqlFreeUpdate);
				
				tableId = Apps.getBase64EncodedText(tableId);
				
				pstmt.setInt(1, gamesCount);
				pstmt.setString(2, tableId);
				pstmt.setString(3, ip);
				pstmt.setString(4, date);
				
				pstmt.executeUpdate();
			
				pstmt.close();
				
			}catch(Exception e){

				Apps.sqlErr("updateIPBasedFreeGamesCount  sqlFreeUpdate :"+ e.toString()+" gamesCount "+gamesCount+" tableId "+tableId
						+" ip "+ip+"   "+date+"  Query "+sqlFreeUpdate, gameBean);
			}
		}
	}
	
	public int getIPBasedFreeGamesCount(GameBean gameBean, String player)
	{
		int freeGamesCount = 0;
		
		
		String ip = null;
		
		if(Apps.isGuestUser(player))
		{
			ip = player;
		}
		else
		{
			User user = Apps.getUserByName(player);
			
			if(user != null)
			{
				ip = user.getSession().getAddress();
			}
			else
			{
				/*
				 * Here user is in disconnection state at the time of game starting so user obj is not available
				 * so we are unable to get the ip address at this time. So here we treat freegames count as 4.
				 * That means no free games available for this ip for this day.
				 */
				Validations.userObjNotAvailableInGetIPBasedFreeGamesCount(gameBean, player, user);
				return 4;
			}			
		}
				
		
		String sqlFreeSelect = "select no_of_games from guest_users where ip_address = ? and game_date = ?";
		Apps.showLog("getIPBasedFreeGamesCount  "+sqlFreeSelect);
		
		String tableId = "private";
		String date = Apps.getDayString();
		
		
		if(gameBean != null)
		{
			tableId = gameBean.tableId;
		}
		
		boolean isRecordFound = false;
		
		try
		{
            PreparedStatement pstmt = con.prepareStatement(sqlFreeSelect);
			
			pstmt.setString(1, ip);	
			pstmt.setString(2, date);
			ResultSet rs = pstmt.executeQuery();
		
			while(rs.next())
			{
				freeGamesCount = rs.getInt("no_of_games");
				isRecordFound = true;
			}
			
			rs.close();
			pstmt.close();
	    }
		catch(Exception e){
			Apps.sqlErr("getIPBasedFreeGamesCount  sqlGuestGamesSelect :"+ e.toString()+" Query "+sqlFreeSelect, gameBean);
		}
		
		if(!isRecordFound)
		{
			String sqlInsert = "insert into guest_users(game_date, ip_address, TableID, no_of_games)values(?, ?, ?, ?)";
			
			try
			{
				PreparedStatement pstmt = con.prepareStatement(sqlInsert);
				
				tableId = Apps.getBase64EncodedText(tableId);
				
				pstmt.setString(1, date);
				pstmt.setString(2, ip);
				pstmt.setString(3, tableId);
				pstmt.setInt(4, 0);
				
				pstmt.executeUpdate();
			
				pstmt.close();
				
			}catch(Exception e){

				Apps.sqlErr(" getIPBasedFreeGamesCount  sqlInsert :"+ e.toString()+" Query "+sqlInsert, gameBean);
				
			}
		}
		
		return freeGamesCount;
	}
	
	public boolean isArabicUser1(String userName)
	{
		String sqlSelect = "select Username from users";
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlSelect);
			
			ResultSet rs = pstmt.executeQuery();
		
			while(rs.next())
			{
				String dbName = rs.getString("Username");
			
				if(userName.equals(dbName))
				{
					return true;
				}
			}
			
			rs.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("arabicUsers  sqlSelect :"+ e.toString()+" Query "+sqlSelect, null);
		}
		
		return false;
	}
	
	public Integer getArabicUserID1(String userName)
	{
		Integer id = null;
		
		String sqlSelect = "select * from users";
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlSelect);
			
			ResultSet rs = pstmt.executeQuery();
		
			while(rs.next())
			{
				String dbName = rs.getString("Username");
			
				if(userName.equals(dbName))
				{
					id = rs.getInt("UserID");
					break;
				}
			}
			
			rs.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("getArabicUserID  sqlSelect :"+ e.toString()+" Query "+sqlSelect, null);
		}
		
		return id;
	}
	
	public String getArabicPassword1(int userID)
	{
		String password = null;
		
		String sqlSelect = "select * from users where UserID = ?";
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlSelect);
			pstmt.setInt(1, userID);
			
			ResultSet rs = pstmt.executeQuery();
		
			while(rs.next())
			{
				password = rs.getString("Password");
				break;
			}
			
			rs.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("getArabicPassword  sqlSelect :"+ e.toString()+" Query "+sqlSelect, null);
		}
		
		return password;
	}
	
	public void setEmailVisible(String userName, int emailVisible)
	{
		String sqlUpdate = "update users set email_visible = ? where UserID = ?";
		
		int userID = getUserID(userName);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setInt(1, emailVisible);
			pstmt.setInt(2, userID);
			
			
			pstmt.executeUpdate();
		
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("setEmailVisible  sqlUpdate :"+ e.toString()+" Query "+sqlUpdate, null);
		}
	}
	
	public void storeUserDeviceType(String userName, String deviceType)
	{
        String sqlUpdate = "update users set device_type = ? where UserID = ?";
		
		int userID = getUserID(userName);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setString(1, deviceType);
			pstmt.setInt(2, userID);			
			
			pstmt.executeUpdate();
		
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("storeUserDeviceType  sqlUpdate :"+ e.toString()+" Query "+sqlUpdate, null);
		}
	}
	
	public void storeGuestUserDeviceType(String userName, String deviceType)
	{
        String sqlUpdate = "update guest_users set device_type = ? where ip_address = ?";
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setString(1, deviceType);
			pstmt.setString(2, userName);			
			
			pstmt.executeUpdate();
		
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("storeGuestUserDeviceType  sqlUpdate :"+ e.toString()+" Query "+sqlUpdate, null);
		}
	}
	
	public ISFSArray getPlayersDeviceTypeList(ArrayList<String> list)
	{
		String sqlSelect = "select Username, Totalnoofwins, device_type, belt_type, user_rank, Points from users where UserID = ?";
		Apps.sqlLog("getPlayersDeviceTypeList :"+sqlSelect);
		
		ISFSArray sfsArr = new SFSArray();
		
		for(String user : list)
		{
			if(Apps.isGuestUser(user))
			{
				sqlSelect = "select  device_type from guest_users where ip_address = ?";
				
				try
				{
					
					PreparedStatement pstmt = con.prepareStatement(sqlSelect);
					pstmt.setString(1, user);
					
				    ResultSet rs = pstmt.executeQuery();			   
				    while (rs.next()) 
					{			    	
				    	ISFSObject sfso = new SFSObject();
				    	
				    	sfso.putUtfString("userName", user);
				    	sfso.putInt("noofWins", 0);
				    	sfso.putUtfString("deviceType", rs.getString("device_type"));
				     	sfso.putUtfString("beltType", "White");
				    	sfso.putInt("userRank", 0);
				    	sfso.putInt("points", 0);
				    	
				    	sfsArr.addSFSObject(sfso);
					}	
				    
					rs.close();
					pstmt.close();
					
				}catch(Exception e){
					Apps.sqlErr("getPlayersDeviceTypeList  :"+ e.toString()+" Query "+sqlSelect, null);
				}
			}
			else
			{
				try
				{
					int userID = Apps.getPlayerId(user);
					
					PreparedStatement pstmt = con.prepareStatement(sqlSelect);
					pstmt.setInt(1, userID);
					
				    ResultSet rs = pstmt.executeQuery();			   
				    while (rs.next()) 
					{			    	
				    	ISFSObject sfso = new SFSObject();
				    	
				    	sfso.putUtfString("userName", rs.getString("Username"));
				    	sfso.putInt("noofWins", rs.getInt("Totalnoofwins"));
				    	sfso.putUtfString("deviceType", rs.getString("device_type"));
				     	sfso.putUtfString("beltType", rs.getString("belt_type"));
				    	sfso.putInt("userRank", rs.getInt("user_rank"));
				    	sfso.putInt("points", rs.getInt("Points"));
				    	
				    	sfsArr.addSFSObject(sfso);
					}	
				    
					rs.close();
					pstmt.close();
					
				}catch(Exception e){
					Apps.sqlErr("getPlayersDeviceTypeList  :"+ e.toString()+" Query "+sqlSelect, null);
				}
			}			
		}
		
		return sfsArr;
	}
	
	public void insetGuestUserRecord(String ipAddress)
	{
		String sqlSelect = "select ip_address from guest_users where game_date = ?";
		boolean isRecordAvailable = false;
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlSelect);
			pstmt.setString(1, Apps.getDayString());
			
		    ResultSet rs = pstmt.executeQuery();			   
		    while (rs.next()) 
			{			    	
		    	if(ipAddress.equals(rs.getString("ip_address")))
		    	{
		    		isRecordAvailable = true;
		    		break;
		    	}
			}		
		    
			rs.close();
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("insetGuestUserRecord  :"+ e.toString()+" Query "+sqlSelect, null);
		}
		
		if(!isRecordAvailable)
		{
			String sqlUpdate = "insert into guest_users (ip_address,  game_date, gender) values(?, ?, ?)";
			
			try
			{
				PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
				pstmt.setString(1, ipAddress);
				pstmt.setString(2, Apps.getDayString());
				pstmt.setString(3, Constant.MALE);
				
			    pstmt.executeUpdate();	   		
			    
				
				pstmt.close();
				
			}catch(Exception e){
				Apps.sqlErr("insetGuestUserRecord  :"+ e.toString()+" Query "+sqlUpdate, null);
			}
		}
	}
	
	public void guestUserDisconnected(String player)
	{
		String sqlUpdate = "update guest_users set is_disconnected_while_playing = 1 where game_date = ? and ip_address = ?";
		Apps.sqlLog("guestUserDisconnected :"+sqlUpdate);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setString(1, Apps.getDayString());
			pstmt.setString(2, player);
			
		    pstmt.executeUpdate();			   
		    
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("guestUserDisconnected  :"+ e.toString()+" Query "+sqlUpdate, null);
		}
	}
	
	public void guestUserConnected1(String player)
	{
		String sqlUpdate = "update guest_users set is_disconnected_while_playing = 0 where game_date = ? and ip_address = ?";
		Apps.sqlLog("guestUserConnected :"+sqlUpdate);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setString(1, Apps.getDayString());
			pstmt.setString(2, player);
			
		    pstmt.executeUpdate();			   
		    
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("guestUserConnected  :"+ e.toString()+" Query "+sqlUpdate, null);
		}
	}
	
	public int getDisconnectionBitForGuestUser(String player)
	{
		String sqlSelect = "select is_disconnected_while_playing from guest_users where game_date = ? and ip_address = ?";
		
		Apps.sqlLog("getDisconnectionBitForGuestUser :"+sqlSelect);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlSelect);
			pstmt.setString(1, Apps.getDayString());
			pstmt.setString(2, player);
			
			ResultSet rs = pstmt.executeQuery();			   
		    while (rs.next()) 
			{			    	
		    	return rs.getInt("is_disconnected_while_playing");
		    	
			}		
		    
			rs.close();			   
		    
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("getDisconnectionBitForGuestUser  :"+ e.toString()+" Query "+sqlSelect, null);
		}
		
		return -1;
	}
	
	public ArrayList<String> getFakePlayersList()
	{
		String sqlSelect = "select Username from users where Password = 'Fake'";
		
		Apps.sqlLog("getFakePlayersList :"+sqlSelect);
		
		ArrayList<String> list = new ArrayList<String>();
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlSelect);
			
			ResultSet rs = pstmt.executeQuery();			   
		   	
		    while(rs.next())
		    {
		    	list.add(rs.getString("Username"));
		    }
			rs.close();			   
		    
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("getFakePlayersList  :"+ e.toString()+" Query "+sqlSelect, null);
		}
		
		return list;
	}
	
	public void updateFakePlayerGamesCount(String player)
	{
		String sqlUpdate = "update users set no_of_games = 50 where UserID = ?";
		Apps.sqlLog("updateFakePlayerGamesCount :"+sqlUpdate);
		
		int userId = Apps.getPlayerId(player);
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setInt(1, userId);
			
		    pstmt.executeUpdate();			   
		    
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("updateFakePlayerGamesCount  :"+ e.toString()+" Query "+sqlUpdate, null);
		}
	}
	
	public int getUserRank(String player)
	{
		int userRank = 0;
		
		String sqlSelect = "select user_rank from users where UserID = ?";
		
		Apps.sqlLog("getUserRank :"+sqlSelect);
		int userId = Apps.getPlayerId(player);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlSelect);
			
			pstmt.setInt(1, userId);
			
			ResultSet rs = pstmt.executeQuery();			   
		   	
		    while(rs.next())
		    {
		    	userRank = rs.getInt("user_rank");
		    }
		    
			rs.close();			    
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("getUserRank  :"+ e.toString()+" Query "+sqlSelect, null);
		}
		
		return userRank;
	}
	
	public int getNoofWins(String player)
	{
		int noofWins = 0;
		
		String sqlSelect = "select Totalnoofwins from users where UserID = ?";
		
		Apps.sqlLog("getNoofWins :"+sqlSelect);
		int userId = Apps.getPlayerId(player);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlSelect);
			
			pstmt.setInt(1, userId);
			
			ResultSet rs = pstmt.executeQuery();			   
		   	
		    while(rs.next())
		    {
		    	noofWins = rs.getInt("Totalnoofwins");
		    }
			rs.close();			   
		    
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("getNoofWins  :"+ e.toString()+" Query "+sqlSelect, null);
		}
		
		return noofWins;
	}
	
	public void updateUserBeltTypeAndRank(String player, String status)
	{
		int noofWins = getNoofWins(player);
		
		String beltType = Apps.getBeltType(noofWins);
		
		String sqlUpdate = "update users set belt_type = ? where UserID = ?";
		Apps.sqlLog("updateUserBeltTypeAndRank:"+sqlUpdate);
		int userId = Apps.getPlayerId(player);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setString(1, beltType);
			pstmt.setInt(2, userId);
			
		    pstmt.executeUpdate();			   
		    
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("updateUserBeltTypeAndRank  :"+ e.toString()+" Query "+sqlUpdate, null);
		}
		
		int userPreviousRank = getUserRank(player);
		
		int currentRank = Apps.calculateUserRank(noofWins, userPreviousRank, status);
		
		sqlUpdate = "update users set user_rank = ? where UserID = ?";
		
		Apps.sqlLog("updateUserBeltTypeAndRank:"+sqlUpdate);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setInt(1, currentRank);
			pstmt.setInt(2, userId);
			
		    pstmt.executeUpdate();			   
		    
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("updateUserBeltTypeAndRank  :"+ e.toString()+" Query "+sqlUpdate, null);
		}		
	}
	
	public void updateGameSoundStatus(String player, String status)
	{
		String sqlUpdate = "update users set game_sound = ? where UserID = ?";
		Apps.sqlLog("updateGameSoundStatus:"+sqlUpdate);
		int userId = Apps.getPlayerId(player);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setString(1, status);
			pstmt.setInt(2, userId);
			
		    pstmt.executeUpdate();			   
		    
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("updateGameSoundStatus  :"+ e.toString()+" Query "+sqlUpdate, null);
		}
	}
	
	public void updateBackGroundSoundStatus(String player, String status)
	{
		String sqlUpdate = "update users set back_ground_sound = ? where UserID = ?";
		Apps.sqlLog("updateBackGroundSoundStatus:"+sqlUpdate);
		int userId = Apps.getPlayerId(player);
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setString(1, status);
			pstmt.setInt(2, userId);
			
		    pstmt.executeUpdate();			   
		    
			pstmt.close();
			
		}catch(Exception e){
			Apps.sqlErr("updateBackGroundSoundStatus  :"+ e.toString()+" Query "+sqlUpdate, null);
		}
	}
	
	public void getGuestUserFreeGamesCount()
	{
		
	}
	
	public  int getUserPoints(String player)
	{

		Integer  userID = Apps.getPlayerId(player);
		int points = 0;
		
		if(Apps.isGuestUser(player))
		{
			
		}
		else
		{
			String sqlSelect = "select Points from users where UserID = ?";
			Apps.sqlLog("getUserPoints :"+sqlSelect);
			
			try
			{
				PreparedStatement pstmt = con.prepareStatement(sqlSelect);
				pstmt.setInt(1,userID);
				
			    ResultSet rs = pstmt.executeQuery();			   
			    while (rs.next()) 
				{
			    	points = rs.getInt("Points");			    	
				}	
			    
				rs.close();
				pstmt.close();
				
			}catch(Exception e){
				Apps.sqlErr("getUserPoints  :"+ e.toString()+" Query "+sqlSelect, null);
			}
		}			
		
		return points;
	
	}
	
	
}// End Class
