package src.baloot.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import src.baloot.beans.GameBean;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.Zone;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class MedalsProxy {
	
Connection con = null;
	
	public MedalsProxy(Zone zone)
	{		
		try {
			con = zone.getDBManager().getConnection();
			Apps.showLog(" MedalsProxy >> SQL Connection Success ");
		} catch (SQLException e) {
			Apps.showLog(" MedalsProxy >> SQL Connection Failed :" + e.toString());
		}
	}
	
	public int getTotalNoofWins(int userId, GameBean gameBean)
	{
		int totalNoofWins = 0;
		
		String sql = "select * from users where UserID = ?";
		Apps.showLog(" MedalsProxy >> getTotalNoofWins "+sql);
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1,userId);	
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				totalNoofWins = rs.getInt("Totalnoofwins");
			}
			
			rs.close();
			pstmt.close();
		}
		catch(Exception e)
		{
			Apps.sqlErr(" MedalsProxy : getTotalNoofWins :" + e.toString()+" Query "+sql, gameBean);
		}
		
		return totalNoofWins;
	}
	
	public int getPoints(int userId, GameBean gameBean)
	{
		int points = 0;
		
		String sql = "select * from users where UserID = ?";
		Apps.sqlLog(" MedalsProxy >> getPoints "+sql);
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1,userId);	
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				points = rs.getInt("Points");
			}
			
			rs.close();
			pstmt.close();
		}
		catch(Exception e)
		{
			Apps.sqlErr(" MedalsProxy : getPoints :" + e.toString()+" Query "+sql, gameBean);
		}
		
		return points;
	}
	
	public void insertMedalToUser(int userId, int medalId, int count, GameBean gameBean)
	{
		String date = getDateString();
		
		String sqlInsert ="Insert INTO user_medals(user_id, medal_id, count, created_date) VALUES (?,?,?,?);";
		Apps.sqlLog(" MedalsProxy >> insertMedalToUser  userId "+userId+" medalId "+medalId+"  coiunt "+count+" query "+sqlInsert);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlInsert);
			pstmt.setInt(1, userId);
			pstmt.setInt(2, medalId);
			pstmt.setInt(3, count);
			pstmt.setString(4, date);
			
			pstmt.executeUpdate();
			
			pstmt.close();
			
		}catch(Exception e)
		{
			Apps.sqlErr(" MedalsProxy : insertMedalToUser :" + e.toString()+"userId "+userId+"  medalId "+medalId+" count "+count+"  Query "+sqlInsert, gameBean);
		}		
	}
	
	public void updateUserRank(int userId, int player_points, int newPlayerPoints, String player, String tableId, GameBean gameBean)
	{
		if(player_points <= 14 && newPlayerPoints >= 15 )
		{			
			int noofrows = getParticularMedalCountofaUser(userId, Commands.MEDAL_6, gameBean);
						
			if(noofrows == 0)
			{				
				sendMedal(Commands.MEDAL_6, 1, player, tableId);				
				insertMedalToUser(userId, Commands.MEDAL_6, 1, gameBean);
			}
		}
		else if(player_points <= 34 && newPlayerPoints >= 35 )
		{			
			int noofrows = getParticularMedalCountofaUser(userId, Commands.MEDAL_7, gameBean);
						
			if(noofrows == 0)
			{				
				// send medal Commands.: Medal-7 and Save
				sendMedal(Commands.MEDAL_7,1, player, tableId);				
				insertMedalToUser(userId, Commands.MEDAL_7, 1, gameBean);			
				
				// send medal Commands.: Medal-14 and Save
				//sendMedal(Commands.MEDAL_14,1, player, tableId);
				//insertMedalToUser(userId, Commands.MEDAL_14, 1, gameBean);
				
				Commands.appInstance.medalsBsn.checkMedal44(player, userId, tableId);
			}
		}
		else if(player_points <= 54 && newPlayerPoints >= 55 )
		{			
			int noofrows = getParticularMedalCountofaUser(userId, Commands.MEDAL_8, gameBean);
			
			if(noofrows == 0)
			{				
				sendMedal(Commands.MEDAL_8,1, player, tableId);
				
				insertMedalToUser(userId, Commands.MEDAL_8, 1, gameBean);
				
				Commands.appInstance.medalsBsn.checkMedal45(player, userId, tableId);
			}
		}
		else if(player_points <= 69 && newPlayerPoints >= 70 )
		{			
			int noofrows = getParticularMedalCountofaUser(userId, Commands.MEDAL_9, gameBean);
			
			if(noofrows == 0)
			{
				sendMedal(Commands.MEDAL_9,1, player, tableId);				
				insertMedalToUser(userId, Commands.MEDAL_9, 1, gameBean);			
				
				
				//sendMedal(Commands.MEDAL_15,1, player, tableId);				
				//insertMedalToUser(userId, Commands.MEDAL_15, 1, gameBean);
								
				Commands.appInstance.medalsBsn.checkMedal46(player, userId, tableId);
			}
		}
		else if(player_points <= 84 && newPlayerPoints >= 85 )
		{
			int noofrows = getParticularMedalCountofaUser(userId, Commands.MEDAL_10, gameBean);
			
			if(noofrows == 0)
			{			
				sendMedal(Commands.MEDAL_10,1, player, tableId);				
				insertMedalToUser(userId, Commands.MEDAL_10, 1, gameBean);
								
				Commands.appInstance.medalsBsn.checkMedal47(player, userId, tableId);
			}
		}
		else if(player_points <= 99 && newPlayerPoints >= 100 )
		{
			int noofrows = getParticularMedalCountofaUser(userId, Commands.MEDAL_11, gameBean);
			
			if(noofrows == 0)
			{				
				sendMedal(Commands.MEDAL_11,1, player, tableId);
				insertMedalToUser(userId, Commands.MEDAL_11, 1, gameBean);
				
				// medal -13 by ramana
				sendMedal(Commands.MEDAL_16,1, player, tableId);				
				insertMedalToUser(userId, Commands.MEDAL_16, 1, gameBean);
				
				
				//sendMedal(Commands.MEDAL_16,1, player, tableId);
				
				//insertMedalToUser(userId, Commands.MEDAL_16, 1, gameBean);
				
				Commands.appInstance.medalsBsn.checkMedal48(player, userId, tableId);
			}
		}
		else if(player_points <= 124 && newPlayerPoints >= 125 )
		{
			int noofrows = getParticularMedalCountofaUser(userId, Commands.MEDAL_12, gameBean);
			
			if(noofrows == 0)
			{
				sendMedal(Commands.MEDAL_12, 1, player, tableId);
				insertMedalToUser(userId, Commands.MEDAL_12, 1, gameBean);
			}
		}
		else if(player_points <= 144 && newPlayerPoints >= 145 )
		{
			int noofrows = getParticularMedalCountofaUser(userId, Commands.MEDAL_13, gameBean);
			
			if(noofrows == 0)
			{
				sendMedal(Commands.MEDAL_13, 1, player, tableId);
				insertMedalToUser(userId, Commands.MEDAL_13, 1, gameBean);
			}
		}
		else if(player_points <= 179 && newPlayerPoints >= 180 )
		{
			int noofrows = getParticularMedalCountofaUser(userId, Commands.MEDAL_14, gameBean);
			
			if(noofrows == 0)
			{
				sendMedal(Commands.MEDAL_14, 1, player, tableId);
				insertMedalToUser(userId, Commands.MEDAL_14, 1, gameBean);
			}
		}
		
	}
	
	public void updateNoofWinsInaRow(int userId, String player, String tableId, GameBean gameBean)
	{

		int nofoWins = getPlayerNoofWinsInaRow(userId, gameBean);		
		
		nofoWins = nofoWins+1;
	
		String sqlUpdate = "update user_data set noof_wins_inarow = ? where user_id = ?;";
		Apps.sqlLog("MedalsProxy >> updateNoofWinsInaRow userId "+userId+" noofwins "+nofoWins+" query "+sqlUpdate);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setInt(1, nofoWins);
			pstmt.setInt(2, userId);
			
			pstmt.executeUpdate();
			
			pstmt.close();
			
		}catch(Exception e)
		{
			Apps.sqlErr(" MedalsProxy : updateNoofWinsInaRow :" + e.toString()+"userId "+userId+"  player "+player+"  Query "+sqlUpdate, gameBean);
		}
		
		if( nofoWins == 5 )
		{
			int noofrows = getParticularMedalCountofaUser(userId, Commands.MEDAL_18, gameBean);
			
			if(noofrows == 0)
			{				
				sendMedal(Commands.MEDAL_18,1, player, tableId);				
				insertMedalToUser(userId, Commands.MEDAL_18, 1, gameBean);				
				
				Commands.appInstance.medalsBsn.checkMedal44(player, userId, tableId);								
			}
			
		}
		else if(nofoWins == 10)
		{
			int noofrows = getParticularMedalCountofaUser(userId, Commands.MEDAL_19, gameBean);
			
			if(noofrows == 0)
			{				
				sendMedal(Commands.MEDAL_19,1, player, tableId);				
				insertMedalToUser(userId, Commands.MEDAL_19, 1, gameBean);
				
				Commands.appInstance.medalsBsn.checkMedal45(player, userId, tableId);
				Commands.appInstance.medalsBsn.checkMedal46(player, userId, tableId);
			}
		}
		else if(nofoWins == 15)
		{
			int noofrows = getParticularMedalCountofaUser(userId, Commands.MEDAL_20, gameBean);
			
			if(noofrows == 0)
			{
				sendMedal(Commands.MEDAL_20,1, player, tableId);
				
				insertMedalToUser(userId, Commands.MEDAL_20, 1, gameBean);
								
				// Check Medal for 47
				Commands.appInstance.medalsBsn.checkMedal47(player, userId, tableId);
				// Check Medal for 48
				Commands.appInstance.medalsBsn.checkMedal48(player, userId, tableId);
			}
		}
		else if(nofoWins == 25)
		{
			int noofrows = getParticularMedalCountofaUser(userId, Commands.MEDAL_21, gameBean);
			
			if(noofrows == 0)
			{
				sendMedal(Commands.MEDAL_21,1, player, tableId);
				
				insertMedalToUser(userId, Commands.MEDAL_21, 1, gameBean);				
			}
		}
		else
		{
			// do nothing.
		}
		
		noofWinsInaDay(userId, player, tableId, gameBean);		
	
	}
	
	public void noofWinsInaDay(Integer userId, String player, String tableId, GameBean gameBean)
	{
		String select = "select * from user_data where user_id="+userId+";";
		
		String dayFirstLogin = null;
		long lastlogin = 0;
		int noofwinsinaday = 0;
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(select);
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				dayFirstLogin = rs.getString("day_first_login");
				noofwinsinaday = rs.getInt("noof_wins_inaday");
			}
			
			rs.close();
			pstmt.close();
			
		}catch(Exception e)
		{
			Apps.sqlErr(" MedalProxy >>  noofWinsInaDay  "+ e.toString()+" userId"+ userId+" Query "+select, gameBean);
		}				
		
		if(dayFirstLogin == null)
		{
			lastlogin = 0;
		}
		else
		{
			lastlogin = Long.parseLong(dayFirstLogin);
		}
		
		long current_time = System.currentTimeMillis();
		long oneday = 24*60*60*1000;						
		
		noofwinsinaday++;
		
		if(lastlogin+oneday < current_time)
		{			
			String sqlUpdate = "update user_data set day_first_login="+current_time +", noof_wins_inaday ="+0+" where user_id="+userId+";";		
			Apps.sqlLog("MedalProxy >> set dayfirst login  userId"+ userId+" sqlUpdate "+ sqlUpdate);
			
			try {
				PreparedStatement stmtUpdate = con.prepareStatement(sqlUpdate);
				
				stmtUpdate.executeUpdate();
				
				stmtUpdate.close();
				
			} catch (Exception e) {
				Apps.sqlErr(" MedalProxy >>  setdayfirstlogin  "+ e.toString()+" userId"+ userId+" Query "+sqlUpdate, gameBean);
			}
		}
		else
		{			
			String sqlUpdate = "update user_data set noof_wins_inaday = ? where user_id = ?;";		
			Apps.sqlLog("MedalProxy >> noofWinsInaDay noofwinsinaday"+noofwinsinaday+" userId"+ userId+" sqlUpdate "+ sqlUpdate);
			try {
					PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
					pstmt.setInt(1, noofwinsinaday);
					pstmt.setInt(2, userId);
					
					pstmt.executeUpdate();
					
					pstmt.close();
					
				} catch (Exception e) {
					Apps.sqlErr(" MedalProxy >>  noofWinsInaDay  "+ e.toString()+" noofwinsinaday "+noofwinsinaday+" userId"+ userId+" Query "+sqlUpdate, gameBean);
				}
		}						
		
		
		if(noofwinsinaday == 10)
		{
			int noofrows = getParticularMedalCountofaUser(userId, Commands.MEDAL_29, gameBean);
			
			if(noofrows == 0)
			{
				sendMedal(Commands.MEDAL_29, 1, player, tableId);
				insertMedalToUser(userId, Commands.MEDAL_29, 1, gameBean);				
				
				Commands.appInstance.medalsBsn.checkMedal46(player, userId, tableId);				
				Commands.appInstance.medalsBsn.checkMedal47(player, userId, tableId);
			}
		}
		if(noofwinsinaday == 15)
		{
			int noofrows = getParticularMedalCountofaUser(userId, Commands.MEDAL_30, gameBean);
			if(noofrows == 0)
			{			
				sendMedal(Commands.MEDAL_30,1, player, tableId);				
				insertMedalToUser(userId, Commands.MEDAL_30, 1, gameBean);	
				
				Commands.appInstance.medalsBsn.checkMedal48(player, userId, tableId);
			}
		}
		if(noofwinsinaday == 25)
		{
			int noofrows = getParticularMedalCountofaUser(userId, Commands.MEDAL_31, gameBean);
			
			if(noofrows == 0)
			{				
				sendMedal(Commands.MEDAL_31, 1, player, tableId);
				insertMedalToUser(userId, Commands.MEDAL_31, 1, gameBean);	
			}
		}		
	}
	
	
	public int getPlayerNoofWinsInaRow(int userId, GameBean gameBean)
	{
		int noofWinsInaRow = 0;
		String select = "select * from user_data where user_id = ?;";
		
		Apps.sqlLog(" MedalsProxy >> getPlayerNoofWinsInaRow userId "+userId+" query "+select);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(select);
			pstmt.setInt(1, userId);
			
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				noofWinsInaRow = rs.getInt("noof_wins_inarow");
			}
			
			rs.close();
			pstmt.close();
			
		}catch(Exception e)
		{
			Apps.sqlErr(" MedalsProxy : getPlayerNoofWinsInaRow :" + e.toString()+"  userId"+userId+" Query "+select, gameBean);
		}
		
		return noofWinsInaRow;
		
	}
	
	public int getParticularMedalCountofaUser(int userId, int medalId, GameBean gameBean)
	{
		int count = 0;
		
		String select = "select * from user_medals where user_id = ? and medal_id = ? ;";
		Apps.sqlLog(" MedalsProxy >> getParticularMedalCountofaUser userId "+userId+" medalId "+medalId+"  query "+select);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(select);
			pstmt.setInt(1, userId);
			pstmt.setInt(2, medalId);
			
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				count++;
			}
			
			rs.close();
			pstmt.close();						
		}
		catch(Exception e)
		{
			Apps.sqlErr(" MedalsProxy : getParticularMedalCountofaUser :" + e.toString()+"  userId"+userId+"  medalId "+medalId+" Query "+select, gameBean);
		}	
		
		return count;
	}
	
	public int getMedalCountofaUser(int userId, int medalId, GameBean gameBean)
	{
		int count = 0;
		
		String select = "select count from user_medals where user_id = ? and medal_id = ? ;";
		Apps.sqlLog(" MedalsProxy >> getMedalCountofaUser userId "+userId+" medalId "+medalId+"  query "+select);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(select);
			pstmt.setInt(1, userId);
			pstmt.setInt(2, medalId);
			
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				count = rs.getInt("count");
			}
			
			rs.close();
			pstmt.close();
		}
		catch(Exception e)
		{
			Apps.sqlErr(" MedalsProxy : getMedalCountofaUser :" + e.toString()+"  userId"+userId+"  medalId "+medalId+" Query "+select, gameBean);
		}	
		
		return count;
		
	}
	
	public int getNoofSigninFordaysInarow(int userId, GameBean gameBean)
	{
		String query = "select * from user_data where user_id="+userId+";";
		Apps.sqlLog(" MedalsProxy >> getNoofSigninFordaysInarow "+query);
		int noofsigninfordaysinarow = 0;
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();					
			
			while (rs.next()) 
			{						
				noofsigninfordaysinarow = rs.getInt("noof_signin_fordays_inarow");
				
			}
			
			rs.close();
			pstmt.close();
			
		}catch(Exception e)
		{
			Apps.sqlErr(" MedalsProxy : getNoofSigninFordaysInarow :" + e.toString()+"  userId"+userId+" Query "+query, gameBean);
		}
		
		return noofsigninfordaysinarow;
	}
	
	public void updateNoofSiraBonus(int userId, int noofSiras, GameBean gameBean)
	{
		String sqlUpdate = "update user_data set noof_sira_bonus = ? where user_id = ?";
		Apps.sqlLog(" MedalsProxy >> updateNoofSiraBonus noofSirsa "+noofSiras+"  userId "+userId+"  query "+sqlUpdate);
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setInt(1, noofSiras);
			pstmt.setInt(2, userId);
			
			pstmt.executeUpdate();
			
			pstmt.close();
			
		}catch(Exception e)
		{
			Apps.sqlErr(" MedalsProxy updateNoofSiraBonus "+ e.toString()+"noofSirsa "+noofSiras+"  userId "+userId+" Query "+sqlUpdate, gameBean);
		}
	}
	
	public void updateNoofFiftyBonus(int userId, int noofFifty, GameBean gameBean)
	{
		String sqlUpdate = "update user_data set noof_fifty_bonus = ? where user_id = ?";
		Apps.sqlLog(" MedalsProxy >> updateNoofFiftyBonus noofFifty "+noofFifty+"  userId "+userId+"  query "+sqlUpdate);
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setInt(1, noofFifty);
			pstmt.setInt(2, userId);
			
			pstmt.executeUpdate();
			
			pstmt.close();
			
		}catch(Exception e)
		{
			Apps.sqlErr(" MedalsProxy updateNoofFiftyBonus "+ e.toString()+"  noofSirsa "+noofFifty+"  userId "+userId+" Query "+sqlUpdate, gameBean);
		}
	}
	
	public void updateNoofHundredBonus(int userId, int noofHundred, GameBean gameBean)
	{
		String sqlUpdate = "update user_data set noof_hundred_bonus = ? where user_id = ?";
		Apps.sqlLog(" MedalsProxy >> updateNoofHundredBonus noofHundred "+noofHundred+"  userId "+userId+"  query "+sqlUpdate);
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setInt(1, noofHundred);
			pstmt.setInt(2, userId);
			
			pstmt.executeUpdate();
			
			pstmt.close();
			
		}catch(Exception e)
		{
			Apps.sqlErr(" MedalsProxy updateNoofHundredBonus "+ e.toString()+"  noofHundred "+noofHundred+"  userId "+userId+" Query "+sqlUpdate, gameBean);
		}
	}
	
	public void updateNoofFourHundredBonus(int userId, int noofFourHundred, GameBean gameBean)
	{
		String sqlUpdate = "update user_data set noof_four_hundred_bonus = ? where user_id = ?";
		Apps.sqlLog(" MedalsProxy >> updateNoofFourHundredBonus noofFourHundred "+noofFourHundred+"  userId "+userId+"  query "+sqlUpdate);
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setInt(1, noofFourHundred);
			pstmt.setInt(2, userId);
			
			pstmt.executeUpdate();
			
			pstmt.close();
			
		}catch(Exception e)
		{
			Apps.sqlErr(" MedalsProxy updateNoofFourHundredBonus "+ e.toString()+" noofFourHundred "+noofFourHundred+"  userId "+userId+" Query "+sqlUpdate, gameBean);
		}
	}
	
	private void sendMedal(Integer medal, Integer count, String player, String roomname)
	{
		Room room = Apps.getRoomByName(roomname); 
		GameBean gameBean = Apps.getGameBean(roomname);
		
		ISFSObject sfso = new SFSObject();	
		
		sfso.putInt("medalId", medal);
		sfso.putInt("count", count);
		sfso.putUtfString("player", player);
		
		Commands.appInstance.send(Commands.WON_MEDAL, sfso, room.getUserList());
		
		if(gameBean != null)
		{
			gameBean.getGameLogInstance().saveMedal(gameBean, player, count, medal);
		}
		
	}
	
	
	public String getDateString()
	{
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Calendar cal = Calendar.getInstance();		
		String date = dateFormat.format(cal.getTime());
		
		return date;
	}

}
