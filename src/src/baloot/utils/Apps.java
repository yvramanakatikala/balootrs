package src.baloot.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import org.apache.commons.codec.binary.Base64;

import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerProfileBean;
import src.baloot.beans.TableBean;
import src.baloot.constants.BeltType;
import src.baloot.constants.Constant;
import src.baloot.exce.Validations;
import src.baloot.exce.beans.ExcBean;
import src.baloot.ping.UserObject;

import com.smartfoxserver.bitswarm.sessions.ISession;
import com.smartfoxserver.v2.api.CreateRoomSettings;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.SFSRoomRemoveMode;
import com.smartfoxserver.v2.entities.User;


public class Apps {
	
	public static void showLog(String str)
	{
		//System.out.println(getDateString()+" Baloot  :  "+str);
	}

	public static void sqlLog(String str)
	{
		//System.out.println("Baloot SQL_Log : "+str);
	}
	
	public static void sqlErr(String str, GameBean gameBean)
	{
		System.out.println("Baloot SQL_EXC : "+str);
		
		
		String gameId = null;
		int ticket = -1;
		
		if(gameBean != null)
		{
			gameId = gameBean.gameId;
			ticket = gameBean.getTicket();
			
			gameBean.getGameLogInstance().sqlException(gameBean, str); 
		}
		
		ExcBean eb = new ExcBean();
		
		eb.setExcMsg("SQL_EXCEPTION");
		eb.setGameID(gameId);
		eb.setPlayer("NotAvailable");
		eb.setPriority("P0");
		eb.setTicket(ticket);
		eb.setVariable(str);
		eb.setAction("SQL_EXCEPTION");
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
	}
	
	public static Date getCurrentTime()
	{
		Date d = null;
		
		Calendar currentTime = Calendar.getInstance();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		String dateCur = dateFormat.format(currentTime.getTime());
		try{
			d = (Date)dateFormat.parse(dateCur);
		}catch(Exception e)
		{
			e.printStackTrace();
		} 
		
		return d;
	}
	
	public static String getDateString()
	{
		String dateCur = null;
		
		Calendar currentTime = Calendar.getInstance();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		dateCur = dateFormat.format(currentTime.getTime());
		
		return dateCur;
	}
	
	public static Long getTimeDifference(Date preTime)
	{
		Long diff = null;
		
		if(preTime != null)
		{
			diff = (getCurrentTime().getTime() - preTime.getTime())/1000;
		}
		
		return diff;
	}
	
	public static String getDayString()
	{
		String dateString = null;
		
		DateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy");  //  yyyy_MM_dd
	 	dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

 	    Date date = new Date();	       
 		dateString = dateFormat.format(date);
 		
 		//showLog(" Date_string_is : "+dateString);
 		
		return dateString;
	}
	
	public static String getMonth()
	{
		String dateString = null;
		
		// DateFormat dateFormat = new SimpleDateFormat("MM_yyyy");  
		DateFormat dateFormat = new SimpleDateFormat("MMMM,yyyy"); 
	 	dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

 	    Date date = new Date();	       
 		dateString = dateFormat.format(date);
 		
 		//showLog(" Date_string_is : "+dateString);
 		
		return dateString;
	}
	
	public static String getHour()
	{
		String current_time = null;
		
		DateFormat dateFormat = new SimpleDateFormat("HH");	
	 	dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

		
	    Date date = new Date();	       
	    current_time = dateFormat.format(date);
	    
	    return current_time;
	}
	
	public static Date getDateFromString(String str)
	{
		Date d = null;
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		try{
			d = (Date)dateFormat.parse(str);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return d;
	}
	
	public static String getZone()
	{
		String zoneName = null;
		
		zoneName = Commands.appInstance.getParentZone().getName();
		
		//showLog(" Zone_name_is : "+zoneName);
		return zoneName;
	}
	
	public static String getZonePath()
	{
		return "gamelogs/bl/"+Apps.getZone();
	}

	public static User getUserByName(String name)
	{		
		User user = null;
	    user = Commands.appInstance.getApi().getUserByName(name);
	    if( user == null )
	    {
	    	showLog("User Not Found : "+name);
	    }	    
	    return user;
	}
	
	public static UserObject getUserObject(String name)
	{
		UserObject userObj;		
		userObj = Commands.appInstance.getUserObjs().get(name);
		if(userObj == null)
		{
			showLog(" UserObject NOt Fount "+userObj+" name "+name);
		}
		
		return userObj;
	}
	
	public static Room getRoomByName(String tableId)
	{
		Room room = null;
		room = Commands.appInstance.getParentZone().getRoomByName(tableId);
		//showLog("tableId : "+tableId+"   Room : $$$"+room);
		
		return room;		
	}	
	public static GameBean getGameBean(String tableId)
	{
		GameBean gameBean = null;
		gameBean = Commands.appInstance.getGames().get(tableId);
		//showLog("tableId : "+tableId+"   gameBean : "+gameBean);
		
		return gameBean;
	}	
	
	public static void createGameRoom(String roomname)
	{		
		try
		{
			CreateRoomSettings settings = new CreateRoomSettings();
		    settings.setName(roomname);	    
		    settings.setMaxUsers(10);
		    settings.setHidden(false);	    
		    settings.setGroupId(Constant.GAME_GROUP); 
		    settings.setAutoRemoveMode(SFSRoomRemoveMode.WHEN_EMPTY);	    	    
		    Commands.appInstance.getApi().createRoom(Commands.appInstance.getParentZone(), settings, null);
		}catch(Exception e)
		{
			//e.printStackTrace();
		}	    
	}
	
/*	public static void joinRoom(User user, Room room)
	{		
		showLog(" Room is ^^^^^^ : "+room.getName());
		try{
				if(!room.containsUser(user))
				{
					Commands.appInstance.getApi().joinRoom(user, room, null, false, null);	
				}
						
		   }catch(Exception e)
		   {
		    	showLog(" join Room^ ");
		    	e.printStackTrace();
		   }
		showLog("User : "+user.getName()+" @@joined into the Room : "+room.getName());
	}*/
	
	public static void joinLobbyRoom(User user, Room room)
	{		
		showLog(" LobbyRoom is ^^^^^^ : "+room.getName());
		try{
				if(!room.containsUser(user))
				{
					Commands.appInstance.getApi().joinRoom(user, room);	
				}
						
		   }catch(Exception e)
		   {
		    	showLog(" join LobbyRoom^ ");
		    	e.printStackTrace();
		   }
		showLog("User : "+user.getName()+" @@joined into the LobbyRoom : "+room.getName());
	}
	
	public static void joinGameRoom(User user, Room room)
	{
		showLog(" GameRoom is ^^^^^^ : "+room.getName());
		try{
				if(!room.containsUser(user))
				{
					Commands.appInstance.getApi().joinRoom(user, room);	
				}
						
		   }catch(Exception e)
		   {
		    	showLog(" join Room^ ");
		    	e.printStackTrace();
		   }
		showLog("User : "+user.getName()+" @@joined into the GameRoom : "+room.getName());
	}
	
	public static void leaveRoom(User user, Room room)
	{
		try
		{
			Commands.appInstance.getApi().leaveRoom(user, room);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		showLog("User : "+user.getName()+" Leave the Room : "+room.getName());
	}
	
	public static void removeRoom(Room room)
	{
		if(room != null)
		{
			String roomName = room.getName();
			showLog(" removeRoom roomName : "+roomName);
			showLog(" Before room users : "+room.getUserList());
			for(User user : room.getUserList())
			{
				room.removeUser(user);
			}
			showLog(" After room users : "+room.getUserList());		
			Commands.appInstance.getApi().removeRoom(room);
			
			showLog(" room removed : "+roomName);
			roomName = null;
		}
	}
	
	public static void removePlayerId(String player)
	{
		Commands.appInstance.getPlayerIds().remove(player);
	}
	
	public static Integer getPlayerId(String player)
	{
		Integer userId = null;
		
		userId = Commands.appInstance.getPlayerIds().get(player);
		
		if(userId == null)
		{
			userId = Commands.appInstance.proxy.getUserID(player);
			if(userId != null && userId != -1)
			{
				Commands.appInstance.getPlayerIds().put(player, userId);
			}
			else
			{
				userId = -1;
				if(player.equals("AI1") || player.equals("AI2") || player.equals("AI3"))
				{
					// This is AI no need to raise exception.
					userId = -1;
				}
				else
				{
					Validations.raiseUserIDNULLException(player, userId);
				}
			}
		}
		
		return userId;
	}
	
	public static String getSuit(int no)
	{
		String suit = null;
		
		if(no>=1 && no<=8)
		{
			suit = "clubs";
		}			
		else if(no>=9 && no<=16)
		{
			suit = "diamonds";
		}			
		else if(no>=17 && no<=24)
		{
			suit = "hearts";
		}			
		else if(no>=25 && no<=32)
		{
			suit = "spades";
		}
			
		return suit;
	}
	
	public static boolean isSameIP(String playerIP, String player)
	{
		if(!Commands.appInstance.gameCV.isAllowIP())
		{
			List<User> allUsers = getAllUsers();
			
			for(User user : allUsers)
			{
				if(user.getSession().getAddress().equals(playerIP))
				{
					Validations.raiseLoginWithSameIPException(user, player, playerIP);
					return true;
				}
			}
		}
		
		return false;		
	}
	
	public static boolean isSameGuestIP(String playerIP, String player)
	{
				
		for(String guestUser : Commands.appInstance.guestUsers)
		{
			User user = Apps.getUserByName(guestUser);
			
			if(user != null)
			{
				if(user.getSession().getAddress().equals(playerIP))
				{
					Validations.raiseLoginWithSameGuestIPException(user, player, playerIP);
					return true;
				}
			}
		}
		
		return false;		
	}
	
	
	
	public  static List<User> getLobbyUsers()
	{
		List<User> listusers = new ArrayList<User>();
	
		Collection<User> users = Commands.appInstance.getParentZone().getUsersInGroup("LobbyGroup");	
		
		Object arr[] = users.toArray();				
		for(int i=0;i<arr.length;i++)
		{
			User no = ((User)arr[i]);
			listusers.add(no);			
		}
		
		return listusers;
	}
	
	public static List<User> getGameGroupUsers()
	{
		List<User> listusers = new ArrayList<User>();
		
		Collection<User> users = Commands.appInstance.getParentZone().getUsersInGroup("GameGroup");	
		
		Object arr[] = users.toArray();				
		for(int i=0;i<arr.length;i++)
		{
			User no = ((User)arr[i]);
			listusers.add(no);			
		}
		
		return listusers;
	}
	
	public static List<User> getAllUsers()
	{
		List<User> listusers = getLobbyUsers();
		
		Collection<User> users = Commands.appInstance.getParentZone().getUsersInGroup("GameGroup");	
		
		Object arr[] = users.toArray();				
		for(int i=0;i<arr.length;i++)
		{
			User no = ((User)arr[i]);
			listusers.add(no);			
		}		
		return listusers;
	}
	
	public static int getOnlineUsersCount()
	{
		
		return getAllUsers().size();
	}
	
	public static ArrayList<User> getRemainingUsers(Room room, User user)
	{
		ArrayList<User> al = new ArrayList<User>();
		
		if(room != null)
		{
			for(User sender : room.getUserList())
			{
				if(!sender.getName().equals(user.getName()))
				{
					al.add(sender);
				}
			}
		}
		
		return al;
	}
	
	public static ArrayList<Integer> getListFromCollection(Collection<Integer> coll)
	{
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		Object arr[] = coll.toArray();				
		for(int i=0;i<arr.length;i++)
		{
			Integer no = ((Integer)arr[i]).intValue();
			list.add(no);
		}
		
		return list;
	}
	
	public static TableBean getPrivateTable(String roomname)
	{
		TableBean grb = null;
		
		for(int i=0;i<Commands.appInstance.privateTables.size();i++)
		{
			if(Commands.appInstance.privateTables.get(i).getTableId().equals(roomname))
			{
				grb = Commands.appInstance.privateTables.get(i);
				break;
			}
		}
		
		return grb;
	}
	
	public static void removePrivateTable(String roomname)
	{
		for(int i=0;i<Commands.appInstance.privateTables.size();i++)
		{
			if(Commands.appInstance.privateTables.get(i).getTableId().equals(roomname))
			{
				Commands.appInstance.privateTables.remove(i);
				break;
			}
		}
	}
	
	public static boolean checkIsGamePlayer(String player, ArrayList<String> players, ArrayList<String> spectators)
	{
		boolean sign = false;
		
		for(int i=0;i<players.size();i++)
		{
			if(player.equals(players.get(i)))
			{
				sign = true;
				break;
			}
		}
		// Check for Spectators
		if(!sign)
		{
			for(int i=0;i<spectators.size();i++)
			{
				if(player.equals(spectators.get(i)))
				{
					sign = true;
					break;
				}
			}
		}
		
		return sign;
	}
	
	public static int getStatus(String userName)
	{
		int status = 0;
		User user = Apps.getUserByName(userName);
		
		if(user != null)
		{
			List<User> list = Apps.getLobbyUsers();
			
			if(list.contains(user))
			{
				status = 1;
			}
			
			list = Apps.getGameGroupUsers();
			
			if(list.contains(user))
			{
				status = 2;
			}
		}
		
		return status;
	}
	
	public static boolean isAI(String player)
	{
		if(player.equals("AI1") || player.equals("AI2") || player.equals("AI3"))
		{
			return true;
		}
		
		return false;
	}
	public static String getGender(String player)
	{
		if(player.equals("AI1") || player.equals("AI2") || player.equals("AI3") || player.equals("AI4"))
		{
			return Constant.MALE;
		}
		else
		{
			PlayerProfileBean pfb = Commands.appInstance.playerProfiles.get(player);
			
			if(pfb != null)
			{
				return pfb.gender;
			}
			else
			{		
				if(Apps.isGuestUser(player))
				{
					/*
					 * Here pfb is null because at this time user is in diconnection state.
					 */
					return Constant.MALE;
				}
				else
				{
					Integer playerId = getPlayerId(player);
					String gender = Commands.appInstance.proxy.getGender(playerId);
					Validations.genderNotFoundDutTo_PFB_null(pfb, gender, player, playerId);
					return gender;
					//return "NA";
				}				
			}
		}
	}
	
	
	public static String getGuestName(ISession session)
	{
		String name = session.getAddress();	
		
		if(!Commands.appInstance.guestUsers.contains(name))
		{
			Commands.appInstance.guestUsers.add(name);
		}		
		
		return name;
	}
	
	public static void removeGuestUser(String userName)
	{
		if(Commands.appInstance.guestUsers.contains(userName))
		{
			Commands.appInstance.guestUsers.remove(userName);
		}
	}
	
	public static String getBeltType(int noofWins)
	{	
		String beltType = BeltType.WHITE;
		
		if(noofWins > 0 && noofWins <= 14)
		{
			beltType = BeltType.WHITE;
		}
		else if(noofWins > 14 && noofWins <= 34)
		{
			beltType = BeltType.YELLOW;
		}
		else if(noofWins > 34 && noofWins <= 54)
		{
			beltType = BeltType.ORANGE;
		}
		else if(noofWins > 54 && noofWins <= 69)
		{
			beltType = BeltType.RED;
		}
		else if(noofWins > 69 && noofWins <= 84)
		{
			beltType = BeltType.GREEN;
		}
		else if(noofWins > 84 && noofWins <= 100)
		{
			beltType = BeltType.BLACK;
		}
		else if(noofWins > 100 && noofWins <= 140)
		{
			beltType = BeltType.SILVER;
		}
		else if(noofWins > 140 && noofWins <= 180)
		{
			beltType = BeltType.GOLD;
		}
		else if(noofWins > 180)
		{
			beltType = BeltType.FIRE;
		}
		
		return beltType;
	}
	
	public static int calculateUserRank(int noofWins, int previousRank, String status)
	{
		int gameRank = 0;
		int currentRank = previousRank;
		
		if(status.equals(Constant.WON))
		{
			gameRank = 2;
		}
		
		
		if(noofWins > 0 && noofWins <= 14)
		{
			currentRank = currentRank +(gameRank/2);
		}
		else if(noofWins > 14 && noofWins <= 34)
		{
			currentRank = currentRank +(gameRank/2);
		}
		else if(noofWins > 34 && noofWins <= 54)
		{
			currentRank = currentRank +(gameRank/2);
		}
		else if(noofWins > 54 && noofWins <= 69)
		{
			currentRank = currentRank +(gameRank - 1);
		}
		else if(noofWins > 69 && noofWins <= 84)
		{
			currentRank = currentRank +(gameRank - 1);
		}
		else if(noofWins > 84 && noofWins <= 100)
		{
			currentRank = currentRank +(gameRank - 1);
		}
		else if(noofWins > 100 && noofWins <= 140)
		{
			currentRank = currentRank +(gameRank +(gameRank/2)- 2);
		}
		else if(noofWins > 140 && noofWins <= 180)
		{
			currentRank = currentRank +(gameRank +(gameRank/2)- 2);
		}
		else if(noofWins > 180)
		{
			currentRank = currentRank +(gameRank +(gameRank/2)- 2);
		}
		
		if(currentRank > 99)
		{
			currentRank = 100;
		}
		
		if(currentRank < 0)
		{
			currentRank = 0;
		}
		
		return currentRank;
	}
	
	/*public static boolean isGuestUser(String name)
	{
		if(Commands.appInstance.guestUsers.contains(name))
		{
			return true;
		}
		else
		{
			return false;
		}
	}*/
	
	/**
	 * Get real users list from the data base and check whether this name exists in the DataBase real users list or
	 * or not. it does not exists then user is Guest user. The following code is not correct code  and it is a hack code.
	 * @param player
	 * @return
	 */
	public static boolean isGuestUser(String player)
	{
		String []s = player.split("\\.");
		
		if(s.length > 3)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static boolean isFakePlayer(String player)
	{
		User user = getUserByName(player);
		
		if(user != null && user.isNpc())
		{
			return true;
		}
		
		return false;
				
	}
	
	public static PlayerProfileBean getPlayerProfile(String userName)
	{
		if(Commands.appInstance.playerProfiles.contains(userName))
		{
			return Commands.appInstance.playerProfiles.get(userName);
		}
		
		return null;
	}
	
	public static int getRandomNumber(int range)
	{
		Random generator = new Random();
		
	    int randomInt = generator.nextInt(range);
	      
	    return (randomInt < 6) ? getRandomNumber(range) : randomInt;
	}
	
	public static ArrayList<Integer> getBalootBonusCards(GameBean gameBean)
	{
		ArrayList<Integer> bonusCards = new ArrayList<Integer>();
		
		if(gameBean._roundBean._trumpSuitType.equals("clubs")){
			bonusCards.add(2);bonusCards.add(3);
		}
		else if(gameBean._roundBean._trumpSuitType.equals("diamonds")){
			bonusCards.add(10);bonusCards.add(11);
		}
		else if(gameBean._roundBean._trumpSuitType.equals("hearts")){
			bonusCards.add(18);bonusCards.add(19);
		}
		else{
			bonusCards.add(26);bonusCards.add(27);
		}
		
		return bonusCards;
	}
	
	public static String getBase64EncodedText(String str)
	{
		byte[]   bytes = Base64.encodeBase64(str.getBytes());
		
		String enstr = new String(bytes);
		
		return enstr;
	}
	
	public static String getBase64DecodedText(String str)
	{
		byte[] bytes = Base64.decodeBase64(str.getBytes());	
		
	    String destr =  new String(bytes);
	    
	    return destr;
	}
	
	/*private void shuffelCards(GameBean gameBean)
	{
		//gameBean._roundBean._trumpSuitType = "clubs";
		//gameBean._roundBean._trumpSuitType = "diamonds";
		//gameBean._roundBean._trumpSuitType = "hearts";
		//gameBean._roundBean._trumpSuitType = "spades";
		
		
		
		for(PlayerRoundBean prb : gameBean._roundBean._playerRoundBeanObjs)
		{
			prb._cards = new ArrayList<Integer>();
		}
		
		// 1 player
		PlayerRoundBean prb = gameBean._roundBean._playerRoundBeanObjs.get(0);		
		
		prb._cards.add(0);
		prb._cards.add(0);
		prb._cards.add(0);
		prb._cards.add(0);
		prb._cards.add(0);
		
		// 1 player
		prb = gameBean._roundBean._playerRoundBeanObjs.get(1);		
		
		prb._cards.add(0);
		prb._cards.add(0);
		prb._cards.add(0);
		prb._cards.add(0);
		prb._cards.add(0);
		
		
		
		// 1 player
		prb = gameBean._roundBean._playerRoundBeanObjs.get(2);		
		
		prb._cards.add(0);
		prb._cards.add(0);
		prb._cards.add(0);
		prb._cards.add(0);
		prb._cards.add(0);
		
		
		
		// 1 player
		prb = gameBean._roundBean._playerRoundBeanObjs.get(3);		
		
		prb._cards.add(0);
		prb._cards.add(0);
		prb._cards.add(0);
		prb._cards.add(0);
		prb._cards.add(0);
	}*/
	
	/**
	 * Here gameType is HOKOM. But current suit is not hokom suit.so sort cards in san order. 
	 * and player have round suit cards.
	 * @param gameBean
	 * @param roundSuitCards
	 */
	public static boolean isPlayerHaveHighestCardThanTheOpenCardsList(GameBean gameBean, ArrayList<Integer> roundSuitCards)
	{
		roundSuitCards = Commands.appInstance.ai.sortSunorder(roundSuitCards);	
		ArrayList<Integer> openCardsList = Commands.appInstance.ai.sortSunorder(gameBean._roundBean.openCardsList);
		
		int sunHighestPos[] = {1,5,2,3,4,6,7,0};
		
		int highestOpenCardRank = 100;
		int highestRoundSuitCardRank = 100;
		
		for(int i=0;i<sunHighestPos.length;i++)
		{
			if(sunHighestPos[i] == openCardsList.get(0)%8)
			{
				highestOpenCardRank = i;
				break;
			}
		}
		
		for(int i=0;i<sunHighestPos.length;i++)
		{
			if(sunHighestPos[i] == roundSuitCards.get(0)%8)
			{
				highestRoundSuitCardRank = i;
				break;
			}
		}
		
		if(highestOpenCardRank > highestRoundSuitCardRank)
		{
			return true;
		}
		
		return false;
	}
	
	public static boolean isAnyOnePlayedHokomSuitCard(GameBean gameBean)
	{
		return isOpenCardsContainsHokomSuitCards(gameBean);
	}
	
	
	public static boolean isOpenCardsContainsHokomSuitCards(GameBean gameBean)
	{
		for(int i : gameBean._roundBean.openCardsList)
		{
			if(getSuit(i).equals(gameBean._roundBean._trumpSuitType))
			{
				return true;
			}			
		}
		
		return false;
	}
	
}
