/**
 * 
 */
package src.baloot.utils;


import java.util.ArrayList;

import src.baloot.beans.BonusBean;
import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerRoundBean;

/**
 * @author MSKumar
 *
 */
public class CalculateScores {

	
	private int clubsArray[] = {1,2,3,4,5,6,7,8};	// AKQJ10987
	private int diamondsArray[] = {9,10,11,12,13,14,15,16}; // AKQJ10987
	private int heartsArray[] = {17,18,19,20,21,22,23,24}; // AKQJ10987
	private int spadesArray[] = {25,26,27,28,29,30,31,32}; // AKQJ10987
	
	private int hokomHighestPos[] = {4,6,1,5,2,3,7,8};
	private int sunHighestPos[] = {1,5,2,3,4,6,7,8};
	

	private ArrayList<Integer> opencards = new ArrayList<Integer>();
	
	
	public ArrayList<Integer> getTeamBonusScores(GameBean gameBean)
	{
		ArrayList<Integer> teamBonus = new ArrayList<Integer>();
		
		Integer team1Bonus = 0;
		Integer team2Bonus = 0;
		
		//System.out.println(" bonusObjects size "+gameBean._roundBean._bonusObjs.size());
		if(gameBean._roundBean._bonusObjs.size() > 0)
		{
			String highBonusPerson = null;
			
			for(String player : gameBean._players)
			{
				if(player.equals(gameBean._roundBean._highestBonusPerson))
				{
					highBonusPerson = player;
					break;
				}
			}
			if(highBonusPerson != null)
			{
				int pos = -1;
				pos = gameBean.getPlayerPosition(highBonusPerson);
				
				for(BonusBean bb : gameBean._roundBean._bonusObjs)
				{
					if((pos == 0 || pos == 2) && bb._isShowedCards)
					{
						team1Bonus = team1Bonus + getBonusPoints(bb._bonusType, gameBean._roundBean._gameType);	
					}
					else if((pos == 1 || pos == 3) && bb._isShowedCards)
					{
						team2Bonus = team2Bonus + getBonusPoints(bb._bonusType, gameBean._roundBean._gameType);
					}
				}
				
				Apps.showLog("Bonuse");			
				Apps.showLog("Team1Bonus  "+team1Bonus);
				Apps.showLog("Team2Bonus  "+team2Bonus);
				
				if(gameBean._roundBean.isDouble)
				{
					team1Bonus = (team1Bonus)*(gameBean._roundBean.doubleCount+1);
					team2Bonus = (team2Bonus)*(gameBean._roundBean.doubleCount+1);			
				}
			}
		}
		
		teamBonus.add(team1Bonus);
		teamBonus.add(team2Bonus);		
		
		return teamBonus;
	}
	
	public ArrayList<Integer> getBalootBonus(GameBean gameBean)
	{
		ArrayList<Integer> balootBonus = new ArrayList<Integer>();
		
		int team1BalootBonus = 0;
		int team2BalootBonus = 0;
		
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{
			for(BonusBean bb : gameBean._roundBean._bonusObjs)
			{
				if(bb._bonusType.equals("baloot"))
				{
					int position = -1;
					
					for(String player : gameBean._players)
					{
						if(bb._playerId.equals(player))
						{
							position = gameBean.getPlayerPosition(player);
							break;
						}
					}
					
					if(position == 0 || position == 2)
					{
						team1BalootBonus = team1BalootBonus + 2;
					}
					else if(position == 1 || position == 3)
					{
						team2BalootBonus = team2BalootBonus + 2;
					}	
					
					break;
				}
			}
		}
		
		balootBonus.add(team1BalootBonus);
		balootBonus.add(team2BalootBonus);
		
		gameBean.getGameLogInstance().balootBonus(gameBean, balootBonus);
		return balootBonus;
	}
	
	public void calculateTeamScoresNew(GameBean gameBean, String wonPlayer)
	{

		GameApps.calculateTeamPoints(gameBean, wonPlayer);
		
		Apps.showLog(" calculateTeamScoresNew");
		
		Integer team1Score = 0;
		Integer team2Score = 0;
		Integer t1RoundScore = 0;
		Integer t2RoundScore = 0;
		Integer totalGamePoints=0;
		
		// Get team's Scores			
		team1Score = gameBean._roundBean._playerRoundBeanObjs.get(0)._totalScore + gameBean._roundBean._playerRoundBeanObjs.get(2)._totalScore;		
		team2Score = gameBean._roundBean._playerRoundBeanObjs.get(1)._totalScore + gameBean._roundBean._playerRoundBeanObjs.get(3)._totalScore;
		
		Apps.showLog("Before_Adding_Last_trick_team1score "+team1Score);
		Apps.showLog("Before_Adding_Last_trick_team2score "+team2Score);
		
		
		Integer doubleCnt = gameBean._roundBean.doubleCount+1;
		
		// Add last trick bonus score
		if(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId.equals(wonPlayer) || gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId.equals(wonPlayer))
		{
			team1Score += 10*doubleCnt;
		}
		else
		{
			team2Score += 10*doubleCnt;
		}
		
		
		Apps.showLog("Double_count  "+doubleCnt);
		Apps.showLog("After_Adding_Last_trick_team1score "+team1Score);
		Apps.showLog("After_Adding_Last_trick_team2score "+team2Score);
		
		/** Round the Score*/
		
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{
			// Total game Points			
			
			//AppMethods.showLog((16*doubleCnt)+"=="+(gameBean._roundBean._gameBonus));
			//totalGamePoints += (16*doubleCnt)+(gameBean._roundBean._gameBonus);
			totalGamePoints += (16*doubleCnt);
			
			Apps.showLog("totalGamePoints hokom "+totalGamePoints);
			
			t1RoundScore += team1Score/10;			
			if(team1Score%10>=5)
				t1RoundScore += 1;	
			
			t2RoundScore += team2Score/10;			
			if(team2Score%10>=5)
				t2RoundScore += 1;
			
			Apps.showLog(""+t1RoundScore);
			Apps.showLog(""+t2RoundScore);
		}
		else // Sun
		{
			// Total game Points Need to implement(double)			
			totalGamePoints += (26*doubleCnt);
			
			//AppMethods.showLog("totalGamePoints sun "+totalGamePoints);
			t1RoundScore += (team1Score/10)*2;
			t2RoundScore += (team2Score/10)*2;
			
			if(team1Score%10 >= 6)
				t1RoundScore +=2;
			else if(team1Score%10 == 5)
				t1RoundScore +=1;
			
			if(team2Score%10 >= 6)
				t2RoundScore +=2;
			else if(team2Score%10 == 5)
				t2RoundScore +=1;
			
			Apps.showLog(""+t1RoundScore);
			Apps.showLog(""+t2RoundScore);
		}		
		
		// Calculating Bonus
		
		Integer team1CardsCount = 0;
		Integer team2CardsCount = 0;
		
		team1CardsCount += gameBean._roundBean._playerRoundBeanObjs.get(0)._wonCards.size();
		team1CardsCount += gameBean._roundBean._playerRoundBeanObjs.get(2)._wonCards.size();
		
		team2CardsCount += gameBean._roundBean._playerRoundBeanObjs.get(1)._wonCards.size();
		team2CardsCount += gameBean._roundBean._playerRoundBeanObjs.get(3)._wonCards.size();
		
		Integer team1Bonus = 0;
		Integer team2Bonus = 0;
		Integer team1BalootBonus = 0;
		Integer team2BalootBonus = 0;
		
		if(gameBean._roundBean._bonusObjs.size()>0)
		{
			
			int highBonusPerson = 0;
			
			for(int i=0;i<gameBean._roundBean._players.size();i++)
			{
				if(gameBean._roundBean._highestBonusPerson.equals(gameBean._players.get(i)))
				{
					highBonusPerson = i;
					break;
				}	
			}
			
			
			for(int i=0;i<gameBean._roundBean._bonusObjs.size();i++)
			{					
				String player = gameBean._roundBean._bonusObjs.get(i)._playerId;
				int no =0;
				for(int j=0;j<gameBean._players.size();j++)
				{
					if(gameBean._players.get(j).equals(player))
					{
						no = j;
						break;
					}
				}
				
				if( (no == 0 || no == 2) && (highBonusPerson == 0 || highBonusPerson == 2) && gameBean._roundBean._bonusObjs.get(i)._isShowedCards)
				{					
					team1Bonus += getBonusPoints(gameBean._roundBean._bonusObjs.get(i)._bonusType, gameBean._roundBean._gameType);					
				}
				else if( (no == 1 || no == 3) && (highBonusPerson == 1 || highBonusPerson == 3)&& gameBean._roundBean._bonusObjs.get(i)._isShowedCards)
				{
					team2Bonus += getBonusPoints(gameBean._roundBean._bonusObjs.get(i)._bonusType, gameBean._roundBean._gameType);
				}						
			}
			
			Apps.showLog("Bonuse");
			
			Apps.showLog("team1Bonus  "+team1Bonus);
			Apps.showLog("team2Bonus  "+team2Bonus);
		}
		
		
		// Adding baloot bonus
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{
			for(int i=0;i<gameBean._roundBean._bonusObjs.size();i++)
			{
				if(gameBean._roundBean._bonusObjs.get(i)._bonusType.equals("baloot"))
				{
					int no =0;
					for(int j=0;j<gameBean._players.size();j++)
					{
						if(gameBean._roundBean._bonusObjs.get(i)._playerId.equals(gameBean._players.get(j)))
						{
							no = j;
							break;
						}
					}
					
					if(no == 0 || no == 2)
					{
						team1BalootBonus += 2;
					}
					else
					{
						team2BalootBonus += 2;
					}					
					break;
				}
			}
		}
		
		Apps.showLog("Baloot Bonus");
		Apps.showLog(" team1BalootBonus "+team1BalootBonus);
		Apps.showLog(" team2BalootBonus "+team2BalootBonus);
		
		// Baloot bonus shouls not get doubled
		if(gameBean._roundBean.isDouble && team1CardsCount != 32 && team2CardsCount != 32)
		{
			team1Bonus = (team1Bonus)*(gameBean._roundBean.doubleCount+1);
			team2Bonus = (team2Bonus)*(gameBean._roundBean.doubleCount+1);			
		}		
		
		Apps.showLog(" RoundScore_before_bonus  t1RoundScore "+t1RoundScore);
		Apps.showLog(" RoundScore_before_bonus  t2RoundScore "+t2RoundScore);
		
		//Adding Bonus to round score		
		t1RoundScore = t1RoundScore + team1Bonus + team1BalootBonus;
		t2RoundScore = t2RoundScore + team2Bonus + team2BalootBonus;
		
		Apps.showLog(" RoundScore_after_bonus  t1RoundScore "+t1RoundScore);
		Apps.showLog(" RoundScore_after_bonus  t2RoundScore "+t2RoundScore);
				
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{	
			// Cutting the game extra points  
			
			totalGamePoints += team1Bonus +team2Bonus +team1BalootBonus +team2BalootBonus;
			
			if(t1RoundScore+t2RoundScore > totalGamePoints)
			{
				if(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId.equals(gameBean._roundBean._bidWonPerson) || gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId.equals(gameBean._roundBean._bidWonPerson))
				{
					t1RoundScore -= 1;
				}
				else
				{
					t2RoundScore -= 1;
				}
			}
		}
		
		// Check who win the bid and getting more points than their opponents will get the score
		// else they may loose their won points to the opponents
		
		String bidWonTeamPlayer = gameBean._roundBean._bidWonPerson;	
		
		
		String bidSelectedPerson = gameBean._roundBean._bidSelectedPerson;
		
		Apps.showLog(" bidWonTeamPlayer "+bidWonTeamPlayer );
		Apps.showLog(" bidSelectedPerson "+bidSelectedPerson);
		
		Apps.showLog(" bonus obj size "+gameBean._roundBean._bonusObjs.size());
		
		
		/*
		 * The below 4 lines code is just to find the bid won team.
		 */
		Integer bidLostTeam = 0;		
		if(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId.equals(bidWonTeamPlayer) || gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId.equals(bidWonTeamPlayer))
		{
			bidLostTeam = 1;
		}
		
		gameBean.getGameLogInstance().scoreCalculationBefore(gameBean, t1RoundScore, t2RoundScore);
		
		/*
		 *This code is added by Ramana 
		 */
		if(gameBean._roundBean.isDouble)
		{
			if(gameBean._roundBean.doubleCount == 1 || gameBean._roundBean.doubleCount == 3)
			{
				if(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId.equals(bidSelectedPerson) || 
						gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId.equals(bidSelectedPerson))
				{
					if(t1RoundScore < t2RoundScore)
					{						
						t2RoundScore = t2RoundScore + t1RoundScore-team1BalootBonus;
						t1RoundScore = 0+team1BalootBonus;
					}
					else
					{						
						//t1RoundScore = t1RoundScore + t2RoundScore-team2BalootBonus;
						//t2RoundScore = 0+team2BalootBonus;
					}
				}
				else
				{
					if(t2RoundScore < t1RoundScore)
					{
						t1RoundScore = t1RoundScore + t2RoundScore-team2BalootBonus; 
						t2RoundScore = 0+team2BalootBonus;
					}
					else
					{
						//t2RoundScore = t2RoundScore + t1RoundScore-team1BalootBonus;					
						//t1RoundScore = 0+team1BalootBonus;
					}
				}
			}
			else
			{
				if(bidLostTeam == 1)
				{
					Apps.showLog("Team one won");
					
					if(gameBean._roundBean._bonusObjs.size() > 0)
					{
						if(t1RoundScore < t2RoundScore)
						{					
							t2RoundScore = t2RoundScore + t1RoundScore-team1BalootBonus;
							t1RoundScore = 0+team1BalootBonus;
						}
						else
						{
							//t2RoundScore += team2BalootBonus;
							//t1RoundScore += team1BalootBonus;
						}
					}
					else
					{			
						if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
						{
							if(t1RoundScore < totalGamePoints/2)
							{						
								t2RoundScore = totalGamePoints;
								t1RoundScore = 0;
							}
						}
						else
						{
							if(t1RoundScore < t2RoundScore)
							{						
								t2RoundScore = totalGamePoints;
								t1RoundScore = 0;
							}
						}
					}			
				}
				else
				{
					Apps.showLog("Team two won");
					if(gameBean._roundBean._bonusObjs.size()>0)
					{
						if(t2RoundScore < t1RoundScore)
						{					
							t1RoundScore += t2RoundScore-team2BalootBonus;
							t2RoundScore = 0+team2BalootBonus;					
						}
						else
						{
							//t2RoundScore += team2BalootBonus;
							//t1RoundScore += team1BalootBonus;
						}
					}
					else
					{
						if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
						{
							if(t2RoundScore < totalGamePoints/2)
							{						
								t1RoundScore = totalGamePoints;
								t2RoundScore = 0;
							}
						}
						else
						{
							if(t2RoundScore < t1RoundScore)
							{						
								t1RoundScore = totalGamePoints;
								t2RoundScore = 0;
							}
						}
					}
				}
			}
		}
		else
		{
			/*
			 * Here no body select double or triple or 4 times.
			 */
			if(bidLostTeam == 1)
			{
				Apps.showLog("Team one won");
				
				if(gameBean._roundBean._bonusObjs.size() > 0)
				{
					if(t1RoundScore < t2RoundScore)
					{					
						t2RoundScore = t2RoundScore + t1RoundScore-team1BalootBonus;
						t1RoundScore = 0+team1BalootBonus;
					}
					else
					{
						//t2RoundScore += team2BalootBonus;
						//t1RoundScore += team1BalootBonus;
					}
				}
				else
				{			
					if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
					{
						if(t1RoundScore < totalGamePoints/2)
						{						
							t2RoundScore = totalGamePoints;
							t1RoundScore = 0;
						}
					}
					else
					{
						if(t1RoundScore < t2RoundScore)
						{						
							t2RoundScore = totalGamePoints;
							t1RoundScore = 0;
						}
					}
				}			
			}
			else
			{
				Apps.showLog("Team two won");
				if(gameBean._roundBean._bonusObjs.size()>0)
				{
					if(t2RoundScore < t1RoundScore)
					{					
						t1RoundScore += t2RoundScore-team2BalootBonus;
						t2RoundScore = 0+team2BalootBonus;					
					}
					else
					{
						//t2RoundScore += team2BalootBonus;
						//t1RoundScore += team1BalootBonus;
					}
				}
				else
				{
					if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
					{
						if(t2RoundScore < totalGamePoints/2)
						{						
							t1RoundScore = totalGamePoints;
							t2RoundScore = 0;
						}
					}
					else
					{
						if(t2RoundScore < t1RoundScore)
						{						
							t1RoundScore = totalGamePoints;
							t2RoundScore = 0;
						}
					}
				}
			}
		}
		
		gameBean.getGameLogInstance().scoreCalculationAfer(gameBean, t1RoundScore, t2RoundScore);
		/*if(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId.equals(bidWonTeamPlayer) || gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId.equals(bidWonTeamPlayer))
		{
			
			Apps.showLog("Team one won");
			
			if(gameBean._roundBean._bonusObjs.size() > 0)
			{
				if(t1RoundScore < t2RoundScore)
				{					
					t2RoundScore = t2RoundScore + t1RoundScore-team1BalootBonus;
					t1RoundScore = 0+team1BalootBonus;
				}
				else
				{
					//t2RoundScore += team2BalootBonus;
					//t1RoundScore += team1BalootBonus;
				}
			}
			else
			{			
				if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
				{
					if(t1RoundScore < totalGamePoints/2)
					{						
						t2RoundScore = totalGamePoints;
						t1RoundScore = 0;
					}
				}
				else
				{
					if(t1RoundScore < t2RoundScore)
					{						
						t2RoundScore = totalGamePoints;
						t1RoundScore = 0;
					}
				}
			}	
			
			
			
			// Double is enable
			if(gameBean._roundBean.isDouble)
			{
				Apps.showLog("Double is enabled Team 1 wons bid");
				if(gameBean._roundBean.doubleCount == 2)
				{			
					if(t1RoundScore < t2RoundScore)
					{						
						t2RoundScore = t2RoundScore + t1RoundScore-team1BalootBonus;
						t1RoundScore = 0+team1BalootBonus;
					}
					else
					{						
						t1RoundScore = t1RoundScore + t2RoundScore-team2BalootBonus;
						t2RoundScore = 0+team2BalootBonus;
						
					}
				}
				else
				{
					if(t1RoundScore < t2RoundScore)
					{						
						t2RoundScore = t2RoundScore + t1RoundScore-team1BalootBonus;
						t1RoundScore = 0+team1BalootBonus;
					}
					else
					{						
						t1RoundScore = t1RoundScore + t2RoundScore-team2BalootBonus;
						t2RoundScore = 0+team2BalootBonus;
					}
				}
			}			
		}
		else
		{
			bidLostTeam = 0;
			Apps.showLog("Team two won");
			if(gameBean._roundBean._bonusObjs.size()>0)
			{
				if(t2RoundScore < t1RoundScore)
				{					
					t1RoundScore += t2RoundScore-team2BalootBonus;
					t2RoundScore = 0+team2BalootBonus;					
				}
				else
				{
					//t2RoundScore += team2BalootBonus;
					//t1RoundScore += team1BalootBonus;
				}
			}
			else
			{
				if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
				{
					if(t2RoundScore < totalGamePoints/2)
					{						
						t1RoundScore = totalGamePoints;
						t2RoundScore = 0;
					}
				}
				else
				{
					if(t2RoundScore < t1RoundScore)
					{						
						t1RoundScore = totalGamePoints;
						t2RoundScore = 0;
					}
				}
			}
			
			// Double is enable
			if(gameBean._roundBean.isDouble)
			{
				
				Apps.showLog("Double is enabled Team 2 wons bid");
				
				if(gameBean._roundBean.doubleCount == 2)
				{			
					if(t2RoundScore < t1RoundScore)
					{						
						t1RoundScore += t2RoundScore-team2BalootBonus;
						t2RoundScore = 0+team2BalootBonus;
					}
					else
					{
						t2RoundScore += t1RoundScore-team1BalootBonus;
						t1RoundScore = 0+team1BalootBonus;						
					}
				}
				else
				{
					if(t2RoundScore < t1RoundScore)
					{
						t1RoundScore += t2RoundScore-team2BalootBonus; 
						t2RoundScore = 0+team2BalootBonus;
					}
					else
					{
						t2RoundScore += t1RoundScore-team1BalootBonus;					
						t1RoundScore = 0+team1BalootBonus;
					}
				}
			}
		}*/
		
		
		
		// For 32 Cards		
		if(team1CardsCount == 32)
		{
			if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
			{				
				if(bidLostTeam == 1)// Second Team Lost the bid		
				{
					// Team 1 bid won 
					t1RoundScore = 25+team1BalootBonus+team1Bonus;
					t2RoundScore = 0+team2BalootBonus;
				}
				else
				{
					// Team 1 bid lost 
					t1RoundScore = 25+team1BalootBonus+team1Bonus+team2Bonus;
					t2RoundScore = 0+team2BalootBonus;
				}
			}
			else // San Or Ashkal
			{
				/*Integer opencard = gameBean._roundBean._openCardId; 
				if( opencard == 1 || opencard == 9 || opencard == 17 || opencard == 25)
				{
					if(bidLostTeam == 0)
					{
						t1RoundScore = 88+team1Bonus;
						t2RoundScore = 0;
					}
				}
				else
				{
					t1RoundScore = 44+team1Bonus;
					t2RoundScore = 0;
				}*/
				
				if(bidLostTeam == 1)
				{
					// Team 1 bid win
					t1RoundScore = 44+team1Bonus;
					t2RoundScore = 0;
				}
				else
				{
					// Team 1 bid lost
					t1RoundScore = 44+team1Bonus+team2Bonus;
					t2RoundScore = 0;
				}
				
			}
			
			/* Show Medal */
			
			Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId, "thirtyTwoCardsWin", gameBean);
			Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId, "thirtyTwoCardsWin", gameBean);		
			
		}
		else if(team2CardsCount == 32)
		{
			if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
			{
				if(bidLostTeam == 0)
				{
					// Team 2 bid won
					t2RoundScore = 25+team2BalootBonus+team2Bonus;
					t1RoundScore = 0+team1BalootBonus;
				}
				else
				{
					// Team 2 bid lost
					t2RoundScore = 25+team2BalootBonus+team2Bonus+team1Bonus;
					t1RoundScore = 0+team1BalootBonus;
				}
			}
			else // San Or Ashkal
			{
				/*Integer opencard = gameBean._roundBean._openCardId;  
				if( opencard == 1 || opencard == 9 || opencard == 17 || opencard == 25)
				{
					if(bidLostTeam == 1)
					{
						t2RoundScore = 88+team2Bonus;
						t1RoundScore = 0;
					}
				}
				else
				{
					t2RoundScore = 44+team2Bonus;
					t1RoundScore = 0;
				}*/
				
				if(bidLostTeam == 0)
				{
					// Team 2 bid win
					t2RoundScore = 44+team2Bonus;
					t1RoundScore = 0;					
				}
				else
				{
					// Team 2 bid lost
					t2RoundScore = 44+team2Bonus+team1Bonus;
					t1RoundScore = 0;
				}				
			}			
			
			Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(1)._playerId, "thirtyTwoCardsWin", gameBean);
			Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(3)._playerId, "thirtyTwoCardsWin", gameBean);
			
		}
		
		/* Save the medal for double count*/
		if(gameBean._roundBean.isDouble)
		{
			
			if(t1RoundScore > t2RoundScore)
			{				
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId, "doubleWins", gameBean);
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId, "doubleWins", gameBean);
			}
			else
			{				
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(1)._playerId, "doubleWins", gameBean);
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(3)._playerId, "doubleWins", gameBean);	
			}
			
		}
		
		/* Save the medal for San or Hokom win */
		if(t1RoundScore > t2RoundScore)
		{
			
			if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
			{
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId, "hokomWins", gameBean);
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId, "hokomWins", gameBean);
			}
			else
			{
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId, "sanWins", gameBean);
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId, "sanWins", gameBean);
			}
		}
		else
		{
			if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
			{
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(1)._playerId, "hokomWins", gameBean);
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(3)._playerId, "hokomWins", gameBean);
			}
			else
			{
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(1)._playerId, "sanWins", gameBean);
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(3)._playerId, "sanWins", gameBean);
			}
		}
		
		
		
		
		gameBean._playersBeansList.get(0)._points.add(t1RoundScore);
		gameBean._playersBeansList.get(2)._points.add(t1RoundScore);
		gameBean._playersBeansList.get(0)._totalPoints += t1RoundScore;
		gameBean._playersBeansList.get(2)._totalPoints += t1RoundScore;
		
		gameBean._playersBeansList.get(1)._points.add(t2RoundScore);
		gameBean._playersBeansList.get(3)._points.add(t2RoundScore);
		gameBean._playersBeansList.get(1)._totalPoints += t2RoundScore;
		gameBean._playersBeansList.get(3)._totalPoints += t2RoundScore;
		
		Apps.showLog("**********Final Scores*************");
		
		Apps.showLog(" Final_t1RoundScore "+t1RoundScore);
		Apps.showLog(" Final_t2RoundScore "+t2RoundScore);	
	}
	
	
	// Calculate the teams scores and the score to total rounds score
	public void calculateTeamScores1(GameBean gameBean, String wonPlayer)
	{
		GameApps.calculateTeamPoints(gameBean, wonPlayer);
		
		Apps.showLog(" calculateTeamScores");
		
		Integer team1Score = 0;
		Integer team2Score = 0;
		Integer t1RoundScore = 0;
		Integer t2RoundScore = 0;
		Integer totalGamePoints=0;
		
		// Get team's Scores			
		team1Score = gameBean._roundBean._playerRoundBeanObjs.get(0)._totalScore + gameBean._roundBean._playerRoundBeanObjs.get(2)._totalScore;		
		team2Score = gameBean._roundBean._playerRoundBeanObjs.get(1)._totalScore + gameBean._roundBean._playerRoundBeanObjs.get(3)._totalScore;
		
		Apps.showLog("Before_Adding_Last_trick_team1score "+team1Score);
		Apps.showLog("Before_Adding_Last_trick_team2score "+team2Score);
		
		
		Integer doubleCnt = gameBean._roundBean.doubleCount+1;
		
		// Add last trick bonus score
		if(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId.equals(wonPlayer) || gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId.equals(wonPlayer))
		{
			team1Score += 10*doubleCnt;
		}
		else
		{
			team2Score += 10*doubleCnt;
		}
		
		
		Apps.showLog("Double_count  "+doubleCnt);
		Apps.showLog("After last card Added");
	    Apps.showLog(""+team1Score);
		Apps.showLog(""+team2Score);
		
		/** Round the Score*/
		
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{
			// Total game Points			
			
			//AppMethods.showLog((16*doubleCnt)+"=="+(gameBean._roundBean._gameBonus));
			//totalGamePoints += (16*doubleCnt)+(gameBean._roundBean._gameBonus);
			totalGamePoints += (16*doubleCnt);
			
			Apps.showLog("totalGamePoints hokom "+totalGamePoints);
			
			t1RoundScore += team1Score/10;			
			if(team1Score%10>=5)
				t1RoundScore += 1;	
			
			t2RoundScore += team2Score/10;			
			if(team2Score%10>=5)
				t2RoundScore += 1;
			
			Apps.showLog(""+t1RoundScore);
			Apps.showLog(""+t2RoundScore);
		}
		else // Sun
		{
			// Total game Points Need to implement(double)			
			totalGamePoints += (26*doubleCnt);
			
			//AppMethods.showLog("totalGamePoints sun "+totalGamePoints);
			t1RoundScore += (team1Score/10)*2;
			t2RoundScore += (team2Score/10)*2;
			
			if(team1Score%10 >= 6)
				t1RoundScore +=2;
			else if(team1Score%10 == 5)
				t1RoundScore +=1;
			
			if(team2Score%10 >= 6)
				t2RoundScore +=2;
			else if(team2Score%10 == 5)
				t2RoundScore +=1;
			
			Apps.showLog(""+t1RoundScore);
			Apps.showLog(""+t2RoundScore);
		}		
		
		// Calculating Bonus
		
		Integer team1CardsCount = 0;
		Integer team2CardsCount = 0;
		
		team1CardsCount += gameBean._roundBean._playerRoundBeanObjs.get(0)._wonCards.size();
		team1CardsCount += gameBean._roundBean._playerRoundBeanObjs.get(2)._wonCards.size();
		
		team2CardsCount += gameBean._roundBean._playerRoundBeanObjs.get(1)._wonCards.size();
		team2CardsCount += gameBean._roundBean._playerRoundBeanObjs.get(3)._wonCards.size();
		
		Integer team1Bonus = 0;
		Integer team2Bonus = 0;
		Integer team1BalootBonus = 0;
		Integer team2BalootBonus = 0;
		
		if(gameBean._roundBean._bonusObjs.size()>0)
		{
			
			int highBonusPerson = 0;
			
			for(int i=0;i<gameBean._roundBean._players.size();i++)
			{
				if(gameBean._roundBean._highestBonusPerson.equals(gameBean._players.get(i)))
				{
					highBonusPerson = i;
					break;
				}	
			}
			
			
			for(int i=0;i<gameBean._roundBean._bonusObjs.size();i++)
			{					
				String player = gameBean._roundBean._bonusObjs.get(i)._playerId;
				int no =0;
				for(int j=0;j<gameBean._players.size();j++)
				{
					if(gameBean._players.get(j).equals(player))
					{
						no = j;
						break;
					}
				}
				
				if( (no == 0 || no == 2) && (highBonusPerson == 0 || highBonusPerson == 2) && gameBean._roundBean._bonusObjs.get(i)._isShowedCards)
				{					
					team1Bonus += getBonusPoints(gameBean._roundBean._bonusObjs.get(i)._bonusType, gameBean._roundBean._gameType);					
				}
				else if( (no == 1 || no == 3) && (highBonusPerson == 1 || highBonusPerson == 3)&& gameBean._roundBean._bonusObjs.get(i)._isShowedCards)
				{
					team2Bonus += getBonusPoints(gameBean._roundBean._bonusObjs.get(i)._bonusType, gameBean._roundBean._gameType);
				}						
			}
			
			Apps.showLog("Bonuse");
			
			Apps.showLog("Team1"+team1Bonus);
			Apps.showLog("Team2"+team2Bonus);
		}
		
		
		// Adding baloot bonus
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{
			for(int i=0;i<gameBean._roundBean._bonusObjs.size();i++)
			{
				if(gameBean._roundBean._bonusObjs.get(i)._bonusType.equals("baloot"))
				{
					int no =0;
					for(int j=0;j<gameBean._players.size();j++)
					{
						if(gameBean._roundBean._bonusObjs.get(i)._playerId.equals(gameBean._players.get(j)))
						{
							no = j;
							break;
						}
					}
					
					if(no == 0 || no == 2)
					{
						team1BalootBonus += 2;
					}
					else
					{
						team2BalootBonus += 2;
					}					
					break;
				}
			}
		}
		
		Apps.showLog("Baloot Bonus");
		Apps.showLog(""+team1BalootBonus);
		Apps.showLog(""+team2BalootBonus);
		
		// Baloot bonus shouls not get doubled
		if(gameBean._roundBean.isDouble && team1CardsCount != 32 && team2CardsCount != 32)
		{
			team1Bonus = (team1Bonus)*(gameBean._roundBean.doubleCount+1);
			team2Bonus = (team2Bonus)*(gameBean._roundBean.doubleCount+1);			
		}
		
		
		
		
		Apps.showLog("Before Bonus");
		Apps.showLog(""+t1RoundScore);
		Apps.showLog(""+t2RoundScore);
		
		//Adding Bonus to round score
		
		t1RoundScore = t1RoundScore + team1Bonus + team1BalootBonus;
		t2RoundScore = t2RoundScore + team2Bonus + team2BalootBonus;
		
		Apps.showLog("After Bonus");
		Apps.showLog(""+t1RoundScore);
		Apps.showLog(""+t2RoundScore);
		
		
		/*// Adding bonus to the teams scores
		
		team1Score += team1Bonus*10 + team1BalootBonus*10;
		team2Score += team2Bonus*10 + team2BalootBonus*10;*/
		
		
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{	
			// Cutting the game extra points  
			
			totalGamePoints += team1Bonus +team2Bonus +team1BalootBonus +team2BalootBonus;
			
			if(t1RoundScore+t2RoundScore > totalGamePoints)
			{
				if(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId.equals(gameBean._roundBean._bidWonPerson) || gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId.equals(gameBean._roundBean._bidWonPerson))
				{
					t1RoundScore -= 1;
				}
				else
				{
					t2RoundScore -= 1;
				}
			}
		}
		
		// Check who win the bid and getting more points than their opponents will get the score
		// else they may loose their won points to the opponents
		
		String bidWonTeamPlayer;
		//bidWonTeamPlayer = gameBean._roundBean._bidWonPerson;	
		
		
		bidWonTeamPlayer = gameBean._roundBean._bidSelectedPerson;
		
		Integer bidLostTeam = 0;		
		
		if(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId.equals(bidWonTeamPlayer) || gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId.equals(bidWonTeamPlayer))
		{
			bidLostTeam = 1;
			Apps.showLog("Team one won");
			
			if(gameBean._roundBean._bonusObjs.size() > 0)
			{
				if(t1RoundScore < t2RoundScore)
				{					
					t2RoundScore += t1RoundScore-team1BalootBonus;
					t1RoundScore = 0+team1BalootBonus;
				}
				else
				{
					//t2RoundScore += team2BalootBonus;
					//t1RoundScore += team1BalootBonus;
				}
			}
			else
			{			
				if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
				{
					if(t1RoundScore < totalGamePoints/2)
					{						
						t2RoundScore = totalGamePoints;
						t1RoundScore = 0;
					}
				}
				else
				{
					if(t1RoundScore < t2RoundScore)
					{						
						t2RoundScore = totalGamePoints;
						t1RoundScore = 0;
					}
				}
			}	
			
			
			
			// Double is enable
			if(gameBean._roundBean.isDouble)
			{
				Apps.showLog("Double is enabled Team 1 wons bid");
				if(gameBean._roundBean.doubleCount == 2)
				{			
					if(t1RoundScore < t2RoundScore)
					{						
						t2RoundScore += t1RoundScore-team1BalootBonus;
						t1RoundScore = 0+team1BalootBonus;
					}
					else
					{						
						t1RoundScore += t2RoundScore-team2BalootBonus;
						t2RoundScore = 0+team2BalootBonus;
						
					}
				}
				else
				{
					if(t1RoundScore < t2RoundScore)
					{						
						t2RoundScore += t1RoundScore-team1BalootBonus;
						t1RoundScore = 0+team1BalootBonus;
					}
					else
					{						
						t1RoundScore += t2RoundScore-team2BalootBonus;
						t2RoundScore = 0+team2BalootBonus;
					}
				}
			}			
		}
		else
		{
			bidLostTeam = 0;
			Apps.showLog("Team two won");
			if(gameBean._roundBean._bonusObjs.size()>0)
			{
				if(t2RoundScore < t1RoundScore)
				{					
					t1RoundScore += t2RoundScore-team2BalootBonus;
					t2RoundScore = 0+team2BalootBonus;					
				}
				else
				{
					//t2RoundScore += team2BalootBonus;
					//t1RoundScore += team1BalootBonus;
				}
			}
			else
			{
				if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
				{
					if(t2RoundScore < totalGamePoints/2)
					{						
						t1RoundScore = totalGamePoints;
						t2RoundScore = 0;
					}
				}
				else
				{
					if(t2RoundScore < t1RoundScore)
					{						
						t1RoundScore = totalGamePoints;
						t2RoundScore = 0;
					}
				}
			}
			
			// Double is enable
			if(gameBean._roundBean.isDouble)
			{
				
				Apps.showLog("Double is enabled Team 2 wons bid");
				
				if(gameBean._roundBean.doubleCount == 2)
				{			
					if(t2RoundScore < t1RoundScore)
					{						
						t1RoundScore += t2RoundScore-team2BalootBonus;
						t2RoundScore = 0+team2BalootBonus;
					}
					else
					{
						t2RoundScore += t1RoundScore-team1BalootBonus;
						t1RoundScore = 0+team1BalootBonus;						
					}
				}
				else
				{
					if(t2RoundScore < t1RoundScore)
					{
						t1RoundScore += t2RoundScore-team2BalootBonus; 
						t2RoundScore = 0+team2BalootBonus;
					}
					else
					{
						t2RoundScore += t1RoundScore-team1BalootBonus;					
						t1RoundScore = 0+team1BalootBonus;
					}
				}
			}
		}
		
		
		
		// For 32 Cards
		
		
		if(team1CardsCount == 32)
		{
			if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
			{				
				if(bidLostTeam == 1)// Second Team Lost the bid		
				{
					// Team 1 bid won 
					t1RoundScore = 25+team1BalootBonus+team1Bonus;
					t2RoundScore = 0+team2BalootBonus;
				}
				else
				{
					// Team 1 bid lost 
					t1RoundScore = 25+team1BalootBonus+team1Bonus+team2BalootBonus+team2Bonus;
					t2RoundScore = 0;
				}
			}
			else // San Or Ashkal
			{
				/*Integer opencard = gameBean._roundBean._openCardId; 
				if( opencard == 1 || opencard == 9 || opencard == 17 || opencard == 25)
				{
					if(bidLostTeam == 0)
					{
						t1RoundScore = 88+team1Bonus;
						t2RoundScore = 0;
					}
				}
				else
				{
					t1RoundScore = 44+team1Bonus;
					t2RoundScore = 0;
				}*/
				
				if(bidLostTeam == 1)
				{
					// Team 1 bid win
					t1RoundScore = 44+team1Bonus;
					t2RoundScore = 0;
				}
				else
				{
					// Team 1 bid lost
					t1RoundScore = 44+team1Bonus+team2Bonus;
					t2RoundScore = 0;
				}
				
			}
			
			/* Show Medal */
			
			Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId, "thirtyTwoCardsWin", gameBean);
			Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId, "thirtyTwoCardsWin", gameBean);		
			
		}
		else if(team2CardsCount == 32)
		{
			if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
			{
				if(bidLostTeam == 0)
				{
					// Team 2 bid won
					t2RoundScore = 25+team2BalootBonus+team2Bonus;
					t1RoundScore = 0+team1BalootBonus;
				}
				else
				{
					// Team 2 bid lost
					t2RoundScore = 25+team2BalootBonus+team2Bonus+team1BalootBonus+team1Bonus;
					t1RoundScore = 0;
				}
			}
			else // San Or Ashkal
			{
				/*Integer opencard = gameBean._roundBean._openCardId;  
				if( opencard == 1 || opencard == 9 || opencard == 17 || opencard == 25)
				{
					if(bidLostTeam == 1)
					{
						t2RoundScore = 88+team2Bonus;
						t1RoundScore = 0;
					}
				}
				else
				{
					t2RoundScore = 44+team2Bonus;
					t1RoundScore = 0;
				}*/
				
				if(bidLostTeam == 0)
				{
					// Team 2 bid win
					t2RoundScore = 44+team2Bonus;
					t1RoundScore = 0;					
				}
				else
				{
					// Team 2 bid lost
					t2RoundScore = 44+team2Bonus+team1Bonus;
					t1RoundScore = 0;
				}				
			}			
			
			Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(1)._playerId, "thirtyTwoCardsWin", gameBean);
			Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(3)._playerId, "thirtyTwoCardsWin", gameBean);
			
		}
		
		/* Save the medal for double count*/
		if(gameBean._roundBean.isDouble)
		{
			
			if(t1RoundScore > t2RoundScore)
			{				
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId, "doubleWins", gameBean);
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId, "doubleWins", gameBean);
			}
			else
			{				
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(1)._playerId, "doubleWins", gameBean);
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(3)._playerId, "doubleWins", gameBean);	
			}
			
		}
		
		/* Save the medal for San or Hokom win */
		if(t1RoundScore > t2RoundScore)
		{
			
			if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
			{
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId, "hokomWins", gameBean);
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId, "hokomWins", gameBean);
			}
			else
			{
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId, "sanWins", gameBean);
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId, "sanWins", gameBean);
			}
		}
		else
		{
			if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
			{
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(1)._playerId, "hokomWins", gameBean);
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(3)._playerId, "hokomWins", gameBean);
			}
			else
			{
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(1)._playerId, "sanWins", gameBean);
				Commands.appInstance.medalsBsn.saveMedal(gameBean._roundBean._playerRoundBeanObjs.get(3)._playerId, "sanWins", gameBean);
			}
		}
		
		
		
		
		gameBean._playersBeansList.get(0)._points.add(t1RoundScore);
		gameBean._playersBeansList.get(2)._points.add(t1RoundScore);
		gameBean._playersBeansList.get(0)._totalPoints += t1RoundScore;
		gameBean._playersBeansList.get(2)._totalPoints += t1RoundScore;
		
		gameBean._playersBeansList.get(1)._points.add(t2RoundScore);
		gameBean._playersBeansList.get(3)._points.add(t2RoundScore);
		gameBean._playersBeansList.get(1)._totalPoints += t2RoundScore;
		gameBean._playersBeansList.get(3)._totalPoints += t2RoundScore;
		
		Apps.showLog("***********************");
		Apps.showLog("Final Scores");
		Apps.showLog(""+t1RoundScore);
		Apps.showLog(""+t2RoundScore);
		Apps.showLog("***********************");
	}
	
	
	private Integer getBonusPoints(String bonusType, String gameType){
		
		Integer points = 0;
		
		if(gameType.equals(Commands.SAN) || gameType.equals(Commands.ASHKAL))
		{
			if(bonusType.equals("sira"))
				points = 4;
			else if(bonusType.equals("50"))
				points = 10;
			else if(bonusType.equals("100"))
				points = 20;
			else if(bonusType.equals("400"))
				points = 40;
		}
		else
		{
			if(bonusType.equals("sira"))
				points = 2;
			else if(bonusType.equals("50"))
				points = 5;
			else if(bonusType.equals("100"))
				points = 10;			
		}		
		//AppMethods.showLog("Bonus Points:"+points);
		
		return points;
	}
	
	
	
	
	// Round Completed Bonus Missed Add Game Points to the teams
	public void addGamePoints(String player, GameBean gameBean){
		
		Integer totalGamePoints = 0;
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{
			Integer doubleCnt = gameBean._roundBean.doubleCount+1;
			totalGamePoints += (16*doubleCnt);
		}
		else
		{
			Integer doubleCnt = gameBean._roundBean.doubleCount+1;
			totalGamePoints += (26*doubleCnt);
		}
		
		if(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId.equals(player)
				|| gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId.equals(player))
		{
			// Team 2 Wins
			gameBean._playersBeansList.get(1)._points.add(totalGamePoints);
			gameBean._playersBeansList.get(3)._points.add(totalGamePoints);
			gameBean._playersBeansList.get(1)._totalPoints += totalGamePoints;
			gameBean._playersBeansList.get(3)._totalPoints += totalGamePoints;
			
			gameBean._playersBeansList.get(0)._points.add(0);
			gameBean._playersBeansList.get(2)._points.add(0);
			gameBean._playersBeansList.get(0)._totalPoints += 0;
			gameBean._playersBeansList.get(2)._totalPoints += 0;
			
		}
		else
		{
			// Team 1 Wins
			gameBean._playersBeansList.get(0)._points.add(totalGamePoints);
			gameBean._playersBeansList.get(2)._points.add(totalGamePoints);
			gameBean._playersBeansList.get(0)._totalPoints += totalGamePoints;
			gameBean._playersBeansList.get(2)._totalPoints += totalGamePoints;
			
			gameBean._playersBeansList.get(1)._points.add(0);
			gameBean._playersBeansList.get(3)._points.add(0);
			gameBean._playersBeansList.get(1)._totalPoints += 0;
			gameBean._playersBeansList.get(3)._totalPoints += 0;
			
		}
		
		calculateGamePoints(gameBean, player);
	}
	
	/**
	 * This method is used to calculate the game points and bonus points by the requirement of Eyad(24-03-2015).
	 * @param gameBean
	 * @param lostPlayer
	 */
	private void calculateGamePoints(GameBean gameBean, String lostPlayer)
	{
		ArrayList<String> lostPlayers = new ArrayList<String>();
		lostPlayers.add(lostPlayer);
		lostPlayers.add(gameBean.getPartner(lostPlayer));
		
		int gamePoints = 0;
		
		/*// Double the score and Add the bonus
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{
			gamePoints = (162)*(gameBean._roundBean.doubleCount+1);
		}
		else
		{
			gamePoints = (130)*(gameBean._roundBean.doubleCount+1);	
		}*/
		
		// Eyad requirement : dont double the detailed points
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{
			gamePoints = 162;
		}
		else
		{
			gamePoints = 130;	
		}
		
		
		// storing detailed game points points.		
		if(GameApps.getTeamOnePlayers(gameBean).contains(lostPlayer))
		{			
			gameBean._roundBean.teamTwoGamePoints = gamePoints;			
		}
		else
		{
			gameBean._roundBean.teamOneGamePoints = gamePoints;
		}
		
		
		// adding bonus points.
		
		int team1Bonus = 0;
		int team2Bonus = 0;
		int team1BalootBonus = 0;
		int team2BalootBonus = 0;
		
		// Get the Bonus
		ArrayList<Integer> teamBonus = GameApps.getTeamBonusPoints(gameBean);
		ArrayList<Integer> balootBonus = GameApps.getBalootBonusPoints(gameBean);
		
		team1Bonus = teamBonus.get(0);
		team2Bonus = teamBonus.get(1);
		team1BalootBonus = balootBonus.get(0);
		team2BalootBonus = balootBonus.get(1);
		
		// As per Eyad requirement dont double the detailed bonus points.	
		Integer doubleCnt = gameBean._roundBean.doubleCount+1;
		if(gameBean._roundBean.isDouble)
		{
			team1Bonus = team1Bonus/doubleCnt;
			team2Bonus = team2Bonus/doubleCnt;
		}
		
		if(GameApps.getTeamOnePlayers(gameBean).contains(gameBean._playersBeansList.get(1)._playerId))
		{
			gameBean._roundBean.teamOneBonusPoints = team2BalootBonus + team2Bonus;
			gameBean._roundBean.teamTwoBounsPoints = team1BalootBonus + team1Bonus;
		}
		else
		{
			gameBean._roundBean.teamOneBonusPoints = team1BalootBonus + team1Bonus;
			gameBean._roundBean.teamTwoBounsPoints = team2BalootBonus + team2Bonus;
		}
	}
	
	// returns the current Round Win Player
	public String getPlayer(GameBean gameBean, int card)
	{
		String player = null;
		
		for(PlayerRoundBean prb : gameBean._roundBean._playerRoundBeanObjs)
		{
			if(prb._currentCard == card)
			{
				player = prb._playerId;
				prb._currentCard = -1;
				prb.isDeclaredHighest = false;
				break;
			}
		}
		
		return player;
	}
	
	
	// returns the score of the current round cards
	public int calculateScore(GameBean gameBean)
	{
		int score = 0;
		
		opencards = gameBean._roundBean.openCardsList;
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{
			//check the card suit type
			for(int i=0;i<opencards.size();i++)
			{
				if(checkSuitType(opencards.get(i)).equals(gameBean._roundBean._trumpSuitType))
				{
					// Hokom type
					score = score + getScoreValue(Commands.HOKOM, opencards.get(i));
					//System.out.print(getScoreValue(Commands.HOKOM, opencards.get(i))+",");
					
				}
				else
				{
					// San type or Ashkal
					score = score + getScoreValue(Commands.SAN, opencards.get(i));
					//System.out.print(getScoreValue(Commands.SAN, opencards.get(i))+",");
				}
			}
		}
		else
		{
			for(int i=0;i<opencards.size();i++)
			{
				score = score + getScoreValue(Commands.SAN, opencards.get(i));
				//System.out.print(getScoreValue(Commands.SAN, opencards.get(i))+",");
			}
		}	
		
		//AppMethods.showLog("Score "+ score);
		
		// Double is enable the double the score
		if(gameBean._roundBean.isDouble)
		{
			 int scoreint = score*(gameBean._roundBean.doubleCount+1);
			 if(scoreint != 0)
				score = scoreint;
			 
		}		
		
		return score;
	}
	
	private int getScoreValue(String type, int no)
	{
		int value = 0;
		int tmpNo = -1;
		
		if(no > 8)
		{
			tmpNo = no%8;
			
			if(tmpNo == 0)
				tmpNo = 8;
		}
		else
			tmpNo = no;
		
		if(type.equals(Commands.HOKOM))
		{
			switch (tmpNo)
			{
				case 1: value = 11;	break;
				case 2: value = 4;	break;
				case 3: value = 3;	break;
				case 4: value = 20;	break;
				case 5: value = 10;	break;
				case 6: value = 14;	break;
				case 7: value = 0;	break;
				case 8: value = 0;	break;				
			}
		}
		else
		{
			switch (tmpNo)
			{
				case 1: value = 11;	break;
				case 2: value = 4;	break;
				case 3: value = 3;	break;
				case 4: value = 2;	break;
				case 5: value = 10;	break;
				case 6: value = 0;	break;
				case 7: value = 0;	break;
				case 8: value = 0;	break;
			}
		}	
		
		return value;
	}
	
	
	private String checkSuitType(Integer no){
		String type = "";
		
		if(no>=1 && no<=8)
			type = "clubs";
		else if(no>=9 && no<=16)
			type = "diamonds";
		else if(no>=17 && no<=24)
			type = "hearts";
		else if(no>=24 && no<=32)
			type = "spades";
		return type;
	}
	
	// Return the current round win card
	public int getRoundWinCard(GameBean gameBean, String currentSuitType )
	{
		opencards = gameBean._roundBean.openCardsList;
		
		int arr[] = {};
		int highestCard = 0;
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{
			// Check for hokom card
			//if hokom suit found then calculate for hokom rule
		    //else calculate as per sun rule
			
			arr = checkCards(gameBean._roundBean._trumpSuitType);
			//System.out.print("arr "+arr.length);
			if(arr.length >1 )
			{
				highestCard =  highestInArray(arr, hokomHighestPos);
			}
			else if(arr.length == 1)
			{				
				highestCard = arr[0];
			}
			else if(arr.length == 0)
			{
				arr = checkCards(currentSuitType);
				if(arr.length>1)
					highestCard = highestInArray(arr, sunHighestPos);
				else
					highestCard = arr[0];
			}
		}
		else
		{
			// Calculate with out hokom suit or sun rule
			arr = checkCards(currentSuitType);
			if(arr.length>1)
				highestCard = highestInArray(arr, sunHighestPos);
			else
				highestCard =  arr[0];
		}
		
		return highestCard;
	}
	
	private int[] checkCards(String suitType)
	{
		//AppMethods.showLog("Check Cards"+suitType);
		
		int arr[] = {};
		
		if(suitType.equals("clubs"))
		{
			arr = checkAndSendCards(clubsArray);
		}
		else if(suitType.equals("diamonds"))
		{
			arr = checkAndSendCards(diamondsArray);
		}
		else if(suitType.equals("hearts"))
		{
			arr = checkAndSendCards(heartsArray);
		}
		else
		{
			arr = checkAndSendCards(spadesArray);
		}
		return arr;		
	}
	// Returns the list of same suit cards in the array	
	private int[] checkAndSendCards(int[] suitArr)
	{
		//AppMethods.showLog("Check and Send Cards"+ suitArr.length);
		int arr[] = {0,0,0,0};
		int count=0;
		for(int i=0;i<suitArr.length;i++)
		{
			for(int j=0;j<opencards.size();j++){
				//AppMethods.showLog(opencards.get(j)+"="+suitArr[i]);
				if(opencards.get(j) == suitArr[i])
				{
					arr[count] =  suitArr[i];
					count++;
					break;
				}
			}			
		}	
			
		int arrvalues[] = new int[count];
		for(int i=0;i<count;i++)
		{			
			arrvalues[i] = arr[i];
		}
		
		
		//AppMethods.showLog("Check arr"+ arrvalues.length);
		return arrvalues;
	}
	
	// Gives the highest card in the array
	private int highestInArray(int[] arr, int [] highestPosArr){
		int high = 0;
		for(int i=0;i<highestPosArr.length;i++)
		{
			for(int j=0;j<arr.length;j++)
			{
				int card = -1;
				if(arr[j]>8)
				{
					card = arr[j]%8;
					if(card == 0)
						card = 8;
				}
				else
					card = arr[j];
				
				
				if(highestPosArr[i] == card)
				{
					high = arr[j];
					break;
				}				
			}
			if(high!=0)
				break;
		}			
		return high;
	}
	
	
	// Add Bonus to the user
	public void addGameBonus(GameBean gameBean){		
		String partner="";
		for(int i=0;i<gameBean._roundBean._players.size();i++)
		{
			if(gameBean._roundBean._highestBonusPerson.equals(gameBean._roundBean._players.get(i)))
			{
				if(i==0)
					partner = gameBean._roundBean._players.get(2);
				else if(i==1)
					partner = gameBean._roundBean._players.get(3);
				else if(i==2)
					partner = gameBean._roundBean._players.get(0);
				else if(i==3)
					partner = gameBean._roundBean._players.get(1);
			}	
		}
		
		
		for(int i=0;i<gameBean._roundBean._bonusObjs.size();i++)
		{
			//AppMethods.showLog("Bonus Person "+ gameBean._roundBean._bonusObjs.get(i)._playerId+"  Bonus Shown "+ gameBean._roundBean._bonusObjs.get(i)._isShowedCards);
			if(gameBean._roundBean._bonusObjs.get(i)._playerId.equals(partner) || gameBean._roundBean._bonusObjs.get(i)._playerId.equals(gameBean._roundBean._highestBonusPerson) )
			{
				addBonus(gameBean._roundBean._bonusObjs.get(i)._bonusType, gameBean._roundBean._bonusObjs.get(i)._playerId, gameBean);
			}
		}		
	}// End addGameBonus
	
	
	private void addBonus(String bonusType,String player, GameBean gameBean)
	{
		
		for(int playerIndex=0; playerIndex<gameBean._roundBean._playerRoundBeanObjs.size(); playerIndex++)
		{
			if(gameBean._playersBeansList.get(playerIndex)._playerId.equals(player))
			{
				Integer var1 = 0;
				if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
				{		
					
					// add the player total score and update the game bonusPoints
					if(bonusType.equals("sira"))			
					{									
						if(playerIndex == 0 || playerIndex == 2)
						{
							//var1 = gameBean._playersBeansList.get(0)._points.get(gameBean._playersBeansList.get(0)._points.size()-1);							
							var1 += 2;
							
						}
						else
						{
							//var1 = gameBean._playersBeansList.get(1)._points.get(gameBean._playersBeansList.get(1)._points.size()-1);							
							var1 += 2;							
						}		
						
						//gameBean._roundBean._playerRoundBeanObjs.get(playerIndex)._totalScore += 20;
						//gameBean._roundBean._gameBonus += 2;				
					}
					else if(bonusType.equals("50"))
					{
						if(playerIndex == 0 || playerIndex == 2)
						{
							//var1 = gameBean._playersBeansList.get(0)._points.get(gameBean._playersBeansList.get(0)._points.size()-1);							
							var1 += 5;							
						}
						else
						{
							//var1 = gameBean._playersBeansList.get(1)._points.get(gameBean._playersBeansList.get(1)._points.size()-1);							
							var1 += 5;							
						}
						//gameBean._roundBean._playerRoundBeanObjs.get(playerIndex)._totalScore += 50;
						//gameBean._roundBean._gameBonus += 5;
					}
					else if(bonusType.equals("100"))
					{
						if(playerIndex == 0 || playerIndex == 2)
						{
							//var1 = gameBean._playersBeansList.get(0)._points.get(gameBean._playersBeansList.get(0)._points.size()-1);							
							var1 += 10;							
						}
						else
						{
							//var1 = gameBean._playersBeansList.get(1)._points.get(gameBean._playersBeansList.get(1)._points.size()-1);							
							var1 += 10;							
						}
						//gameBean._roundBean._playerRoundBeanObjs.get(playerIndex)._totalScore += 100;
						//gameBean._roundBean._gameBonus += 10;
					}			
					else if(bonusType.equals("Baloot"))
					{
						if(playerIndex == 0 || playerIndex == 2)
						{
							//var1 = gameBean._playersBeansList.get(0)._points.get(gameBean._playersBeansList.get(0)._points.size()-1);							
							var1 += 2;							
						}
						else
						{
							//var1 = gameBean._playersBeansList.get(1)._points.get(gameBean._playersBeansList.get(1)._points.size()-1);							
							var1 += 2;							
						}
						//gameBean._roundBean._playerRoundBeanObjs.get(playerIndex)._totalScore += 20;
						//gameBean._roundBean._gameBonus += 2;
					}
					Integer points = 0;
					if(playerIndex == 0 || playerIndex == 2)
					{			
						points = gameBean._playersBeansList.get(0)._points.get(gameBean._playersBeansList.get(0)._points.size()-1);
						Apps.showLog("Before "+points);
						points = points + var1;
						Apps.showLog("After "+points);
						gameBean._playersBeansList.get(0)._points.remove(gameBean._playersBeansList.get(0)._points.size()-1);
						gameBean._playersBeansList.get(2)._points.remove(gameBean._playersBeansList.get(2)._points.size()-1);
						gameBean._playersBeansList.get(0)._points.add(points);
						gameBean._playersBeansList.get(2)._points.add(points);
						gameBean._playersBeansList.get(0)._totalPoints += var1;
						gameBean._playersBeansList.get(2)._totalPoints += var1;
						
					}
					else
					{
						points = gameBean._playersBeansList.get(1)._points.get(gameBean._playersBeansList.get(1)._points.size()-1);
						Apps.showLog("Before "+points);
						points = points + var1;
						Apps.showLog("After "+points);
						gameBean._playersBeansList.get(1)._points.remove(gameBean._playersBeansList.get(1)._points.size()-1);
						gameBean._playersBeansList.get(3)._points.remove(gameBean._playersBeansList.get(3)._points.size()-1);
						gameBean._playersBeansList.get(1)._points.add(points);
						gameBean._playersBeansList.get(3)._points.add(points);
						gameBean._playersBeansList.get(1)._totalPoints += var1;
						gameBean._playersBeansList.get(3)._totalPoints += var1;
					}
					
					
					//AppMethods.showLog("Total Score After"+ gameBean._roundBean._playerRoundBeanObjs.get(playerIndex)._totalScore);
				}
				else
				{					
					if(bonusType.equals("sira"))			
					{
						if(playerIndex == 0 || playerIndex == 2)
						{
							//var1 = gameBean._playersBeansList.get(0)._points.get(gameBean._playersBeansList.get(0)._points.size()-1);							
							var1 += 4;							
						}
						else
						{
							//var1 = gameBean._playersBeansList.get(1)._points.get(gameBean._playersBeansList.get(1)._points.size()-1);							
							var1 += 4;							
						}
						//gameBean._roundBean._playerRoundBeanObjs.get(playerIndex)._totalScore += 40;
						//gameBean._roundBean._gameBonus += 4;
					}
					else if(bonusType.equals("50"))
					{
						if(playerIndex == 0 || playerIndex == 2)
						{
							//var1 = gameBean._playersBeansList.get(0)._points.get(gameBean._playersBeansList.get(0)._points.size()-1);							
							var1 += 10;							
						}
						else
						{
							//var1 = gameBean._playersBeansList.get(1)._points.get(gameBean._playersBeansList.get(1)._points.size()-1);							
							var1 += 10;							
						}
						//gameBean._roundBean._playerRoundBeanObjs.get(playerIndex)._totalScore += 100;
						//gameBean._roundBean._gameBonus += 10;
					}
					else if(bonusType.equals("100"))
					{
						if(playerIndex == 0 || playerIndex == 2)
						{
							//var1 = gameBean._playersBeansList.get(0)._points.get(gameBean._playersBeansList.get(0)._points.size()-1);							
							var1 += 20;							
						}
						else
						{
							//var1 = gameBean._playersBeansList.get(1)._points.get(gameBean._playersBeansList.get(1)._points.size()-1);							
							var1 += 20;							
						}
						//gameBean._roundBean._playerRoundBeanObjs.get(playerIndex)._totalScore += 200;
						//gameBean._roundBean._gameBonus += 20;
					}
					else if(bonusType.equals("400"))
					{
						if(playerIndex == 0 || playerIndex == 2)
						{
							//var1 = gameBean._playersBeansList.get(0)._points.get(gameBean._playersBeansList.get(0)._points.size()-1);							
							var1 += 40;							
						}
						else
						{
							//var1 = gameBean._playersBeansList.get(1)._points.get(gameBean._playersBeansList.get(1)._points.size()-1);							
							var1 += 40;							
						}
						//gameBean._roundBean._playerRoundBeanObjs.get(playerIndex)._totalScore += 400;
						//gameBean._roundBean._gameBonus += 40;
					}
					
					
					//AppMethods.showLog("Total Score After"+ gameBean._roundBean._playerRoundBeanObjs.get(playerIndex)._totalScore);
				}
				
				Apps.showLog("BonusPoints "+ var1);
				Integer points = 0;
				if(playerIndex == 0 || playerIndex == 2)
				{						
					points = gameBean._playersBeansList.get(0)._points.get(gameBean._playersBeansList.get(0)._points.size()-1);
					Apps.showLog("Before "+points);
					points = points + var1;
					Apps.showLog("After "+points);
					
					gameBean._playersBeansList.get(0)._points.remove(gameBean._playersBeansList.get(0)._points.size()-1);
					gameBean._playersBeansList.get(2)._points.remove(gameBean._playersBeansList.get(2)._points.size()-1);
					gameBean._playersBeansList.get(0)._points.add(points);
					gameBean._playersBeansList.get(2)._points.add(points);
					gameBean._playersBeansList.get(0)._totalPoints += var1;
					gameBean._playersBeansList.get(2)._totalPoints += var1;					
					
				}
				else
				{
					points = gameBean._playersBeansList.get(1)._points.get(gameBean._playersBeansList.get(1)._points.size()-1);
					Apps.showLog("Before "+points);
					points = points + var1;
					Apps.showLog("After "+points);
					
					gameBean._playersBeansList.get(1)._points.remove(gameBean._playersBeansList.get(1)._points.size()-1);
					gameBean._playersBeansList.get(3)._points.remove(gameBean._playersBeansList.get(3)._points.size()-1);
					gameBean._playersBeansList.get(1)._points.add(points);
					gameBean._playersBeansList.get(3)._points.add(points);
					gameBean._playersBeansList.get(1)._totalPoints += var1;
					gameBean._playersBeansList.get(3)._totalPoints += var1;
					
				}				
			}	
			
		}
		Apps.showLog("After Bonus");
		Apps.showLog("Team 1 Score :"+gameBean._playersBeansList.get(0)._totalPoints);
		Apps.showLog("Team 2 Score :"+gameBean._playersBeansList.get(1)._totalPoints);
	}// End addBonus
	
	
	public boolean isHighest(Integer card, GameBean gameBean, String suit)
	{
		ArrayList<Integer> cards = new ArrayList<Integer>();
		int n1=0, n2=0;
		
		if(suit.equals("clubs"))
		{
			n1=1;n2=8;
		}
		else if(suit.equals("diamonds"))
		{
			n1=9;n2=16;
		}
		else if(suit.equals("hearts"))
		{
			n1=17;n2=24;
		}
		else if(suit.equals("spades"))
		{
			n1=25;n2=32;
		}
		
		for(int i=0;i<gameBean._roundBean._cardsInHand.size();i++)
		{
			Integer cardid = gameBean._roundBean._cardsInHand.get(i);
			if(cardid>=n1 && cardid<=n2)
			{
				cards.add(cardid);
			}
		}
		
		cards = sortSunorder(cards);
		
		if(card == cards.get(0))
		{
			return true;
		}
		
		return false;
	}
	
	public int getHighestCard(Integer card, GameBean gameBean, String suit)
	{
		ArrayList<Integer> cards = new ArrayList<Integer>();
		int n1=0, n2=0;
		
		if(suit.equals("clubs"))
		{
			n1=1;n2=8;
		}
		else if(suit.equals("diamonds"))
		{
			n1=9;n2=16;
		}
		else if(suit.equals("hearts"))
		{
			n1=17;n2=24;
		}
		else if(suit.equals("spades"))
		{
			n1=25;n2=32;
		}
		
		for(int i=0;i<gameBean._roundBean._cardsInHand.size();i++)
		{
			Integer cardid = gameBean._roundBean._cardsInHand.get(i);
			if(cardid>=n1 && cardid<=n2)
			{
				cards.add(cardid);
			}
		}
		
		cards = sortSunorder(cards);
		
		return cards.get(0);
		
	}
	private ArrayList<Integer> sortSunorder(ArrayList<Integer> alist)
	{
		ArrayList<Integer> arr = new ArrayList<Integer>();
		
		int sunHighestPos[] = {1,5,2,3,4,6,7,0};
		
		for(int j=0;j<sunHighestPos.length;j++)
		{				
			for(int i=0;i<alist.size();i++)
			{				
				if(alist.get(i)%8 == sunHighestPos[j])
				{						
					arr.add(alist.get(i));					
				}
			}			
		}		
		return arr;
	}
	
	
}// End Class
