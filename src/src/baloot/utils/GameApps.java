package src.baloot.utils;

import java.util.ArrayList;












import com.smartfoxserver.v2.entities.data.ISFSObject;

import src.baloot.beans.BonusBean;
import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerBean;
import src.baloot.beans.PlayerRoundBean;

public class GameApps {
	
	public static ISFSObject getGameObject(GameBean gameBean, ISFSObject sfso, String player)
	{
		sfso.putUtfString("tableId", gameBean.tableId); 
		sfso.putUtfString("gameId",gameBean.gameId);
		sfso.putUtfStringArray("players", gameBean._players);
		sfso.putUtfStringArray("initialPlayers", gameBean._players);
		sfso.putUtfString("host", gameBean.host);
		sfso.putBool("isSinglePlayer", gameBean.isSinglePlayer);
		sfso.putBool("isRematchPhase", gameBean.isRematchPhase);
		sfso.putBool("isQuickStartPhase", gameBean.isQuickStartPhase);
		sfso.putBool("isGameStarted", gameBean.isGameStarted);
		sfso.putBool("isPrivateTable", gameBean.isPrivateTable());
		sfso.putInt("roundCount", gameBean.getRoundCount());
		sfso.putInt("ticket", gameBean.getTicket());
		sfso.putLong("remainingQuickStartTime", gameBean.getRemainingQuickStartTime());
		sfso.putIntArray("positions", gameBean.getPositions());
		sfso.putUtfString("gameStartTime", gameBean.gameStartedTimeForClient);
		
		if(gameBean.isGameStarted)
		{
			//if(!gameBean.isRematchPhase)
			//{
				sfso.putSFSArray("playerCards",gameBean._roundBean.getPlayersCards());
			//}
			
			sfso.putUtfString("gameType", gameBean._roundBean._gameType);
			sfso.putUtfString("bidWonPerson", gameBean._roundBean._bidWonPerson);
			sfso.putUtfString("openCardTaker", gameBean._roundBean._openCardTaker);
			sfso.putInt("doubleCount", gameBean._roundBean.doubleCount);
			sfso.putUtfString("gameTrumpSuit", gameBean._roundBean._trumpSuitType);
			sfso.putSFSArray("playerScores", gameBean.getPlayerScores());
			sfso.putLong("timeElapsed", gameBean._roundBean.getTimeConsumed());
			sfso.putInt("trickCount", gameBean._roundBean.trickCount);
			sfso.putUtfString("currentRoundSuit", gameBean._roundBean.currentSuit);
			sfso.putLong("remainingTime", gameBean.getRemainingTime());
			sfso.putUtfString("doubleType", gameBean._roundBean._doubleType);
			sfso.putUtfString("bidSelected", gameBean._roundBean.bidSelected);
			sfso.putUtfString("bidSelectedPerson", gameBean._roundBean._bidSelectedPerson);
			sfso.putUtfStringArray("initialPlayers", gameBean.initialPlayers);
			sfso.putUtfString("shuffler", gameBean.getShuffler());
			sfso.putUtfString("shufflerGender", Apps.getGender(gameBean.getShuffler()));
			
			if(gameBean._roundBean.isRemainingCardsDistributed)
			{					
				// In Discard Mode
				sfso.putBool("isbiddingMode", false);
				sfso.putIntArray("openCards", gameBean._roundBean.openCardsList);
				sfso.putUtfString("turn", gameBean._roundBean.getTurn());
				sfso.putBool("isDeclaredBonus", isDeclaredBonus(gameBean, gameBean._roundBean.getTurn()));
				sfso.putBool("isDeclaredBalootBonus", isDeclaredBalootBonus(gameBean, player) );
				sfso.putIntArray("playerAllCards", getPlayerAllCards(gameBean, player));
			}
			else
			{
				// In Bidding Mode	
				sfso.putBool("isbiddingMode", true);
				sfso.putInt("opencard", gameBean._roundBean._openCardId );
				
				sfso.putUtfString("turn", gameBean._roundBean.getTurnForBid());
				sfso.putInt("bidRound", gameBean._roundBean._bidRoundCount);
				sfso.putUtfString("bidSelected", gameBean._roundBean._gameType);
				sfso.putUtfString("bidSelectedPerson", gameBean._roundBean._bidSelectedPerson);
				sfso.putSFSArray("playersEnableBits", gameBean._roundBean.getPlayerBidEnableBits(player));
			}	
			
			sfso = pushRoundScoreDetails(gameBean, sfso);
		}
		
		return sfso;
	}
	
	public static boolean isDeclaredBonus(GameBean gameBean, String player)
	{
		PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(player);
		
		if(prb != null)
		{
			return prb.isDeclaredBonus;
		}
		else
		{
			return false;
		}
	}
	
	public static boolean isDeclaredBalootBonus(GameBean gameBean, String player)
	{
		PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(player);
		
		if(prb != null)
		{
			return prb.isDeclaredBalootBonus;
		}
		else
		{
			return false;
		}
	}
	
	public static ArrayList<Integer> getPlayerAllCards(GameBean gameBean, String player)
	{
		PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(player);
		
		ArrayList<Integer> al = new ArrayList<Integer>();
		
		if(prb != null)
		{
			al.addAll(prb._cards);
			al.addAll(prb._playedcards);
		}
		
		return al;		
	}
	
	public static int getTeamOneTricksWonCount(GameBean gameBean)
	{
		ArrayList<String> teamOnePlayers = getTeamOnePlayers(gameBean);
		
		int teamOneTricksWonCount = 0;
		
		for(String player : teamOnePlayers)
		{
			PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(player);
			
			if(prb != null)
			{
				teamOneTricksWonCount = teamOneTricksWonCount + prb.tricksWonCount;
			}
		}
		
		return teamOneTricksWonCount;
	}
	
	public static int getTeamTwoTricksWonCount(GameBean gameBean)
	{
		ArrayList<String> teamTwoPlayers = GameApps.getTeamTwoPlayers(gameBean);
		
		int teamTwoTricksWonCount = 0;
		
		for(String player : teamTwoPlayers)
		{
			PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(player);
			
			if(prb != null)
			{
				teamTwoTricksWonCount = teamTwoTricksWonCount + prb.tricksWonCount;
			}
		}
		
		return teamTwoTricksWonCount;
	}
	public static ArrayList<String> getCurrentRoundHighestScoreTeam(GameBean gameBean)
	{
		ArrayList<String> list = new ArrayList<String>();
		
		if(getTeamOnecurrentScore(gameBean) > getTeamTwocurrentScore(gameBean))
		{
			list =  getTeamOnePlayers(gameBean);
		}
		else if(getTeamOnecurrentScore(gameBean) < getTeamTwocurrentScore(gameBean))
		{
			list =  getTeamTwoPlayers(gameBean);
		}	
		
		return list;
	}
	
	public static ISFSObject pushRoundScoreDetails(GameBean gameBean, ISFSObject sfso)
	{
		sfso.putSFSArray("playerScores", gameBean.getPlayerScores());
		sfso.putInt("teamOneCurrentRoundScore", getTeamOnecurrentScore(gameBean));
		sfso.putInt("teamTwoCurrentRoundScore", getTeamTwocurrentScore(gameBean));
		sfso.putInt("teamOnePreviousScore", getTeamOnePreviousScore(gameBean));
		sfso.putInt("teamTwoPreviousScore", getTeamTwoPreviousScore(gameBean));
		sfso.putIntArray("teamOneAllScores", getTeamOneAllScores(gameBean));
		sfso.putIntArray("teamTwoAllScores", getTeamTwoAllScores(gameBean));
		sfso.putUtfStringArray("teamOnePlayers", getTeamOnePlayers(gameBean));
		sfso.putUtfStringArray("teamTwoPlayers", getTeamTwoPlayers(gameBean));
		
		sfso.putInt("teamOneGamePoints", gameBean._roundBean.teamOneGamePoints);
		sfso.putInt("teamTwoGamePoints", gameBean._roundBean.teamTwoGamePoints);
		sfso.putInt("teamOneBonusPoints", gameBean._roundBean.teamOneBonusPoints);
		sfso.putInt("teamTwoBounsPoints", gameBean._roundBean.teamTwoBounsPoints);
		
		sfso.putInt("teamOneTotalPoints", gameBean._roundBean.teamOneGamePoints + gameBean._roundBean.teamOneBonusPoints);
		sfso.putInt("teamTwoTotalPoints", gameBean._roundBean.teamTwoGamePoints + gameBean._roundBean.teamTwoBounsPoints);
		
		return sfso;
	}
	
	public static int getTeamOnecurrentScore(GameBean gameBean)
	{
		int score = 0;
		
		/*for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(pb.position == 0)
			{
				if(pb._points.size() > 0)
				{
					score = pb._points.get(pb._points.size() - 1);
				}
			}
		}*/
		
		// To stop rotating of scores in client need to maintain the fixed positions by using initial positions.
		PlayerBean pb = gameBean.getPlayerBean(gameBean.initialPlayers.get(0));
		if(pb._points.size() > 0)
		{
			score = pb._points.get(pb._points.size() - 1);
		}
		
		return score;
	}
	
	public static int getTeamOnePreviousScore(GameBean gameBean)
	{
		int score = 0;
		
		/*for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(pb.position == 0)
			{
				if(pb._points.size() > 0)
				{
					for(int i=0;i<pb._points.size()-1;i++)
					{
						score = score + pb._points.get(i);
					}
				}
			}
		}*/
		
		// To stop rotating of scores in client need to maintain the fixed positions by using initial positions.
		PlayerBean pb = gameBean.getPlayerBean(gameBean.initialPlayers.get(0));
		if(pb._points.size() > 0)
		{
			for(int i=0;i<pb._points.size()-1;i++)
			{
				score = score + pb._points.get(i);
			}
		}
		
		return score;
	}
	
	public static int getTeamTwoPreviousScore(GameBean gameBean)
	{
		int score = 0;
		
		/*for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(pb.position == 1)
			{
				if(pb._points.size() > 0)
				{
					for(int i=0;i<pb._points.size()-1;i++)
					{
						score = score + pb._points.get(i);
					}
				}
			}
		}*/
		
		// To stop rotating of scores in client need to maintain the fixed positions by using initial positions.
		PlayerBean pb = gameBean.getPlayerBean(gameBean.initialPlayers.get(1));
		if(pb._points.size() > 0)
		{
			for(int i=0;i<pb._points.size()-1;i++)
			{
				score = score + pb._points.get(i);
			}
		}
		
		return score;
	}
	
	public static int getTeamTwocurrentScore(GameBean gameBean)
	{
		int score = 0;
		
		/*for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(pb.position == 1)
			{
				if(pb._points.size() > 0)
				{
					score = pb._points.get(pb._points.size() - 1);
				}
			}
		}*/
		
		// To stop rotating of scores in client need to maintain the fixed positions by using initial positions.
		PlayerBean pb = gameBean.getPlayerBean(gameBean.initialPlayers.get(1));
		if(pb._points.size() > 0)
		{
			score = pb._points.get(pb._points.size() - 1);
		}
		
		return score;
	}
	
	public static int getTeamOneScore(GameBean gameBean)
	{
		int score = 0;
		
		/*for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(pb.position == 0)
			{
				score = pb._totalPoints;
			}
		}*/
		
		// To stop rotating of scores in client need to maintain the fixed positions by using initial positions.
		PlayerBean pb = gameBean.getPlayerBean(gameBean.initialPlayers.get(0));
		if(pb != null)
		{
			score = pb._totalPoints;
		}
		
		return score;
	}
	
	public static int getTeamTwoScore(GameBean gameBean)
	{
		int score = 0;
		
		/*for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(pb.position == 1)
			{
				score = pb._totalPoints;
			}
		}*/
		
		// To stop rotating of scores in client need to maintain the fixed positions by using initial positions.
		PlayerBean pb = gameBean.getPlayerBean(gameBean.initialPlayers.get(1));
		if(pb != null)
		{
			score = pb._totalPoints;
		}
		
		return score;
	}
	
	public static ArrayList<String> getTeamOnePlayers(GameBean gameBean)
	{
		ArrayList<String> list = new ArrayList<String>();
		
   /*	for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(pb.position == 0 || pb.position == 2)
			{
				list.add(pb._playerId);
			}
		}*/
		
		// To stop rotating of scores in client need to maintain the fixed positions by using initial positions.
		for(int i=0;i<gameBean.initialPlayers.size();i++)
		{
			if(i == 0 || i == 2)
			{
				list.add(gameBean.initialPlayers.get(i));
			}
		}
		
		return list;
	}
	
	public static ArrayList<String> getTeamTwoPlayers(GameBean gameBean)
	{
		ArrayList<String> list = new ArrayList<String>();
		
		/*for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(pb.position == 1 || pb.position == 3)
			{
				list.add(pb._playerId);
			}
		}*/
		
		// To stop rotating of scores in client need to maintain the fixed positions by using initial positions.
		for(int i=0;i<gameBean.initialPlayers.size();i++)
		{
			if(i == 1 || i == 3)
			{
				list.add(gameBean.initialPlayers.get(i));
			}
			
		}
		
		return list;
	}
	
	public static ArrayList<Integer> getTeamOneAllScores(GameBean gameBean)
	{
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		/*for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(pb.position == 0 || pb.position == 2)
			{
				list = pb._points;
				break;
			}
		}*/
		
		// To stop rotating of scores in client need to maintain the fixed positions by using initial positions.
		PlayerBean pb = gameBean.getPlayerBean(gameBean.initialPlayers.get(0));
		if(pb != null)
		{
			list = pb._points;
		}
		
		return list;
	}
	
	public static ArrayList<Integer> getTeamTwoAllScores(GameBean gameBean)
	{
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		/*for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(pb.position == 1 || pb.position == 3)
			{
				list = pb._points;
				break;
			}
		}*/
		
		// To stop rotating of scores in client need to maintain the fixed positions by using initial positions.
		PlayerBean pb = gameBean.getPlayerBean(gameBean.initialPlayers.get(1));
		if(pb != null)
		{
			list = pb._points;
		}
		
		return list;
	}
	
	public synchronized static boolean isPositionAvailable(GameBean gameBean, int position)
	{
		boolean isPositionAvailable = true;
		
		for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(pb.position != null)
			{
				if(position == pb.position)
				{
					isPositionAvailable = false;
					break;
				}
			}
		}
		
		return isPositionAvailable;
	}
	
	public static String getSuit(int cardId)
	{
		String suit = null;
		
		if(cardId >= 1 && cardId <= 8)
		{
			suit = "clubs";
		}			
		else if(cardId >= 9 && cardId <= 16)
		{
			suit = "diamonds";
		}			
		else if(cardId >= 17 && cardId <= 24)
		{
			suit = "hearts";
		}			
		else if(cardId >= 25 && cardId <= 32)
		{
			suit = "spades";
		}
		
		return suit;
	}
	
	public static String getHighestCardPlayer(GameBean gameBean, int highestCard)
	{
		String player = "NA";
		
		for(PlayerRoundBean prb : gameBean._roundBean._playerRoundBeanObjs)
		{
			if(prb._cards.contains(highestCard))
			{
				player = prb._playerId;
				break;
			}
		}
		
		return player;
	}
	
	public static ArrayList<Integer> getTeamBonusPoints(GameBean gameBean)
	{
		ArrayList<Integer> teamBonus = new ArrayList<Integer>();
		
		Integer team1Bonus = 0;
		Integer team2Bonus = 0;
		
		if(gameBean._roundBean._bonusObjs.size() > 0)
		{
			String highBonusPerson = null;
			
			for(String player : gameBean._players)
			{
				if(player.equals(gameBean._roundBean._highestBonusPerson))
				{
					highBonusPerson = player;
					break;
				}
			}
			if(highBonusPerson != null)
			{
				int pos = -1;
				pos = gameBean.getPlayerPosition(highBonusPerson);
				
				for(BonusBean bb : gameBean._roundBean._bonusObjs)
				{
					if((pos == 0 || pos == 2) && bb._isShowedCards)
					{
						team1Bonus = team1Bonus + getBonusPoints(bb._bonusType, gameBean._roundBean._gameType);	
					}
					else if((pos == 1 || pos == 3) && bb._isShowedCards)
					{
						team2Bonus = team2Bonus + getBonusPoints(bb._bonusType, gameBean._roundBean._gameType);
					}
				}
				
				Apps.showLog("Bonuse");			
				Apps.showLog("Team1Bonus  "+team1Bonus);
				Apps.showLog("Team2Bonus  "+team2Bonus);
				
				if(gameBean._roundBean.isDouble)
				{
					team1Bonus = (team1Bonus)*(gameBean._roundBean.doubleCount+1);
					team2Bonus = (team2Bonus)*(gameBean._roundBean.doubleCount+1);			
				}
			}
		}
		
		teamBonus.add(team1Bonus);
		teamBonus.add(team2Bonus);
		
		return teamBonus;
	}
	
	private static Integer getBonusPoints(String bonusType, String gameType)
	{		
		Integer points = 0;
		
		if(gameType.equals(Commands.SAN) || gameType.equals(Commands.ASHKAL))
		{
			if(bonusType.equals("sira"))
				points = 20;
			else if(bonusType.equals("50"))
				points = 50;
			else if(bonusType.equals("100"))
				points = 100;
			else if(bonusType.equals("400"))
				points = 200;
		}
		else
		{
			if(bonusType.equals("sira"))
				points = 20;
			else if(bonusType.equals("50"))
				points = 50;
			else if(bonusType.equals("100"))
				points = 100;			
		}		
		//AppMethods.showLog("Bonus Points:"+points);
		
		return points;
	}
	
	public static ArrayList<Integer> getBalootBonusPoints(GameBean gameBean)
	{
		ArrayList<Integer> balootBonus = new ArrayList<Integer>();
		
		int team1BalootBonus = 0;
		int team2BalootBonus = 0;
		
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{
			for(BonusBean bb : gameBean._roundBean._bonusObjs)
			{
				if(bb._bonusType.equals("baloot"))
				{
					int position = -1;
					
					for(String player : gameBean._players)
					{
						if(bb._playerId.equals(player))
						{
							position = gameBean.getPlayerPosition(player);
							break;
						}
					}
					
					if(position == 0 || position == 2)
					{
						team1BalootBonus = team1BalootBonus + 20;
					}
					else if(position == 1 || position == 3)
					{
						team2BalootBonus = team2BalootBonus + 20;
					}	
					
					break;
				}
			}
		}
		
		balootBonus.add(team1BalootBonus);
		balootBonus.add(team2BalootBonus);
		
		//gameBean.getGameLogInstance().balootBonus(gameBean, balootBonus);
		return balootBonus;
	}
	
	public static void calculateTeamPoints(GameBean gameBean, String wonPlayer)
	{
		Apps.showLog("calculateTeamPoints");
		Integer team1points = 0;
		Integer team2points = 0;
		// Get team's Scores			
		team1points = gameBean._roundBean._playerRoundBeanObjs.get(0)._totalScore + gameBean._roundBean._playerRoundBeanObjs.get(2)._totalScore;
		
		Apps.showLog("Before Adding Last trick "+team1points);
		
		team2points = gameBean._roundBean._playerRoundBeanObjs.get(1)._totalScore + gameBean._roundBean._playerRoundBeanObjs.get(3)._totalScore;
		Apps.showLog("  "+team2points);	
		
		Integer doubleCnt = gameBean._roundBean.doubleCount+1;		
		
		// Add last trick bonus score
		if(gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId.equals(wonPlayer) || gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId.equals(wonPlayer))
		{
			team1points += 10*doubleCnt;
		}
		else
		{
			team2points += 10*doubleCnt;
		}
		
		Apps.showLog("After last card Added ");
	    Apps.showLog(""+team1points);
		Apps.showLog(""+team2points);
		
		//As Per Eyad Requirement : dont double the details points.		
		if(gameBean._roundBean.isDouble)
		{
			team1points = team1points/doubleCnt;
			team2points = team2points/doubleCnt;
		}
				
		
		// Calculating Bonus
		
		Integer team1CardsCount = 0;
		Integer team2CardsCount = 0;
		
		team1CardsCount += gameBean._roundBean._playerRoundBeanObjs.get(0)._wonCards.size();
		team1CardsCount += gameBean._roundBean._playerRoundBeanObjs.get(2)._wonCards.size();
		
		team2CardsCount += gameBean._roundBean._playerRoundBeanObjs.get(1)._wonCards.size();
		team2CardsCount += gameBean._roundBean._playerRoundBeanObjs.get(3)._wonCards.size();
		
		Integer team1Bonus = 0;
		Integer team2Bonus = 0;
		Integer team1BalootBonus = 0;
		Integer team2BalootBonus = 0;
		
		if(gameBean._roundBean._bonusObjs.size()>0)
		{
			
			int highBonusPerson = 0;
			
			for(int i=0;i<gameBean._roundBean._players.size();i++)
			{
				if(gameBean._roundBean._highestBonusPerson.equals(gameBean._players.get(i)))
				{
					highBonusPerson = i;
					break;
				}	
			}
			
			
			for(int i=0;i<gameBean._roundBean._bonusObjs.size();i++)
			{					
				String player = gameBean._roundBean._bonusObjs.get(i)._playerId;
				int no =0;
				for(int j=0;j<gameBean._players.size();j++)
				{
					if(gameBean._players.get(j).equals(player))
					{
						no = j;
						break;
					}
				}
				
				if( (no == 0 || no == 2) && (highBonusPerson == 0 || highBonusPerson == 2) && gameBean._roundBean._bonusObjs.get(i)._isShowedCards)
				{					
					team1Bonus += getBonusPoints(gameBean._roundBean._bonusObjs.get(i)._bonusType, gameBean._roundBean._gameType);					
				}
				else if( (no == 1 || no == 3) && (highBonusPerson == 1 || highBonusPerson == 3)&& gameBean._roundBean._bonusObjs.get(i)._isShowedCards)
				{
					team2Bonus += getBonusPoints(gameBean._roundBean._bonusObjs.get(i)._bonusType, gameBean._roundBean._gameType);
				}						
			}
			
			Apps.showLog("Bonuse");
			
			Apps.showLog("Team1"+team1Bonus);
			Apps.showLog("Team2"+team2Bonus);
		}
		
		
		// Adding baloot bonus
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{
			/*for(int i=0;i<gameBean._roundBean._bonusObjs.size();i++)
			{
				if(gameBean._roundBean._bonusObjs.get(i)._bonusType.equals("baloot"))
				{
					int no =0;
					for(int j=0;j<gameBean._players.size();j++)
					{
						if(gameBean._roundBean._bonusObjs.get(i)._playerId.equals(gameBean._players.get(j)))
						{
							no = j;
							break;
						}
					}
					
					if(no == 0 || no == 2)
					{
						team1BalootBonus += 10;
					}
					else
					{
						team2BalootBonus += 10;
					}	
					
					break;
				}
			}*/
			
			for(BonusBean bb : gameBean._roundBean._bonusObjs)
			{
				if(bb._bonusType.equals("baloot"))
				{
					int position =0;
					
					for(int j=0;j<gameBean._players.size();j++)
					{
						if(bb._playerId.equals(gameBean._players.get(j)))
						{
							position = j;
							break;
						}
					}
					
					if(position == 0 || position == 2)
					{
						team1BalootBonus = 20;
					}
					else
					{
						team2BalootBonus = 20;
					}	
					
					break;
				}
			}
		}
		
		Apps.showLog("Baloot Bonus team1BalootBonus "+team1BalootBonus+"   team2BalootBonus "+team2BalootBonus);
		
		// Baloot bonus shouls not get doubled and remaining bonus should be doubled.
		if(gameBean._roundBean.isDouble && team1CardsCount != 32 && team2CardsCount != 32)
		{
			/*team1Bonus = (team1Bonus)*(gameBean._roundBean.doubleCount+1);
			team2Bonus = (team2Bonus)*(gameBean._roundBean.doubleCount+1);*/			
		}		
		
	
		int teamOneTotalBonus = team1Bonus + team1BalootBonus;
		int teamTwoTotalBonus = team2Bonus + team2BalootBonus;
		
		
		if(getTeamOnePlayers(gameBean).contains(gameBean._playersBeansList.get(0)._playerId))
		{
			gameBean._roundBean.teamOneGamePoints = team1points;
			gameBean._roundBean.teamOneBonusPoints = teamOneTotalBonus;
			
			gameBean._roundBean.teamTwoGamePoints = team2points;
			gameBean._roundBean.teamTwoBounsPoints = teamTwoTotalBonus;			
		}
		else
		{
			gameBean._roundBean.teamOneGamePoints = team2points;
			gameBean._roundBean.teamOneBonusPoints = teamTwoTotalBonus;
			
			gameBean._roundBean.teamTwoGamePoints = team1points;
			gameBean._roundBean.teamTwoBounsPoints = teamOneTotalBonus;
		}
		
		gameBean.getGameLogInstance().gamePoints(gameBean);
	}
}
