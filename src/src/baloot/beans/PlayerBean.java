/**
 * 
 */
package src.baloot.beans;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import src.baloot.game.Player;

public class PlayerBean extends Player
{
	public String _playerId;
	public ArrayList<Integer> _points  = new ArrayList<Integer>();
	public Integer _totalPoints = 0;
	public Integer position = null;
	public Integer initialPosition = null;
	public boolean _isTurn = false;	
	public boolean _isAI = false;
	public boolean isFakePlayer = false;
	public boolean isLeave = false;
	public boolean isAutoPlay = false;
	public boolean isHost = false;
	public boolean isNewlyOccupiedSeat = false;
	public String _aiPlayer = "";
	public String leftPlayer;
	public boolean isShuffler = false;
	public String gender = null;
	

	// for AI sound	
	public  ConcurrentHashMap<String, Integer> aiSoundCount = new ConcurrentHashMap<String, Integer>();
	public int teammateWonBidAndLostRoundCount = 0;
}
