/**
 * 
 */
package src.baloot.beans;

/**
 * @author MSKumar
 *
 */
public class MessageBean {
	
	public String _roundSuit;
	public String _messageCardSuit;
	public Integer _messageCard;
	public String _message;
	public Integer _messagePeriority;
	
	public MessageBean(){
		
		this._roundSuit = "";
		this._messageCardSuit = "";
		this._messageCard = 0;
		this._message = "";
		this._messagePeriority = 0;
	}
}

