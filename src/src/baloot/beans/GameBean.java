/**
 * 
 */
package src.baloot.beans;

import java.awt.event.ActionEvent;



import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.Timer;

import src.baloot.bsn.AIBsn;
import src.baloot.constants.Constant;
import src.baloot.exce.Validations;
import src.baloot.exce.beans.LogBean;
import src.baloot.game.Game;
import src.baloot.listeners.BidActionListener;
import src.baloot.listeners.HokomSuitSelectActionListener;
import src.baloot.logs.LoadLogFile;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class GameBean extends Game {
	
	
	private static final long serialVersionUID = 1L;

	public ArrayList<String> initialPlayers = new ArrayList<String>();
	public ArrayList<String> _players = new ArrayList<String>();
	
	public String shuffler;
	public RoundBean _roundBean = null;
	private int roundCount = 0;
	public boolean isGameStarted = false;
	public boolean isGameCompleted = false;
	public boolean isHostJoined = false;
	public String host;
	public boolean isRematchPhase = false;
	public boolean isQuickStartPhase = false;
	public ArrayList<String> rematchPlayers = new ArrayList<String>();
	public boolean isSinglePlayer = false;
	private ArrayList<String> spectators = new ArrayList<String>();
	public ArrayList<PlayerBean> _playersBeansList = new ArrayList<PlayerBean>();
	public boolean isStopped = false;
	public boolean isRoundCompletedResultTime = false;
	private int winnersPosition = -1;
	
	
	public String tableType = Constant.PUBLIC;
	
	private Timer timer;
	private Date lastUpdate = null;
	
	private boolean isPrivateTable = false;

	/*
	 * This variable is particularly for "TRICK_COMPLETED_SOUND" sound only.
	 * For all remaining sound counts variable "aiSoundCount" is used in the PlayerBean class. 
	 */
	public  ConcurrentHashMap<String, Integer> aiRoundBasedSoundCount = new ConcurrentHashMap<String, Integer>();
	
	public int trickCompletedSoundPlayedCount = 0;
	
	public Timer getTimer()
	{
		return timer;
	}
	
	public Date getLastUpdate()
	{
		return lastUpdate;
	}
	
	public void setRematchPlayers()
	{
		rematchPlayers = new ArrayList<String>();
	}
	
	public void addSpectator(String name)
	{
		spectators.add(name);
	}
	
	public void addSpectatorsList(ArrayList<String> list)
	{
		for(String spectator : list)
		{
			spectators.add(spectator);
		}
	}
	
	public void incrementRoundCount()
	{
		this.roundCount++;
	}	
	
	public boolean isPrivateTable() {
		return isPrivateTable;
	}

	public void setPrivateTable(boolean isPrivateTable) {
		this.isPrivateTable = isPrivateTable;
	}

	public int getWinnersPosition() {
		return winnersPosition;
	}
	public void setWinnersPosition(int winnersPosition) {
		this.winnersPosition = winnersPosition;
	}

	public ArrayList<Integer> getPositions()
	{
		ArrayList<Integer> positions = new ArrayList<Integer>();
		
		for(PlayerBean pb : _playersBeansList)
		{
			if(pb.position != null)
			{
				positions.add(pb.position);
			}
		}
		
		return positions;
	}
	
	public ArrayList<Integer> getInitialPositions()
	{
		ArrayList<Integer> positions = new ArrayList<Integer>();
		
		for(PlayerBean pb : _playersBeansList)
		{
			if(pb.initialPosition != null)
			{
				positions.add(pb.initialPosition);
			}
		}
		
		return positions;
	}
	
	public synchronized void setShuffler()
	{
		String preShuffler = shuffler;
		
		
		if(shuffler == null)
		{
			for(PlayerBean pb : _playersBeansList)
			{
				if(pb.position == 3)
				{
					shuffler = pb._playerId;
					pb.isShuffler = true;
					break;
				}
			}
		}
		else
		{
			// move the shuffler positin
			for(PlayerBean pb : _playersBeansList)
			{
				int pos = pb.position - 1;
				
				if(pos == -1)
				{
					pos = 3;
				}
				
				pb.position = pos;
				pb.isShuffler = false;
				pb._isTurn = false;
			}
			
			// setting the shuffler
			for(PlayerBean pb : _playersBeansList)
			{
				if(pb.position == 3)
				{
					shuffler = pb._playerId;
					pb.isShuffler = true;
					break;
				}
			}
			
			// reanrrange the lists
			ArrayList<PlayerBean> beans = new ArrayList<PlayerBean>();
			ArrayList<String>  players = new ArrayList<String>();
			
			for(int i=0;i<4;i++)
			{
				for(PlayerBean pb : _playersBeansList)
				{
					if(pb.position == i)
					{
						beans.add(pb);
						players.add(pb._playerId);
					}
				}
			}
			
			_playersBeansList = beans;
			_players = players;
			
		}
		
		getGameLogInstance().setShuffler(this, preShuffler);
	}

	public Integer getSpecatatorsCount()
	{
		return spectators.size();
	}
	
	public ArrayList<String> getSpecatatores(){
		return spectators;
	}
	
	public void removeSpecatator(String player)
	{		
		spectators.remove(player);
	}
	
	public Integer getRoundCount(){
		return roundCount;
	}
	
	public String getPartner(String player)
	{
		int pos = getPartnerPosition(player);
		
		return getPlayer(pos);
	}
	
	
	public Integer getActiveUsersCount()
	{
		Integer no=0;
		for(int i=0;i<_playersBeansList.size();i++)
		{
			if(!_playersBeansList.get(i)._isAI)
			{
				no++;
			}
		}
		
		return no;
	}
	
	public ArrayList<String> getTeam(String player)
	{
		ArrayList<String> list = new ArrayList<String>();
		
		list.add(player);
		list.add(getPartner(player));
		
		return list;
	}
	
	public Integer getPlayersTotalPoints(String player)
	{
		Integer totalpoints = 0;
		for(int i=0;i<_playersBeansList.size();i++)
		{
			if(_playersBeansList.get(i)._playerId.equals(player))
			{
				totalpoints = _playersBeansList.get(i)._totalPoints;
				break;
			}
		}
		return totalpoints;
	}
	
	
	public String getShuffler()
	{			
		return shuffler;
	}
	
	public Integer getShufflerPosition(String shuffler)
	{
		Integer pos = null;
	
		for(PlayerBean pb : _playersBeansList)
		{
			if(pb._playerId.equals(shuffler))
			{
				pos = pb.position;
				break;
			}
		}	
		
		return pos;
	}
	
	public String getTurn()
	{
		String turn = null;
		
		for(PlayerBean pb : _playersBeansList)
		{
			if(pb._isTurn)
			{
				turn = pb._playerId;
				break;
			}
		}
		
		return turn;
	}
	
	public SFSArray getPlayerScores()
	{
		SFSArray player_scores = new SFSArray();
		
		for(int i=0; i<maxPlayers;i++)
		{
			if(_playersBeansList.size() == maxPlayers)
			{
				ISFSObject sfso = new SFSObject();
				
				sfso.putInt("totalPoints", _playersBeansList.get(i)._totalPoints);
				sfso.putUtfString("playerId", _playersBeansList.get(i)._playerId);
				player_scores.addSFSObject(sfso);
			}
		}	
		
		return player_scores;
	}
	
	public String getAliveUser()
	{
		String player="";
		for(int i=0;i<_playersBeansList.size();i++)
		{
			if(!_playersBeansList.get(i)._isAI)
			{
				player = _playersBeansList.get(i)._playerId;
				break;
			}
		}
		return player;		
	}
	
	public PlayerBean getPlayerBean(String player)
	{
		PlayerBean plBeean = null;
		
		for(PlayerBean pb : _playersBeansList)
		{
			if(pb._playerId.equals(player))
			{
				plBeean = pb;
				break;
			}
		}
		
		return plBeean;
	}
	
	public Integer getPlayerPosition(String player)
	{
		Integer pos = null;
		
		PlayerBean pb = getPlayerBean(player);
		
		if(pb != null)
		{
			pos = pb.position;
		}
		
		return pos;
	}
	
	public Integer getPartnerPosition(String player)
	{
		Integer pos = getPlayerPosition(player);
		Integer partnerPos = null;
		
		if(pos == 0)
		{
			partnerPos = 2;
		}			
		else if(pos == 1)
		{
			partnerPos =3;
		}			
		else if(pos == 2)
		{
			partnerPos =0;
		}			
		else if(pos == 3)
		{
			partnerPos =1;
		}
			
		return partnerPos;
	}
	
	public String getPlayer(int pos)
	{
		String player = null;
		
		for(PlayerBean pb : _playersBeansList)
		{
			if(pb.position == pos)
			{
				player = pb._playerId;
				break;
			}
		}
		
		return player;
	}
		
	public PlayerBean getLeftPlayerBean(String player)
	{
		PlayerBean pb = null;
		
		for(PlayerBean bean : _playersBeansList)
		{
			if(player.equals(bean.leftPlayer))
			{
				pb = bean;
				break;
			}
		}
		
		return pb;
	}

	public long getTotalGameRunningTime()
	{
		long diff = Apps.getTimeDifference(getGameStartedTime());
		
		diff = diff/60;
		
		return diff;
	}
	
	public long getRemainingQuickStartTime()
	{				
		long elapsed = Apps.getTimeDifference(lastUpdate);
		long rTime = -1;
		
		if(timer != null)
		{
			rTime = (timer.getDelay()/1000) - elapsed;
		}
		
		String var = " GameBean remainingQuickStartTime "+rTime+"   isPrivateTable "+isPrivateTable+"  lastUpdate "+lastUpdate.toString()+"  elapsedTime "+elapsed;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "Timer", var);
		addLog(lb);
		lb = null;	
		
		return rTime;
	}
	
	public long getRemainingTime()
	{
		long elapsed = Apps.getTimeDifference(lastUpdate);
		long rTime = -1;
		
		if(timer != null)
		{
			rTime = (timer.getDelay()/1000) - elapsed;
		}
		
		return rTime;
	}
	
	public void startBidTimer()
	{
		_roundBean.stopAllPlayerIndividualTimers();
		
		if(!isRematchPhase)
		{
			stopTimer();	
			int seconds = Constant.BIDDING_TIME;
			
			if(_roundBean.isGameTypeSelected)
			{
				seconds = Constant.BIDDING_TIME_AFTER_GAMETYPE_SELECTION;
			}
			
			timer = new Timer(seconds*1000, new BidActionListener(tableId));
			timer.start();
			lastUpdate = Apps.getCurrentTime();
			isStopped = false;
			
			String var = " GameBean_BidTimer Started  delayTime "+seconds+"  player "+_roundBean.getTurnForBid()+" isGameTypeSelected "+_roundBean.isGameTypeSelected;
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "BidTimer", var);
			addLog(lb);
		}
		else
		{
			String var = "ShouldNotStop_Rematch_Timer  ";
			LogBean lb = new LogBean();
			lb.setValues(Constant.EXC, "P0", "", "BidTimer", var);
			addLog(lb);
		}
	}
	
	public void startSelectHokomSuitTimer(int seconds, String player)
	{
		_roundBean.stopAllPlayerIndividualTimers();
		
		stopTimer();
		
		timer = new Timer(seconds*1000, new HokomSuitSelectActionListener(tableId, player));
		timer.start();
		lastUpdate = Apps.getCurrentTime();
		isStopped = false;
		
		String var = " GameBean SelectHokomSuitTimer Started  delayTime "+seconds+"  player "+player;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "SelectHokomSuitTimer", var);
		addLog(lb);
		lb = null;
		
	}
	
	public void startTimer(int seconds, String player, String command) 
	{					
		if(!isRematchPhase)
		{
			try{
				stopTimer();		         		         	         
			}catch(Exception exception){}
			
			timer = new Timer(seconds*1000, new GameBeanTimerActionListener(tableId, player, command));
			timer.start();
			lastUpdate = Apps.getCurrentTime();
			isStopped = false;
			
			String var = " GameBean Timer Started command "+command+" delayTime "+seconds+"  player "+player;
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "Timer", var);
			addLog(lb);
			lb = null;
		}
		else
		{
			String var = "ShouldNotStop_Rematch_Timer Command :"+command;
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "Timer", var);
			addLog(lb);
			lb = null;			
		}
	}
	
	public void stopAllTimers()
	{
		stopTimer();
	}
	
	public void stopTimer()
	{		
		if(_roundBean != null)
		{
			_roundBean.stopTimer();
			_roundBean.stopAllPlayerIndividualTimers();
		}
		
		if(timer != null)
		{
			String var = " GameBean Timer Stopped delayTime "+timer.getDelay()/1000+" runningTime "+Apps.getTimeDifference(lastUpdate);
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "Timer", var);
			addLog(lb);
			lb = null;	
			
			try{ timer.stop();}catch(Exception e){}
			
			timer = null;
		}
		
		isStopped = true;
	}	
	
	public void initGame()
	{
		isQuickStartPhase = false;
		isGameStarted = true;
		stopTimer();
		LoadLogFile.load();
		setGameStartedTimeString(Apps.getDateString());
		setGameStartedTime(Apps.getCurrentTime());
		
		
		ArrayList<PlayerBean> preBeans = new ArrayList<PlayerBean>();
		
		ArrayList<Integer> positions = new ArrayList<Integer>();
		
		for(PlayerBean pb : _playersBeansList)
		{
			positions.add(pb.position);
			preBeans.add(pb);
		}
		
		// add the required ai players
		if(_playersBeansList.size() < 4)
		{
			int noofAIs = 4 - _playersBeansList.size();
			
			for(int i=1;i<=noofAIs;i++)
			{
				for(int j=0;j<4;j++)
				{
					if(positions.contains(j))
					{
						// do nothing
					}
					else
					{
						String aiName = "AI"+i;
						PlayerBean aiBean = new PlayerBean();
						aiBean._playerId = aiName;
						aiBean._aiPlayer = aiName;
						aiBean.position = j;
						aiBean._isAI = true;
						aiBean.gender = "Male";
						
						_players.add(aiName);
						_playersBeansList.add(aiBean);
						positions.add(j);
						
						break;
					}
				}
			}
		}
		
		// assign initialPositions to players.
		for(PlayerBean pb : _playersBeansList)
		{
			pb.initialPosition = pb.position;
		}
		
		getGameLogInstance().initGame(this, preBeans);
		
		// rearrange players with respect to positions
		ArrayList<String> rePlayers = new ArrayList<String>();
		ArrayList<PlayerBean> reBeans = new ArrayList<PlayerBean>();
		
		for(int i=0;i<4;i++)
		{
			for(PlayerBean pb : _playersBeansList)
			{
				if( i == pb.position)
				{
					reBeans.add(pb);
					rePlayers.add(pb._playerId);
					break;
				}
			}
		}
		
		if(rePlayers.size() == 4)
		{
			_players = rePlayers;
			_playersBeansList = reBeans;
			setInitialPlayers();
			
			Commands.appInstance.proxy.gameStarted(this);
			
			if (!isPrivateTable)
			{
				for(PlayerBean  pb : _playersBeansList)
				{
					if(!pb._isAI)
					{
						Commands.appInstance.proxy.isHavingEnoughGamesToPlay(this, pb._playerId, false, true);
					}
				}
			}
			
			startGame();
		}
		else
		{
			Validations.raiseGameStartWithIncompleteDataException(this);
		}
	}
	
	public void startGame()
	{
		Commands.appInstance.ulBsn.updateLobby(this);
		
		Apps.showLog("  StartGame");		
		setShuffler();
		_playersBeansList.get(0)._isTurn = true;	
		Apps.showLog(" shuffler  "+getShuffler());
		incrementRoundCount();
		
		_roundBean = new RoundBean();
		
		_roundBean.tableId = tableId;
		_roundBean._players = _players;
		_roundBean.gameBean = this;
		_roundBean._turnForBid = getTurn();
		_roundBean._shufflerPosition = getShufflerPosition(getShuffler());
		
		_roundBean.init();
				
		if(!isSinglePlayer)
		{
			Commands.appInstance.ulBsn.updateLobby(this);
		}
		
		getGameLogInstance().startGame(this);
		getGameLogInstance().initRound(this);
		// Start the game
		distributeCards(this);
		incrementTicket();
	}
	
	public void setInitialPlayers()
	{
		for(String player : _players)
		{
			if(!initialPlayers.contains(player))
			{
				initialPlayers.add(player);
			}
		}
	}
	public void distributeCards(GameBean gameBean){
		
		Room room = Apps.getRoomByName(gameBean.tableId);
		
		ISFSObject sfso = new SFSObject();
		
		sfso.putUtfString("tableId", gameBean.tableId); 
		sfso.putUtfString("gameId", gameBean.gameId); 
		sfso.putUtfString("host", host);
		sfso.putBool("isPrivateTable", gameBean.isPrivateTable);
		sfso.putUtfStringArray("players", gameBean._players);
		sfso.putUtfStringArray("initialPlayers", gameBean.initialPlayers);
		sfso.putSFSArray("playerCards",gameBean._roundBean.getPlayersCards());
		sfso.putInt("opencard", gameBean._roundBean.getOpencard() );
		sfso.putUtfString("shuffler", gameBean.getShuffler());
		sfso.putUtfString("shufflerGender", Apps.getGender(gameBean.getShuffler()));
		sfso.putSFSArray("playersEnableBits", gameBean._roundBean.getBidEnableBits(gameBean._roundBean._turn));
		sfso.putInt("roundCount", roundCount);
		sfso.putInt("bidRoundCount", _roundBean._bidRoundCount);
		sfso.putInt("ticket", getTicket());
		
		Commands.appInstance.send(Commands.DISTRIBUTE_CARDS,sfso, room.getUserList());
		getGameLogInstance().cardsDistribution(this);
		
		gameBean.startBidTimer();
		Commands.appInstance.ai.checkPlayerTurnBiddingorDiscard(gameBean, gameBean._roundBean.getTurnForBid());
	}
}
//End Class

class GameBeanTimerActionListener implements ActionListener 
{		
	GameBean gameBean;
	
	String player;	
	String command;
	String tableId;
	
	public GameBeanTimerActionListener(String tableId, String player, String command)
	{				
		this.tableId = tableId;
		this.player = player;	
		this.command = command;
	}
	
	public void actionPerformed(ActionEvent e) 
	{
		//Stop the timer	
		gameBean = Apps.getGameBean(tableId);
		
		
		if(gameBean != null)
		{				
			Timer timer = gameBean.getTimer();
			
			if(timer != null)
			{
				int delayTime = timer.getDelay()/1000;
				
				String var = " GameBeanTimerActionListener Timer Completed command "+command+" player "+player
								+" delayTime "+delayTime+"  runningTime "+Apps.getTimeDifference(gameBean.getLastUpdate());
				
				LogBean lb = new LogBean();
				lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "Timer", var);
				gameBean.addLog(lb);
				lb = null;	
			}
		
			gameBean.stopTimer();
			
			Room room = Apps.getRoomByName(tableId);
			
			if(command.equals(Commands.QUICK_START_GAME))
			{			
				gameBean.initGame();
			}
			else if(command.equals(Commands.QUICK_START_PRIVATE_TABLE))
			{
				gameBean.initGame();
			}
			else if(command.equals(Commands.REMATCH))
			{
                gameBean.getRematchInstance().startRematchGame(gameBean);
			}
			else if(command.equals(Commands.HOKOM_SELECTED_IN_SECOND_ROUND))
			{
				ISFSObject sfso = new SFSObject();
				sfso.putUtfString("bidSelected", gameBean._roundBean._gameType);
				sfso.putUtfString("gameType", gameBean._roundBean._gameType);
				sfso.putUtfString("bidWonPerson", gameBean._roundBean._bidWonPerson);
				sfso.putUtfString("bidSelectedPerson", player);
				
				Commands.appInstance.send(Commands.SELECT_HOKOM_SUIT, sfso, room.getUserList());
				
				gameBean.startTimer(Constant.BIDDING_TIME, player, Commands.SELECT_HOKOM_SUIT);
			}
			else if(command.equals(Commands.START_NEXT_ROUND))
			{
				gameBean.isRoundCompletedResultTime = false;
				gameBean.startGame();
			}
			else if(command.equals(Commands.DISCARD))
			{	
				if(!gameBean._roundBean.getTurn().equals(player))
				{
					Validations.raiseListenerPlayerAndActualPlayerNotMatchException(gameBean, player, gameBean._roundBean.getTurn(), command, "GameBean");
					player = gameBean._roundBean.getTurn();
				}
				
				PlayerBean pb = gameBean.getPlayerBean(player);
				
				/*if(pb != null)
				{
					 Apps.showLog(" $ User found $ ");
				}
				else
                {
                    Apps.showLog("User not found");
                    pb = gameBean.getLeftPlayerBean(player);
                    player = pb._playerId;
                }
				*/
				pb.isAutoPlay = true;
				
				// Add Log					
				String var = " player "+player+" isAi "+pb._isAI+"  isAutoplay "+pb.isAutoPlay+" isLeave "+pb.isLeave;
				
				LogBean lb = new LogBean();
				lb.setValues(Constant.LOG, "P3", player, "PlayerTurnedAsAIINActionListener", var);
				gameBean.addLog(lb);
				lb = null;	
				
				//Commands.appInstance.ai.checkPlayersTurnBiddingOrGame(gameBean, player);	
				
				User user = Apps.getUserByName(player);	
				if(user != null)
				{
					Commands.appInstance.send(Commands.PLAYER_CHANGED_AS_AI, new SFSObject(), user);
				}
				
				new AIBsn().playAI(command, gameBean, player);
			}
		}		
	}
}
