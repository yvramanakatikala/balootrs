/**
 * 
 */
package src.baloot.beans;

import java.util.ArrayList;

import src.baloot.utils.Apps;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

/**
 * @author MSKumar
 *
 */
public class TableBean {
	
	private String _tableId = "null";
	private String _team1Player1 = "null";
	private String _team1Player2 = "null";
	private String _team2Player1 = "null";
	private String _team2Player2 = "null";
	private Integer _team1Score = 0;
	private Integer _team2Score = 0;
	private Integer _noofSpectators = 0;	
	private boolean _isStarted = false;
	private String _password = "";
	private String _tableType = "";
	private boolean _isEnablePassword = false;
	private boolean _isBlock = false;
	private String _createdDate = "";
	private ArrayList<String> _spectators = new ArrayList<String>();
	private String _host = "";
	

	/** setters */
	public void setTableid(String _tableId){
		this._tableId = _tableId;
	}
	public void setTeam1Player1(String name){
		this._team1Player1 = name;
	}
	public void setTeam1Player2(String name){
		this._team1Player2 = name;
	}
	public void setTeam2Player1(String name){
		this._team2Player1 = name;
	}
	public void setTeam2Player2(String name){
		this._team2Player2 = name;
	}
	public void setTeam1Score(Integer score){
		this._team1Score = score;
	}
	public void setTeam2Score(Integer score){
		this._team2Score = score;
	}
	public void setNoofSpectators(Integer no){
		this._noofSpectators = no;
	}
	public void setIsStarted(boolean bool){
		this._isStarted = bool;
	}
	public void setPassword(String password){
		this._password = password;
	}
	public void setTableType(String tableType){
		this._tableType = tableType;
	}
	public void setIsEnablePassword(boolean enablePassword){
		this._isEnablePassword = enablePassword;
	}
	public void setIsBlock(boolean block){
		this._isBlock = block;
	}
	public void setCreatedDate(String createdDate){
		this._createdDate = createdDate;
	}
	public void setSpecators(ArrayList<String> spe){
		this._spectators = spe;
	}
	public void setHost(String host){
		this._host = host;
	}
	
	/** Getters */
	public String getTableId(){
		return this._tableId;
	}
	public String getTeam1Player1(){
		return this._team1Player1;
	}
	public String getTeam1Player2(){
		return this._team1Player2;
	}
	public String getTeam2Player1(){
		return this._team2Player1;
	}
	public String getTeam2Player2(){
		return this._team2Player2;
	}
	public Integer getTeam1Score(){
		return this._team1Score;
	}
	public Integer getTeam2Score(){
		return this._team2Score;
	}
	public Integer getNoofSpectators(){
		return this._noofSpectators;
	}
	public boolean getIsStarted(){
		return this._isStarted;
	}
	public String getPassword(){
		return this._password;
	}
	public String getTableType(){
		return this._tableType;
	}
	public boolean getIsEnablePassword(){
		return this._isEnablePassword;
	}
	public boolean getIsBlock(){
		return this._isBlock;
	}
	
	public String getCreatedDate(){
		return _createdDate;
	}
	public ArrayList<String>getSpecators(){
		return this._spectators;
	}
	public String getHost(){
		return _host;
	}
	public boolean isStarted()
	{
		return _isStarted;
	}
	
	
	public ISFSObject getSFSObject(){
		
		//AppMethods.showLog("Players"+_team1Player1+","+_team1Player2+","+_team2Player1+","+_team2Player2+","+ _tableid);
		ISFSObject sfso = new SFSObject();
		
		sfso.putUtfString("tableId", _tableId);
		sfso.putUtfString("team1Player1", _team1Player1);
		sfso.putUtfString("team1Player2", _team1Player2);
		sfso.putUtfString("team2Player1", _team2Player1);
		sfso.putUtfString("team2Player2", _team2Player2);
		sfso.putInt("team1Score", _team1Score);
		sfso.putInt("team2Score", _team2Score);
		sfso.putInt("noofSpectators", _noofSpectators);
		sfso.putBool("isStarted", _isStarted);
		sfso.putUtfString("password", _password);
		sfso.putUtfString("tableType", _tableType);		
		sfso.putBool("enablePassword", _isEnablePassword);
		sfso.putBool("block", _isBlock);
		sfso.putUtfStringArray("spectators", _spectators);
		sfso.putUtfString("host", _host);
		sfso.putInt("onlineUsersCount", Apps.getOnlineUsersCount());
		
		//printData(sfso);
		return sfso; 
	}
	
	/**
	 * This method is to clear the previous data in the TableBean.
	 * 
	 * 
	 * @param tb
	 */
	public void clearPreviousData()
	{
		_team1Player1 = "null";
		_team1Player2 = "null";
		_team2Player1 = "null";
		_team2Player2 = "null";
	}
	
	public void printData(ISFSObject sfso)
	{
		/*System.out.println("YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY");
		System.out.println("  talbleId     "+sfso.getUtfString("tableId"));
		System.out.println("  team1Player1 "+sfso.getUtfString("team1Player1"));
		System.out.println("  team1Player2 "+sfso.getUtfString("team1Player2"));
		System.out.println("  team2Player1 "+sfso.getUtfString("team2Player1"));
		System.out.println("YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY");*/
	}
}
