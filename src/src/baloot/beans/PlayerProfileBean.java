package src.baloot.beans;

import src.baloot.constants.Constant;

public class PlayerProfileBean {

	public String name;
	public String gender = Constant.MALE;
	public boolean isGuestUser = false;
	public boolean isDisconnected = false;
	
	public PlayerProfileBean(String name)
	{
		this.name = name;
	}
}
