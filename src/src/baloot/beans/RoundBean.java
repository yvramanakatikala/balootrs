/**
 * 
 */
package src.baloot.beans;

import java.awt.event.ActionEvent;



import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Random;

import javax.swing.Timer;

import src.baloot.bsn.AIBsn;
import src.baloot.constants.Constant;
import src.baloot.exce.Validations;
import src.baloot.exce.beans.LogBean;
import src.baloot.game.Round;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;
import src.baloot.utils.GameApps;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class RoundBean extends Round{

	public String tableId;
	public int trickCount = 0;
	public Integer _shufflerPosition;
	public ArrayList<String> _players = new ArrayList<String>();
	public ArrayList<PlayerRoundBean> _playerRoundBeanObjs = new ArrayList<PlayerRoundBean>();
	public ArrayList<Integer> openCardsList = new ArrayList<Integer>();	
	public String _turn;
	public String _gameType = "null";	
	public String bidSelected = "pass";
	public String _trumpSuitType = "null";
	public boolean isGameTypeSelected = false;
	public boolean isDouble = false;
	public String _openCardTaker = "null";
	public String _bidWonPerson = "null";
	public int doubleCount = 0;
	public String _doubleType =  Commands.NOT_SELECTED;
	public Integer _bidRoundCount = 0;
	public String _turnForBid = "";
	public String currentSuit = "";	
	public String firstPerson = "";
	public String _bidSelectedPerson = "null";
	public Integer _openCardId = 0;	
	public Integer passCount = 0;	
	public boolean _balootShowed = false;
	public boolean _delayDiscard = false;	
	public ArrayList<BonusBean> _bonusObjs = new ArrayList<BonusBean>();
	public String _highestBonusPerson = "null";	
	public ArrayList<Integer> _cardsInHand = new ArrayList<Integer>();
	public ArrayList<Integer> _cardsPlayed = new ArrayList<Integer>();	
	public ArrayList<PlayedCard> _lastHokomSuitTrick = new ArrayList<PlayedCard>();
	public Integer _noofHokomSuitTricks = 0;
	private Date lastUpdateTime = null;	
	public boolean isRemainingCardsDistributed = false;
	private ArrayList<Integer> cards = new ArrayList<Integer>();
	public ArrayList<Integer> _deckCards = new ArrayList<Integer>();
	private int maxPlayers = 4;	
	private Timer timer = null;
	public boolean isStopped = false;
	public GameBean gameBean = null;
	public boolean isRemainingAllPlayersPassHokom = false;	
	
	
	// for detail score points requested by Eyad.
	public int teamOneGamePoints = 0;
	public int teamTwoGamePoints = 0;
	public int teamOneBonusPoints = 0;
	public int teamTwoBounsPoints = 0;
	
	public String preTrickWinner = "NA";
	public boolean isAnyPlayerWonFourTricksInSeries = false;
	public boolean isBalootBonusDeclared = false;
	
	
	
	public void init()
	{
		for(String player : _players)
		{
			PlayerRoundBean prb = new PlayerRoundBean();
			
			prb._playerId = player;
			prb.setTableId(tableId);
			
			_playerRoundBeanObjs.add(prb);
		}
		
		setOpenCardTaker();
		shuffelCards();
		setLastUpdatedTime();
	}

	public void setLastUpdatedTime()
	{
		lastUpdateTime = Apps.getCurrentTime();
	}
	
	public Date getLastUpdate()
	{
		return lastUpdateTime;
	}
	
	public Timer getTimer()
	{
		return timer;
	}
	
	public void stopAllPlayerIndividualTimers()
	{
		for(PlayerRoundBean prb : _playerRoundBeanObjs)
		{
			prb.stopTimer();
		}
	}
	public void setOpenCardTaker()
	{
		_turnForBid = gameBean._playersBeansList.get(0)._playerId;
		_turn =  gameBean._playersBeansList.get(0)._playerId;
		firstPerson = gameBean._playersBeansList.get(0)._playerId;
		
		Apps.showLog("Open card Taker "+_turnForBid);
	}
	
	public void setTurn()
	{
		String preTurn = _turn;
		
		if(_turn == null)
		{
			_turn = _openCardTaker;
		}			
		else
		{
			int pos = 0;
			
			PlayerBean prePB = gameBean.getPlayerBean(_turn);
			pos = prePB.position;
			prePB._isTurn = false;
			
			pos++;
			
			if(pos >= gameBean.maxPlayers)
			{
				pos = pos%gameBean.maxPlayers;
			}
			
			for(PlayerBean pb : gameBean._playersBeansList)
			{
				if(pb.position == pos)
				{
					pb._isTurn = true;
					_turn = pb._playerId;
					break;
				}
			}
		}
		
		sendSyncDataToNewlySeatOccupiedPlayer();
		gameBean.getGameLogInstance().setTurn(gameBean, preTurn);
	}
	
	public void sendSyncDataToNewlySeatOccupiedPlayer()
	{
		if(_turn != null && isRemainingCardsDistributed)
		{
			GameBean gameBean = Apps.getGameBean(tableId);
			
			if(gameBean != null)
			{
				PlayerBean pb = gameBean.getPlayerBean(_turn);
				User sender = Apps.getUserByName(_turn);
				
				//System.out.println(" player bean "+pb);
				if(pb.isNewlyOccupiedSeat)
				{
					ISFSObject params = new SFSObject();
					
					params.putUtfString("tableId", gameBean.tableId );
					params.putUtfString("gameId", gameBean.gameId);
					params.putUtfString("aiPlayer", pb._aiPlayer);
					params.putUtfString("actualPlayer", pb._playerId);
					params.putUtfString("shuffler", gameBean.getShuffler());
					params.putUtfStringArray("initialPlayers", gameBean.initialPlayers);
					params.putUtfString("bidWonPerson", gameBean._roundBean._bidWonPerson);
					params.putUtfString("gender", Apps.getGender(pb._playerId));
					
					if(sender != null)
					{
						Commands.appInstance.send(Commands.CHANGE_PLAYER, params, sender);
						gameBean.getGameLogInstance().changePlayer(gameBean, sender.getName(), params);
					}
					
					params = GameApps.getGameObject(gameBean, params, _turn);
					
					if(sender != null)
					{
						Commands.appInstance.send(Commands.SINK_DATA, params, sender);
						gameBean.getGameLogInstance().syncDataSendToNewlyOccupiedSeatPlayer(gameBean, pb); 
						pb.isAutoPlay = false;
					}
				}
				
				pb.isNewlyOccupiedSeat = false;
			}
		}
	}
	
	public void setTurnForBid()
	{		
		String preTurnPlayer = _turnForBid;
		
		int pos = 0;
		for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(pb._isTurn)
			{
				pos = pb.position;
				pb._isTurn = false;
				break;
			}
		}
		
		pos++;
		
		if(pos >= gameBean.maxPlayers)
		{
			pos = pos%gameBean.maxPlayers;
		}
		
		for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(pb.position == pos)
			{
				pb._isTurn = true;
				_turnForBid = pb._playerId;
				break;
			}
		}
		
		gameBean.getGameLogInstance().setTurnForBid(gameBean, preTurnPlayer);
	}
	
	public int getBidTime()
	{
		int bidTime = Constant.BIDDING_TIME;
		
		if(isGameTypeSelected)
		{
			bidTime = Constant.BIDDING_TIME_AFTER_GAMETYPE_SELECTION;
		}
		
		return bidTime;
	}
	
	public PlayerRoundBean getPlayerRoundBeanObject(String player)
	{
		PlayerRoundBean prBean = null;
		
		for(PlayerRoundBean prb : _playerRoundBeanObjs)
		{
			if(prb._playerId.equals(player))
			{
				prBean = prb;
				break;
			}
		}
		
		return prBean;
	}

	public long getTimeConsumed()
	{
		return Apps.getTimeDifference(lastUpdateTime);
	}
	
	public SFSArray getBidEnableBits(String player)
	{		
		SFSArray sfsarr = new SFSArray();
		
		Apps.showLog(" getPlayerBidEnableBits");
		Apps.showLog(" GameType "+_gameType);
		Apps.showLog(" player "+player);
		
		BidBitBean bbb;		
		ISFSObject sfso;
		
		if(!gameBean._roundBean.isGameTypeSelected)
		{			
			Apps.showLog(" TurnForBid  "+getTurnForBid());
			// only one Player
			bbb = new BidBitBean(); 
			bbb.isSan = true;
			bbb.isHokom = true;
			bbb.isPass = true;
				
			PlayerBean pb = gameBean.getPlayerBean(getTurnForBid());
			
			 if((pb.position == 2 || pb.position == 3) && _bidRoundCount != 2)
			 {
				 bbb.isAshkal = true;
			 }

			sfso = new SFSObject();
			
			sfso.putUtfString("playerId", getTurnForBid());
			sfso.putIntArray("enableBidBits", bbb.getBitArrayList());	
			
			sfsarr.addSFSObject(sfso);
			
			gameBean.getGameLogInstance().bidEnableBits(gameBean, getTurnForBid(), bbb.getBitArrayList());
		}
		else
		{
			Apps.showLog("getPlayerBidEnableBits gameType == notnull");
			// remaining three players
			Integer playerPos = gameBean.getPlayerPosition(_bidWonPerson);
			Integer partnerPos = gameBean.getPartnerPosition(_bidWonPerson);
			
			for(PlayerBean pb : gameBean._playersBeansList)
			{
				if(pb._playerId.equals(_bidWonPerson))
				{
					// for the turn player.
					if(_gameType.equals(Commands.SAN) || _gameType.equals(Commands.ASHKAL))
					{
						// nothing will be availbe for the turn player who select bid san or ashkal.			
					}
					else if(_gameType.equals(Commands.HOKOM))
					{
						bbb = new BidBitBean();
						
						if(!gameBean._roundBean.isDouble && _bidRoundCount != 2)
						{
							bbb.isSan = true;
							bbb.isPass = true;
						}
						
						bbb.setDoubleBitsForHokom(playerPos, partnerPos, doubleCount, pb);
						bbb.setPassEnableBitForTeamate(gameBean, pb._playerId);
						sfso = new SFSObject();
						
						sfso.putUtfString("playerId", pb._playerId);
						sfso.putIntArray("enableBidBits", bbb.getBitArrayList());
						
						if(bbb.isHavingEnableBits())
						{
							sfsarr.addSFSObject(sfso);
						}
						
						gameBean.getGameLogInstance().bidEnableBits(gameBean, pb._playerId, bbb.getBitArrayList());
					}
				}
				else
				{
					bbb = new BidBitBean();
					// for the non turn players.
					if(_gameType.equals(Commands.SAN))
					{
						bbb.isPass = true;
						
						if(_bidRoundCount == 1)
						{
							// set true for all the previous players.
							if(pb.position < playerPos)
							{
								bbb.isSan = true;
							}
							
							// set false for partner
							if(pb.position == partnerPos)
							{
								bbb.isSan = false;
							}
							
							// set the double for opponents
							if(pb.position != playerPos && pb.position != partnerPos)
							{
								bbb.isDouble = true;
							}
							
							if(playerPos != 0)
							{
								if(pb.position == 2 || pb.position ==3)
								{
									if(pb.position != partnerPos)
									{
										bbb.isAshkal = true;
									}
								}
							}
						}
						else if(_bidRoundCount == 2)
						{
							// set the double for opponents
							if(pb.position != playerPos && pb.position != partnerPos)
							{
								bbb.isDouble = true;
							}
						}
					}
					else if(_gameType.equals(Commands.ASHKAL))
					{
						bbb.isPass = true;
						
						if(_bidRoundCount == 1)
						{
							if(pb.position > 1 && pb.position < playerPos)
							{
								bbb.isAshkal = true;
							}
							
							if(playerPos == 3 && pb.position == 0)
							{
								bbb.isSan = true;
							}	
							
							// set the double for opponents
							if(pb.position != playerPos && pb.position != partnerPos)
							{
								bbb.isDouble = true;
							}
						}
					}
					else if(_gameType.equals(Commands.HOKOM))
					{
						
						if(!gameBean._roundBean.isDouble)
						{
							bbb.isSan = true;
							bbb.isPass = true;
						}					
						
						// set the double for opponents
						bbb.setDoubleBitsForHokom(playerPos, partnerPos, doubleCount, pb);
						bbb.setPassEnableBitForTeamate(gameBean, pb._playerId);
						if(_bidRoundCount != 2 && !gameBean._roundBean.isDouble)
						{
							if(pb.position == 2 || pb.position == 3)
							{
								if(playerPos != pb.position)
								{
									bbb.isAshkal = true;
									bbb.isPass = true;
								}
							}
						} 
					}
					
					sfso = new SFSObject();
					
					sfso.putUtfString("playerId", pb._playerId);
					sfso.putIntArray("enableBidBits", bbb.getBitArrayList());
					
					if(bbb.isHavingEnableBits())
					{
						sfsarr.addSFSObject(sfso);
					}
					
					gameBean.getGameLogInstance().bidEnableBits(gameBean, pb._playerId, bbb.getBitArrayList());
				}
			}
		}	
		
		return sfsarr;
	}
	
	public SFSArray getPlayerBidEnableBits(String player)
	{
		SFSArray playerEnableBits = new SFSArray();
		
		SFSArray playerbits = gameBean._roundBean.getBidEnableBits(player);
		
		for(int i=0;i<playerbits.size();i++)
		{
			if(playerbits.getSFSObject(i).getUtfString("playerId").equals(player))
			{												
				playerEnableBits.addSFSObject(playerbits.getSFSObject(i));
				break;
			}							
		}
		
		return playerEnableBits;
	}
	
	public int getPosition(String player,ArrayList<String> playerspos)
	{
		int pos=0;
		for(int i=0;i<playerspos.size();i++)
		{
			if(playerspos.get(i).equals(player))
			{
				pos=i;
				break;
			}
		}
		return pos;
	}

	public String getTurnForBid(){
		return _turnForBid;
	}
	
	public String getTurn(){
		return _turn;
	}
	
	public String getSuit(Integer no)
	{
		String suit="";
		if(no>=1&&no<=8)
			suit = "clubs";
		else if(no>=9&&no<=16)
			suit = "diamonds";
		else if(no>=17&&no<=24)
			suit = "hearts";
		else if(no>=25&&no<=32)
			suit = "spades";
		
		return suit;
	}
	
	
	public SFSArray getPlayersCards()
	{
		SFSArray player_cards = new SFSArray();		
		
		for(int i=0; i<maxPlayers;i++)
		{			
			ISFSObject sfso = new SFSObject();
			sfso.putIntArray("cards", sortCards(_playerRoundBeanObjs.get(i)._cards));
			sfso.putUtfString("playerid", _playerRoundBeanObjs.get(i)._playerId);
			sfso.putBool("isDeclaredBonus", _playerRoundBeanObjs.get(i).isDeclaredBonus);
			sfso.putUtfString("gender", Apps.getGender(_playerRoundBeanObjs.get(i)._playerId));
			
			player_cards.addSFSObject(sfso);
		}	
		
		return player_cards;
	}
	
	private ArrayList<Integer> sortCards(ArrayList< Integer> aalist){
		
		ArrayList<Integer>cards = new ArrayList<Integer>();		
		ArrayList<Integer> clubs = new ArrayList<Integer>();
		ArrayList<Integer> diamonds = new ArrayList<Integer>();
		ArrayList<Integer> hearts = new ArrayList<Integer>();
		ArrayList<Integer> spades = new ArrayList<Integer>();
		
		for(int i=0; i<aalist.size();i++)
		{
			if(aalist.get(i)>= 1 && aalist.get(i)<=8)
				clubs.add(aalist.get(i));
			else if(aalist.get(i)>= 9 && aalist.get(i)<=16)
				diamonds.add(aalist.get(i));
			else if(aalist.get(i)>= 17 && aalist.get(i)<=24)
				hearts.add(aalist.get(i));
			else if(aalist.get(i)>= 25 && aalist.get(i)<=32)
				spades.add(aalist.get(i));			
		}	
		
		Collections.sort(hearts);
		Collections.sort(spades);
		Collections.sort(diamonds);
		Collections.sort(clubs);
		
		if(hearts.size()>0)
		{				
			for(int i=hearts.size();i>0;i--){				
				cards.add(hearts.get(i-1));
			}
		}
		if(spades.size()>0)
		{
			for(int i=spades.size();i>0;i--){
				cards.add(spades.get(i-1));
			}
		}
		if(diamonds.size()>0)
		{
			for(int i=diamonds.size();i>0;i--){
				cards.add(diamonds.get(i-1));
			}
		}
		if(clubs.size()>0)
		{
			for(int i=clubs.size();i>0;i--){
				cards.add(clubs.get(i-1));
			}
		}		
		return cards;
	}
	
	
	public Integer getOpencard()
	{
		_openCardId = openCardsList.get(0);
		
		return _openCardId;
	}

	private void shuffelCards()
	{		
		_cardsInHand = new ArrayList<Integer>();
		_cardsPlayed = new ArrayList<Integer>();
		
		Random randomGenerator = new Random();
		for (int idx = 1; idx <= 32; ++idx){							
			cards.add(idx);
			_cardsInHand.add(idx);
		}
		
		for (int idx = 1; idx <= 32; ++idx)
		{			
	      int randomInt = randomGenerator.nextInt(cards.size());
	      _deckCards.add(cards.get(randomInt));
	      cards.remove(randomInt);
	    }
		
		
		
		//AppMethods.showLog(_deckCards.toString());
		//AppMethods.showLog(_shufflerPosition);
		
		int no=_deckCards.size()-1;
		
		// For 3 cards
		for(int i=1;i<=maxPlayers;i++)
		{
			for(int j=0;j<3;j++)
			{
				int agno=0;
				if((i+_shufflerPosition)>=maxPlayers)
				{
					agno = (i+_shufflerPosition)%maxPlayers;				
				}
				else
				{				
					agno = (i+_shufflerPosition);				
				}
				
				_playerRoundBeanObjs.get(agno)._cards.add(_deckCards.get(no));				
				_deckCards.remove(no);				
				no--;
			}
		}
		// For 2 cards
		for(int i=1;i<=maxPlayers;i++)
		{
			for(int j=0;j<2;j++)
			{
				int agno=0;
				if((i+_shufflerPosition)>=maxPlayers)
				{
					agno = (i+_shufflerPosition)%maxPlayers;				
				}
				else
				{				
					agno = (i+_shufflerPosition);				
				}
				
				_playerRoundBeanObjs.get(agno)._cards.add(_deckCards.get(no));				
				_deckCards.remove(no);				
				no--;				
			}
		}
		
		/** Open card*/
		openCardsList.add(_deckCards.get(no));
		_deckCards.remove(no);
		no--;	
		
		if(openCardsList.get(0)>= 1 && openCardsList.get(0)<=8)
		{
			_trumpSuitType = "clubs";	
		}				
		else if(openCardsList.get(0)>=9 && openCardsList.get(0)<=16)
		{
			_trumpSuitType = "diamonds";
		}			
		else if(openCardsList.get(0)>=17 && openCardsList.get(0)<=24)
		{
			_trumpSuitType = "hearts";
		}			
		else if(openCardsList.get(0)>=25 && openCardsList.get(0)<=32)
		{
			_trumpSuitType = "spades";
		}
		
		_bidRoundCount ++;
	}
	
	public void distributeRemainingCards()
	{			
		// Stop the timers
		for(int i=0;i<_playerRoundBeanObjs.size();i++)
		{				
			try{
				_playerRoundBeanObjs.get(i).stopTimer(); 
			}
			catch(Exception exception)
			{Apps.showLog("Player Round Bean Timer Exc "+exception);}				
		}
		
		setLastUpdatedTime();
		int no=_deckCards.size()-1;
		
		// Set the position of card Taker
		
		for(int i=0;i<_players.size();i++)
		{
			if(_players.get(i).equals(_openCardTaker))
			{
				_shufflerPosition =  i;
				break;
			}
		}
		
        // Arrange the total cards for card Taker
		//AppMethods.showLog("Open card Taker :"+ _playerRoundBeanObjs.get(_shufflerPosition)._playerId +"=="+_openCardTaker);
		_playerRoundBeanObjs.get(_shufflerPosition)._cards.add(openCardsList.get(0));
		openCardsList.remove(0);
		
		for(int i=0;i<2;i++)
		{
			_playerRoundBeanObjs.get(_shufflerPosition)._cards.add(_deckCards.get(no));				
			_deckCards.remove(no);				
			no--;
		}
		
		// For 3 cards
		for(int i=1;i<=maxPlayers-1;i++)
		{
			
			for(int j=0;j<3;j++)
			{				
				// Arrange the total cards
				int agno=0;
				if((i+_shufflerPosition)>=maxPlayers)
				{
					agno = (i+_shufflerPosition)%maxPlayers;				
				}
				else
				{				
					agno = (i+_shufflerPosition);				
				}
				
				_playerRoundBeanObjs.get(agno)._cards.add(_deckCards.get(no));				
				_deckCards.remove(no);				
				no--;
			}			
		}			
	}
	
	public void startTimer(int seconds, String command, GameBean gameBean, String player) 
	{					
		try{
			stopTimer();		         		         	         
		}catch(Exception exception){}
		
		//System.out.println("StartTimer : GameRoundBean : Seconds "+ seconds +" : Command "+command);
		
		timer = new Timer(seconds*1000, new MyTimerActionListener(command, gameBean.tableId, player));
		timer.start();
		lastUpdateTime = Apps.getCurrentTime();
		isStopped = false;
		
		String var = " RoundBean_Timer_Started command "+command+" delayTime "+seconds+"  player "+player;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "Timer", var);
		
		gameBean.addLog(lb);
		lb = null;
	}
	
	public void stopTimer()
	{
		if(timer != null)
		{
			//System.out.println("StopTimer : GameRoundBean");	
			
			String var = " Round Timer Stopped delayTime "+timer.getDelay()/1000+" runningTime "+Apps.getTimeDifference(lastUpdateTime);
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "Timer", var);
			GameBean gameBean = Apps.getGameBean(tableId);
			gameBean.addLog(lb);
			
			lb = null;	
			
			try{ timer.stop();}catch(Exception e){}
			timer = null;
		}
		
		isStopped = true;
	}
}



class MyTimerActionListener implements ActionListener 
{	
	String commandStr = "";	
	GameBean gameBean;
	String tableId;
	String player = "";
	
	public MyTimerActionListener(String str,String tableID, String playerStr)
	{
		commandStr = str;		
		tableId = tableID;
		player = playerStr;
	}

	public void actionPerformed(ActionEvent e) 
	{
		gameBean = Apps.getGameBean(tableId);
		
		if(gameBean != null)
		{
			
			Timer timer = gameBean._roundBean.getTimer();
			
			if(timer != null)
			{
				int delayTime = timer.getDelay()/1000;
				String var = "MyTimerActionListener Completed player "+player+" commandStr "+commandStr
								+" delayTime "+delayTime+" runningTime "+Apps.getTimeDifference(gameBean._roundBean.getLastUpdate());
				LogBean lb = new LogBean();
				lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "Timer", var);
				gameBean.addLog(lb);
				lb = null;
			}
			
			//Stop the timer
			try{ 			
				gameBean._roundBean.stopTimer(); 
			}catch(Exception exception){Apps.showLog("Round Bean Timer Exc "+exception);}
			
			if(commandStr.equals(Commands.BID))
			{
				if(!gameBean._roundBean.getTurnForBid().equals(player))
				{
					Validations.raiseListenerPlayerAndActualPlayerNotMatchException(gameBean, player, gameBean._roundBean.getTurnForBid(), commandStr, "RoundBean");
					player = gameBean._roundBean.getTurnForBid();
				}
			}
			else if(commandStr.equals(Commands.DISCARD))
			{
				if(!gameBean._roundBean.getTurn().equals(player))
				{
					Validations.raiseListenerPlayerAndActualPlayerNotMatchException(gameBean, player, gameBean._roundBean.getTurn(), commandStr, "RoundBean");
					player = gameBean._roundBean.getTurn();
				}
			}			
			
			new AIBsn().playAI(commandStr, gameBean, player);
		}
	}
}
