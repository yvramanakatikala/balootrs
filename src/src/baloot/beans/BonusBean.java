/**
 * 
 */
package src.baloot.beans;

import java.util.ArrayList;

public class BonusBean {
	
	public String _playerId;
	public String _bonusType;
	public ArrayList<Integer> _cards = new ArrayList<Integer>();
	public boolean _isShowedCards  = false;	
	
	public void setPlayerId(String playerid){
		_playerId = playerid;
	}
	
	public void setBonusType(String bonusType){
		_bonusType = bonusType;
	}
	
	public void setCards(ArrayList<Integer> cards){
		_cards = cards;
	}
	public void setShowCardsBits(boolean sh){
		_isShowedCards = sh;
	}
	
	public ArrayList<Integer> getCards()
	{		
		return _cards;
	}
	
}
