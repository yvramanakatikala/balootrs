/**
 * 
 */
package src.baloot.beans;

import java.util.ArrayList;

public class BidBitBean {

	public boolean isSan;
	public boolean isHokom;
	public boolean isPass;
	public boolean isNumbers;
	public boolean isAshkal;
	public boolean isDouble;
	public boolean isDoubleThree;
	public boolean isDoubleFour;	

	public ArrayList<Integer> getBitArrayList()
	{
		ArrayList<Integer> bitArr = new ArrayList<Integer>();
		
		if(isSan)
			bitArr.add(1);
		else
			bitArr.add(0);
		
		if(isHokom)
			bitArr.add(1);
		else
			bitArr.add(0);
		
		if(isPass)
			bitArr.add(1);
		else
			bitArr.add(0);
		
		if(isNumbers)
			bitArr.add(1);
		else
			bitArr.add(0);

		if(isAshkal)
			bitArr.add(1);
		else
			bitArr.add(0);
		
		if(isDouble)
			bitArr.add(1);
		else
			bitArr.add(0);
		
		if(isDoubleThree)
			bitArr.add(1);
		else
			bitArr.add(0);
		
		if(isDoubleFour)
			bitArr.add(1);
		else
			bitArr.add(0);	
		
		return bitArr;
	}
	
	public boolean isHavingEnableBits()
	{
		boolean ishaving = false;
		
		if(getBitArrayList().contains(1))
		{
			ishaving = true;
		}
		
		return ishaving;
	}	
	
	public void setDoubleBitsForHokom(int playerPos, int partnerPos, int dooubleCount, PlayerBean pb)
	{
		if(dooubleCount == 0 || dooubleCount == 2)
		{
			if(pb.position != playerPos && pb.position != partnerPos)
			{
				setDoubleCount(dooubleCount);
			}
		}
		else if(dooubleCount == 1)
		{
			if(pb.position == playerPos || pb.position == partnerPos)
			{
				setDoubleCount(dooubleCount);
			}
		}
	}
	
	private void setDoubleCount(int dooubleCount)
	{
		if(dooubleCount == 0)
		{
			isDouble = true;
			isPass = true;
		}
		else if(dooubleCount == 1)
		{
			isDoubleThree = true;
			isPass = true;
		}
		else if(dooubleCount == 2)
		{
			isDoubleFour = true;
			isPass = true;
		}
	}
	
	public void setPassEnableBitForTeamate(GameBean gameBean, String player)
	{
		if(gameBean._roundBean.isDouble)
		{
			String bidSelectedPerson = gameBean._roundBean._bidSelectedPerson;
			
			if(player.equals(gameBean.getPartner(bidSelectedPerson)))
			{
				isPass = true;
			}
		}
	}
}
