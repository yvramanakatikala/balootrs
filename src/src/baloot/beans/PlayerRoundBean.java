/**
 * 
 */
package src.baloot.beans;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.Timer;

import src.baloot.bsn.AIBsn;
import src.baloot.constants.Constant;
import src.baloot.exce.Validations;
import src.baloot.exce.beans.LogBean;
import src.baloot.game.PlayerRound;
import src.baloot.utils.Apps;

public class PlayerRoundBean extends PlayerRound{
	
	public String _playerId;
	private String tableId;
	public Integer position = null;
	public ArrayList<Integer> _cards  = new ArrayList<Integer>();
	public ArrayList<Integer> _playedcards = new ArrayList<Integer>();	
	public ArrayList<Integer> _wonCards = new ArrayList<Integer>();
	public int _totalScore = 0 ;
	public int _currentCard = -1;
	public int _round = 1;
	public String _bidSelected = "null";
	public String _secondRoundBidSelected = "null";
	public boolean isDeclaredHighest = false;	
	public ArrayList<Integer> _bonusCards  = new ArrayList<Integer>();
	public Integer _messagesCount = 0;
	public ArrayList<MessageBean> _aiMessages = new ArrayList<MessageBean>();
	public ArrayList<MessageBean> _hisMessagesForTeammate = new ArrayList<MessageBean>();
	public ArrayList<Integer> _firstPlayedCard = new ArrayList<Integer>();
	private Date lastUpdateTime = null;
	
	public boolean isDeclaredBonus = false;
	public boolean isDeclaredBalootBonus = false;
	
	
	public int tricksWonCount = 0;
	public int continuousTrickWonCount = 0;
	
	
	private Timer timer;
	
	public Timer getTimer()
	{
		return timer;
	}
	public Date getLastUpdateTime()
	{
		return lastUpdateTime;
	}
	
	public void setTableId(String tableId)
	{
		this.tableId = tableId;
	}
	
	public void incrementTricksWonCount()
	{
		tricksWonCount++;
	}
	public void startTimer(int seconds, String command, GameBean gameBean, String player) 
	{	
		//AppMethods.showLog("Start timer in Player round Bean");
		
		try{
			stopTimer();		         		         	         
		}catch(Exception exception){}	
		
		//System.out.println("StartTimer : PlayerRoundBean : Command "+command +" : Player "+player);
		timer = new Timer(seconds*1000, new MyPlayerTimerActionListener(command, gameBean, player));	
		timer.start();
		lastUpdateTime = Apps.getCurrentTime();
		
		String var = " PlayerRoundBean Timer Started command "+command+" delayTime "+seconds+"  player "+player;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "Timer", var);
		
		gameBean.addLog(lb);
		lb = null;
	}
	
	public void stopTimer()
	{
		if(timer != null)
		{
			//System.out.println("StopTimer : PlayerRoundBean");
			
			String var = " Round Timer Stopped delayTime "+timer.getDelay()/1000+" runningTime "+Apps.getTimeDifference(lastUpdateTime);
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "Timer", var);
			GameBean gameBean = Apps.getGameBean(tableId);
			gameBean.addLog(lb);
			
			lb = null;
			
			try{ timer.stop();}catch(Exception e){}
			timer = null;
		}
	}
	
}

class MyPlayerTimerActionListener implements ActionListener 
{	
	String command = "";	
	GameBean gameBean;
	String player = "";
	public MyPlayerTimerActionListener(String command, GameBean gameBean, String player)
	{
		this.command = command;		
		this.gameBean = gameBean;
		this.player = player;
	}
	
	public void actionPerformed(ActionEvent e)
	{
					
		//System.out.println("ActionListener :PlayerRoundBean "+command);
		
		PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(player);
		if(prb != null)
		{
			// do nothing.
		}
		else
		{
			// player becomes as ai so get the left player bean.
			PlayerBean pb  = gameBean.getLeftPlayerBean(player);
			prb = gameBean._roundBean.getPlayerRoundBeanObject(pb._playerId);
			
			Validations.raiseListenerPlayerAndActualPlayerNotMatchException(gameBean, player, prb._playerId, command, "PlayerRoundBean");
			
		}
		
		Timer timer = prb.getTimer();
		
		if(timer != null)
		{
			int delayTime = timer.getDelay()/1000;
			long runningTime = Apps.getTimeDifference(gameBean._roundBean.getPlayerRoundBeanObject(prb._playerId).getLastUpdateTime());
			String var = "MyPlayerTimerActionListener Completed player "+player+" command "+command+" delayTime "+delayTime+" runningTime "+runningTime;
							
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "Timer", var);
			gameBean.addLog(lb);
			lb = null;
		}
		
		try{ prb.stopTimer();}catch(Exception exception){Apps.showLog("Player Round Bean Timer Exc "+exception);}
		
		new AIBsn().playAI(command, gameBean, prb._playerId);
	}
}
