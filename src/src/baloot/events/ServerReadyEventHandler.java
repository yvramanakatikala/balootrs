package src.baloot.events;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;

import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerBean;
import src.baloot.beans.PlayerProfileBean;
import src.baloot.beans.TableBean;
import src.baloot.constants.Constant;
import src.baloot.exce.Validations;
import src.baloot.exce.beans.LogBean;
import src.baloot.logs.FakeGameLog;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;

public class ServerReadyEventHandler extends BaseServerEventHandler{
	
	@Override
	public void handleServerEvent(ISFSEvent arg0) throws SFSException
	{
		Apps.showLog("$$$$$$$$$$$$$$$ ServerReadyEventHandler $$$$$$$$$$$$$$$ ");		
	
		try{
			String str = InetAddress.getByName("192.168.1.198").getHostName();
			
			//System.out.println("  this_is_host_name "+str);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		for(String player : Commands.appInstance.proxy.getFakePlayersList())
		{
			User fakePlayer = getApi().createNPC(player, Commands.appInstance.getParentZone(), false);
			
			Commands.appInstance.fakePlayers.add(fakePlayer);
			
			//System.out.println(" fake_user_created "+fakePlayer+"    name "+fakePlayer.getName());		
			Room room = Apps.getRoomByName(Constant.LOBBY);			
				
			Apps.joinLobbyRoom(fakePlayer, room);	
			
			//trace("fake_player_ip_address "+fakePlayer.getIpAddress());
			
			PlayerProfileBean pfb = new PlayerProfileBean(fakePlayer.getName());
			
			Integer userId = Apps.getPlayerId(player);
			String gender = Commands.appInstance.proxy.getGender(userId);
			
			if(gender != null)
			{
				pfb.gender = gender;
			}
			else
			{
				Validations.raiseGenderNullInSFSReady(player, userId);
			}
			
			Commands.appInstance.playerProfiles.put(fakePlayer.getName(), pfb);
			
			String log = " player "+pfb.name+"  userid "+userId+"  gender "+pfb.gender;
			
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P0", Constant.SERVER, "PlayerGender", log);
			FakeGameLog.addLog(lb);
		}
		
		Collections.shuffle(Commands.appInstance.fakePlayers);	
		
		//Commands.appInstance.calculatePoolSize();
		
		
		
		String var = " fakePlayersSize  "+Commands.appInstance.fakePlayers.size();
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", Constant.SERVER, "ServerReadyEvent", var);
		FakeGameLog.addLog(lb);
		
		if(Commands.appInstance.gameCV.isAiSpan())
		{			
			//int maxTables = (Commands.appInstance.fakePlayers.size() - Commands.appInstance.poolSize)/2;
			
			int maxTables = (Commands.appInstance.fakePlayers.size())/2;
			
			if(maxTables >= 2)
			{
				var = " fakePlayersSize  "+Commands.appInstance.fakePlayers.size()+" maxTables "+maxTables;
				lb = new LogBean();
				lb.setValues(Constant.LOG, "P0", Constant.SERVER, "InitFakeTables", var);
				FakeGameLog.addLog(lb);
				
				initAISpaning(maxTables);
			}
			else
			{
				var = " fakePlayersSize  "+Commands.appInstance.fakePlayers.size()+" maxTables "+maxTables;
				lb = new LogBean();
				lb.setValues(Constant.EXC, "P0", Constant.SERVER, "InitFakeTablesFailed", var);
				FakeGameLog.addLog(lb);
			}
		}
	}
	
	public void initAISpaning(int maxTables)
	{	
		for(int k=0;k<maxTables;k++)
		{
			TableBean tb = Commands.appInstance.publicTables.get(k);
			
			ArrayList<User> fakeList = new ArrayList<User>();
			
			for(User user : Commands.appInstance.fakePlayers)
			{
				fakeList.add(user);
				
				if(fakeList.size() == 2)
				{
					break;
				}
			}
			
			
			if(fakeList.size() == 2)
			{
				Commands.appInstance.fakePlayers.removeAll(fakeList);
				
				String tableId = tb.getTableId();
				Apps.createGameRoom(tableId);
				Room room = Apps.getRoomByName(tableId);
				
				GameBean gameBean = new GameBean();
				gameBean.tableId = tableId;
				
				for(int i=0;i<2;i++)
				{
					if(i == 0 || i == 1)
					{
						String fakePlayerName = "ramana";
						
						Apps.joinGameRoom(fakeList.get(i), room);
						
						if(i == 0)
						{
							fakePlayerName = fakeList.get(i).getName();
							gameBean.host = fakePlayerName;
						}
						else if(i ==1)
						{
							fakePlayerName = fakeList.get(i).getName();
						}
						
						
						// game logic
						
						gameBean._players.add(fakePlayerName);
						
						PlayerBean pb = new PlayerBean();
						pb._playerId = fakePlayerName;
						pb._aiPlayer = fakePlayerName;					
						pb.position = i;
						pb.isFakePlayer = true;
						
						gameBean._playersBeansList.add(pb);	
						
						if(pb.position == 0)
						{
							pb.isHost = true;
						}
					}					
				}
				
				gameBean.isHostJoined = true;		
				
				// Add the current gameBean to games list
				Commands.appInstance.getGames().put(tableId, gameBean);
				gameBean.incrementTicket();
				
				gameBean.tableType = Constant.PUBLIC;
				
				// Add Log					
				String var = " tableId "+tableId+" players "+gameBean._players;
				LogBean lb = new LogBean();
				lb.setValues(Constant.LOG, "P0", Constant.SERVER, "StaringFakeGame", var);
				gameBean.addLog(lb);
				gameBean.writeGameLog();	
				FakeGameLog.addLog(lb);
				
				gameBean.initGame();
					
				// Update Lobby
				Commands.appInstance.ulBsn.updateLobby(gameBean);
			}						
		}			
	}
}
