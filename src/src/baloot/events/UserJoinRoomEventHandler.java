/**
 * 
 */
package src.baloot.events;

import src.baloot.utils.Apps;


import src.baloot.utils.Commands;

import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;

public class UserJoinRoomEventHandler extends BaseServerEventHandler{
	
	public synchronized void handleServerEvent(ISFSEvent event) throws SFSException
	{
		//Apps.showLog("***********UserJoinRoomEventHandler*********");
		Room room = (Room) event.getParameter(SFSEventParam.ROOM);	
		User user = (User) event.getParameter(SFSEventParam.USER);
		//boolean isGuestUser = Apps.isGuestUser(user.getName());
		
		Apps.showLog("  UserJoinRoomEventHandler >> "+user.getName()+"  room "+room.getName());		
		
		if(room.getName().equals("Lobby"))
		{
			if( !user.getName().equals("admin") )
			{
				Commands.appInstance.pingBsn.addConnectedUser(user);
			}
			
			Room lobbyRoom = Commands.appInstance.ulBsn.getLobbyRoom();
			
			try{
					Apps.joinLobbyRoom(user, lobbyRoom);
					
					ISFSObject sfso = new SFSObject();	
					
					sfso.putUtfString("command", "getLobby");
					
					Commands.appInstance.handleClientRequest(Commands.GET_USER_DETAILS, user, sfso);
					
					
					Commands.appInstance.handleClientRequest(Commands.GET_LOBBY, user, sfso);	
					
					//System.out.println("User_successfully_joined_lobby_room "+lobbyRoom.getName());
					
					Commands.appInstance.sgBsn.sendCurrentTables(user.getName());
				
		   }catch(Exception e)
		   {
		    	Apps.showLog("Exception_while_joining_lobby_room "+lobbyRoom.getName());
		    	e.printStackTrace();
		   }
			
			Commands.appInstance.ulBsn.sendOnlineUsersCount();
		}
	}
}
