/**
 * 
 */

package src.baloot.events;
import java.util.List;

import src.baloot.beans.PlayerProfileBean;
import src.baloot.constants.Constant;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;

public class UserDisconnectedEventHandler extends BaseServerEventHandler {
	
	public void handleServerEvent(ISFSEvent event) throws SFSException
	{
		User user = (User) event.getParameter(SFSEventParam.USER);
		
		Apps.showLog("********* UserDisconnectedEventHandler ****** "+user.getName());
		
		@SuppressWarnings("unchecked")
		List<Room> joinedRooms = (List<Room>)event.getParameter(SFSEventParam.JOINED_ROOMS);
		
		
		String player = user.getName();
		
		for(Room room : joinedRooms)
		{
			if(room.getGroupId().equals(Constant.GAME_GROUP))
			{
				Commands.appInstance.getDisconnectionInstance().disconnectUser(room, player);
			}
		}			
		
		Apps.removeGuestUser(player);
		
		Commands.appInstance.playerProfiles.remove(player);
		Commands.appInstance.getUserObjs().remove(player);
		Commands.appInstance.getPlayerIds().remove(player);
		
		Commands.appInstance.ulBsn.sendOnlineUsersCount();
	}
}
