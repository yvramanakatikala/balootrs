/**
 * 
 */
package src.baloot.events;

import src.baloot.beans.PlayerProfileBean;
import src.baloot.constants.Constant;
import src.baloot.exce.Validations;
import src.baloot.exce.beans.LogBean;
import src.baloot.logs.UserLogs;
import src.baloot.ping.UserObject;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.bitswarm.sessions.ISession;
import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSConstants;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.exceptions.SFSErrorCode;
import com.smartfoxserver.v2.exceptions.SFSErrorData;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.exceptions.SFSLoginException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;

public class LoginEventHandler extends BaseServerEventHandler{
	
	public void handleServerEvent(ISFSEvent event) throws SFSException
	{
		
		String userName = (String) event.getParameter(SFSEventParam.LOGIN_NAME);
		String cryptedPass = (String) event.getParameter(SFSEventParam.LOGIN_PASSWORD);
		ISession session = (ISession) event.getParameter(SFSEventParam.SESSION);
		Apps.showLog("***********LoginEventHandler*********** "+userName);
		
		//Apps.showLog("User Name "+userName);
		//Apps.showLog("User Currrent IP:"+session.getAddress());
		//Apps.showLog("GetFullServerIpAddress IP:"+session.getServerAddress());
		
		
		
		Integer userId = null;
		String dbPassword = null;
		String dbUsername = null;
		
		if(userName.startsWith("##"))
		{
			//trace(" ### CUSTOM_LOGIN_SUCCESSFUL_FOR_DUMMY_USER _"+userName);
		}
		else if(userName.startsWith("$$"))
		{
			//trace(" ### CUSTOM_LOGIN_SUCCESSFUL_FOR_DUMMY_GUEST_USER _"+userName);
		}
		else if(userName.startsWith("@@"))
		{
			// guest user login
			if(Apps.isSameGuestIP(session.getAddress(), userName))
			{
				SFSErrorData data = new SFSErrorData(SFSErrorCode.LOGIN_BANNED_IP);
				
			     //trace(" ## Try_to_Join_with_Same_IPD ##     ");
				 String var = "Login Exception Try_to_Join_with_Same_IP";
				 LogBean lb = new LogBean();
				 lb.setValues(Constant.ERR, "P0", userName, "Login", var);
				 UserLogs.addUserLog(lb, "NA");
				 
				 throw new SFSLoginException("Try_to_Join_with_Same_IP", data);
			}
			else
			{
				trace(" ### trying_to_guestlogin_success_"+userName);
				userName = Apps.getGuestName(session);
				
				//trace(" ### trying_to_guestname"+userName);
				PlayerProfileBean pfb = Commands.appInstance.playerProfiles.get(userName);
				
				if(pfb == null)
				{
					pfb = new PlayerProfileBean(userName);
					pfb.gender = Constant.MALE;
					pfb.isGuestUser = true;
					
					Commands.appInstance.playerProfiles.put(userName, pfb);
				}			
				
				ISFSObject outData = (ISFSObject) event.getParameter(SFSEventParam.LOGIN_OUT_DATA);
		        outData.putUtfString(SFSConstants.NEW_LOGIN_NAME, userName);
		        
		        Commands.appInstance.proxy.insetGuestUserRecord(session.getAddress());
				
				//trace(" @@@@@ CUSTOM_LOGIN_SUCCESSFUL_FOR_GUEST_USER _"+userName);
			}
			
		}
		else if(Apps.isGuestUser(userName))
		{
			/**
			 * When user disconnected(Not Refresh). ie disabling LAN connected or unplug the net cable
			 * At this movement client sends the user name like 192.168.1.101...
			 * so we must login this user(if user object already exists in the server then SFS automatically throws UserAlreadyExists Exception) 
			 */
			
			userName = Apps.getGuestName(session);
			
			//trace(" ### trying_to_guestname_after_disconnection "+userName);
			PlayerProfileBean pfb = Commands.appInstance.playerProfiles.get(userName);
			
			if(pfb == null)
			{
				pfb = new PlayerProfileBean(userName);
				pfb.gender = Constant.MALE;
				pfb.isGuestUser = true;
				
				Commands.appInstance.playerProfiles.put(userName, pfb);
			}			
			
			ISFSObject outData = (ISFSObject) event.getParameter(SFSEventParam.LOGIN_OUT_DATA);
	        outData.putUtfString(SFSConstants.NEW_LOGIN_NAME, userName);
	        
	        Commands.appInstance.proxy.insetGuestUserRecord(session.getAddress());
	        
	        
	        String var = "Login_successful_after_disconnection "+userName;
	        LogBean lb = new LogBean();
		    lb.setValues(Constant.LOG, "P0", userName, "Login", var);
		    UserLogs.addUserLog(lb, "NA");
			
			//trace(" @@@@@ CUSTOM_LOGIN_SUCCESSFUL_FOR_GUEST_USER_AFTER_DISCONNECTION "+userName);
		}
		else if(!userName.startsWith("##"))
		{
			// CustomLogin
			//trace(" ### trying_to_reallogin_success_"+userName);
			
			userId = Commands.appInstance.proxy.getUserID(userName);
			if(userId != null)
			{
				dbPassword = Commands.appInstance.proxy.getPassword(userId);
				dbUsername = Commands.appInstance.proxy.getStoredDatabaseUserName(userName);
			}

			/*System.out.println("userName_is : "+userName);
			System.out.println("userId_is   : "+userId);
			System.out.println("password_is : "+dbPassword);
			System.out.println("password_client : "+cryptedPass);*/

			LogBean lb;
			if(userId == -1 || userId == null || userId.equals(""))
			{
				SFSErrorData data = new SFSErrorData(SFSErrorCode.LOGIN_BAD_USERNAME);
			   // trace(" ## USER NOT REGISTERED ##");
			    
			    
			    String var = "Login Exception user not registered "+userName;
			    lb = new LogBean();
			    lb.setValues(Constant.ERR, "P0", userName, "Login", var);
			    UserLogs.addUserLog(lb, "NA");
			    
			    throw new SFSLoginException("Please Register ", data);	
			}
			else
			{
				if(dbPassword == null )
				{
					SFSErrorData data = new SFSErrorData(SFSErrorCode.LOGIN_GUEST_NOT_ALLOWED);
				    //trace(" ## USER NOT REGISTERED ##");
				    
				    
				    String var = "Login Exception dbPassword is null";
				    lb = new LogBean();
				    lb.setValues(Constant.ERR, "P0", userName, "Login", var);
				    UserLogs.addUserLog(lb, "NA");
				    
				    throw new SFSLoginException("Please Register ", data);			
				}
				else if (dbPassword.equals("") || dbPassword == null)
				{
					SFSErrorData data = new SFSErrorData(SFSErrorCode.LOGIN_BAD_PASSWORD);
				    //trace(" ## You must enter a password. ##");
				    
				    
				    String var = "Login Exception dbpassword is null or space";
				    lb = new LogBean();
				    lb.setValues(Constant.ERR, "P0", userName, "Login", var);
				    UserLogs.addUserLog(lb, "NA");
				    
				    throw new SFSLoginException(" You must enter a password.", data);
				}			
				else if (!getApi().checkSecurePassword(session, dbPassword, cryptedPass))
				{
			         SFSErrorData data = new SFSErrorData(SFSErrorCode.LOGIN_BAD_PASSWORD);
			         //trace("Login failed for user     : "+userName);  
			         
					    
				     String var = "Login Exception login with badpassword";
				     lb = new LogBean();
				     lb.setValues(Constant.ERR, "P0", userName, "Login", var);
				     UserLogs.addUserLog(lb, "NA");
			         
			         throw new SFSLoginException("Login failed for user: "  + userName, data);
				}					
				else if(!Commands.appInstance.proxy.isEmailActivated(userId))
				{
					SFSErrorData data = new SFSErrorData(SFSErrorCode.INVITATION_NOT_VALID);
				    data.addParameter("NotActivated");
				    //trace(" ## Email Not Activated .Plz Activate ##");
				    
				    
				    String var = "Login Exception email not activated";
				    lb = new LogBean();
				    lb.setValues(Constant.ERR, "P0", userName, "Login", var);
				    UserLogs.addUserLog(lb, "NA");
				    
				    throw new SFSLoginException(" Email Not Activated .Plz Activate ", data);
				}					
				else
				{    	
				    String var = "Login Success";
				    lb = new LogBean();
				    lb.setValues(Constant.LOG, "P0", userName, "Login", var);
				    UserLogs.addUserLog(lb, "NA");
				    
				    Commands.appInstance.getPlayerIds().put(userName, userId);	
				    
				    UserObject userObj = Commands.appInstance.getUserObjs().get(userName);
				    
				    if(userObj == null)
				    {
				    	userObj = new UserObject();
				    	userObj.setUserName(userName);	
				    	
				    	Commands.appInstance.getUserObjs().put(userName, userObj);
				    }
				    
				    Commands.appInstance.proxy.setFirstLogin(userName, userId, session.getAddress());
				    
				    ISFSObject outData = (ISFSObject) event.getParameter(SFSEventParam.LOGIN_OUT_DATA);
			        outData.putUtfString(SFSConstants.NEW_LOGIN_NAME, dbUsername);
				      
				      //trace("##################################################");
				     // trace("    "+userName+"            "+dbUsername+"        ");
				     // trace("##################################################");
				     // trace("##################################################");
				      
					//trace("############  CUSTOM_LOGIN_SUCCESSFUL  #############  : "+userName);   
					
					PlayerProfileBean pfb = Commands.appInstance.playerProfiles.get(userName);
					
					if(pfb == null)
					{
						pfb = new PlayerProfileBean(userName);
						
						String gender = Commands.appInstance.proxy.getGender(userId);
						
						if(gender != null)
						{
							pfb.gender = gender;
						}
						else
						{
							Validations.raiseGenderNullInLogin(userName, userId);
						}						
						
						Commands.appInstance.playerProfiles.put(userName, pfb);
					}					
				}
			}
		
		}
		else
		{
			trace(" CustomLoginException username "+userName);
		}
	}
	
	public void dummy()
	{/*
		if(isArabicUser)
		{
			userId = Commands.appInstance.proxy.getArabicUserID(userName);
			dbPassword = Commands.appInstance.proxy.getArabicPassword(userId);
			
			LogBean lb;
			if(userId == null || userId.equals(""))
			{
				SFSErrorData data = new SFSErrorData(SFSErrorCode.LOGIN_BAD_USERNAME);
			    trace(" ## ARABIC_USER NOT REGISTERED ##");
			    
			    
			    String var = " ARABIC_USER Login Exception user not registered";
			    lb = new LogBean();
			    lb.setValues(Constant.ERR, "P0", userName, "Login", var);
			    UserLogs.addUserLog(lb, "NA");
			    
			    throw new SFSLoginException(" ARABIC_USER Please Register ", data);	
			}
			else
			{
				if(dbPassword == null )
				{
					SFSErrorData data = new SFSErrorData(SFSErrorCode.LOGIN_GUEST_NOT_ALLOWED);
				    trace(" ## ARABIC_USER NOT REGISTERED ##");
				    
				    
				    String var = " ARABIC_USER Login Exception dbPassword is null";
				    lb = new LogBean();
				    lb.setValues(Constant.ERR, "P0", userName, "Login", var);
				    UserLogs.addUserLog(lb, "NA");
				    
				    throw new SFSLoginException(" ARABIC_USER Please Register ", data);			
				}
				else if (dbPassword.equals("") || dbPassword == null)
				{
					SFSErrorData data = new SFSErrorData(SFSErrorCode.LOGIN_BAD_PASSWORD);
				    trace(" ## ARABIC_USER You must enter a password. ##");
				    
				    
				    String var = " ARABIC_USER Login Exception dbpassword is null or space";
				    lb = new LogBean();
				    lb.setValues(Constant.ERR, "P0", userName, "Login", var);
				    UserLogs.addUserLog(lb, "NA");
				    
				    throw new SFSLoginException(" ARABIC_USER You must enter a password.", data);
				}			
				else if (!getApi().checkSecurePassword(session, dbPassword, cryptedPass))
				{
			         SFSErrorData data = new SFSErrorData(SFSErrorCode.LOGIN_BAD_PASSWORD);
			         trace("ARABIC_USER Login failed for user     : "+userName);  
			         
					    
					    String var = " ARABIC_USER Login Exception login with badpassword";
					    lb = new LogBean();
					    lb.setValues(Constant.ERR, "P0", userName, "Login", var);
					    UserLogs.addUserLog(lb, "NA");
			         
			         throw new SFSLoginException(" ARABIC_USER Login failed for user: "  + userName, data);
				}						
				else if(!Commands.appInstance.proxy.isEmailActivated(userName))
				{
					SFSErrorData data = new SFSErrorData(SFSErrorCode.INVITATION_NOT_VALID);
				    data.addParameter("NotActivated");
				    trace(" ## ARABIC_USER Email Not Activated .Plz Activate ##");
				    
				    
				    String var = " ARABIC_USER Login Exception email not activated";
				    lb = new LogBean();
				    lb.setValues(Constant.ERR, "P0", userName, "Login", var);
				    UserLogs.addUserLog(lb, "NA");
				    
				    throw new SFSLoginException(" ARABIC_USER Email Not Activated .Plz Activate ", data);
				}
				else
				{    	
				    String var = "ARABIC_USER Login Success";
				    lb = new LogBean();
				    lb.setValues(Constant.ERR, "P0", userName, "Login", var);
				    UserLogs.addUserLog(lb, "NA");
				    
				    Commands.appInstance.getPlayerIds().put(userName, userId);	
				    
				    UserObject userObj = Commands.appInstance.getUserObjs().get(userName);
				    
				    if(userObj == null)
				    {
				    	userObj = new UserObject();
				    	userObj.setUserName(userName);	
				    	
				    	Commands.appInstance.getUserObjs().put(userName, userObj);
				    }
				    Commands.appInstance.proxy.setFirstLogin(userName);
					trace("############  ARABIC_USER CUSTOM_LOGIN_SUCCESSFUL  #############  : "+userName);   
				}
		}
	}
	*/}
}
