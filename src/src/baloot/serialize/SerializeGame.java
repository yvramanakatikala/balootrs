package src.baloot.serialize;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import src.baloot.beans.GameBean;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

public class SerializeGame implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public  static void serializeGame(GameBean gameBean)
	{
		//String gameId = gameBean._gameId;
		try
	      {
	         FileOutputStream fileOut = new FileOutputStream("ramana"+".ser");
	        		 		
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         
	         out.writeObject(gameBean);
	         out.close();
	         fileOut.close();
	         
	         //System.out.println("Serialized data is saved in /tmp/employee.ser");
	      }catch(IOException i)
	      {
	          i.printStackTrace();
	      }
	}
	
	public static void deserializeGame( )
	{
		GameBean gameBean = null;
		
		try
	      {
	         FileInputStream fileIn = new FileInputStream("ramana"+".ser");
	         
	         if(fileIn != null)
	         {
	        	 ObjectInputStream in = new ObjectInputStream(fileIn);
		         
		         gameBean = (GameBean) in.readObject();
		         in.close();
		         fileIn.close();
		         
	         }
	         
	      }
	      catch(Exception c)
	      {	         
	         c.printStackTrace();
	      }
		
		
		
	}
	
	public synchronized static void serializeSummaryLog()
	{
		Map<String, Long> serverSummary = Commands.appInstance.meLog.serverExcCount;
		Map<String, Long> clientSummary = Commands.appInstance.meLog.clientExcCount;
		
		try
	      {
			
			String filePath = Apps.getZonePath()+"/"+"serverSummary"+".ser";	
			Path path = Paths.get(filePath);		
			
	 		File serFile = new File(filePath);
	 		
	 		if(!serFile.exists())
	 		{
	 			Files.createDirectories(path.getParent());
	 			Files.createFile(path);
	 			
	 		}
	 		
	         FileOutputStream fileOut = new FileOutputStream(filePath);
	        		 		
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         
	         out.writeObject(serverSummary);
	         out.close();
	         fileOut.close();
	         
	       //  Commands.appInstance.trace("serverSummary_Serialized data is saved at "+"gamelogs/bl/"+Apps.getZone()+"/"+"serverSummary"+".ser"+" Time "+Apps.getDateString());
	      }catch(IOException i)
	      {
	          i.printStackTrace();
	      }
		
		try
	      {
			
			String filePath = Apps.getZonePath()+"/"+"clientSummary"+".ser";	
			Path path = Paths.get(filePath);		
			
	 		File serFile = new File(filePath);
	 		
	 		if(!serFile.exists())
	 		{
	 			Files.createDirectories(path.getParent());
	 			Files.createFile(path);
	 			
	 		}
	 		
	         FileOutputStream fileOut = new FileOutputStream(filePath);
	        		 		
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         
	         out.writeObject(clientSummary);
	         out.close();
	         fileOut.close();
	         
	       //  Commands.appInstance.trace("clientSummary_Serialized data is saved at "+"gamelogs/bl/"+Apps.getZone()+"/"+"clientSummary"+".ser"+"  Time "+Apps.getDateString());
	      }catch(IOException i)
	      {
	          i.printStackTrace();
	      }
		
	}
	
	public synchronized static void deserializeSummaryLog()
	{
		
		ConcurrentHashMap<String, Long> serverSummary = null;
		ConcurrentHashMap<String, Long> clientSummary = null;
		
		try
	      {
			String filePath = Apps.getZonePath()+"/"+"serverSummary"+".ser";	
			Path path = Paths.get(filePath);		
			
	 		File serFile = new File(filePath);
	 		
	 		if(!serFile.exists())
	 		{
	 			Files.createDirectories(path.getParent());
	 			Files.createFile(path);
	 			
	 		}
	 		
	         FileInputStream fileIn = new FileInputStream(filePath);
	         
	         if(fileIn != null)
	         {
	        	 ObjectInputStream in = new ObjectInputStream(fileIn);
		         
	        	 serverSummary = (ConcurrentHashMap<String, Long>) in.readObject();
		         in.close();
		         fileIn.close();
		         
		        // Commands.appInstance.trace(" ser ser ser ser ser ser ser ser");		         
		        // Commands.appInstance.trace(" ServerSummary_Deserialized_successfully  exe type count "+serverSummary.size());
		         //Commands.appInstance.trace(" ser ser ser ser ser ser ser ser");
		         
		         Commands.appInstance.meLog.serverExcCount = serverSummary;
	         }
	         
	      }
	      catch(Exception c)
	      {
	         //Commands.appInstance.trace("Exception_while_Deserializing_the_serverSummary ");
	         c.printStackTrace();
	      }
		
		try
	      {
			String filePath = Apps.getZonePath()+"/"+"clientSummary"+".ser";	
			Path path = Paths.get(filePath);		
			
	 		File serFile = new File(filePath);
	 		
	 		if(!serFile.exists())
	 		{
	 			Files.createDirectories(path.getParent());
	 			Files.createFile(path);
	 			
	 		}
	 		
	         FileInputStream fileIn = new FileInputStream(filePath);
	         
	         if(fileIn != null)
	         {
	        	 ObjectInputStream in = new ObjectInputStream(fileIn);
		         
	        	 clientSummary = (ConcurrentHashMap<String, Long>) in.readObject();
		         in.close();
		         fileIn.close();
		         
		        // Commands.appInstance.trace(" client client client client client");		         
		         //Commands.appInstance.trace(" ClientSummary_Deserialized_successfully  exe type count "+clientSummary.size());
		         //Commands.appInstance.trace(" client client client client client");
		         
		         Commands.appInstance.meLog.clientExcCount = clientSummary;
	         }
	         
	      }
	      catch(Exception e)
	      {
	    	 // Commands.appInstance.trace("Exception_while_Deserializing_the_clientSummary");
	          e.printStackTrace();
	      }
		
	}
		
}
