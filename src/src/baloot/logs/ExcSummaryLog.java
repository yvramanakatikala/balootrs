package src.baloot.logs;

import java.io.BufferedWriter;


import java.io.File;
import java.io.FileWriter;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

public class ExcSummaryLog implements Serializable{

	private static final long serialVersionUID = 1L;

	public synchronized static void incrementServerExcTypeCount(String command)
	{
		Long count = Commands.appInstance.meLog.serverExcCount.get(command);
		
		if(count == null)
		{
			 Commands.appInstance.meLog.serverExcCount.put(command, 1l);
		}
		else
		{
			count = count + 1;
			
			Commands.appInstance.meLog.serverExcCount.put(command, count);
		}
	}
	
	public synchronized static void incrementClientExcTypeCount(String command)
	{
		Long count = Commands.appInstance.meLog.clientExcCount.get(command);
		
		if(count == null)
		{
			Commands.appInstance.meLog.clientExcCount.put(command, 1l);
		}
		else
		{
			count = count + 1;
			
			Commands.appInstance.meLog.clientExcCount.put(command, count);			
		}
	}
	
	public synchronized static void writeSummaryLog()throws Exception
	{
		String filePath = Apps.getZonePath()+"/"+Apps.getMonth()+"/"+Apps.getDayString()+"/"+"summary_"+Apps.getDayString() + ".txt";	
		Path path = Paths.get(filePath);		
		
 		File userFile = new File(filePath);
 		
 		if(!userFile.exists())
 		{
 			// Todays file is not available
 			// we need create file and before creation clear the previous exception count.
 			
 			Commands.appInstance.meLog.serverExcCount = new ConcurrentHashMap<String, Long>();
 			Commands.appInstance.meLog.clientExcCount = new ConcurrentHashMap<String, Long>();
 			
 			Files.createDirectories(path.getParent());
 			Files.createFile(path);
 		}

		FileWriter fileWriter = new FileWriter(userFile, false); 				
		BufferedWriter out = new BufferedWriter(fileWriter);	
		
		//write the data
		if(Commands.appInstance.meLog.serverExcCount.size() > 0)
		{
			try{
				Iterator<String> it = Commands.appInstance.meLog.serverExcCount.keySet().iterator();
				
				while(it.hasNext())
				{
					String command = it.next();
					
					long count = Commands.appInstance.meLog.serverExcCount.get(command);
					
					String fileData = Apps.getDateString()+" | "+"server"+" |"+command+":"+count;
					
					out.write(fileData);
					out.newLine();	
				}
			}
			catch(Exception e)
			{
				e.getMessage();
			}
			catch(Error e)
			{
				e.getMessage();
			}
		}
		else
		{
			out.write("No_Server_Exception_Occur "+Apps.getDateString());
			out.newLine();
		}
		
		if(Commands.appInstance.meLog.clientExcCount.size() > 0)
		{
			try
			{
				Iterator<String> it = Commands.appInstance.meLog.clientExcCount.keySet().iterator();
				
				while(it.hasNext())
				{
					String command = it.next();
					
					long count = Commands.appInstance.meLog.clientExcCount.get(command);
					
					String fileData = Apps.getDateString()+" | "+"client"+" | "+command+" : "+count;
					
					out.write(fileData);
					out.newLine();	
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			catch(Error e)
			{
				e.printStackTrace();
			}			
		}
		else
		{
			out.write("No_Client_Exception_Occur "+Apps.getDateString());
			out.newLine();
		}
		
			out.close(); 
			fileWriter.close();
	}
}
