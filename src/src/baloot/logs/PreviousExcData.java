package src.baloot.logs;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class PreviousExcData implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public synchronized  boolean isNewClientExc(String player, String action)
	{
		boolean isNewLog = true;		
		ISFSArray sfsArr = Commands.appInstance.meLog.getPreviousClientExcData();
		
		for(int i = sfsArr.size()-1; i>-1;i--)
		{
			ISFSObject sfso = sfsArr.getSFSObject(i);
			
			String oldPlayer = sfso.getUtfString("player");
			String oldAction = sfso.getUtfString("action");
			String preTime = sfso.getUtfString("time");
			
			if(player.equals(oldPlayer) && action.equals(oldAction))
			{
				if(isLessThanClientThrottlingTime(preTime))
				{
					isNewLog = false;
					return isNewLog;
				}
				else
				{
					addClientLog(player, action, sfsArr);
					return isNewLog;
				}
			}			
		}
		
		if(isNewLog)
		{
			addClientLog(player, action, sfsArr);
		}
		
		return isNewLog;
	}
	
	private void addClientLog(String player, String action, ISFSArray sfsArr)
	{
		ISFSObject sfso = new SFSObject();
		
		sfso.putUtfString("player", player);
		sfso.putUtfString("time", getDateString());
		sfso.putUtfString("action", action);
		
		sfsArr.addSFSObject(sfso);
	}
	
	public boolean isLessThanClientThrottlingTime(String preTimeString)
	{
		boolean isLess = false;
		
		Date currTime = Apps.getCurrentTime();
		Date preTime = getDateObject(preTimeString);
		
		long diff = (currTime.getTime() - preTime.getTime())/1000;
		
		
		if(diff < Commands.appInstance.gameCV.getClientThrottlingTime())
		{
			isLess = true;
		}
		
		return isLess;
	}
	
	public synchronized boolean isNewServerLog(String player, String errMessage)
	{
		boolean isNewLog = true;		
		ISFSArray sfsArr = Commands.appInstance.meLog.getPreviousServerData();
		
		for(int i = sfsArr.size()-1; i>-1;i--)
		{
			ISFSObject sfso = sfsArr.getSFSObject(i);
			
			String oldPlayer = sfso.getUtfString("player");
			String oldErrMessage = sfso.getUtfString("errMessage");
			String preTime = sfso.getUtfString("time");
			
			if(player.equals(oldPlayer) && errMessage.equals(oldErrMessage))
			{
				if(isLessThanServerThrottlingTime(preTime))
				{
					isNewLog = false;
					
					return isNewLog;
				}
				else
				{
					addServerLog(player, errMessage, sfsArr);
					return isNewLog;
				}
			}			
		}
		
		if(isNewLog)
		{
			addServerLog(player, errMessage, sfsArr);
		}
		
		return isNewLog;
	}
	
	public boolean isLessThanServerThrottlingTime(String preTimeString)
	{
		boolean isLess = false;
		
		Date currTime = Apps.getCurrentTime();
		Date preTime = getDateObject(preTimeString);
		
		long diff = (currTime.getTime() - preTime.getTime())/1000;
		
		
		if(diff < Commands.appInstance.gameCV.getServerThrottlingTime())
		{
			isLess = true;
		}
		
		return isLess;
	}
	
	private void addServerLog(String player, String errMessage, ISFSArray sfsArr)
	{
		ISFSObject sfso = new SFSObject();
		
		sfso.putUtfString("player", player);
		sfso.putUtfString("time", getDateString());
		sfso.putUtfString("errMessage", errMessage);
		
		sfsArr.addSFSObject(sfso);
	}
	
	private Date getDateObject(String preTime)
	{
		Date d = null;		
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		try{
			d = (Date)dateFormat.parse(preTime);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return d;
	}
	
	private String getDateString()
	{		
		Calendar currentTime = Calendar.getInstance();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		String dateCur = dateFormat.format(currentTime.getTime());
		
		return dateCur;		
	}
}


