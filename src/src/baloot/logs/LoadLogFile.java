package src.baloot.logs;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import src.baloot.utils.Apps;
import src.baloot.utils.Commands;


public class LoadLogFile {
	
	public static void load()
	{		

		try {
			 
			File fXmlFile = new File("src/baloot/xml/logs.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
	 
			
			NodeList nList = doc.getElementsByTagName(doc.getDocumentElement().getNodeName());
			
			Node nNode = nList.item(0);
			
				
			if (nNode.getNodeType() == Node.ELEMENT_NODE) 
			{
				Element eElement = (Element) nNode;			
			    
			    if(getTagValue("isDynamicXmlLoading", eElement).equals("true"))
			    {
			    	Commands.appInstance.gameCV.setDynamicXmlLoading(true);
			    }
			    else
			    {
			    	Commands.appInstance.gameCV.setDynamicXmlLoading(false);
			    }
			    
			    if(Commands.appInstance.gameCV.isDynamicXmlLoading())
			    {
				    Commands.appInstance.gameCV.setClientThrottlingTime(getTagValue("clientThrottlingTime", eElement));
				    Commands.appInstance.gameCV.setServerThrottlingTime(getTagValue("serverThrottlingTime", eElement));
				    Commands.appInstance.gameCV.setClientLogPerMinute(getTagValue("clientLogPerMinute", eElement));
				    Commands.appInstance.gameCV.setPingCountToCheckPlayingTables(getTagValue("pingCountToCheckPlayingTables", eElement));
				    Commands.appInstance.gameCV.setServerPingTimeOut(getTagValue("serverPingTimeOut", eElement));
				    Commands.appInstance.gameCV.setServerBuildVersion(getTagValue("serverBuildVersion", eElement)); 
				    
				    
				    
				    if(getTagValue("allowIP", eElement).equals("true"))	
				    {
				    	Commands.appInstance.gameCV.setAllowIP(true);
				    }
				    else
				    {
				    	Commands.appInstance.gameCV.setAllowIP(false);
				    }
				    
				    if(getTagValue("aiSpan", eElement).equals("true"))	
				    {
				    	Commands.appInstance.gameCV.setAiSpan(true);
				    }
				    else
				    {
				    	Commands.appInstance.gameCV.setAiSpan(false);
				    }
				    
				    if(getTagValue("writeServerExceptions", eElement).equals("true"))	
				    {
				    	Commands.appInstance.gameCV.setWriteServerExceptionns(true);
				    }
				    else
				    {
				    	Commands.appInstance.gameCV.setWriteServerExceptionns(false);
				    }
				    
				    
				    if(getTagValue("writeClientExceptions", eElement).equals("true"))
				    {
				    	Commands.appInstance.gameCV.setWriteClientExceptions(true);
				    }
				    else
				    {
				    	Commands.appInstance.gameCV.setWriteClientExceptions(false);
				    }
				    
				    if(getTagValue("writeClientLogsIntoGame", eElement).equals("true"))
				    {
				    	Commands.appInstance.gameCV.setWriteClientLogsIntoGame(true);
				    }
				    else
				    {
				    	Commands.appInstance.gameCV.setWriteClientLogsIntoGame(false);
				    }
				    
				    if(getTagValue("ServerPingEnabled", eElement).equals("true"))
				    {
				    	Commands.appInstance.gameCV.setServerPingEnabled(true);
				    }
				    else
				    {
				    	Commands.appInstance.gameCV.setServerPingEnabled(false);
				    }
				    
			    }
			    else
			    {
			    	Apps.showLog(" Load_XML_Dynamically "+Commands.appInstance.gameCV.isDynamicXmlLoading());
			    }
			}
				 
		}
		catch (Exception e) {
			e.printStackTrace();			  
		}			   
	}

	private static String getTagValue(String sTag, Element eElement) {
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes(); 
        Node nValue = (Node) nlList.item(0); 
        return nValue.getNodeValue();
	}
}
