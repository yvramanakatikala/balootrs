package src.baloot.logs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import src.baloot.exce.beans.LogBean;
import src.baloot.utils.Apps;

public class FakeGameLog {
	
	public static void writeFakeGameLog(String str)throws Exception
	{
		String logPath = Apps.getZonePath()+"/"+Apps.getMonth()+"/"+Apps.getDayString()+"/"+"FakeGame_"+Apps.getDayString() + ".txt";
		Path path = Paths.get(logPath);		
		
 		File serverFile = new File(logPath);
 		
 		if(!serverFile.exists())
 		{
			Files.createDirectories(path.getParent());
			Files.createFile(path);
 		}

		FileWriter fileWriter = new FileWriter(serverFile, true); 				
		BufferedWriter serverP0Writer = new BufferedWriter(fileWriter); 
			
		serverP0Writer.append(str);
		serverP0Writer.flush();
		serverP0Writer.newLine();
		
		
		serverP0Writer.close();
		fileWriter.close();
	}
	
	public static void addLog(LogBean lb)
	{
		String curTime = Apps.getDateString();
		
		String str = lb.getLogType()+" | "+lb.getPriority()+" | "+curTime+" | "+lb.getAction()+" | " +lb.getVariables();
		
		try
		{
			writeFakeGameLog(str);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}
}
