package src.baloot.logs;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.Timer;

import src.baloot.classes.GameConfigVariables;
import src.baloot.constants.Constant;
import src.baloot.exce.P0Exc;
import src.baloot.exce.WriteExceptions;
import src.baloot.exce.beans.LogBean;
import src.baloot.listeners.BidActionListener;
import src.baloot.listeners.ClientThrottlingActionListener;
import src.baloot.listeners.GameStatusActionListener;
import src.baloot.listeners.ServerThrottlingActionListener;
import src.baloot.serialize.SerializeGame;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.SFSArray;

public class MainExtensionLog implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public WriteExceptions wrExc = new WriteExceptions();
	
	public ConcurrentHashMap<String, String> filePaths = new ConcurrentHashMap<String, String>();
	public Map<String, Long> serverExcCount = new ConcurrentHashMap<String, Long>();
	public Map<String, Long> clientExcCount = new ConcurrentHashMap<String, Long>();
	public ConcurrentHashMap<String, Date> gameStatus = new ConcurrentHashMap<String, Date>();
	
	private ISFSArray previousClientExcData = new SFSArray();
	private ISFSArray previousClenetLogData = new SFSArray();
	private ISFSArray previousServerData = new SFSArray();
	
	public PreviousExcData preExcData = new PreviousExcData();
	public PreviousLogData preLogData = new PreviousLogData();
		
	// for logs 
	private String clientLog = null;
	private String serverLog = null;
	private String clientP0Log = null;
	private String serverP0Log = null;
	
	public Timer clientThrottlingTimer;
	public Timer serverThrottlingTimer;
	public Timer gameStatusTimer;	
	
	private int logCountToWrite = 0;
	
	public void init()
	{
		Apps.showLog("MainExtensionLog init()");
                
		clientThrottlingTimer = new Timer(1000 * Commands.appInstance.gameCV.getClientThrottlingTime(), new ClientThrottlingActionListener());
		clientThrottlingTimer.start();
		
		serverThrottlingTimer = new Timer(1000 * Commands.appInstance.gameCV.getServerThrottlingTime(), new ServerThrottlingActionListener());
		serverThrottlingTimer.start();
		
		//gameStatusTimer = new Timer(1000 * 180, new GameStatusActionListener());
		//gameStatusTimer.start();
		
		 LoadLogFile.load();
		
		try
		{
			SerializeGame.deserializeSummaryLog();
			ExcSummaryLog.writeSummaryLog();
			
		}catch(Exception e){}
	}
	
	public synchronized void addServerLog(String str)
	{
		if(serverLog != null)
		{
			serverLog +="---";
			serverLog +=str;
			checkAndWriteServerLog();
		}
		else
		{
			serverLog = str;
		}
	}
	
	
	public synchronized void checkAndWriteServerLog()
	{
		if(serverLog != null)
		{
			String[] logs = serverLog.split("---");
			
			if(logs.length > logCountToWrite)
			{
				try
				{
					wrExc.writeServerLogFile(logs);
				}catch(Exception e){e.printStackTrace();}
				serverLog = null;
			}
		}
	}
	
	public synchronized void writeServerLog()
	{
		if(serverLog != null)
		{
			String[] logs = serverLog.split("---");
			
			if(logs.length > 0)
			{
				try
				{ 
					wrExc.writeServerLogFile(logs);
				}catch(Exception e){e.printStackTrace();}
				serverLog = null;
			}
		}
	}
	
	public synchronized void addClientLog(String str)
	{
		if(clientLog != null)
		{
			clientLog +="---";
			clientLog +=str;
			checkAndWriteClentLog();
		}
		else
		{
			clientLog = str;
		}
	}
	
	public synchronized void checkAndWriteClentLog()
	{
		String[] logs = clientLog.split("---");
		
		if(logs.length > logCountToWrite)
		{
			try
			{
				wrExc.writeClientLogFile(logs);
			}catch(Exception e){e.printStackTrace();}
			clientLog = null;
		}
	}
	
	public synchronized void writeClientLog()
	{
		if(clientLog != null)
		{
			String[] logs = clientLog.split("---");
			
			if(logs.length > 0)
			{
				try
				{
					wrExc.writeClientLogFile(logs); 
				}catch(Exception e){e.printStackTrace();}
				clientLog = null;
			}
		}
	}
	
	public synchronized void addServerP0Log(String str)
	{
		if(serverP0Log != null)
		{
			serverP0Log +="---";
			serverP0Log +=str;
			checkAndWriteServerPoLog();
		}
		else
		{
			serverP0Log = str;
		}
	}
	
	public synchronized void checkAndWriteServerPoLog()
	{
		String[] logs = serverP0Log.split("---");
		
		if(logs.length > logCountToWrite)
		{
			try
			{
				P0Exc.writeServerP0Log(logs);
			}catch(Exception e){e.printStackTrace();}
			
			serverP0Log = null;
		}
	}
	
	public synchronized void writeServerP0Log()
	{
		if(serverP0Log != null)
		{
			String[] logs = serverP0Log.split("---");
			
			if(logs.length > 0)
			{
				try
				{
					P0Exc.writeServerP0Log(logs);
				}catch(Exception e)
				{
					e.printStackTrace();
				}
				
				serverP0Log = null;
			}
		}
	}
	
	public synchronized void addClientP0Log(String str)
	{
		if(clientP0Log != null)
		{
			clientP0Log +="---";
			clientP0Log +=str;
			checkAndWriteClientPoLog();
		}
		else
		{
			clientP0Log = str;
		}
	}
	
	public synchronized void checkAndWriteClientPoLog()
	{
		String[] logs = clientP0Log.split("---");
		
		if(logs.length > logCountToWrite)
		{
			try
			{
				P0Exc.writeClientP0Log(logs);
			}catch(Exception e){e.printStackTrace();}
			clientP0Log = null;
		}
	}
	
	public synchronized void writeClientP0Log()
	{
		if(clientP0Log != null)
		{
			String[] logs = clientP0Log.split("---");
			
			if(logs.length > 0)
			{
				try
				{
					P0Exc.writeClientP0Log(logs);
				}catch(Exception e){e.printStackTrace();}
				clientP0Log = null;
			}
		}
	}
	
	public synchronized void writeGameEndLog()
	{
		writeClientLog();
		writeServerLog();
		writeServerP0Log();
		writeClientP0Log();
	}	
	
	public ISFSArray getPreviousClientExcData() {
		return previousClientExcData;
	}

	public void setPreviousClientExcData(ISFSArray previousClientExcData) {
		this.previousClientExcData = previousClientExcData;
	}

	public ISFSArray getPreviousClenetLogData() {
		return previousClenetLogData;
	}

	public void setPreviousClenetLogData(ISFSArray previousClenetLogData) {
		this.previousClenetLogData = previousClenetLogData;
	}

	public ISFSArray getPreviousServerData() {
		return previousServerData;
	}

	public void setPreviousServerData(ISFSArray previousServerData) {
		this.previousServerData = previousServerData;
	}
}
