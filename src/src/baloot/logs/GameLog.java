package src.baloot.logs;

import java.io.Serializable;
import java.util.ArrayList;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;

import src.baloot.beans.BonusBean;
import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerBean;
import src.baloot.beans.PlayerRoundBean;
import src.baloot.beans.RoundBean;
import src.baloot.constants.Constant;
import src.baloot.exce.beans.LogBean;


public class GameLog implements Serializable{

	private static final long serialVersionUID = 1L;
	
	// methods	
	public void addClietnLog(GameBean gameBean, String gameId, String player, String action, String variables, String logType, String priority)
	{
		LogBean lb = new LogBean();
		
		lb.setValues(logType, priority, player, action, variables);
		lb.setFrom(Constant.CLIENT);
		gameBean.addLog(lb);
				
	}	
	
	public void singlePlayerGame(GameBean gameBean, String player)
	{
		String var = " player "+player+" tableId "+gameBean.tableId+" players "+gameBean._players.toString()+" host "+gameBean.host
						+" isHostJoined "+gameBean.isHostJoined+" tableType "+gameBean.tableType+" isSinglePlayerGame "+gameBean.isSinglePlayer;
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", player, "SinglePlayerGame", var);
		gameBean.addLog(lb);
	}
	
	public void resetPositions(GameBean gameBean)
	{
		String var = " ";
		
		for(PlayerBean pb : gameBean._playersBeansList)
		{
			var = var+" player "+pb._playerId+" position "+pb.position+"  ";
		}
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", "System", "ResetPositions", var);
		gameBean.addLog(lb);
	}
	public void initGame(GameBean gameBean, ArrayList<PlayerBean> preBeans)
	{
		String var = " isPrivateTable "+gameBean.isPrivateTable()+" tableType "+gameBean.tableType+" tableId "+gameBean.tableId
				+"  spetators "+gameBean.getSpecatatores().toString()+" isHostJoined "+gameBean.isHostJoined
				+" isSinglePlayer "+gameBean.isSinglePlayer+" actualJoinedPlayers  ";
				
				
		for(PlayerBean pb : preBeans)
		{
			var = var+" player "+pb._playerId+" position "+pb.position+"   initialPosition  "+pb.initialPosition;
		}
		
		var = var+"afterjoining AI's";
		
		for(PlayerBean pb : gameBean._playersBeansList)
		{
			var = var+" player "+pb._playerId+" position "+pb.position+"  ";
		}
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "InitGame", var);
		gameBean.addLog(lb);
	}
	
	public void startGame(GameBean gameBean)
	{
		String var = " shuffler "+gameBean.getShuffler()+" turnForBid "+gameBean.getTurn()+"  players "+gameBean._players.toString()
						+"  roundCount "+gameBean.getRoundCount()+" initialPlayers "+gameBean.initialPlayers;
						
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "StartGame", var);
		gameBean.addLog(lb);
	}
	
	public void initRound(GameBean gameBean)
	{
		RoundBean rb = gameBean._roundBean;
		
		String var = " trickCount "+rb.trickCount+" shufflerPosition "+rb._shufflerPosition+" turn "+rb._turn+" openCardTaker "+rb._openCardTaker
				+" bidRound "+rb._bidRoundCount+" turnForBid "+rb._turnForBid+" openCardId "+rb._openCardId+" firstPerson "+rb.firstPerson;
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "InitRound", var);
		gameBean.addLog(lb);
	}
	
	public void cardsDistribution(GameBean gameBean)
	{
		RoundBean rb = gameBean._roundBean;
		
		String var = " CardsDistribution  ";
		
		for(PlayerRoundBean prb : rb._playerRoundBeanObjs)
		{
			var = var +"  player "+prb._playerId+" cards "+prb._cards.toString();
		}
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "CardsDistribution", var);
		gameBean.addLog(lb);
	}
	
	public void bidEnableBits(GameBean gameBean, String player, ArrayList<Integer> bits)
	{
		String var = " player "+player+" bidEnbleBits "+bits.toString();
	
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "BidEnableBits", var);
		gameBean.addLog(lb);
	}
	
	public void bid(GameBean gameBean)
	{
		RoundBean rb = gameBean._roundBean;
		
		String var = " bidTurnPlayer "+rb._turnForBid+" bidRoundCount "+rb._bidRoundCount+" gameType "+rb._gameType+" doubleType "+gameBean._roundBean._doubleType
						+" trumpSuitType  "+rb._trumpSuitType+" bidWonPerson "+rb._bidWonPerson+" openCardTaker "+rb._openCardTaker
						+" isGameTypeSelected "+gameBean._roundBean.isGameTypeSelected;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "Bid", var);
		gameBean.addLog(lb);
	}
	
	public void bidSelected(GameBean gameBean, String player, String bidSelected, String gameTrumpSuit)
	{
		String variables = " player "+player+" selected bid "+bidSelected +" gametrumpsuit "+gameTrumpSuit
				+" isRemainingCardsDistrubuted "+gameBean._roundBean.isRemainingCardsDistributed;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", player, "BidSelected", variables);
		gameBean.addLog(lb);
	}
	
	
	public void bonusRequest(String command, ArrayList<Integer> cards, String bonusType, String player, GameBean gameBean, boolean isAI, PlayerRoundBean prb)
	{
		// Add Log 
		String var = " player "+player+" bonustype "+bonusType +"  cards "+cards.toString()+" command "+command+" isAI "+isAI+" gameType "+gameBean._roundBean._gameType
						+" roundCount "+gameBean.getRoundCount()+" trickCount "+gameBean._roundBean.trickCount+" firstPerson "+gameBean._roundBean.firstPerson
						+" cards "+prb._cards+"  playedCards "+prb._playedcards;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", player, "BonusRequest", var);
		gameBean.addLog(lb);
	}
	
	public void serverToClientBonus(GameBean gameBean, ISFSObject sfso)
	{
		String var = " command "+sfso.getUtfString("command")+" player "+sfso.getUtfString("player")+" gender "+sfso.getUtfString("gender")
				     +"  bonusType "+sfso.getUtfString("bonusType")+" roundCount "+sfso.getInt("roundCount")+"  trickCount "+sfso.getInt("trickCount");
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", "server", "ServerToClientBonus", var);
		gameBean.addLog(lb);
	}
	
	public void serverToClientBonusArray(GameBean gameBean, ISFSObject sfso)
	{
		String var = " command "+sfso.getUtfString("command")+" player "+sfso.getUtfString("player")+" gender "+sfso.getUtfString("gender")
				     +" bonusCardsArray "+sfso.getIntArray("bonusCards")
				     +"  bonusTypeArray "+sfso.getUtfStringArray("bonusType")+" roundCount "+sfso.getInt("roundCount")+"  trickCount "+sfso.getInt("trickCount");
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", "server", "ServerToClientBonusArray", var);
		gameBean.addLog(lb);
	}
	
	public void setShuffler(GameBean gameBean, String preShuffler)
	{
		String var = " Shuffler "+gameBean.getShuffler()+" previousShuffler "+preShuffler;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "SetShuffler", var);
		gameBean.addLog(lb);
	}
	
	public void setTurnForBid(GameBean gameBean, String preTurnPlayer)
	{
		String var = "  curBidTurnPlayer "+gameBean._roundBean._turnForBid+" preBidTurnPlayer "+preTurnPlayer;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", gameBean._roundBean._turnForBid, "BidTurn", var);
		gameBean.addLog(lb);
	}
	
	public void setTurn(GameBean gameBean, String preTurn)
	{
		String var = " turn "+gameBean._roundBean.getTurn()+" preTurn "+preTurn;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "SetTurn", var);
		gameBean.addLog(lb);
	}
	
	public void setTurnToTrickWinner(GameBean gameBean, String trickWinner)
	{
		String var = " trickWinner "+trickWinner+"  turn "+gameBean._roundBean._turn;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "TurnToTrickWinner", var);
		gameBean.addLog(lb);
	}
	public void discardLog(GameBean gameBean, String player, boolean isDeclaredHighest, int cardId, boolean isBonus, int bonusSize, String command)
	{
		String var = " player "+player+" cardId "+cardId+"  isDeclaredHighest "+isDeclaredHighest+" isBonus "+isBonus+"  bonusSize "
						+bonusSize+" command "+command+" opencards  "+gameBean._roundBean.openCardsList;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", player, "Discard", var);
		gameBean.addLog(lb);
	}
	
	public void discardRequest(GameBean gameBean, String player, int card, String request)
	{
		// addLog
		String var = " player "+player+" card "+card+" request  "+request+" bonusObjSize "+gameBean._roundBean._bonusObjs.size();
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", player, "DiscardRequest", var);
		gameBean.addLog(lb);
	}
	
	public void highestFailed(GameBean gameBean, String player, int cardId, boolean isDeclaredHighest, String suit, boolean isHighest)
	{
		String var = " cardId "+cardId+" isDeclaredHighest "+isDeclaredHighest+" suit "+suit+"  isHighest "+isHighest;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P1", player, "HighestFailed", var);
		gameBean.addLog(lb);
	}
	
	public void closedDoubleFailed(GameBean gameBean, String player, String var)
	{
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P1", player, "ClosedDoubleFailed", var);
		gameBean.addLog(lb);
	} 
	
	public void scoresLog(GameBean gameBean)
	{
		// Add log
		String var = " scores ";
		
		for(PlayerBean pb : gameBean._playersBeansList)
		{
			var = var+"  player "+pb._playerId+" score "+pb._totalPoints+" points "+pb._points;
		}
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", "System", "GameScore", var);
		gameBean.addLog(lb);
	}
	
	public void trickCompleted(GameBean gameBean, String player, int card, int score)
	{
		String var = "Trick wonplayer "+player+"  woncard "+card+"  score "+score+"  trickCount "+gameBean._roundBean.trickCount;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", player, "TrickCompleted", var);
		gameBean.addLog(lb);
		
		gameBean.writeGameLog();
	}
	
	public void startNextTrick(GameBean gameBean)
	{
		String var = "Turn "+gameBean._roundBean.getTurn()+" trickCount "+gameBean._roundBean.trickCount+"  roundCount"+gameBean.getRoundCount();
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", "System", "TrickStarted", var);
		gameBean.addLog(lb);
	}
	
	public void roundCompleted(GameBean gameBean, ISFSObject sfso)
	{
		String var = " RoundCompleted  roundCount "+gameBean.getRoundCount();
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", "System", "RoundCompleted", var);
		gameBean.addLog(lb);
		
		gameBean.writeGameLog();
	}
	
	public void gameCompleted(GameBean gameBean)
	{
		// TODO : write more logs;
		
		String var = "isGameStarted "+gameBean.isGameStarted;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", "System", "GameCompleted", var);
		gameBean.addLog(lb);
		
		gameBean.writeGameLog();
	}
	
	public void bidWinner(GameBean gameBean)
	{
		String var = "Game Type: "+gameBean._roundBean._gameType+" TrumpSuitType: "+gameBean._roundBean._trumpSuitType+", Bid won person: "+gameBean._roundBean._bidWonPerson+", Turn: "+gameBean._roundBean.getTurn();
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", gameBean._roundBean._bidWonPerson, "BidWinner", var);
		gameBean.addLog(lb);
	}
	
	public void distributeRemainingCards(GameBean gameBean)
	{
		String var = " roundCount "+gameBean.getRoundCount();
		for(PlayerRoundBean prb : gameBean._roundBean._playerRoundBeanObjs)
		{			
			var = var +" player "+prb._playerId+" cards : "+prb._cards.toString();
		}
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", "System", "DistributeRemainngCards", var);
		gameBean.addLog(lb);
	}
	
	public void highestBonusPerson(GameBean gameBean)
	{
		String var = " highestBonusPerson "+gameBean._roundBean._highestBonusPerson+" roundCount "+gameBean.getRoundCount()+"  trickCount "+gameBean._roundBean.trickCount;
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", "System", "HighestBonusPerson", var);
		gameBean.addLog(lb);
	}
	
	public void isSaidBonus(GameBean gameBean, String player, boolean sign)
	{
		String var = " player "+player+" highestBonusPerson "+gameBean._roundBean._highestBonusPerson+"  bonusObjSize "+gameBean._roundBean._bonusObjs.size()+" sign "+sign;
		
		for(BonusBean bb : gameBean._roundBean._bonusObjs)
		{
			var = var +"  player "+bb._playerId+" bonusType "+bb._bonusType+" cards "+bb._cards+"  isShowedCards "+bb._isShowedCards;
		}
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P2", player, "IsSaidBonus", var);
		gameBean.addLog(lb);
	}
	
	public void isSaidBonusRemovedBonuses(GameBean gameBean, String player, boolean sign, ArrayList<BonusBean> list)
	{
		String var = " player "+player+" highestBonusPerson "+gameBean._roundBean._highestBonusPerson+"  removed bonusObjSize "+list.size()+" sign "+sign;
		
		for(BonusBean bb : list)
		{
			var = var +"  player "+bb._playerId+" bonusType "+bb._bonusType+" cards "+bb._cards+"  isShowedCards "+bb._isShowedCards;
		}
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P2", player, "IsSaidBonusRemovedBonuses", var);
		gameBean.addLog(lb);
	}
	
	public void showBonusAutomatically(GameBean gameBean, String player)
	{
		String var = " player "+player;
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P2", player, "ShowBonusAutomatically", var);
		gameBean.addLog(lb);
	}
	
	public void userJoinInRematchPhase(GameBean gameBean, String player)
	{
		String var = " player "+player+" rematchPlayers "+gameBean.rematchPlayers.toString()+" isRematchPhase "+gameBean.isRematchPhase+" players  "+gameBean._players.toString()
				+" spetators  "+gameBean.getSpecatatores().toString();
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P2", player, "UserJoinInRematchPhase", var);
		gameBean.addLog(lb);
	}
	
	public void realPlayerNotAvailableToStartRematchTimer(GameBean gameBean)
	{
		String var = "";
		
		for(PlayerBean pb : gameBean._playersBeansList)
		{
			var = var+" player "+pb._playerId+"   isAI "+pb._isAI+"  isLeave "+pb.isLeave+" isFakePlayer "+pb.isFakePlayer;
		}
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P2", "System", "RealPlayerNotAvailableToStartRematchTimer", var);
		gameBean.addLog(lb);
	}
	
	public void fakePlayerJoinInRematchPhase(GameBean gameBean, PlayerBean pb)
	{
		String var = " player "+pb._playerId+" isFakePlayer "+pb.isFakePlayer;
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", "System", "FakePlayerJOinInRematchPhase", var);
		gameBean.addLog(lb);
	}
	
	public void fakePlayerJoinInAnotherGame(GameBean gameBean, PlayerBean pb)
	{
		String var = " player "+pb._playerId+" isFakePlayer "+pb.isFakePlayer;
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", "System", "FakePlayerJoinInAnotherGame", var);
		gameBean.addLog(lb);
	}
	
	public void playerChangedToSpectator(GameBean gameBean, ArrayList<User>  speUsers)
	{
		String var = " ";
		
		for(User user : speUsers)
		{
			var = var+"   player "+user.getName();
		}
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P2", "System", "PlayerChangedToSpectators", var);
		gameBean.addLog(lb);
	}
	public void childRematchGameDetais(GameBean gameBean, GameBean childGame)
	{
		String var = " PresentGameDetails  players "+gameBean._players.toString()+"  spectators "+gameBean.getSpecatatores().toString()
						+"  rematchPlayers "+gameBean.rematchPlayers.toString()+"  isPrivateTable "+gameBean.isPrivateTable()+" host "+gameBean.host
						+" isHostJoined "+gameBean.isHostJoined+" tableId "+gameBean.tableId+" tableType "+gameBean.tableType;
						
		
			var = var+" ChildGameDetails  players "+childGame._players.toString()+"  spectators "+childGame.getSpecatatores().toString()
					+"  rematchPlayers "+childGame.rematchPlayers.toString()+"  isPrivateTable "+childGame.isPrivateTable()+" host "+childGame.host
					+" isHostJoined "+childGame.isHostJoined+" tableId "+childGame.tableId+"  childGameId "+childGame.gameId+"  tableType "+childGame.tableType;
			
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P2", "System", "ChildRematchGameDetais", var);
			gameBean.addLog(lb);
			
			gameBean.writeGameLog();
	}
	
	public void parentRematchGameDetails(GameBean gameBean, GameBean parentGame)
	{
		String var = " ChildGameDetails  players "+gameBean._players.toString()+"  spectators "+gameBean.getSpecatatores().toString()
				+"  rematchPlayers "+gameBean.rematchPlayers.toString()+"  isPrivateTable "+gameBean.isPrivateTable()+" host "+gameBean.host
				+" isHostJoined "+gameBean.isHostJoined+" tableId "+gameBean.tableId+" tableType "+gameBean.tableType;
				

			var = var+" ParentGameDetails  players "+parentGame._players.toString()+"  spectators "+parentGame.getSpecatatores().toString()
					+"  rematchPlayers "+parentGame.rematchPlayers.toString()+"  isPrivateTable "+parentGame.isPrivateTable()+" host "+parentGame.host
					+" isHostJoined "+parentGame.isHostJoined+" tableId "+parentGame.tableId+"  parentGameId "+parentGame.gameId+"  tableType "+parentGame.tableType;
	
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P2", "System", "ParentRematchGameDetails", var);
			gameBean.addLog(lb);
			
			gameBean.writeGameLog();
	
	}
	
	public void replaceUserWithAI(GameBean gameBean, String realPlayer, String aiPlayer)
	{
		String var = " realPlayer "+realPlayer+" aiPlayer "+aiPlayer+"  turn "+gameBean._roundBean.getTurn()+" isGameStarted "+gameBean.isGameStarted
						+" isGameTypeSelected "+gameBean._roundBean.isGameTypeSelected+" roundCount "+gameBean.getRoundCount()+" trickCount "+gameBean._roundBean.trickCount
						+"  bidRoundCount "+gameBean._roundBean._bidRoundCount+" turnForBid "+gameBean._roundBean.getTurnForBid()+" bidSelectedPersong "+gameBean._roundBean.bidSelected
						+" bidWonPerson "+gameBean._roundBean._bidWonPerson+" players "+gameBean._players.toString()+" shuffler "+gameBean.getShuffler()+" highestBonusPerson "+gameBean._roundBean._highestBonusPerson
						+" openCardTaker "+gameBean._roundBean._openCardTaker+" initialPlayers "+gameBean.initialPlayers;
				
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P2", realPlayer, "ReplaceUserWithAI", var);
		gameBean.addLog(lb);
	}
	
	public void leaveTableAIplayersCount(GameBean gameBean, int aiCount)
	{
		String var = " aiPlayersCount "+aiCount;
		
		for(PlayerBean pb : gameBean._playersBeansList)
		{
			var = var+"   player "+pb._playerId+"  isAI "+pb._isAI+" isFakePlayer "+pb.isFakePlayer+"  isLeave "+pb.isLeave+"  isAutoPlay "+pb.isAutoPlay+"  isNewlyOccupiedSeat "+pb.isNewlyOccupiedSeat;
		}
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P2", "System", "LeaveTableAIplayersCount", var);
		gameBean.addLog(lb);
	}
	
	public void changePlayer(GameBean gameBean, String player, ISFSObject sfso)
	{
		String var = " player "+player+"  aiPlayer "+sfso.getUtfString("aiPlayer")+"  actualPlayer "+sfso.getUtfString("actualPlayer");	
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P1", "System", "ChangePlayer", var);
		gameBean.addLog(lb);
	}
	
	public void syncDataSendToNewlyOccupiedSeatPlayer(GameBean gameBean, PlayerBean pb)
	{
		String var = " newlySeatOccupiedPlayer "+pb._playerId+"  isNeqlyOccupiedPlayer "+pb.isNewlyOccupiedSeat+" aiplayer "+pb._aiPlayer;
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P2", pb._playerId, "SyncDataSendToNewlyOccupiedSeatPlayer", var);
		gameBean.addLog(lb);
	}
	
	public void spectatorLeft(GameBean gameBean, String player)
	{
		String var = " player "+player+"   players "+gameBean._players.toString()+" spectators "+gameBean.getSpecatatores().toString();
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", player, "SpectatorLeft", var);
		gameBean.addLog(lb);
	}
	
	public void userLeft(GameBean gameBean, String player)
	{
		String var = " player "+player+"   players "+gameBean._players+" spectators "+gameBean.getSpecatatores()+" isGameStarted "+gameBean.isGameStarted;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", player, "UserLeft", var);
		gameBean.addLog(lb);
	}
	
	public void getAIPlayer(GameBean gameBean, String aiPlayer, String requestedPlayer)
	{
		String var = " aiPlayer "+aiPlayer+"  requestedPlayer "+requestedPlayer+"   players "+gameBean._players.toString()+" spectators "+gameBean.getSpecatatores().toString();
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", aiPlayer, "GetAIPlayer", var);
		gameBean.addLog(lb);
	}
	
	public void resetPlayerBean(GameBean gameBean, String leftPlayer, String newPlayer)
	{
		String var = " leftPlayer "+leftPlayer+" newPlayer "+newPlayer+"  players "+gameBean._players+" initialPlayers "+gameBean.initialPlayers+"  shuffler "+gameBean.shuffler
						+"  turn "+gameBean._roundBean._turn+"  turnForBid "+gameBean._roundBean._turnForBid+"  highestBonusPerson "+gameBean._roundBean._highestBonusPerson
						+" bidSelectedPerson "+gameBean._roundBean._bidSelectedPerson+"  bidWonPerson "+gameBean._roundBean._bidWonPerson+" openCardTaker "+gameBean._roundBean._openCardTaker;
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", leftPlayer, "ResetPlayerBean", var);
		gameBean.addLog(lb);
	}
	
	public void joinAfterDisconnection(GameBean gameBean, PlayerBean pb)
	{
		String var = " player "+pb._playerId+"  tableId "+gameBean.tableId+" isAI "+pb._isAI+" isFakePlayr "+pb.isFakePlayer+" isAutoPlay "+pb.isAutoPlay+" isLeave "
						+pb.isLeave+" isNewlyOccupiedSeat "+pb.isNewlyOccupiedSeat;
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", pb._playerId, "JoinAfterDisconnection", var);
		gameBean.addLog(lb);
	}
	public void sendSyncData(GameBean gameBean, String player)
	{
		String var = " player "+player;
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P1", player, "SendSyncData", var);
		gameBean.addLog(lb);
	}
	
	public void disconnectionLog(GameBean gameBean, String player)
	{
		 String var = " player "+player+" isGameStarted "+gameBean.isGameStarted+" isRematchPhase "+gameBean.isRematchPhase
			 		+" isRoundCompletedResultTime"+gameBean.isRoundCompletedResultTime+" players "+gameBean._players+" spectators "+gameBean.getSpecatatores();
			 		
		 LogBean lb = new LogBean();
		 lb.setValues(Constant.LOG, "P3", player, "DisconnectionLog", var);
		 gameBean.addLog(lb);
	}
	
	public void userDisconnedted(GameBean gameBean, String player, boolean isSpecatatorFound)
	{
		 String var = " player "+player+" isGameStarted "+gameBean.isGameStarted+"  isSpecatatorFound "+isSpecatatorFound+" isRematchPhase "+gameBean.isRematchPhase
				 		+" isRoundCompletedResultTime"+gameBean.isRoundCompletedResultTime+" players "+gameBean._players+" spectators "+gameBean.getSpecatatores();
		 LogBean lb = new LogBean();
		 lb.setValues(Constant.LOG, "P3", player, "UserDisconnected", var);
		 gameBean.addLog(lb);
	}
	
	public void spectatorDisconnected(GameBean gameBean, String player, boolean isSpecatatorFound)
	{
		 String var = " player "+player+" isGameStarted "+gameBean.isGameStarted+"  isSpecatatorFound "+isSpecatatorFound+" isRematchPhase "+gameBean.isRematchPhase
				 		+" players "+gameBean._players+" spectators "+gameBean.getSpecatatores();
		 LogBean lb = new LogBean();
		 lb.setValues(Constant.LOG, "P3", player, "SpectatorDisconnected", var);
		 gameBean.addLog(lb);
	}
	
	public void sqlException(GameBean gameBean, String var)
	{
		 LogBean lb = new LogBean();
		 lb.setValues(Constant.EXC, "P3", "System", "SQL_EXCEPTION", var);
		 gameBean.addLog(lb);
		 
		 gameBean.writeGameLog();
	}
	
	public void userJoinBack(GameBean gameBean, PlayerBean pb, int playerPos)
	{
		String var = " player "+pb._playerId+" position "+playerPos+" turn "+gameBean._roundBean.getTurn()+"  isNewlyOccupiedSeat "+pb.isNewlyOccupiedSeat
				+" isAi "+pb._isAI+" isFakePlayer "+pb.isFakePlayer+"  isAutoplay "+pb.isAutoPlay+"  isLeave "+pb.isLeave;

			LogBean lb = new LogBean();						
			lb.setValues(Constant.LOG, "P0", pb._playerId, "UserJoinBack", var);
			gameBean.addLog(lb);
	}
	
	public void playerJoinedInAIPlace(GameBean gameBean, PlayerBean pb, int playerPos)
	{
		String var = " player "+pb._playerId+" position "+playerPos+" turn "+gameBean._roundBean.getTurn()+"  isNewlyOccupiedSeat "+pb.isNewlyOccupiedSeat
				+" isAi "+pb._isAI+"  isFakePlayer "+pb.isFakePlayer+"  isAutoplay "+pb.isAutoPlay+"  isLeave "+pb.isLeave+" aiPlayer "+pb._aiPlayer+" initialPlayers "+gameBean.initialPlayers;

		LogBean lb = new LogBean();						
		lb.setValues(Constant.LOG, "P1", pb._playerId, "PlayerJoinedInAiPlace", var);
		gameBean.addLog(lb);
	}
	
	public void playerJoinInPrivateTable(GameBean gameBean, PlayerBean pb)
	{
		String var = " player "+pb._playerId+" posiotion "+pb.position+" players "+gameBean._players+" spectators "+gameBean.getSpecatatores();
		LogBean lb = new LogBean();						
		lb.setValues(Constant.LOG, "P1", pb._playerId, "PlayerJoinInPrivateTable", var);
		gameBean.addLog(lb);
	}
	
	public void occupieSeat(GameBean gameBean, int playerPos, String gameId, String tableId, String player)
	{
		String var = " player "+player+" playerPos "+playerPos+"  gameId "+gameId+" tableId "+tableId; 
			
		LogBean lb = new LogBean();						
		lb.setValues(Constant.LOG, "P1", player, "OccupieSeat", var);
		gameBean.addLog(lb);
	}
	
	public void resouceBusyTryAgain(GameBean gameBean, int playerPos, String gameId, String tableId, String player, ISFSObject sfso)
	{
		String var = " player "+player+" playerPos "+playerPos+"  gameId "+gameId+" tableId "+tableId
				+" extensionCommand "+sfso.getUtfString("extensionCommand"); 
		
		LogBean lb = new LogBean();						
		lb.setValues(Constant.LOG, "P1", player, "ResourceBusyTryAgain", var);
		gameBean.addLog(lb);
	}
	
	public void resetAIBits(GameBean gameBean, String var)
	{
		LogBean lb = new LogBean();						
		lb.setValues(Constant.LOG, "P1", "System", "ResetAIBits", var);
		gameBean.addLog(lb);
	}
	
	public void balootBonus(GameBean gameBean, ArrayList<Integer> al)
	{
		String var = " balootBonus "+al;
		
		LogBean lb = new LogBean();						
		lb.setValues(Constant.LOG, "P1", "System", "BalootBonus", var);
		gameBean.addLog(lb);
	}
	
	public void checkPlayerTurnBiddingorDiscard(GameBean gameBean, PlayerBean pb)
	{
		String var = " player "+pb._playerId+"  isAI "+pb._isAI+" isFakePlayer"+pb.isFakePlayer+"  isAutoplay "+pb.isAutoPlay+"  isRemainingCardsDistributed "+gameBean._roundBean.isRemainingCardsDistributed
						+" turn "+gameBean._roundBean.getTurn()+" turnForBid "+gameBean._roundBean.getTurnForBid();
		
		LogBean lb = new LogBean();						
		lb.setValues(Constant.LOG, "P1", pb._playerId, "CheckPlayerTurnBiddingorDiscard", var);
		gameBean.addLog(lb);
	}
	
	public void aiBid(GameBean gameBean, String player, String command)
	{
		String var = "AI Bid Timer Started Player :"+player+" Command "+command;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", player, "AI_Bid", var);
		gameBean.addLog(lb);
	}
	
	public void aiDiscard(GameBean gameBean, String player, String command)
	{
		String var = "AI Discard Timer Started Player :"+player+" Command "+command;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", player, "AI_Discard", var);
		gameBean.addLog(lb);
	}
	
	public void forceLoginUserDisconneted(GameBean gameBean, String player, String dummyUser)
	{
		String var = "player :"+player+"  dummyUser "+dummyUser;
		LogBean lb = new LogBean();
		lb.setValues(Constant.EXC, "P0", player, "ForceLoginUserDisconneted", var);
		gameBean.addLog(lb);
	}
	
	public void gamesCountBefore(GameBean gameBean, String player, int reduceCount, int freeGamesCount, int paidGamesCount)
	{
		String var = "player :"+player+"  reduceCount "+reduceCount+"  freeGamesCount "+freeGamesCount+"  paidGamesCount "+paidGamesCount+" isPrivateTable "+gameBean.isPrivateTable();
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", player, "GamesCountBefore", var);
		gameBean.addLog(lb);
	}
	
	public void gamesCountAfter(GameBean gameBean, String player, int reduceCount, int freeGamesCount, int paidGamesCount)
	{
		String var = "player :"+player+"  reduceCount "+reduceCount+"  freeGamesCount "+freeGamesCount+"  paidGamesCount "+paidGamesCount+" isPrivateTable "+gameBean.isPrivateTable();
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", player, "GamesCountAfter", var);
		gameBean.addLog(lb);
	}
	
	
	public void scoreCalculationBefore(GameBean gameBean, int t1RoundScore, int t2RoundScore)
	{
		String var = " bidwonperson "+gameBean._roundBean._bidWonPerson+" bid selected person "+gameBean._roundBean._bidSelectedPerson
					   +" isDobule "+gameBean._roundBean.isDouble+" doubleCount "+gameBean._roundBean.doubleCount
				       +" t1RoundScore "+t1RoundScore+"   t2RoundScore "+t2RoundScore
				       +" get(0)._playerId "+gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId
				       +" get(2)._playerId "+gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId;
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", "Server", "ScoreCalculationBefore", var);
		gameBean.addLog(lb);
	}
	
	public void scoreCalculationAfer(GameBean gameBean, int t1RoundScore, int t2RoundScore)
	{
		String var = " bidwonperson "+gameBean._roundBean._bidWonPerson+" bid selected person "+gameBean._roundBean._bidSelectedPerson
				   +" isDobule "+gameBean._roundBean.isDouble+" doubleCount "+gameBean._roundBean.doubleCount
			       +" t1RoundScore "+t1RoundScore+"   t2RoundScore "+t2RoundScore
			       +" get(0)._playerId "+gameBean._roundBean._playerRoundBeanObjs.get(0)._playerId
			       +" get(2)._playerId "+gameBean._roundBean._playerRoundBeanObjs.get(2)._playerId;
	
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P0", "Server", "ScoreCalculationAfter", var);
			gameBean.addLog(lb);
	}
	public void aiSound(GameBean gameBean, ISFSObject sfso, String info)
	{
		String var = " AISound  soundType "+sfso.getUtfString("soundType")+" info "+info+" roundCount "+gameBean.getRoundCount()
				+" trickCount "+gameBean._roundBean.trickCount+" player "+sfso.getUtfString("player");
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", "System", "AISound", var);
		gameBean.addLog(lb);
		
		gameBean.writeGameLog();
	}
	
	public void saveMedal(GameBean gameBean, String player, int count, int medal)
	{
		String var = " SaveMedal  player "+player+"  count "+count+"  medal "+medal;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", "System", "SaveMedal", var);
		gameBean.addLog(lb);
		
		gameBean.writeGameLog();
	}
	
	public void waitingPlayerJoined(GameBean gameBean, String player)
	{
		String var = " waitingPlayerJoined  player "+player+"  isRematchPhase "+gameBean.isRematchPhase;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", player, "WaitingPlayerJoined", var);
		gameBean.addLog(lb);
		
		gameBean.writeGameLog();
	}
	
	public void waitingPlayerLeft(GameBean gameBean, String player)
	{
		String var = " waitingPlayerLeft  player "+player+"  isRematchPhase "+gameBean.isRematchPhase;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", player, "WaitingPlayerLeft", var);
		gameBean.addLog(lb);
		
		gameBean.writeGameLog();
	}
	
	public void gamePoints(GameBean gameBean)
	{
		String var = " teamOneGamePoints "+gameBean._roundBean.teamOneGamePoints+" teamTwoGamePoints "
				+gameBean._roundBean.teamTwoGamePoints+" teamOneBonusPoints "+gameBean._roundBean.teamOneBonusPoints
				+"  teamTwoBounsPoints "+gameBean._roundBean.teamTwoBounsPoints;
		
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", "System", "GamePoints", var);
		gameBean.addLog(lb);
		
		gameBean.writeGameLog();
	}
}
