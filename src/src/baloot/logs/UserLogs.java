package src.baloot.logs;

import java.io.BufferedWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import src.baloot.exce.beans.LogBean;
import src.baloot.utils.Apps;


public class UserLogs implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static void addUserLog(LogBean lb, String gameId)
	{
		try
		{
			writeUserLog(lb, gameId);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

	private synchronized static void writeUserLog(LogBean lb, String gameId)throws Exception
	{
		String fileData = Apps.getDateString()+" | "+gameId+" | "+lb.getAction()+" | "+lb.getVariables();
				
		String filePath = Apps.getZonePath()+"/"+"user_logs"+"/"+getUserFileName(lb.getPlayer())+".txt";		
		Path path = Paths.get(filePath);		
		
 		File userFile = new File(filePath);
 		
 		if(!userFile.exists())
 		{
 				Files.createDirectories(path.getParent());
 				Files.createFile(path);
 		}

		FileWriter fileWriter = new FileWriter(userFile, true); 				
		BufferedWriter out = new BufferedWriter(fileWriter);
		
		out.write(fileData);
		out.newLine();
	
		out.close(); 
		fileWriter.close();
	}
	
	
	
	public static String getUserFileName(String player)
	{
		String fileName = "zzz"+"/"+player+"_"+Apps.getDayString();
		String str = player.toLowerCase();
		
		if(player.length() > 2)
		{
			fileName = str.charAt(0)+"/"+str.substring(0,3)+"/"+player+"_"+Apps.getDayString();
		}
		
		return fileName;
	}


}
