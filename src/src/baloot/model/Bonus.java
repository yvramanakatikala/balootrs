package src.baloot.model;

import java.util.ArrayList;



import src.baloot.beans.BonusBean;
import src.baloot.beans.GameBean;
import src.baloot.constants.Constant;
import src.baloot.exce.beans.LogBean;
import src.baloot.utils.Apps;
import src.baloot.utils.CalculateScores;

import com.smartfoxserver.v2.entities.Room;

public class Bonus {
	
	public synchronized boolean isSaidBonus(GameBean gameBean, String player)
	{
		//Apps.showLog("isSaidBonus");
		
		//Apps.showLog("Player "+ player);
		//Apps.showLog("Player Highest"+ gameBean._roundBean._highestBonusPerson);
		
		boolean sign = true;
		ArrayList<BonusBean> removedItems = new ArrayList<BonusBean>();
		
		if(gameBean._roundBean._highestBonusPerson.equals(player))
		{
			for(BonusBean bb : gameBean._roundBean._bonusObjs)
			{
				if(bb._playerId.equals(player))
				{
					if(!bb._isShowedCards && bb._bonusType.equals("sira") )
					{
						// Set the highest remaining and remove the existing one
						//AppMethods.showLog("Forget To Say sira highest bonus****");
						removedItems.add(bb);
						sign = true;
					}					
					else if(bb._isShowedCards ||bb._bonusType.equals("sira") || bb._bonusType.equals("baloot"))
					{
						sign = true;
					}
					else
					{					
						sign = false;						
					}
					
					if(!sign)
						break;
				}
			
			}
			
			if(sign && removedItems.size()>0)
			{
				gameBean._roundBean._bonusObjs.removeAll(removedItems);					
				gameBean.getDiscardInstance().setHighestBonusPerson(gameBean);
			}
			
			gameBean.getGameLogInstance().isSaidBonusRemovedBonuses(gameBean, player, sign, removedItems);
		}
			
		gameBean.getGameLogInstance().isSaidBonus(gameBean, player, sign);
		
		return sign;
	}
	
	public synchronized boolean isPartnerSaidShowCards(GameBean gameBean, String player, Room room, int cardId)
	{
		boolean sign = true;
		
		//AppMethods.showLog("checkPartnerSaidShowCards"); 
		if(gameBean._roundBean._bonusObjs.size() >= 2)
		{
			//Apps.showLog(" >= 2"); 
			String partner = gameBean.getPartner(player);
			
			// Check for partner show the cards
			for(BonusBean bb : gameBean._roundBean._bonusObjs)
			{
				boolean isRoundCompleted = false;
				
				if(bb._playerId.equals(partner))
				{
					if(bb._isShowedCards)
					{
						ArrayList<String> bonusTypes = new ArrayList<String>();
						for(BonusBean bonusBean : gameBean._roundBean._bonusObjs)
						{
							if(bonusBean._playerId.equals(player))
							{								
								bonusBean._isShowedCards = true;
								bonusTypes.add(bonusBean._bonusType);
								
								gameBean.getDiscardInstance().trickCompleted(gameBean, player, cardId, room);									
								break;
							}							
						}						
					}
					else
					{
						// Game Round Completed
						CalculateScores cs = new CalculateScores();
						cs.addGamePoints(player, gameBean);
						sign = false;
						
						// Add log 
						String var = " player "+player+" bonus_failed";
						LogBean lb = new LogBean();
						lb.setValues(Constant.LOG, "P3", player, "BonusFailed", var);
						gameBean.addLog(lb);
						lb = null;	
						
						gameBean.getDiscardInstance().roundCompleted(gameBean, player, cardId, "",  true, false, false);
						
						isRoundCompleted = true;
					}
				}
				else
				{
					CalculateScores cs = new CalculateScores();
					cs.addGamePoints(player, gameBean);	
					sign = false;
					// Add log 
					String var =" player "+player+" bonus_failed3";
					
					LogBean lb = new LogBean();
					lb.setValues(Constant.LOG, "P2", player, "BonusFailed", var);
					gameBean.addLog(lb);
					lb = null;	
					
					gameBean.getDiscardInstance().roundCompleted(gameBean, player, cardId, "", true, false, false);	
					isRoundCompleted = true;
				}
				
				if(isRoundCompleted)
				{
					break;
				}
			}
		}
		else
		{
			// Game Round Completed
			Apps.showLog("Game completed");
			CalculateScores cs = new CalculateScores();
			cs.addGamePoints(player, gameBean);
			sign = false;
			// Add log 
			String var = " player "+player+" bonus_failed4";
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P3", player, "BonusFailed", var);
			gameBean.addLog(lb);
			lb = null;
			
			gameBean.getDiscardInstance().roundCompleted(gameBean, player, cardId, "", true, false, false );
		}	
		
		return sign;
	}
	
	public synchronized void showBonusAutomatically(GameBean gameBean, String player, Room room)
	{
		Apps.showLog(" showBonusAutomatically");
		boolean showBonus = false;
		
		
		for(BonusBean bb : gameBean._roundBean._bonusObjs)
		{
			if(bb._playerId.equals(player))
			{
				if(gameBean._roundBean._highestBonusPerson.equals(player) || gameBean._roundBean._highestBonusPerson.equals(gameBean.getPartner(player)))
				{
					for(BonusBean bonusBean : gameBean._roundBean._bonusObjs)
					{
						if(bonusBean._playerId.equals(gameBean.getPartner(player)))
						{
							if(bonusBean._isShowedCards)
							{
								showBonus = true;
								break;
							}
						}
					}
				}
			}
			
			if(showBonus)
			{
				break;
			}
		}
		
		if(showBonus)
		{
			ArrayList<Integer> bonusCards = new ArrayList<Integer>();
			String bonusType = "";
			gameBean.getDiscardInstance().bonusRequest("showBonus", bonusCards, bonusType, player, gameBean, false);
			gameBean.getGameLogInstance().showBonusAutomatically(gameBean, player);
		}		
	}

}
