package src.baloot.model;

import java.util.ArrayList;



import java.util.Collections;

import src.baloot.ai.sounds.AISound;
import src.baloot.beans.BonusBean;
import src.baloot.beans.GameBean;
import src.baloot.beans.PlayedCard;
import src.baloot.beans.PlayerBean;
import src.baloot.beans.PlayerRoundBean;
import src.baloot.classes.RememberMessage;
import src.baloot.constants.Constant;
import src.baloot.exce.DataValidation;
import src.baloot.exce.Validations;
import src.baloot.utils.Apps;
import src.baloot.utils.CalculateScores;
import src.baloot.utils.Commands;
import src.baloot.utils.GameApps;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class Discard {
	
	public synchronized void discardRequest(GameBean gameBean, String player, int cardId, String suit, String request)
	{	
		Room room = Apps.getRoomByName(gameBean.tableId);
		gameBean.incrementTicket();
		
		PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(player);
		
		if(prb._playedcards.size() == gameBean._roundBean.trickCount-1 && player.equals(gameBean._roundBean.getTurn()))
		{	
			gameBean.getGameLogInstance().discardRequest(gameBean, player, cardId, request);
			
			// Set the last Updated Time
			gameBean._roundBean.setLastUpdatedTime();
			gameBean._roundBean._delayDiscard = false;
			// Remember Message 
			RememberMessage rm = new RememberMessage(gameBean, player, cardId, suit);
			rm.saveMessage();
			rm = null;
			
			// add discard to open card
			Integer removedCard = new Integer(cardId);
			
			gameBean._roundBean.openCardsList.add(cardId);	
			prb._currentCard = cardId;
			prb._playedcards.add(cardId);
			prb._cards.remove(removedCard);
			
			//System.out.println(" Open Card Count " +gameBean._roundBean._openCard.size());
			PlayedCard pc = new PlayedCard(); 
			
			if(gameBean._roundBean._gameType.equals(Commands.HOKOM) && gameBean._roundBean.openCardsList.size() == 1 && gameBean._roundBean._trumpSuitType.equals(getSuit(cardId)))
			{
				gameBean._roundBean._lastHokomSuitTrick = new ArrayList<PlayedCard>();
				gameBean._roundBean._noofHokomSuitTricks++;
				pc._playerId = player;
				pc._cardid = cardId;
				gameBean._roundBean._lastHokomSuitTrick.add(pc);
			}
			else if(gameBean._roundBean._lastHokomSuitTrick.size()>=1 && gameBean._roundBean._lastHokomSuitTrick.size()<=3)
			{
				pc._playerId = player;
				pc._cardid = cardId;
				gameBean._roundBean._lastHokomSuitTrick.add(pc);
			}

			/*// Write the code for bonus baloot
			if(gameBean._roundBean._gameType.equals(Commands.HOKOM) && gameBean._roundBean._bonusObjs.size()>=1)
			{
				for(BonusBean bb : gameBean._roundBean._bonusObjs)
				{
					if(bb._bonusType.equals(Commands.BALOOT) && bb._playerId.equals(player) && gameBean._roundBean.trickCount == 7)
					{
						checkBalootBonusNew(cardId, player, gameBean, room);
						break;	
					}
				}
			}*/
			
			
			// bonus code written by Ramana
			if((gameBean._roundBean.trickCount == 7 || gameBean._roundBean.trickCount == 8) && !gameBean._roundBean._balootShowed)
			{
				if(gameBean._roundBean._gameType.equals(Commands.HOKOM) && gameBean._roundBean._bonusObjs.size()>=1)
				{				
					for(BonusBean bb : gameBean._roundBean._bonusObjs)
					{
						if(bb._bonusType.equals(Commands.BALOOT) && bb._playerId.equals(player))
						{
							//checkBalootBonusNew(cardId, player, gameBean, room);
							
							gameBean._roundBean._balootShowed = true;
															
							ISFSObject sfso = new SFSObject();	
							
							sfso.putUtfString("command", "requestBonus");
							sfso.putUtfString("player", player );
							sfso.putUtfString("gender", Apps.getGender(player ));
							sfso.putUtfString("bonusType", Commands.BALOOT);
							
							sfso.putInt("ticket", gameBean.getTicket());
							sfso.putInt("roundCount", gameBean.getRoundCount());
							sfso.putInt("trickCount", gameBean._roundBean.trickCount);
							
							Commands.appInstance.send(Commands.Bonus, sfso, room.getUserList());
							gameBean.getGameLogInstance().serverToClientBonus(gameBean, sfso);
							
							break;	
						}
					}				
				}
			}
			
			
			if(gameBean._roundBean.openCardsList.size() == 1)
			{
				gameBean._roundBean.currentSuit = suit;
				for(int i=0;i<gameBean._roundBean._playerRoundBeanObjs.size();i++)
				{
					gameBean._roundBean._playerRoundBeanObjs.get(i)._firstPlayedCard = new ArrayList<Integer>();
				}
				
				gameBean._roundBean.getPlayerRoundBeanObject(player)._firstPlayedCard.add(cardId);			
			}	
			
			// Check for open cards count
			if(gameBean._roundBean.openCardsList.size() == 4)
			{		
				gameBean._roundBean._delayDiscard = true;
				
				// Calculate who will win the round
				CalculateScores cs = new CalculateScores();			
				
				int card = cs.getRoundWinCard(gameBean, gameBean._roundBean.currentSuit);
				
				int score = cs.calculateScore(gameBean);
				
				String wonPlayer = cs.getPlayer(gameBean, card);
			
				gameBean.getGameLogInstance().trickCompleted(gameBean, wonPlayer, card, score);
				
				
				AISound.sendTrickCompletedSound(gameBean, room, wonPlayer, card);
				AISound.checkPlayerWonTrickInSeries(gameBean, room, wonPlayer);
				
				if(wonPlayer == null)
				{
					Validations.raiseTrickWinnerNotFoundException(gameBean, player, wonPlayer);
				}

				prb = gameBean._roundBean.getPlayerRoundBeanObject(wonPlayer);
				prb._totalScore = prb._totalScore + score;										
				
				
				prb.incrementTricksWonCount();
				
				for(int j=0;j<gameBean._roundBean.openCardsList.size();j++)
				{
					Integer cardID = gameBean._roundBean.openCardsList.get(j);
					prb._wonCards.add(cardID);	
					
					for(int k=0;k<gameBean._roundBean._cardsInHand.size();k++)
					{
						if(gameBean._roundBean._cardsInHand.get(k) == cardID )
						{
							gameBean._roundBean._cardsPlayed.add(gameBean._roundBean._cardsInHand.get(k));
							gameBean._roundBean._cardsInHand.remove(k);
							break;
						}
					}
					
					Apps.showLog("Card Removed");
				}
				
				if(gameBean._roundBean.trickCount == 8)
				{				
					Apps.showLog("Game Round Completed");					
					cs.calculateTeamScoresNew(gameBean, wonPlayer);	
										
					roundCompleted(gameBean, player, cardId, wonPlayer, false, false, false);	
				}
				else
				{	
					setTurnToTrickWinner(gameBean, wonPlayer);
					
					if(gameBean._roundBean.trickCount == 2 && gameBean._roundBean._bonusObjs.size() > 0)
					{
						Apps.showLog("******");
						if(gameBean.getBonusInstance().isSaidBonus(gameBean, player))
						{
							trickCompleted(gameBean, player, cardId, room);
						}
						else
						{							
							gameBean.getBonusInstance().isPartnerSaidShowCards(gameBean, player, room, cardId);						
						}
					}
					else
					{				
						trickCompleted(gameBean, player, cardId, room);
					}				
				}
			}
			else
			{
				// Open card Count less than 4
				Apps.showLog("Open card Count less than 4");
				if(gameBean._roundBean.trickCount == 2 && gameBean._roundBean._bonusObjs.size()>0)
				{
					if(gameBean.getBonusInstance().isSaidBonus(gameBean, player))
					{
						Apps.showLog("Said bonus");
						gameBean._roundBean.setTurn();
						
						ISFSObject sfso = new SFSObject();
						
						sfso.putUtfString("suit", gameBean._roundBean.currentSuit );
						sfso.putInt("cardId", cardId);
						sfso.putUtfString("discardUser", player);
						sfso.putUtfString("turn",  gameBean._roundBean.getTurn());
						sfso.putInt("ticket", gameBean.getTicket());
						sfso.putInt("roundCount", gameBean.getRoundCount());
						sfso.putInt("trickCount", gameBean._roundBean.trickCount);
						sfso.putBool("isDeclaredBonus", isDeclaredBonus(gameBean, gameBean._roundBean.getTurn()));
	
						Commands.appInstance.send(Commands.DISCARD,sfso, room.getUserList());	
						
						gameBean.getBonusInstance().showBonusAutomatically(gameBean, gameBean._roundBean.getTurn(), room);
						
						gameBean.startTimer(Constant.DISCARD_TIME, gameBean._roundBean.getTurn(), Commands.DISCARD);
						
						Commands.appInstance.ai.checkPlayerTurnBiddingorDiscard(gameBean, gameBean._roundBean.getTurn());	
					}
					else
					{
						Apps.showLog("Check partner said bonus");
						gameBean._roundBean.setTurn();
						
						ISFSObject sfso = new SFSObject();
						sfso.putUtfString("suit", gameBean._roundBean.currentSuit );
						sfso.putInt("cardId", cardId);
						sfso.putUtfString("discardUser", player);
						sfso.putUtfString("turn",  gameBean._roundBean.getTurn());
						
						boolean sign = gameBean.getBonusInstance().isPartnerSaidShowCards(gameBean, player, room, cardId);						
						
						if(sign)
						{
							gameBean.getBonusInstance().showBonusAutomatically(gameBean, gameBean._roundBean.getTurn(),room);
						}
					}
				}
				else
				{
					// Send card and turn event
					gameBean._roundBean.setTurn();
					
					ISFSObject sfso = new SFSObject();
					
					sfso.putUtfString("suit", gameBean._roundBean.currentSuit );
					sfso.putInt("cardId", cardId);
					sfso.putUtfString("discardUser", player);
					sfso.putUtfString("turn",  gameBean._roundBean.getTurn());
					sfso.putInt("ticket", gameBean.getTicket());
					sfso.putInt("roundCount", gameBean.getRoundCount());
					sfso.putInt("trickCount", gameBean._roundBean.trickCount);
					sfso.putBool("isDeclaredBonus", isDeclaredBonus(gameBean, gameBean._roundBean.getTurn()));
					
					Commands.appInstance.send(Commands.DISCARD, sfso, room.getUserList());
					gameBean.startTimer(Constant.DISCARD_TIME, gameBean._roundBean.getTurn(), Commands.DISCARD);
					
					Commands.appInstance.ai.checkPlayerTurnBiddingorDiscard(gameBean, gameBean._roundBean.getTurn());	
				}			
			}
		}
		else
		{
			Validations.raiseTrickCountCardsSizeNotMatchException(gameBean, prb);
		}
		
	}
	
	public synchronized void setTurnToTrickWinner(GameBean gameBean, String trickWinner)
	{
		for(PlayerBean pb : gameBean._playersBeansList)
		{
			pb._isTurn = false;
		}
		
		gameBean._roundBean._turn = trickWinner;
		PlayerBean pb = gameBean.getPlayerBean(trickWinner);
		pb._isTurn = true;		
		gameBean.getGameLogInstance().setTurnToTrickWinner(gameBean, trickWinner);
	}
	
	public synchronized void trickCompleted(GameBean gameBean, String player, int cardId, Room room)
	{
		gameBean._roundBean.openCardsList =  new ArrayList<Integer>();
		gameBean._roundBean.trickCount++;
		
		ISFSObject sfso = new SFSObject();				
		sfso.putUtfString("discardUser", player);
		sfso.putInt("cardId", cardId);
		sfso.putUtfString("turn",  gameBean._roundBean.getTurn());
		sfso.putInt("ticket", gameBean.getTicket());
		sfso.putInt("roundCount", gameBean.getRoundCount());
		sfso.putInt("trickCount", gameBean._roundBean.trickCount);
		sfso.putBool("isDeclaredBonus", isDeclaredBonus(gameBean, gameBean._roundBean.getTurn()));
		
		Commands.appInstance.send(Commands.TRICK_COMPLETED, sfso, room.getUserList());		
		gameBean.startTimer(Constant.DISCARD_TIME, gameBean._roundBean.getTurn(), Commands.DISCARD);
		gameBean._roundBean.sendSyncDataToNewlySeatOccupiedPlayer();
		Commands.appInstance.ai.checkPlayerDiscardForTrickCompletion(gameBean, gameBean._roundBean.getTurn());
			
		gameBean.getGameLogInstance().startNextTrick(gameBean);
	}
	
	public synchronized void roundCompleted(GameBean gameBean, String player, int cardId, String winner, boolean isBonusFail, boolean isHighestFail, boolean isClosedDoubleFail)
	{
		// Stop All Timers
		gameBean.stopAllTimers();
		Room room = Apps.getRoomByName(gameBean.tableId);
		
		ISFSObject sfso = new SFSObject();
		
		sfso = GameApps.pushRoundScoreDetails(gameBean, sfso);
		sfso.putInt("ticket", gameBean.getTicket());
		sfso.putInt("roundCount", gameBean.getRoundCount());
		sfso.putInt("trickCount", gameBean._roundBean.trickCount);
		sfso.putUtfString("turn",  winner);	
		
		gameBean.getGameLogInstance().scoresLog(gameBean);
		gameBean.incrementTicket();
		
		if(isGameEnd(gameBean))
		{
			ArrayList<String> winners = getWinnersTeam(gameBean);
			ArrayList<String> loosers = getLoosersTeam(gameBean, winners);
			setWinnerPosition(winners, gameBean);
			
			sfso.putUtfString("discardUser", player);
			sfso.putInt("cardId", cardId);
			sfso.putUtfStringArray("winners", winners);
			sfso.putUtfStringArray("loosers", loosers);	
			sfso.putBool("isBonusFail", isBonusFail);
			sfso.putBool("isHighestFail", isHighestFail);
			sfso.putBool("isPrivateTable", gameBean.isPrivateTable());
			sfso.putUtfString("host", gameBean.host);
			
			if(isHighestFail)
			{
				CalculateScores cs = new CalculateScores();
				int highestCard = cs.getHighestCard(cardId, gameBean, GameApps.getSuit(cardId));
				sfso.putBool("isClosedDoubleFail", false);
				sfso.putInt("highestCard", highestCard);
				sfso.putUtfString("highestCardPlayer", GameApps.getHighestCardPlayer(gameBean, highestCard));
			}
			else
			{
				if(isClosedDoubleFail)
				{
					sfso.putBool("isClosedDoubleFail", true);						
				}
				else
				{
					sfso.putBool("isClosedDoubleFail", false);
				}
			}
			
			AISound.sendGameCompletedSound(gameBean, room);
			Commands.appInstance.send(Commands.GAME_COMPLETED, sfso, room.getUserList());
			Commands.appInstance.sgBsn.gameCompleted(gameBean);
		}
		else
		{
			// Game not completed ... start next round.
			Apps.showLog("Is Bonus"+ isBonusFail);
			sfso.putUtfString("discardUser", player);
			sfso.putInt("cardId", cardId);
			sfso.putBool("isBonusFail", isBonusFail);
			
			ArrayList<String> bonusLosers = new ArrayList<String>();
			ArrayList<String> closedDoubleLosers = new ArrayList<String>();
			ArrayList<String> highestLosers =new ArrayList<String>();
			boolean isHighFail = false;
			boolean isCloseFail = false;
			
			if(isBonusFail)
			{
				bonusLosers = gameBean.getTeam(player);
			}
			else
			{
				if(isHighestFail)
				{
					highestLosers = gameBean.getTeam(player);
					isHighFail = true;
					
					CalculateScores cs = new CalculateScores();
					int highestCard = cs.getHighestCard(cardId, gameBean, GameApps.getSuit(cardId));
					sfso.putInt("highestCard", highestCard);
					sfso.putUtfString("highestCardPlayer", GameApps.getHighestCardPlayer(gameBean, highestCard));
				}
				else
				{
					if(isClosedDoubleFail)
					{
						isCloseFail = true;
						closedDoubleLosers = gameBean.getTeam(player);
					}
				}	
			}
			
			sfso.putBool("isHighestFail", isHighFail);
			sfso.putBool("isClosedDoubleFail", isCloseFail);
			sfso.putUtfStringArray("bonusLosers", bonusLosers );
			sfso.putUtfStringArray("highestLosers", highestLosers);
			sfso.putUtfStringArray("closedDoubleLosers", closedDoubleLosers );			
			
			Commands.appInstance.send(Commands.ROUND_COMPLETED, sfso, room.getUserList());
			gameBean.isRoundCompletedResultTime = true;
			gameBean.getGameLogInstance().roundCompleted(gameBean, sfso);
			Commands.appInstance.ulBsn.updateLobby(gameBean);	
			
			AISound.roundCompletedSound(gameBean, room);
			
			if(isBonusFail || isHighFail || isCloseFail)
			{
				gameBean.startTimer(Constant.NEXT_ROUND_START_TIME + 1, gameBean._roundBean.getTurn(), Commands.START_NEXT_ROUND);
			}
			else
			{
				gameBean.startTimer(Constant.NEXT_ROUND_START_TIME, gameBean._roundBean.getTurn(), Commands.START_NEXT_ROUND);
			}
		}
	}
	
	private synchronized boolean isGameEnd(GameBean gameBean)
	{
		for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(pb._totalPoints >= Constant.GAME_END_POINTS)
			{
				return true;
			}
		}
		
		return false;
	}
	
	private synchronized ArrayList<String> getWinnersTeam(GameBean gameBean)
	{
		
		ArrayList<String> winners = new ArrayList<String>();
		
		if(GameApps.getTeamOneScore(gameBean) >= Constant.GAME_END_POINTS && GameApps.getTeamTwoScore(gameBean) >= Constant.GAME_END_POINTS)
		{
			if(GameApps.getTeamOneScore(gameBean) > GameApps.getTeamTwoScore(gameBean))
			{
				winners = GameApps.getTeamOnePlayers(gameBean);
			}
			else
			{
				winners = GameApps.getTeamTwoPlayers(gameBean);
			}
		}
		else if(GameApps.getTeamOneScore(gameBean) >= Constant.GAME_END_POINTS)
		{
			winners = GameApps.getTeamOnePlayers(gameBean);
		}
		else if(GameApps.getTeamTwoScore(gameBean) >= Constant.GAME_END_POINTS)
		{
			winners = GameApps.getTeamTwoPlayers(gameBean);
		}
		
		return winners;
	}
	
	public synchronized ArrayList<String> getLoosersTeam(GameBean gameBean, ArrayList<String> winners)
	{
		ArrayList<String> loosers = new ArrayList<String>();
		
		for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(!winners.contains(pb._playerId))
			{
				loosers.add(pb._playerId);
			}
		}
		
		return loosers;
	}
		
	private synchronized void checkBalootBonus(Integer cardId,String player,GameBean gameBean, Room room)
	{			
		ArrayList<Integer> bonusCards = new ArrayList<Integer>();
		
		if(gameBean._roundBean._trumpSuitType.equals("clubs")){
			bonusCards.add(2);bonusCards.add(3);
		}
		else if(gameBean._roundBean._trumpSuitType.equals("diamonds")){
			bonusCards.add(10);bonusCards.add(11);
		}
		else if(gameBean._roundBean._trumpSuitType.equals("hearts")){
			bonusCards.add(18);bonusCards.add(19);
		}
		else{
			bonusCards.add(26);bonusCards.add(27);
		}
		
		for(int i=0; i<gameBean._roundBean._playerRoundBeanObjs.size();i++)
		{
			if(gameBean._roundBean._playerRoundBeanObjs.get(i)._playerId.equals(player))
			{				
				boolean sign = true;				
				if(sign)
				{
					int cardsCount = 0;
					int balootCards = 0;
					
					for(int j=0;j<gameBean._roundBean._playerRoundBeanObjs.get(i)._cards.size();j++)
					{
						
						for(int k=0;k<bonusCards.size();k++)
						{
							if(gameBean._roundBean._playerRoundBeanObjs.get(i)._cards.get(j) == bonusCards.get(k))
							{
								cardsCount++;							
							}
						}
					}
					
					
					
					for(int j=0;j<gameBean._roundBean._playerRoundBeanObjs.get(i)._playedcards.size();j++)
					{
						for(int k=0;k<bonusCards.size();k++)
						{
							if(gameBean._roundBean._playerRoundBeanObjs.get(i)._playedcards.get(j) == bonusCards.get(k))
							{
								balootCards++;							
							}
						}
					}
					
					
					if((cardsCount == 1 || balootCards == 2) && !gameBean._roundBean._balootShowed)
					{						
						for(int l=0;l<gameBean._roundBean._bonusObjs.size();l++)
						{
							if(gameBean._roundBean._bonusObjs.get(l)._playerId.equals(player) && gameBean._roundBean._bonusObjs.get(l)._bonusType.equals("baloot"))
							{
								//AppMethods.showLog("*********** Request Bonus **********");
								gameBean._roundBean._balootShowed = true;
								// Send the baloot Cards								
								ISFSObject sfso = new SFSObject();	
								
								sfso.putUtfString("command", "requestBonus");
								sfso.putUtfString("player",gameBean._roundBean._bonusObjs.get(l)._playerId );
								sfso.putUtfString("gender",Apps.getGender(gameBean._roundBean._bonusObjs.get(l)._playerId ));
								//bonusTypes.add(gameBean._roundBean._bonusObjs.get(l)._bonusType);
								sfso.putUtfString("bonusType","baloot");
								//sfso.putIntArray("bonusCards",gameBean._roundBean._bonusObjs.get(l)._cards);	
								
								sfso.putInt("ticket", gameBean.getTicket());
								sfso.putInt("roundCount", gameBean.getRoundCount());
								sfso.putInt("trickCount", gameBean._roundBean.trickCount);
								
								Commands.appInstance.send(Commands.Bonus,sfso, room.getUserList());
								gameBean.getGameLogInstance().serverToClientBonus(gameBean, sfso);
								break;
							}							
						}						
					}					
				}
				
				break;
			}			
		}
		
	}
	
	private synchronized void checkBalootBonusNew(Integer cardId, String player, GameBean gameBean, Room room)
	{				
		if(!gameBean._roundBean._balootShowed)
		{						
			for(int l=0;l<gameBean._roundBean._bonusObjs.size();l++)
			{
				if(gameBean._roundBean._bonusObjs.get(l)._playerId.equals(player) && gameBean._roundBean._bonusObjs.get(l)._bonusType.equals("baloot"))
				{
					//AppMethods.showLog("*********** Request Bonus **********");
					gameBean._roundBean._balootShowed = true;
					// Send the baloot Cards								
					ISFSObject sfso = new SFSObject();	
					
					sfso.putUtfString("command", "requestBonus");
					sfso.putUtfString("player", gameBean._roundBean._bonusObjs.get(l)._playerId );
					sfso.putUtfString("gender", Apps.getGender(gameBean._roundBean._bonusObjs.get(l)._playerId ));
					//bonusTypes.add(gameBean._roundBean._bonusObjs.get(l)._bonusType);
					sfso.putUtfString("bonusType","baloot");
					//sfso.putIntArray("bonusCards",gameBean._roundBean._bonusObjs.get(l)._cards);		
					
					sfso.putInt("ticket", gameBean.getTicket());
					sfso.putInt("roundCount", gameBean.getRoundCount());
					sfso.putInt("trickCount", gameBean._roundBean.trickCount);
					
					Commands.appInstance.send(Commands.Bonus,sfso, room.getUserList());					
					gameBean.getGameLogInstance().serverToClientBonus(gameBean, sfso);
					break;
				}							
			}						
		}		
	}
	
	public synchronized void bonusRequest(String command, ArrayList<Integer> cards, String bonusType, String player, GameBean gameBean, boolean isAI)
	{
		// ExceptionValidations 
		Room room = Apps.getRoomByName(gameBean.tableId);
		
		//DataValidation.bonusHandleClientRequest(command, cards, bonusType, player, gameBean, room, isAI);
		gameBean.incrementTicket();
		
		PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(player);
		
		gameBean.getGameLogInstance().bonusRequest(command, cards, bonusType, player, gameBean, isAI, prb);
		
		if(command.equals("requestBonus"))
		{		
			BonusBean bbean = new BonusBean();
			bbean.setPlayerId(player);
			bbean.setBonusType(bonusType);
			bbean.setCards(cards);		
			
			gameBean._roundBean._bonusObjs.add(bbean);
			
			if(!bonusType.equals("baloot"))
			{
				setHighestBonusPerson(gameBean);
			}
			 
			if(bonusType.equals("baloot"))
			{
				if(prb != null)
				{
					prb.isDeclaredBalootBonus = true;
				}				
			}
			
			ISFSObject sfso = new SFSObject();
			
			sfso.putUtfString("command", command);
			sfso.putUtfString("player", player );
			sfso.putUtfString("gender", Apps.getGender(player) );
			sfso.putUtfString("bonusType", bonusType);	
			
			sfso.putInt("ticket", gameBean.getTicket());
			sfso.putInt("roundCount", gameBean.getRoundCount());
			sfso.putInt("trickCount", gameBean._roundBean.trickCount);
			
			Commands.appInstance.send(Commands.Bonus, sfso, room.getUserList());
			gameBean.getGameLogInstance().serverToClientBonus(gameBean, sfso);
			AISound.sendBonusSound(gameBean, room, player, bonusType);
			
			
			if(prb != null)
			{
				prb.isDeclaredBonus = true;
			}
		}
		else if(command.equals("showBonus"))
		{
			// Check that person having the highest bonus or not			
			if(player.equals(gameBean._roundBean._highestBonusPerson) || player.equals(gameBean.getPartner(gameBean._roundBean._highestBonusPerson)))
			{
				ArrayList<String> bonusTypes = new ArrayList<String>();
				ArrayList<Integer> bonusCards = new ArrayList<Integer>();
				boolean sign = false;
				
				// Send the bonus
				for(int i=0;i<gameBean._roundBean._bonusObjs.size();i++)
				{	
					if(gameBean._roundBean._bonusObjs.get(i)._playerId.equals(player) && !gameBean._roundBean._bonusObjs.get(i)._isShowedCards)
					{
						sign = true;
						gameBean._roundBean._bonusObjs.get(i).setShowCardsBits(true);	
						
						if(!gameBean._roundBean._bonusObjs.get(i)._bonusType.equals("baloot"))
						{
							bonusTypes.add(gameBean._roundBean._bonusObjs.get(i)._bonusType);
							ArrayList< Integer> bonuscards = gameBean._roundBean._bonusObjs.get(i).getCards();
							for(int j=0;j<bonuscards.size();j++)
							{
								bonusCards.add(bonuscards.get(j));
							}
						}
						else
						{
							// no need to add the baloot bouns
						}
							
						
						/* Medals list implementation for bonuses */	
						try
						{
							Commands.appInstance.medalsBsn.bonusMedals(player, bonusTypes, gameBean);
						}catch(Exception e)
						{
							e.printStackTrace();
						}
						
					}
				}
						
				if(sign)
				{					
					ArrayList<Integer> bcards = new ArrayList<Integer>();
					
					// Remove the duplicate cards
					for(int i=0;i<bonusCards.size();i++)
					{
						boolean sin = false;
						for(int j=0;j<bcards.size();j++)
						{							
							if(bcards.get(j) == bonusCards.get(i))
							{
								sin = true;
								break;
							}							
						}
						if(!sin)
							bcards.add(bonusCards.get(i));						
					}
				
					ISFSObject sfso = new SFSObject();
					
					sfso.putUtfString("command", command);
					sfso.putUtfString("player",player );
					sfso.putUtfString("gender", Apps.getGender(player));
					sfso.putUtfStringArray("bonusType",bonusTypes);						
					sfso.putIntArray("bonusCards", bonusCards);	
					
					sfso.putInt("ticket", gameBean.getTicket());
					sfso.putInt("roundCount", gameBean.getRoundCount());
					sfso.putInt("trickCount", gameBean._roundBean.trickCount);
					
					Commands.appInstance.send(Commands.Bonus, sfso, room.getUserList());
					gameBean.getGameLogInstance().serverToClientBonusArray(gameBean, sfso);
					// Check for baloot bonus
					anounceBalootBonus(gameBean, player, bcards, room);
					
					if(bonusType.equals("400"))
					{
						AISound.show400BonusSound(gameBean, bonusType, player);
					}
					
				}
			}
		}	
		
	}
	
	private synchronized void anounceBalootBonus(GameBean gameBean, String player, ArrayList< Integer> bonusCards, Room room )	
	{
		
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{
			String gameSuit = gameBean._roundBean._trumpSuitType;
			String bonuscardSuit = "";
			ArrayList<Integer> balootBonusCards = new ArrayList<Integer>();
			
			
			// Check for K and Q
			for(int i=0;i<bonusCards.size();i++)
			{
				if(bonusCards.get(i)%8 == 2 || bonusCards.get(i)%8 == 3)
				{
					balootBonusCards.add(bonusCards.get(i));
				}
			}
				
			if(balootBonusCards.size()== 2)
			{				
				bonuscardSuit = gameBean._roundBean.getSuit(balootBonusCards.get(0));
				if(gameSuit.equals(bonuscardSuit))
				{
					
					BonusBean bbean = new BonusBean();
					bbean.setPlayerId(player);
					bbean.setBonusType("baloot");
					bbean.setCards(balootBonusCards);
					
					boolean balootBonus = false;
					for(int i=0;i<gameBean._roundBean._bonusObjs.size();i++)
					{
						if(gameBean._roundBean._bonusObjs.get(i)._bonusType.equals("baloot"))
						{
							balootBonus = true;
						}
					}
					if(!balootBonus)
					{
						// Adding Baloot Bonus
						gameBean._roundBean._bonusObjs.add(bbean);
					}
				}
			}
		}
		
	}
	
	public synchronized void setHighestBonusPerson(GameBean gameBean)
	{
		if(gameBean._roundBean._bonusObjs.size() > 0)
		{
			if(gameBean._roundBean._bonusObjs.size() == 1)
			{
				gameBean._roundBean._highestBonusPerson = gameBean._roundBean._bonusObjs.get(0)._playerId;			
			}
			else
			{
				BonusBean tempBonusBean = gameBean._roundBean._bonusObjs.get(0);
				for(int i=1; i<gameBean._roundBean._bonusObjs.size(); i++)
				{
					tempBonusBean = getHighestBonusPerson(tempBonusBean, gameBean._roundBean._bonusObjs.get(i), gameBean);
					gameBean._roundBean._highestBonusPerson = tempBonusBean._playerId;
				}		
			}
		}
		
		gameBean.getGameLogInstance().highestBonusPerson(gameBean);
	}
	
	private synchronized BonusBean getHighestBonusPerson(BonusBean ob1, BonusBean ob2, GameBean gameBean)
	{	
		// ExceptionValidations 
		//DataValidation.getHighestBonusPerson(ob1, ob2, gameBean);
		
		BonusBean bonusBean = new BonusBean();
		
			int obj1=0;
			int obj2=0;
			
			//AppMethods.showLog(ob1._bonusType+"=="+ob2._bonusType);
			
			if(ob1._bonusType.equals("sira" ))
				obj1 = 1;
			else if(ob1._bonusType.equals("50"))
				obj1 = 2;
			else if(ob1._bonusType.equals("100"))
				obj1 = 3;
			else if(ob1._bonusType.equals("400"))
				obj1 = 4;
			else if(ob1._bonusType.equals("baloot"))
				obj1 = 0;
			
			if(ob2._bonusType.equals("sira" ))
				obj2 = 1;
			else if(ob2._bonusType.equals("50"))
				obj2 = 2;
			else if(ob2._bonusType.equals("100"))
				obj2 = 3;
			else if(ob2._bonusType.equals("400"))
				obj2 = 4;
			else if(ob2._bonusType.equals("baloot"))
				obj2 = 0;
			
			
			//AppMethods.showLog(obj1+"=="+obj2);
			
			// Checking with highest bonus type
			if(obj1>obj2)				
				bonusBean = ob1;
			else if(obj1<obj2)				
				bonusBean = ob2;
			else if(obj1 == obj2)
			{
				if(ob1._cards.size() > ob2._cards.size())
				{
					bonusBean = ob1; 
				}
				else if(ob1._cards.size() < ob2._cards.size())
				{
					bonusBean = ob2;
				}
				else
				{
					// Checking with highest Card
					int high1=0;
					int high2=0;
					
					high1 = getHighestCard(ob1.getCards());
					high2 = getHighestCard(ob2.getCards());
					
					if(high1 == 0)
						high1 = 8;
					if(high2 == 0)
						high2 = 8;
						
					
					if(high1<high2)
						bonusBean = ob1;
					else if(high1>high2)
						bonusBean = ob2;
					else 
					{
						// Checking with position
						// Calculate the right person from the dealer
						
						/*int firstTurnPos = 0;
						ArrayList<String> playerspos = new ArrayList<String>();
						// Finding the first players position
						for(int i=0; i<gameBean.maxPlayers;i++)
						{
							if(gameBean._roundBean._players.get(i).equals(gameBean._roundBean.firstPerson))
							{
								firstTurnPos = i+1;
								break;
							}
						}
						// Getting the players positions
						for(int i=0;i<gameBean.maxPlayers;i++)
						{
							int agno=0;
							if((i+firstTurnPos)>=gameBean.maxPlayers)
							{
								agno = (i+firstTurnPos)%gameBean.maxPlayers;				
							}
							else
							{				
								agno = (i+firstTurnPos);				
							}
							playerspos.add(gameBean._roundBean._players.get(agno));
						}
						
						// Find 
						int per1Pos = getPosition(ob1._playerId,playerspos);
						int per2Pos = getPosition(ob2._playerId,playerspos);
						
						if(per1Pos > per2Pos)
						{
							bonusBean = ob1; 
						}
						else
						{
							bonusBean = ob2;
						}
						*/
						
						int ob1Pos = gameBean.getPlayerPosition(ob1._playerId);
						int ob2Pos = gameBean.getPlayerPosition(ob2._playerId);
						
						if(ob1Pos < ob2Pos)
						{
							bonusBean = ob1; 
						}
						else
						{
							bonusBean = ob2;
						}
					}
				}
			}	
			
			
		return bonusBean;
	}
	
	
	private synchronized Integer getPosition(String player, ArrayList<String> arr)
	{
		// Exception validations
		DataValidation.getPosition(player, arr);
		
		Integer no = 0;
		for(int i=0;i<arr.size();i++)
		{
			if(arr.get(i).equals(player))
			{
				no = i; 
				break;
			}
		}
		
		return no;
	}
	
	private synchronized Integer getHighestCard(ArrayList<Integer> cards)
	{
		int no=0;
		
		Collections.sort(cards);
		no = cards.get(0);
		no = no%8;
	
		return no;
	}
	
	public synchronized boolean isHavingOtherSuitCards(PlayerRoundBean prb, String suit)
	{
		for(int cardId : prb._cards)
		{
			if(suit.equals(getSuit(cardId)))
			{
				
			}
			else
			{
				return true;
			}
		}
		
		return false;
	}
	
	private synchronized void setWinnerPosition(ArrayList<String> winners, GameBean gameBean)
	{
		int pos = -1;
		
		for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(winners.contains(pb._playerId))
			{
				if(pb.position == 0 || pb.position == 2)
				{
					pos = 0;
				}
				else
				{
					pos = 1;
				}
				break;
			}
		}
		
		gameBean.setWinnersPosition(pos);		
	}
	
	private static String getSuit(int cardId)
	{
		String suit = null;
		
		if(cardId >= 1 && cardId <= 8)
		{
			suit = "clubs";
		}			
		else if(cardId >= 9 && cardId <= 16)
		{
			suit = "diamonds";
		}			
		else if(cardId >= 17 && cardId <= 24)
		{
			suit = "hearts";
		}			
		else if(cardId >= 25 && cardId <= 32)
		{
			suit = "spades";
		}
		
		return suit;
	}
	
	public boolean isDeclaredBonus(GameBean gameBean, String player)
	{
		PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(player);
		
		if(prb != null)
		{
			return prb.isDeclaredBonus;
		}
		else
		{
			return false;
		}
	}
}
