package src.baloot.model;

import src.baloot.ai.sounds.AISound;

import src.baloot.beans.GameBean;


import src.baloot.beans.PlayerBean;
import src.baloot.beans.PlayerRoundBean;
import src.baloot.constants.Constant;
import src.baloot.exce.Validations;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class Bid {

	public synchronized void bidSelectedRequest(GameBean gameBean, String player, String bidSelected, String gameTrumpSuit)
	{
		
		Apps.showLog("   bidhadlerClientRequest ");
		Room room = Apps.getRoomByName(gameBean.tableId);
		// Exception validations
		//DataValidation.bidhadlerClientRequest(gameBean, room, player, bidSelected, gameTrumpSuit);
		
		// Set the last Updated Time
		gameBean._roundBean.setLastUpdatedTime();
		gameBean.incrementTicket();
		gameBean.getGameLogInstance().bidSelected(gameBean, player, bidSelected, gameTrumpSuit);
		
		if(!gameBean._roundBean.isRemainingCardsDistributed)
		{			
			if(!gameBean._roundBean.isGameTypeSelected)
			{			
				// Update users decision
				PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(player);
				prb._bidSelected = bidSelected;
			}
			
			if(bidSelected.equals(Commands.SAN))
			{
				gameBean.stopTimer();				
				
				AISound.sendSanSelectSound(gameBean, room, player);
				
				
				gameBean._roundBean._gameType = bidSelected;
				gameBean._roundBean.isGameTypeSelected = true;
				gameBean._roundBean.passCount = 0;
				gameBean._roundBean.doubleCount = 0;
				
				gameBean._roundBean._openCardTaker = player;	
				gameBean._roundBean._bidWonPerson = player;
				gameBean._roundBean._bidSelectedPerson = player;
				gameBean._roundBean._trumpSuitType = gameTrumpSuit;	
				gameBean._roundBean.bidSelected = bidSelected;
				gameBean._roundBean._doubleType =  Commands.NOT_SELECTED;
				
				sendBid(gameBean, player);	
				
			}
			else if(bidSelected.equals(Commands.HOKOM))
			{
				gameBean.stopTimer();	
				
				gameBean._roundBean._gameType = bidSelected;
				gameBean._roundBean.isGameTypeSelected = true;
				gameBean._roundBean.passCount = 0;
				/*
				 * gameBean._roundBean.doubleCount = 0;
				 * Here we dont write the above line. Becaue in hokom game type after Hokom game type selection if any person 
				 * select the double we give a chance to change the hokom suit( for bid selected person).
				 * if he change the hokom suit client send the Bid handler command only.
				 * At this time double count should be 1 not 0
				 * 
				 * so we must not zero the double count when player select the Hokom.
				 */
				
				
				gameBean._roundBean._openCardTaker = player;
				gameBean._roundBean._bidWonPerson = player;
				gameBean._roundBean._bidSelectedPerson = player;
				gameBean._roundBean._trumpSuitType = gameTrumpSuit;
				gameBean._roundBean.bidSelected = bidSelected;
				
				if(gameBean._roundBean.isRemainingAllPlayersPassHokom)
				{
					distributeRemainingCards(gameBean, bidSelected);
				}
				else
				{
					sendBid(gameBean, player);
				}
			}
			else if(bidSelected.equals(Commands.ASHKAL))
			{
				gameBean.stopTimer();	
				
				gameBean._roundBean._gameType = bidSelected;
				gameBean._roundBean.isGameTypeSelected = true;
				gameBean._roundBean.passCount = 0;
				gameBean._roundBean.doubleCount = 0;
				
				gameBean._roundBean._openCardTaker = gameBean.getPartner(player);
				gameBean._roundBean._bidWonPerson = player;
				gameBean._roundBean._bidSelectedPerson = player;
				gameBean._roundBean._trumpSuitType = gameTrumpSuit;
				gameBean._roundBean.bidSelected = bidSelected;
				
				sendBid(gameBean, player);
			}
			else if(bidSelected.equals(Commands.OPEN_DOUBLE))
			{
				gameBean.stopTimer();
							
				gameBean._roundBean._doubleType = Commands.OPEN_DOUBLE;
				gameBean._roundBean.isDouble = true;
				gameBean._roundBean.doubleCount++;				
				gameBean._roundBean._bidSelectedPerson = player;
				bidSelected = "double"+(gameBean._roundBean.doubleCount+1);	
				gameBean._roundBean.bidSelected = bidSelected;
				gameBean._roundBean.passCount = 0;
				
				if(gameBean._roundBean.doubleCount == 1)
				{
					AISound.doubleSound(gameBean, room, player);
				}
				
				
				if(gameBean._roundBean._gameType.equals(Commands.SAN) || gameBean._roundBean._gameType.equals(Commands.ASHKAL))
				{
					distributeRemainingCards(gameBean, bidSelected);
				}
				else
				{
					if(gameBean._roundBean.doubleCount == 3)
					{
						distributeRemainingCards(gameBean, bidSelected);
					}
					else
					{
						if(gameBean._roundBean.doubleCount == 1 && gameBean._roundBean._bidRoundCount == 2)
						{
							selectHokomSuit(gameBean, gameBean._roundBean._bidWonPerson, bidSelected);
						}
						else
						{
							sendBid(gameBean, player);
						}
					}
				}
			}
			else if(bidSelected.equals(Commands.CLOSED_DOUBLE))
			{
				gameBean.stopTimer();				
								
				
				gameBean._roundBean._doubleType = Commands.CLOSED_DOUBLE;
				gameBean._roundBean.isDouble = true;
				gameBean._roundBean.doubleCount++;				
				gameBean._roundBean._bidSelectedPerson = player;
				bidSelected = "double"+(gameBean._roundBean.doubleCount+1);	
				gameBean._roundBean.bidSelected = bidSelected;
				gameBean._roundBean.passCount = 0;
				
				if(gameBean._roundBean.doubleCount == 1)
				{
					AISound.doubleSound(gameBean, room, player);
				}
				
				if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
				{
					if(gameBean._roundBean.doubleCount == 3)
					{
						distributeRemainingCards(gameBean, bidSelected);
					}
					else
					{
						if(gameBean._roundBean.doubleCount == 1 && gameBean._roundBean._bidRoundCount == 2)
						{
							selectHokomSuit(gameBean, gameBean._roundBean._bidWonPerson, bidSelected);
						}
						else
						{
							sendBid(gameBean, player);
						}
					}
				}
				else
				{
					Validations.raiseCloseDoubleNotInHokomException(gameBean, gameBean._roundBean._gameType, bidSelected, player);
				}
			}
			else if(bidSelected.equals(Commands.PASS))
			{		
				gameBean._roundBean.passCount++;
				
				if(gameBean._roundBean.isGameTypeSelected)
				{
					if(gameBean._roundBean.passCount == 3 && (gameBean._roundBean._gameType.equals(Commands.SAN) || gameBean._roundBean._gameType.equals(Commands.ASHKAL)))
					{
						distributeRemainingCards(gameBean, bidSelected);
					}
					else if(gameBean._roundBean.passCount == 4 && gameBean._roundBean._gameType.equals(Commands.HOKOM) && gameBean._roundBean._bidRoundCount == 1)
					{
						distributeRemainingCards(gameBean, bidSelected);
					}
					else if(gameBean._roundBean.passCount == 3 && gameBean._roundBean._gameType.equals(Commands.HOKOM) && gameBean._roundBean._bidRoundCount == 2)
					{
						gameBean._roundBean.isRemainingAllPlayersPassHokom = true;
						
						if(gameBean._roundBean.isDouble)
						{
							distributeRemainingCards(gameBean, bidSelected);
						}
						else
						{
							selectHokomSuit(gameBean, gameBean._roundBean._bidWonPerson, bidSelected);
						}
					}
				}
				else
				{
					if(gameBean._roundBean.passCount == 4 && gameBean._roundBean._bidRoundCount == 1)
					{
						gameBean._roundBean._bidRoundCount++;
						
						gameBean._roundBean.doubleCount = 0;
						
						gameBean._roundBean.setTurnForBid();
						sendBid(gameBean, player);												
					}
					else if(gameBean._roundBean.passCount == 8 && gameBean._roundBean._bidRoundCount == 2)
					{
						gameBean.stopTimer(); 
						gameBean.startGame();
					}
					else
					{
						gameBean._roundBean.setTurnForBid();
						sendBid(gameBean, player);
					}
				}
			}
		}
		else
		{
			Apps.showLog("Cards Distributed");
			Validations.raiseBidRequestAfterDistributionException(gameBean, player, bidSelected, gameTrumpSuit);
		}
	}
	
	public synchronized void sendBid(GameBean gameBean, String player)
	{
		gameBean.stopTimer();
		
		ISFSObject sfso = new SFSObject();
		
		Room room = Apps.getRoomByName(gameBean.tableId);
		
		sfso.putUtfString("bidSelected", gameBean._roundBean.bidSelected);
		sfso.putSFSArray("playersEnableBits", gameBean._roundBean.getBidEnableBits(gameBean._roundBean.getTurnForBid()));
		sfso.putUtfString("gameType", gameBean._roundBean._gameType);
		sfso.putUtfString("gameTrumpSuit", gameBean._roundBean._trumpSuitType);
		sfso.putUtfString("bidWonPerson", gameBean._roundBean._bidWonPerson);
		sfso.putUtfString("bidSelectedPerson", player);
		sfso.putUtfString("gender", Apps.getGender(player));	
		sfso.putUtfString("doubleType", gameBean._roundBean._doubleType);
		sfso.putInt("ticket", gameBean.getTicket());
		sfso.putInt("roundCount", gameBean.getRoundCount());
		sfso.putInt("bidRoundCount", gameBean._roundBean._bidRoundCount);
		sfso.putInt("bidTime", gameBean._roundBean.getBidTime());
			
		Commands.appInstance.send(Commands.BID, sfso, room.getUserList());	
		gameBean.getGameLogInstance().bid(gameBean);
		gameBean.writeGameLog();
		
		gameBean.startBidTimer();
		
		if(gameBean._roundBean.isGameTypeSelected)
		{
			int delayTime = 1;
			for(PlayerBean pb : gameBean._playersBeansList)
			{
				if((pb._isAI && !player.equals(pb._playerId)) || (pb.isFakePlayer && !player.equals(pb._playerId)))
				{
					PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(pb._playerId);
					// Start AI timer
					Apps.showLog("AI Starts in Bidding after bid selected");
					
					prb.startTimer(delayTime, Commands.BID, gameBean, pb._playerId);
					delayTime++;
					//break;
				}
			}
		}
		else
		{
			// if the bid turn player is AI then start the ai code.
			Commands.appInstance.ai.checkPlayerTurnBiddingorDiscard(gameBean, gameBean._roundBean.getTurnForBid());
		}
	}
	
	public synchronized void distributeRemainingCards(GameBean gameBean, String bidSelected)
	{

		if(!gameBean._roundBean.isRemainingCardsDistributed)
		{
			gameBean._roundBean.isRemainingCardsDistributed = true;
			gameBean._roundBean.trickCount = 1;
			
			gameBean.stopTimer();
			
			ISFSObject sfso = new SFSObject();
			Room room = Apps.getRoomByName(gameBean.tableId);
											
			gameBean._roundBean.distributeRemainingCards();			
			
			sfso.putUtfString("gameType", gameBean._roundBean._gameType);
			sfso.putUtfString("gameTrumpSuit", gameBean._roundBean._trumpSuitType);
			sfso.putUtfString("bidWonPerson", gameBean._roundBean._bidWonPerson);
			sfso.putUtfString("bidSelected", bidSelected);
			sfso.putUtfString("bidSelectedPerson", gameBean._roundBean._bidSelectedPerson);
			sfso.putUtfString("gender", Apps.getGender(gameBean._roundBean._bidSelectedPerson));
			sfso.putSFSArray("playerCards",gameBean._roundBean.getPlayersCards() );	
			sfso.putUtfString("turn", gameBean._roundBean.getTurn());		
			sfso.putUtfString("doubleType", gameBean._roundBean._doubleType);
			sfso.putInt("ticket", gameBean.getTicket());
			sfso.putInt("roundCount", gameBean.getRoundCount());
			sfso.putInt("trickCount", gameBean._roundBean.trickCount);
			
			gameBean.getGameLogInstance().bidWinner(gameBean);
			gameBean.getGameLogInstance().distributeRemainingCards(gameBean);
			
			// Start timer for Next Event Delay Timer
			gameBean.startTimer(Constant.DISCARD_TIME, gameBean._roundBean.getTurn(), Commands.DISCARD);
			gameBean._roundBean.sendSyncDataToNewlySeatOccupiedPlayer();
			Commands.appInstance.ai.checkPlayerTurnBiddingorDiscard(gameBean, gameBean._roundBean.getTurn());		
			
			Commands.appInstance.send(Commands.DISTRIBUTE_REMAINING_CARDS, sfso, room.getUserList());
		}
	}
	
	public synchronized void selectHokomSuit(GameBean gameBean, String player, String bidSelected)
	{
		gameBean.stopTimer();
		
		ISFSObject sfso = new SFSObject();
		
		sfso.putUtfString("gameType", gameBean._roundBean._gameType);
		sfso.putUtfString("bidWonPerson", gameBean._roundBean._bidWonPerson);
		sfso.putUtfString("bidSelected", bidSelected);
		sfso.putUtfString("bidSelectedPerson", gameBean._roundBean._bidSelectedPerson);
		sfso.putUtfString("gameTrumpSuit", gameBean._roundBean._trumpSuitType);
		sfso.putInt("ticket", gameBean.getTicket());
		sfso.putInt("roundCount", gameBean.getRoundCount());
		sfso.putInt("trickCount", gameBean._roundBean.trickCount);
		sfso.putUtfString("doubleType", gameBean._roundBean._doubleType);
		
		Room room = Apps.getRoomByName(gameBean.tableId);
		
		Commands.appInstance.send(Commands.SELECT_HOKOM_SUIT, sfso, room.getUserList());
		
		gameBean.startSelectHokomSuitTimer(Constant.SELECT_HOKOM_SUIT_TIME, player);
		
		PlayerBean pb = gameBean.getPlayerBean(player);
		
		if(pb._isAI || pb.isFakePlayer)
		{
			PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(player);
			prb.startTimer(1, Commands.SELECT_HOKOM_SUIT, gameBean, player);
		}				
	}
}
