package src.baloot.model;

import java.util.ArrayList;

import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerBean;
import src.baloot.constants.Constant;
import src.baloot.exce.Validations;
import src.baloot.exce.beans.LogBean;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;

public class JoinLeave {
	
	public synchronized void joinOrLeaveUser(String tableId, boolean isPrivateTable, boolean isJoin, User sender, ISFSObject params)
	{
		if(isJoin)
		{
			String player = sender.getName();
			
			GameBean gameBean = Apps.getGameBean(tableId);
			
			Room room = null;
			room = Apps.getRoomByName(tableId);
			
			if(gameBean._players.contains(player))
			{
				Validations.raiseDuplicateUserException(gameBean, player);
				Commands.appInstance.send(Commands.DUPLICATE_USER_EXCEPTION, params, sender);
			}
			else if(gameBean.getSpecatatores().contains(player))
			{
				Validations.raiseDuplicateSpectatorException(gameBean, player);
				Commands.appInstance.send(Commands.DUPLICATE_SPECTATOR_EXCEPTION, params, sender);
			}
			else
			{
				if(gameBean.isRematchPhase)
				{
					// Rematch Phase Join the User as Spectator						
					if(gameBean.getSpecatatorsCount() < 26)
					{
						// Join the user
						gameBean.addSpectator(player);
						Apps.joinGameRoom(sender, room);
						//Add Log
						String var = " player "+player +" joined at rematch phase";
						LogBean lb = new LogBean();
						lb.setValues(Constant.LOG, "P3", player, "SpectatorJoin", var);
						gameBean.addLog(lb);
						lb = null;	
						
						// Update Lobby
						Commands.appInstance.ulBsn.updateLobby(gameBean);
						
						// User joined
						params.putUtfString("tableId", gameBean.tableId);
						params.putUtfString("gameId", gameBean.gameId);
						params.putUtfStringArray("players", gameBean._players);
						params.putUtfString("joinedUser", player);
						params.putBool("rematchPhase", true);
						params.putBool("isSpectator", true);
						params.putInt("ticket", gameBean.getTicket());
						params.putIntArray("positions", gameBean.getPositions());
						params.putUtfString("gameStartTime", gameBean.gameStartedTimeForClient);
						
						Commands.appInstance.send(Commands.USER_JOINED, params, sender);	
						gameBean.getSyncDataInstance().sendSyncData(gameBean, player);
						
						params.putUtfString("waitingPlayer", player);
						Commands.appInstance.send(Commands.WAITING_PLAYER_JOINED, params, room.getUserList());
						gameBean.getGameLogInstance().waitingPlayerJoined(gameBean, player);
					}
					else
					{
						// Room Full
						Commands.appInstance.send(Commands.ROOM_FULL, params, sender);
					}
				}
				else
				{				
					if(!gameBean.isGameStarted)
					{
						//System.out.println("Game Not Started");
						if(gameBean.isPrivateTable())
						{
							params.putBool("isSpectator", true);
							gameBean.addSpectator(player);
							Apps.joinGameRoom(sender, room);
							
							// Add log
							String var = " player "+player +" tableId "+tableId;
							LogBean lb = new LogBean();
							lb.setValues(Constant.LOG, "P3", player, "UserJoin", var);
							gameBean.addLog(lb);
							lb = null;
							
							// Prepare Object 
							params.putUtfString("tableId", gameBean.tableId);
							params.putUtfString("gameId", gameBean.gameId);
							params.putUtfStringArray("players", gameBean._players);
							params.putUtfString("joinedUser", player);
							params.putBool("rematchPhase", false);
							params.putLong("remainingQuickStartTime", gameBean.getRemainingQuickStartTime());
							params.putInt("ticket", gameBean.getTicket());
							params.putIntArray("positions", gameBean.getPositions());
							params.putUtfString("gameStartTime", gameBean.gameStartedTimeForClient);
							// Update Lobby
							Commands.appInstance.ulBsn.updateLobby(gameBean);
							
							// User joined
							Commands.appInstance.send(Commands.USER_JOINED, params, room.getUserList());
							gameBean.incrementTicket();
							// Start the game
							if(gameBean._players.size() == 4)
							{					
								gameBean.initGame();
							}
						}
						else
						{
							if(Commands.appInstance.proxy.isHavingEnoughGamesToPlay(gameBean, player, false, false))
							{
								params.putBool("isSpectator", false);
								
								PlayerBean pb = new PlayerBean();
								pb._playerId = player;
								pb._aiPlayer = player;
								gameBean._players.add(player);
								
								pb.position = gameBean._playersBeansList.size();
								gameBean._playersBeansList.add(pb);
								Commands.appInstance.proxy.updatePlayingStatusTrue(gameBean, player);
								
								Apps.joinGameRoom(sender, room);
								
								// Add log
								String var = " player "+player +" tableId "+tableId;
								LogBean lb = new LogBean();
								lb.setValues(Constant.LOG, "P3", player, "UserJoin", var);
								gameBean.addLog(lb);
								lb = null;
								
								// Prepare Object 
								params.putUtfString("tableId", gameBean.tableId);
								params.putUtfString("gameId", gameBean.gameId);
								params.putUtfStringArray("players", gameBean._players);
								params.putUtfString("joinedUser", player);
								params.putBool("rematchPhase", false);
								params.putLong("remainingQuickStartTime", gameBean.getRemainingQuickStartTime());
								params.putInt("ticket", gameBean.getTicket());
								params.putIntArray("positions", gameBean.getPositions());
								params.putUtfString("gameStartTime", gameBean.gameStartedTimeForClient);
								// Update Lobby
								Commands.appInstance.ulBsn.updateLobby(gameBean);
								
								// User joined
								Commands.appInstance.send(Commands.USER_JOINED, params, room.getUserList());
								gameBean.incrementTicket();
								// Start the game
								if(gameBean._players.size() == 4)
								{					
									gameBean.initGame();
								}
							}
							else
							{
								Commands.appInstance.send(Commands.NOT_HAVEING_ENOUGH_GAMES, params, sender);
							}
						}
					}
					else
					{
						//System.out.println("Game Started " );
						
						// Join the User as Spectator
						if(gameBean.getSpecatatorsCount() < 26)
						{				
							Apps.joinGameRoom(sender, room);
							if(!gameBean._players.contains(player) && !gameBean.getSpecatatores().contains(player))
							{
								gameBean.addSpectator(player);									
								
								//Add Log
								String var = " player "+player +" joined";									
								LogBean lb = new LogBean();
								lb.setValues(Constant.LOG, "P3", player, "SpectatorJoin", var);
								gameBean.addLog(lb);
								lb = null;
							}						
							// Update Lobby
							Commands.appInstance.ulBsn.updateLobby(gameBean);
							
							// User joined
							params.putUtfString("tableId", gameBean.tableId);
							params.putUtfString("gameId", gameBean.gameId);
							params.putUtfStringArray("players", gameBean._players);
							params.putUtfString("joinedUser", player);
							params.putBool("isSpectator", true);	
							params.putBool("rematchPhase", false);
							params.putInt("ticket", gameBean.getTicket());
							params.putIntArray("positions", gameBean.getPositions());
							params.putUtfString("gameStartTime", gameBean.gameStartedTimeForClient);
							
							Commands.appInstance.send(Commands.USER_JOINED, params, sender);
							gameBean.getSyncDataInstance().sendSyncData(gameBean, player);
							
							params.putUtfString("waitingPlayer", player);
							Commands.appInstance.send(Commands.WAITING_PLAYER_JOINED, params, room.getUserList());
							gameBean.getGameLogInstance().waitingPlayerJoined(gameBean, player);
							
						}
						else
						{
							// Room Full
							Commands.appInstance.send(Commands.ROOM_FULL, params, sender);
						}
					}			
				}
			}
		}
		else
		{
			// player leaves table.			
			String player = sender.getName();
			GameBean gameBean = Apps.getGameBean(tableId);
			params.putUtfStringArray("initialPlayers", gameBean.initialPlayers);
			
			Room room = Apps.getRoomByName(tableId);
			// Get game bean
			if(!gameBean.isRematchPhase)
			{
				gameBean.incrementTicket();
				PlayerBean pb = gameBean.getPlayerBean(player);
				// Check if that user is in spectator's list or not
				if(gameBean.getSpecatatores().contains(player))
				{
					gameBean.getGameLogInstance().spectatorLeft(gameBean, player);
					
					gameBean.getSpecatatores().remove(player);
					Commands.appInstance.ulBsn.updateLobby(gameBean);						
					params.putUtfString("command", "LeaveTable");
					
					Commands.appInstance.send(Commands.LEAVE_TABLE, params, sender);
					
					params.putUtfString("waitingPlayer", player);
					Commands.appInstance.send(Commands.WAITING_PLAYER_LEFT, params, room.getUserList());
					gameBean.getGameLogInstance().waitingPlayerLeft(gameBean, player);
				}				
				else if(gameBean.isGameStarted)
				{
					// addLog								
					gameBean.getGameLogInstance().userLeft(gameBean, player);
					gameBean.leaveTablesUsers.add(player);
					
					pb._isAI = true;
					pb.isNewlyOccupiedSeat = false;
					
					Commands.appInstance.proxy.updateUserProfile(gameBean, player, 0, gameBean.tableId, gameBean.gameId);
					Commands.appInstance.proxy.updatePlayingStatusFalse(gameBean, player);
							
					
					int count = getAIPlatyersCount(gameBean);
					
					if(count < 4)
					{
						params.putUtfString("leavePerson", player);
						
						// Change AI Player
                        String aiPlayer =  gameBean.getReplaceUserInstance().replaceUserWithAI(gameBean, pb);
                        
						params.putUtfString("aiPlayer", aiPlayer );					
						params.putUtfString("command", "LeaveTable");
						
						params.putUtfStringArray("initialPlayers", gameBean.initialPlayers);
						
						Commands.appInstance.send(Commands.LEAVE_TABLE, params, sender);
						
						params.putUtfString("gender", Apps.getGender(aiPlayer));
						params.putUtfString("command", "AI");									
						Commands.appInstance.send(Commands.LEAVE_TABLE, params, Apps.getRemainingUsers(room, sender));
						
						// Update the Lobby
						Commands.appInstance.ulBsn.updateLobby(gameBean);
						
						if(gameBean._roundBean.isRemainingCardsDistributed && !gameBean.isRoundCompletedResultTime && aiPlayer.equals(gameBean._roundBean.getTurn()))
						{										
							Commands.appInstance.ai.checkPlayerTurnBiddingorDiscard(gameBean, aiPlayer);
						}
					}					 
					else
					{									
						params.putUtfString("command", "GameCompleted");
						gameBean.stopAllTimers();
						params.putUtfString("leavePerson", player);
						
						ArrayList<String> al = new ArrayList<String>();
						int winPosition = -1;
						
						if(pb.position == 0 || pb.position == 2)
						{
							winPosition = 1;
							
							al.add(gameBean._playersBeansList.get(1)._playerId);
							al.add(gameBean._playersBeansList.get(3)._playerId);
						}
						else
						{
							winPosition = 0;
							
							al.add(gameBean._playersBeansList.get(0)._playerId);
							al.add(gameBean._playersBeansList.get(2)._playerId);
						}
						
						params.putUtfStringArray("wonTeam", al );
						
						Commands.appInstance.send(Commands.LEAVE_TABLE, params, room.getUserList());
						
						gameBean.setWinnersPosition(winPosition);
						Commands.appInstance.sgBsn.gameCompleted(gameBean);
					}								 
				}
				else if(!gameBean.isGameStarted)				
				{
					gameBean._players.remove(player);
					gameBean._playersBeansList.remove(pb);
					gameBean.leaveTablesUsers.add(player);
					
					resetPositions(gameBean);
					
					//Add Log
					gameBean.getGameLogInstance().userLeft(gameBean, player);	
					
					Commands.appInstance.ulBsn.updateLobby(gameBean);
					Commands.appInstance.proxy.updatePlayingStatusFalse(gameBean, player);
					
					if(gameBean._players.size() == 0)
					{
						gameBean.stopAllTimers();
						
						gameBean.setWinnersPosition(-1);
						Commands.appInstance.sgBsn.gameCompleted(gameBean);
					}
					
					// Leave table that user
					params.putUtfString("command", "LeaveTable");
					
					Commands.appInstance.send(Commands.LEAVE_TABLE, params, sender);					
				}					
			}
			else
			{
				gameBean.incrementTicket();					
				leaveUserInRemathcPhase(params, sender, gameBean);
			}
		}
	}
	
	private synchronized void leaveUserInRemathcPhase(ISFSObject params, User sender, GameBean gameBean)
	{
		String player = sender.getName();
		Room room = Apps.getRoomByName(gameBean.tableId);
		
		
		//========================================================================
		if(gameBean.getSpecatatores().contains(player))
		{
			params.putUtfString("waitingPlayer", player);
			Commands.appInstance.send(Commands.WAITING_PLAYER_LEFT, params, room.getUserList());
			gameBean.getGameLogInstance().waitingPlayerLeft(gameBean, player);
		}			
		//========================================================================
		
		
		ArrayList<String> beforePlayer = gameBean._players;
		ArrayList<String> beforeRematchPlayers = gameBean.rematchPlayers;
		ArrayList<String> beforeSpetators = gameBean.getSpecatatores();
		gameBean._players.remove(player);
		gameBean.rematchPlayers.remove(player);
		gameBean.getSpecatatores().remove(player);
		
		String var = " player "+player+" isRematchPhase "+gameBean.isRematchPhase+" beforeplayers "+beforePlayer+"  afterplayer "+gameBean._players.toString()
						+"  beforeRematchPlayers "+beforeRematchPlayers+"  afterRematchPlayers "+gameBean.rematchPlayers
						+ "  beforeSpetators "+beforeSpetators+"  afterSpetators "+gameBean.getSpecatatores();
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", player, "RematchLeaveTable", var);
		gameBean.addLog(lb);
		lb = null;	
		
		
		Apps.leaveRoom(sender, room);
		
		params.putUtfString("command", "LeaveTable");
		Commands.appInstance.send(Commands.LEAVE_TABLE, params, sender);
		
		Commands.appInstance.proxy.updatePlayingStatusFalse(gameBean, player);
	}
	
	private synchronized int getAIPlatyersCount(GameBean gameBean)
	{
		int count = 0;
		
		for(PlayerBean pb: gameBean._playersBeansList)
		{
			if(pb._isAI)
			{
				count++;
			}
		}
		
		gameBean.getGameLogInstance().leaveTableAIplayersCount(gameBean, count);
		return count; 
	}
	
	public synchronized void resetPositions(GameBean gameBean)
	{
		for(int i=0;i<gameBean._playersBeansList.size();i++)
		{
			PlayerBean pb  = gameBean._playersBeansList.get(i);
			pb.position = i;
		}
		
		gameBean.getGameLogInstance().resetPositions(gameBean);
	}
}
