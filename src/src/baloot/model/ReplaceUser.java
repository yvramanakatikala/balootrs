package src.baloot.model;

import java.util.ArrayList;

import src.baloot.beans.BonusBean;
import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerBean;
import src.baloot.beans.PlayerRoundBean;
import src.baloot.constants.Constant;
import src.baloot.exce.beans.LogBean;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

public class ReplaceUser {

	public synchronized String replaceUserWithAI(GameBean gameBean, PlayerBean pb)
	{
		String realPlayer = pb._playerId;
		
		String aiPlayer = getAIPlayer(gameBean, realPlayer);
		resetPlayerBean(gameBean, realPlayer, aiPlayer);
		
		pb._playerId = aiPlayer;
		pb.leftPlayer = realPlayer;
		pb._aiPlayer = aiPlayer;
		pb._isAI = true;	
		pb.gender = "Male";
		
		gameBean.getGameLogInstance().replaceUserWithAI(gameBean, realPlayer, aiPlayer);
		
		return aiPlayer;
	}	
	
	public synchronized void replacAIwithRealUser(GameBean gameBean, PlayerBean pb, String player)
	{
		Apps.showLog("  replacAIwithRealUser");
		
		String aiPlayer = pb._playerId;
		pb._isAI = false;
		pb.isAutoPlay = false;
		pb._playerId = player;
		pb.leftPlayer = aiPlayer;
		
		resetPlayerBean(gameBean, aiPlayer, player);
		
		//Add log			
		String var = "Turn :"+gameBean._roundBean.getTurn() +" AIPlayer "+aiPlayer+" player :"+player+" leftPlayer "+pb.leftPlayer
						+" players  "+gameBean._players+" initialPlayers "+gameBean.initialPlayers;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", player, "ReplaceAIwithRealUser", var);
		gameBean.addLog(lb);
		lb = null;
		
	}	
	
	public void resetPlayerBean(GameBean gameBean, String leftPlayer, String newPlayer)
	{
		ArrayList<String> newPlayers = new ArrayList<String>();
		
		for(int k=0;k<gameBean._players.size();k++)
		{
			if(gameBean._players.get(k).equals(leftPlayer))
			{
				newPlayers.add(newPlayer);
			}										
			else
			{
				newPlayers.add(gameBean._players.get(k));
			}
		}
		
		gameBean._players = newPlayers;
		gameBean._roundBean._players = newPlayers;
		
		// for lobby update.
		ArrayList<String> newInitialPlayers = new ArrayList<String>();
		for(String player : gameBean.initialPlayers)
		{
			if(player.equals(leftPlayer))
			{
				newInitialPlayers.add(newPlayer);
			}										
			else
			{
				newInitialPlayers.add(player);
			}
		}
		gameBean.initialPlayers = newInitialPlayers;
		
		PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(leftPlayer);
		prb._playerId = newPlayer;
		
		if(gameBean.shuffler.equals(leftPlayer))
		{
			gameBean.shuffler = newPlayer;
		}

		if(gameBean._roundBean._turn.equals(leftPlayer))
		{
			gameBean._roundBean._turn = newPlayer;
		}
		
		if(gameBean._roundBean._turnForBid.equals(leftPlayer))
        {
            gameBean._roundBean._turnForBid = newPlayer;
        }
		
		if(gameBean._roundBean._highestBonusPerson.equals(leftPlayer))
		{
			gameBean._roundBean._highestBonusPerson = newPlayer;
		}
			
		if(gameBean._roundBean._bidSelectedPerson.equals(leftPlayer))
		{
			gameBean._roundBean._bidSelectedPerson = newPlayer;
		}
		
		if(gameBean._roundBean._bidWonPerson.equals(leftPlayer))
		{
			gameBean._roundBean._bidWonPerson = newPlayer;
		}
			
		if(gameBean._roundBean._openCardTaker.equals(leftPlayer))
		{
			gameBean._roundBean._openCardTaker = newPlayer;
		}
			
		for(BonusBean bb : gameBean._roundBean._bonusObjs)
		{
			if(bb._playerId.equals(leftPlayer))
			{
				bb._playerId = newPlayer;
			}
		}
		
		gameBean.getGameLogInstance().resetPlayerBean(gameBean, leftPlayer, newPlayer);
	}
	
	public String getAIPlayer(GameBean gameBean, String requestedPlayer)
	{
		String aiPlayer = "AI4";
		boolean ai1 = false;
		boolean ai2 = false;
		boolean ai3 = false;
		
		// Check which AI name is not used
		for(int i=0;i<gameBean._roundBean._players.size();i++)
		{
			if(gameBean._roundBean._players.get(i).equals("AI1")){
				ai1 = true;
			}
			else if(gameBean._roundBean._players.get(i).equals("AI2")){
				ai2 = true;
			}
			else if(gameBean._roundBean._players.get(i).equals("AI3")){
				ai3 = true;
			}
		}
		
		if(!ai1)
		{
			aiPlayer = "AI1";
		}			
		else if(!ai2)
		{
			aiPlayer = "AI2";
		}			
		else if(!ai3)
		{
			aiPlayer = "AI3";
		}
		
		gameBean.getGameLogInstance().getAIPlayer(gameBean, aiPlayer, requestedPlayer);
		return aiPlayer;
	}
}
