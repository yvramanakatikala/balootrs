package src.baloot.model;

import java.util.ArrayList;




import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerBean;
import src.baloot.constants.Constant;
import src.baloot.exce.beans.LogBean;
import src.baloot.listeners.FakeGame;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class Rematch {
	
	public void startRematchGame(GameBean gameBean)
	{
		Commands.appInstance.proxy.gameCompleted(gameBean);
		
		gameBean.isRematchPhase = false;
		gameBean._roundBean.isRemainingCardsDistributed = false;
		gameBean.stopAllTimers();
		Room room = Apps.getRoomByName(gameBean.tableId);
		
		
		// throw the fake players to another game.
		ArrayList<User> list = new ArrayList<User>();
		for(PlayerBean pb : gameBean._playersBeansList)
		{
			if(pb.isFakePlayer)
			{
				list.add(Apps.getUserByName(pb._playerId));
				
				//gameBean.getGameLogInstance().fakePlayerJoinInAnotherGame(gameBean, pb);
			}
		}	
	
		if(gameBean.rematchPlayers.size() > 0)
		{
			ArrayList<User>  speUsers = new ArrayList<User>();
			
			for(String player : gameBean._players)
			{
				if(gameBean.rematchPlayers.contains(player))
				{
					// do nothing.
				}
				else
				{
					User user = room.getUserByName(player);
					if(user != null)
					{
						gameBean.addSpectator(player);
						speUsers.add(user);
					}
					
					Commands.appInstance.proxy.updatePlayingStatusFalse(gameBean, player);
				}
			}
			
			if(speUsers.size() > 0)
			{
				Commands.appInstance.send(Commands.CHANGE_PLAYER_TO_SPECTATOR, new SFSObject(), speUsers);
				Commands.appInstance.ulBsn.updateLobby(gameBean);
			}
			
			
			// Prepare the new game.
			GameBean rematchBean = new GameBean();
			rematchBean.tableId = gameBean.tableId;
			rematchBean.addSpectatorsList(gameBean.getSpecatatores());
			rematchBean.setPrivateTable(gameBean.isPrivateTable());
			
			if(rematchBean.isPrivateTable())
			{
				rematchBean.tableType = Constant.PRIVATE;
				rematchBean.host = gameBean.tableId;
			}
			else
			{
				rematchBean.tableType = Constant.PUBLIC;
				rematchBean.host = gameBean.rematchPlayers.get(0);
			}
			
			rematchBean.isHostJoined = true;
			rematchBean._players.addAll(gameBean.rematchPlayers);
			
			// logs
			gameBean.getGameLogInstance().playerChangedToSpectator(gameBean, speUsers);
			gameBean.getGameLogInstance().childRematchGameDetais(gameBean, rematchBean);
			
			rematchBean.getGameLogInstance().playerChangedToSpectator(rematchBean, speUsers);
			rematchBean.getGameLogInstance().parentRematchGameDetails(rematchBean, gameBean);
			
			
			// push the new rematch gamebean into hashmap.
			Commands.appInstance.getGames().put(rematchBean.tableId, rematchBean);
			
			
			// prepare the beans
			for(int i=0;i<rematchBean._players.size();i++)
			{
				PlayerBean pb = new PlayerBean();
				pb._playerId = rematchBean._players.get(i);
				pb.position = i;
				
				if(pb.position == 0)
				{
					pb.isHost = true;
				}
				
				rematchBean._playersBeansList.add(pb);		
			}
			
			rematchBean.initGame();
		}
		else
		{			
			String var = " players "+gameBean._players.toString()+" rematchPlayers  "+gameBean.rematchPlayers.toString()
							+" spetators  "+gameBean.getSpecatatores().toString();
			
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P3", "System", "NoBodyJoinInRematchPhase", var);
			gameBean.addLog(lb);
			lb = null;
			
			Commands.appInstance.sgBsn.freeTable(gameBean);						
		}
		
		
		if(list.size() > 0)
		{
			NewFakeGame nfg = new NewFakeGame();
			nfg.startGameWithAI(list, gameBean);
		}		
		
		/*if(list.size() > 0)
		{
			if(Commands.appInstance.isLock())
			{
				new FakeGame(list, gameBean);
			}
			else
			{
				Commands.appInstance.fakePlayers.addAll(list);
			}
		}	*/	
	}
}
