package src.baloot.model;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

import src.baloot.beans.GameBean;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;
import src.baloot.utils.GameApps;

public class SyncData {	
	
	public synchronized void sendSyncData(GameBean gameBean, String player)
	{
		ISFSObject sfso = GameApps.getGameObject(gameBean, new SFSObject(), player);
		
		User user = Apps.getUserByName(player);
		
		if(user != null)
		{
			Commands.appInstance.send(Commands.SINK_DATA, sfso, user);
			gameBean.getGameLogInstance().sendSyncData(gameBean, player);
		}
	}	
}
