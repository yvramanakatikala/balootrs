package src.baloot.model;

import src.baloot.ai.sounds.AISound;
import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerBean;
import src.baloot.constants.Constant;
import src.baloot.exce.Validations;
import src.baloot.exce.beans.LogBean;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;
import src.baloot.utils.GameApps;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;

public class OccupySeat {
	
	private boolean isLock = false;

	
	public boolean isLock() {
		return isLock;
	}

	public void setLock(boolean isLock) {
		this.isLock = isLock;
	}

	public void occupySeat(GameBean gameBean, int playerPos, String player, ISFSObject params, String gameId, String tableId)
	{
		isLock = true;
		
		User sender = Apps.getUserByName(player);
		Room room = Apps.getRoomByName(tableId);
		
		boolean isHavingEnoughGames = false;
		
		if(!gameBean.isRematchPhase)
		{
			if(gameBean.isGameStarted)
			{
				Apps.showLog("Game Started "+playerPos);
				
				if(gameBean._players.contains(player))
				{
					PlayerBean pb = gameBean.getPlayerBean(player);
					
					params.putUtfString("gameId", gameBean.gameId);
					params.putUtfString("aiPlayer", pb._playerId);
					params.putUtfString("actualPlayer", pb._playerId);
					params.putUtfString("shuffler", gameBean.getShuffler());
					params.putUtfStringArray("initialPlayers", gameBean.initialPlayers);
					params.putUtfString("bidWonPerson", gameBean._roundBean._bidWonPerson);
					params.putUtfString("gender", Apps.getGender(player));
					
					Commands.appInstance.send(Commands.CHANGE_PLAYER, params, Apps.getRemainingUsers(room, sender));
					
					if(gameBean.isRoundCompletedResultTime)
					{
						pb.isNewlyOccupiedSeat = true; 
						pb.isAutoPlay = true;// real player should not participate in bidding so he should be in auto play.
					}
					else
					{
						if(gameBean._roundBean.isRemainingCardsDistributed)
						{
							if(gameBean._roundBean.getTurn().equals(player))
							{
								//gameBean._roundBean.stopTimer();
								
								//send(Commands.CHANGE_PLAYER, params, sender);
								//gameBean.getSyncDataInstance().sendSyncData(gameBean, player);
								pb.isNewlyOccupiedSeat = true; 
							}
							else
							{
								pb.isNewlyOccupiedSeat = true; 
								pb.isAutoPlay = false;
							}
						}
						else
						{
							pb.isNewlyOccupiedSeat = true; 
							pb.isAutoPlay = true;  // real player should not participate in bidding so he should be in auto play.
						}
					}
					
					gameBean.getGameLogInstance().userJoinBack(gameBean, pb, playerPos);
					// Update the players profile
					Commands.appInstance.proxy.updateMiddleJoinedUserprofile(gameBean, gameBean.tableId, gameBean.gameId, Apps.getPlayerId(player),player, gameBean.tableType);
			
					Commands.appInstance.ulBsn.updateLobby(gameBean);
					
				}
				else
				{
					PlayerBean pb = getExsitingPlayerBean(playerPos, gameBean, sender, params);
												
					if(pb != null)
					{
						if(gameBean.isPrivateTable())
						{
							isHavingEnoughGames = true;
						}
						else
						{
							if(Commands.appInstance.proxy.isHavingEnoughGamesToPlay(gameBean, player, false, true))
							{
								isHavingEnoughGames = true;
							}
							else
							{
								Commands.appInstance.send(Commands.NOT_HAVEING_ENOUGH_GAMES, params, sender);
							}
						}
						
						gameBean.removeSpecatator(player);
						
						if(isHavingEnoughGames)
						{
							replaceAIwithUser(gameBean, pb, player);
							
							params.putUtfString("gameId", gameBean.gameId);
							params.putUtfString("aiPlayer", pb._aiPlayer);
							params.putUtfString("actualPlayer", pb._playerId);
							params.putUtfString("shuffler", gameBean.getShuffler());
							params.putUtfStringArray("initialPlayers", gameBean.initialPlayers);
							params.putUtfString("bidWonPerson", gameBean._roundBean._bidWonPerson);
							params.putUtfString("gender", Apps.getGender(pb._playerId));
							
							Commands.appInstance.send(Commands.CHANGE_PLAYER, params, Apps.getRemainingUsers(room, sender));
							Commands.appInstance.send(Commands.REPLACE_AI_WITH_USER, params, sender);
							
							AISound.joinOnAISound(gameBean, room, player);
							
							if(gameBean.isRoundCompletedResultTime)
							{
								pb.isNewlyOccupiedSeat = true; 
								pb.isAutoPlay = true; // real player should not participate in bidding so ai is true.
							}
							else
							{
								if(gameBean._roundBean.isRemainingCardsDistributed)
								{
									if(gameBean._roundBean.getTurn().equals(player))
									{
										//gameBean._roundBean.stopTimer();
										
										//send(Commands.CHANGE_PLAYER, params, sender);
										//gameBean.getSyncDataInstance().sendSyncData(gameBean, player);
										pb.isNewlyOccupiedSeat = true;
									}
									else
									{
										pb.isNewlyOccupiedSeat = true; 
										pb.isAutoPlay = false;
									}
								}
								else
								{
									pb.isNewlyOccupiedSeat = true; 
									pb.isAutoPlay = true;  // real player should not participate in bidding so auto play is true.
								}
							}
							
							gameBean.getGameLogInstance().playerJoinedInAIPlace(gameBean, pb, playerPos);
							Commands.appInstance.ulBsn.updateLobby(gameBean);
						}
					}
					else
					{
						Validations.raiseOccupieSeatPlayerBeanNotAvailableException(gameBean, player, pb, playerPos);
					}
				}
			}
			else
			{
				// Game not started
				Apps.showLog(" Occupy seat Game not started");
				
				if(gameBean.isPrivateTable())
				{
					if(gameBean._players.contains(player))
					{
						PlayerBean pb = gameBean.getPlayerBean(player);
						params.putInt("occupiedPosition", pb.position);
						
						Commands.appInstance.send(Commands.YOU_ALREADY_OCCUPIED, params, sender);
						
						Validations.raiseTwoTimesOccupieSeatInPrivateTableException(gameBean, player, playerPos, pb.position);
					}
					else if(GameApps.isPositionAvailable(gameBean, playerPos))
					{
						PlayerBean pb = new PlayerBean();
						
						pb._playerId = player;
						pb.position = playerPos;					
						gameBean._players.add(player);
						
						gameBean._playersBeansList.add(pb);
						
						gameBean.removeSpecatator(player);
						
						params.putUtfStringArray("players", gameBean._players);
						params.putUtfStringArray("initialPlayers", gameBean._players);
						params.putLong("remainingQuickStartTime", gameBean.getRemainingQuickStartTime());
						params.putUtfString("command", "seatAvailable");
						Commands.appInstance.send(Commands.OCCUPY_SEAT, params, room.getUserList());
						
						gameBean.getGameLogInstance().playerJoinInPrivateTable(gameBean, pb);
						Commands.appInstance.proxy.updatePlayingStatusTrue(gameBean, player);
						
						if(gameBean._players.size() == 4)
						{
							gameBean.initGame();
						}
					}
					else
					{
						Validations.raisePositionNotAvailabeException(gameBean, player, playerPos);
					}
				}
				else
				{
					System.out.println(" occupu_seatIn_public_table "+player);
				}
				
				Commands.appInstance.ulBsn.updateLobby(gameBean);
			}
		}
		else
		{
			String var = " gameId "+gameId+" tableId "+tableId+" player "+player+" position "+playerPos
							+" isRematchPhase "+gameBean.isRematchPhase+"   OccupySeatHandler";
			Validations.raisePlayerRequestInRemathcPhaseException(gameBean, player, var);
		}	
	}
	
	private void replaceAIwithUser(GameBean gameBean, PlayerBean pb, String player)
	{
		gameBean.getReplaceUserInstance().replacAIwithRealUser(gameBean, pb, player);
		Commands.appInstance.proxy.updateMiddleJoinedUserprofile(gameBean, gameBean.tableId, gameBean.gameId, Apps.getPlayerId(player), player, gameBean.tableType);
		Commands.appInstance.ulBsn.updateLobby(gameBean);		
		
	}
	
	private PlayerBean getExsitingPlayerBean(int position, GameBean gameBean, User user, ISFSObject params)
	{
		PlayerBean pb = null;
		
		for(PlayerBean pbBean : gameBean._playersBeansList)
		{
			if(pbBean.initialPosition == position)
			{
				if(pbBean._isAI)
				{
					pb = pbBean;
					
					String var = " initialPosition "+pbBean.initialPosition+" position "+pbBean.position+" reqPos "+position+"  previousplayer  "+pbBean._playerId+"  requestedPlayer "+user.getName();
					
					LogBean lb = new LogBean();						
					lb.setValues(Constant.LOG, "P0", pbBean._playerId, "PreviousExsitingPlayer", var);
					gameBean.addLog(lb);
				}
				else
				{
					params.putUtfString("command", "seatAlreadyOccupied");
					Commands.appInstance.send(Commands.OCCUPY_SEAT, params, user);
					
					String var = " PlayerBean  "+pb+"  initialPosition "+pbBean.initialPosition+" position "+pbBean.position+"  reqPos "+position+"  previousplayer  "+pbBean._playerId+"  requestedPlayer "+user.getName();
					
					LogBean lb = new LogBean();						
					lb.setValues(Constant.EXC, "P0", pbBean._playerId, "ExistingPlayerIsNotAI", var);
					gameBean.addLog(lb);
				}				
				
				break;
			}
		}
		
		return pb;
	}
}
