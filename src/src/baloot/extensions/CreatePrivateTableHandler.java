/**
 * 
 */
package src.baloot.extensions;

import src.baloot.beans.TableBean;


import src.baloot.constants.Constant;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

/**
 * @author MSKumar
 *
 */
public class CreatePrivateTableHandler extends BaseClientRequestHandler{	
	
	public void handleClientRequest(User sender, ISFSObject params)
	{
		Apps.showLog("*****CreatePrivateTableHandler******");
		
		// Exception validations
		
		//String command = params.getUtfString("command");
		String tableId = params.getUtfString("tableId");
		String password = params.getUtfString("password");			
		boolean enablePassword = params.getBool("enablePassword");
		boolean isBlock = params.getBool("isBlock");
		String player = sender.getName();
		
		
		if(Commands.appInstance.proxy.isHavingEnoughGamesToPlay(null, player, true, true))
		{
			String date = Apps.getDateString();
			
			// Add table to Private Tables list			
			TableBean grb = new TableBean();
			
			grb.setTableid(tableId);
			grb.setPassword(password);
			grb.setTableType(Constant.PRIVATE);
			grb.setIsEnablePassword(enablePassword);
			grb.setIsBlock(isBlock);
			grb.setCreatedDate(date);
			
			Commands.appInstance.privateTables.add(grb);			

			
			// Add Row to data base
			Commands.appInstance.proxy.createPrivateTable(tableId, date);
			
			// Update Lobby
			Commands.appInstance.ulBsn.addLobbyRoom(grb);
			
			// Command 
			Commands.appInstance.send(Commands.CREATE_PRIVATE_TABLE, params, sender);
		}
		else
		{
			send(Commands.NOT_HAVEING_ENOUGH_GAMES, params, sender);
		}
				
	}
}
