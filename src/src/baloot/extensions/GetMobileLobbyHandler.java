package src.baloot.extensions;

import src.baloot.beans.GameBean;
import src.baloot.beans.TableBean;
import src.baloot.bsn.UpdateLobbyBsn;
import src.baloot.exce.Validations;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.buddylist.Buddy;
import com.smartfoxserver.v2.buddylist.BuddyList;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class GetMobileLobbyHandler extends BaseClientRequestHandler {

	public void handleClientRequest(User sender, ISFSObject params)
	{
		Apps.showLog("*******GetMobileLobbyHandler******* "+sender.getName());
		
		String command = params.getUtfString("command");
		params.putInt("onlineUsersCount", Apps.getOnlineUsersCount());
		//DataValidation.GetLobbyHandler(command);
		
		if(command.equals("friends"))
		{
			// Get the users buddy list and search for that user playing game			
			SFSArray tablesList = new SFSArray();
			Integer minRows = 15;
			
			BuddyList buddyList = Commands.appInstance.getParentZone().getBuddyListManager().getBuddyList(sender.getName());
			for(Buddy buddy : buddyList.getBuddies())
			{
				buddy.getName();
				Apps.showLog("Buddy Name"+buddy.getName());
				
				for(int i=0;i<Commands.appInstance.publicTables.size();i++)
				{
					TableBean grb =  Commands.appInstance.publicTables.get(i);
					GameBean gameBean = Apps.getGameBean(grb.getTableId());
					boolean addBean = false;
					if(gameBean != null)
					{
						for(int j = 0; j<gameBean._players.size();j++)
						{
							if(gameBean._players.get(j).equals(buddy.getName()))
							{
								addBean = true;
								boolean sign = false;
								
								for(int a=0;a<tablesList.size();a++)
								{
									if(tablesList.getSFSObject(a).getUtfString("tableId").equals(gameBean.tableId))
									{
										sign = true;
										break;
									}
								}
								if(sign)
									addBean = false;
								break;
							}
						}
						
						if(addBean)
						{
							pushData(grb, gameBean);
							
							tablesList.addSFSObject(grb.getSFSObject());
						}
					}
				}
				
				for(int i=0;i<Commands.appInstance.privateTables.size();i++)
				{
					TableBean grb =  Commands.appInstance.privateTables.get(i);
					GameBean gameBean = Apps.getGameBean(grb.getTableId());
					boolean addBean = false;
					if(gameBean != null)
					{
						for(int j = 0; j<gameBean._players.size();j++)
						{
							if(gameBean._players.get(j).equals(buddy.getName()))
							{
								addBean = true;
								boolean sign = false;								
								for(int a=0;a<tablesList.size();a++)
								{
									if(tablesList.getSFSObject(a).getUtfString("tableId").equals(gameBean.tableId))
									{
										sign = true;
										break;
									}
								}
								if(sign)
									addBean = false;
								break;
							}
						}
						
						if(addBean)
						{
							pushData(grb, gameBean);
							
							tablesList.addSFSObject(grb.getSFSObject());
						}
					}
				}				
			}
			
			SFSArray tables = new SFSArray();
			params.putUtfString("command", "friends");
			
			for(int a=0;a<tablesList.size();a++)
			{				
				tables.addSFSObject(tablesList.getSFSObject(a));
				if(tables.size() == minRows )
				{
					
					params.putSFSArray("tableList", tables);
					send(Commands.GET_LOBBY, params, sender);
				}
			}
			
			if(tables.size() != 0)
			{
				
				params.putSFSArray("tableList", tables);
				send(Commands.GET_LOBBY, params, sender);				
			}			
		}
		else
		{
			// Get the list of Public tables
			SFSArray publicTablesList = new SFSArray();
			SFSArray privateTablesList = new SFSArray();
			int maxRows = 15;
			
			for(int i=0;i<Commands.appInstance.publicTables.size();i++)
			{
				TableBean grb =  Commands.appInstance.publicTables.get(i);
				GameBean gameBean = Apps.getGameBean(grb.getTableId());
				pushData(grb, gameBean);
				
				publicTablesList.addSFSObject(grb.getSFSObject());
				
				if(publicTablesList.size() == maxRows)
				{
					params.putUtfString("command", "publicTables");
					params.putSFSArray("tableList", publicTablesList);
					send(Commands.GET_LOBBY, params, sender);
					publicTablesList = new SFSArray();
				}
			}
			
			if(publicTablesList.size() < maxRows && publicTablesList.size() > 0)
			{
				params.putUtfString("command", "publicTables");
				params.putSFSArray("tableList", publicTablesList);
				send(Commands.GET_LOBBY, params, sender);		
			}
			
			// Get the list of Private tables
			
			for(int i=0;i<Commands.appInstance.privateTables.size();i++)
			{
				TableBean grb =  Commands.appInstance.privateTables.get(i);
				GameBean gameBean = Apps.getGameBean(grb.getTableId());
				pushData(grb, gameBean);
				
				privateTablesList.addSFSObject(grb.getSFSObject());
				
				if(publicTablesList.size() == maxRows)
				{
					params.putUtfString("command", "privateTables");
					params.putSFSArray("tableList", privateTablesList);
					send(Commands.GET_LOBBY, params, sender);
					privateTablesList = new SFSArray();
				}
			}
			
			if(privateTablesList.size() < maxRows)
			{
				params.putUtfString("command", "privateTables");
				params.putSFSArray("tableList", privateTablesList);
				
				send(Commands.GET_LOBBY, params, sender);		
			}
		}		
	}
	
	private void pushData(TableBean grb, GameBean gameBean)
	{
		if(gameBean != null)
		{
			// Fill the data from the gameBean	
			if(gameBean.isGameCompleted)
			{
				if(grb != null)
				{			
					grb.setTeam1Player1("null");
					grb.setTeam1Player2("null");
					grb.setTeam2Player1("null");
					grb.setTeam2Player2("null");
					grb.setTeam1Score(0);
					grb.setTeam2Score(0);
					grb.setNoofSpectators(0);
					grb.setIsStarted(false);
					
					// Send the Object to room users
					Commands.appInstance.send(Commands.UPDATE_LOBBY, grb.getSFSObject(), UpdateLobbyBsn.getAllUsers());
					Validations.updateLobbyAfterGameCompletionException(gameBean, grb);
				}
			}
			else if(gameBean.isGameStarted)
			{
				/*for(int j=0;j<gameBean._players.size();j++)
				{
					if(j == 0)
						grb.setTeam1Player1(gameBean._players.get(j));
					else if(j == 1)
						grb.setTeam2Player1(gameBean._players.get(j));
					else if(j == 2)
						grb.setTeam1Player2(gameBean._players.get(j));
					else if(j == 3)
						grb.setTeam2Player2(gameBean._players.get(j));
				}
				
				grb.setTeam1Score(gameBean._playersBeansList.get(0)._totalPoints);
				grb.setTeam2Score(gameBean._playersBeansList.get(1)._totalPoints);	*/
				
				grb.setTeam1Score(gameBean.getPlayerBean(gameBean.initialPlayers.get(0))._totalPoints);
				grb.setTeam2Score(gameBean.getPlayerBean(gameBean.initialPlayers.get(1))._totalPoints);
				
				if(gameBean.initialPlayers.size() == 4)
				{
					grb.setTeam1Player1(gameBean.initialPlayers.get(0));
					grb.setTeam1Player2(gameBean.initialPlayers.get(2));
					grb.setTeam2Player1(gameBean.initialPlayers.get(1));
					grb.setTeam2Player2(gameBean.initialPlayers.get(3));
				}
				else
				{
					Validations.raiseLobbyUpdateException(gameBean);
				}
			}
			else
			{
				for(int j=0;j<gameBean._players.size();j++)
				{
					if(j == 0)
						grb.setTeam1Player1(gameBean._players.get(j));
					else if(j == 1)
						grb.setTeam2Player1(gameBean._players.get(j));
					else if(j == 2)
						grb.setTeam1Player2(gameBean._players.get(j));
					else if(j == 3)
						grb.setTeam2Player2(gameBean._players.get(j));
				}						
			}
			
			grb.setNoofSpectators(gameBean.getSpecatatorsCount());
		}
	}
}
