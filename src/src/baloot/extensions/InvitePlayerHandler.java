
package src.baloot.extensions;


import java.util.ArrayList;

import java.util.Collection;
import java.util.Iterator;

import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class InvitePlayerHandler extends BaseClientRequestHandler {
	
	public void handleClientRequest(User sender, ISFSObject params)
	{		
		Apps.showLog(" ****** InvitePlayerHandler ***** "+sender.getName());
		// Exception validations
		
		Collection<String> invitee = params.getUtfStringArray("invitee");
		//String tableId = params.getUtfString("tableId");
		
		Iterator<String> iterator = invitee.iterator();
		
		ArrayList<User> list = new ArrayList<User>();	
		
		while(iterator.hasNext())
		{
			String username = iterator.next();
			User user = Apps.getUserByName(username);
			
			if(user != null)
			{
				list.add(user);
			}
		}
		
		params.putUtfString("command", "invite");
    	params.putUtfString("invitor", sender.getName());
		
		send(Commands.INVITE_PLAYER, params, list);
		
		ArrayList<String> offlineUsers = new ArrayList<String>();
		/*while(iterator.hasNext())
		{
			String username = iterator.next();
			User user = Apps.getUserByName(username);
			if(user == null)
			{
				offlineUsers.add(username);
			}
			else
			{
				Collection<User> users = Commands.appInstance.getParentZone().getUsersInGroup("GameGroup");
                Object arr[] = users.toArray();
                
                boolean isBusy = false;
                
                for(int i = 0; i < arr.length; i++)
                {
                    User gameuser = (User)arr[i];
                    if(!gameuser.getName().equals(user.getName()))
                    {
                        continue;
                    }
                    isBusy = true;
                    break;
                }
                if(!isBusy)
                {
                	params.putUtfString("command", "invite");
                	params.putUtfString("invitor", sender.getName());
					
					send(Commands.INVITE_PLAYER, params, user);
                }
			}
		}
		
		if(offlineUsers.size()>0)
		{
			params.putUtfString("command", "usersLeft");
			params.putUtfStringArray("offlineUsers", offlineUsers);
			
			send(Commands.INVITE_PLAYER, params, sender);
		}*/	
		
	}
}
