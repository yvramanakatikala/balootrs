/**
 * 
 */
package src.baloot.extensions;

import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class SinglePlayerGameHandler extends BaseClientRequestHandler
{
	public synchronized void handleClientRequest(User sender, ISFSObject params)
	{
		Apps.showLog(" ***** SinglePlayerGameHandler ***** "+sender.getName());
		String player = sender.getName();
		
		if(Commands.appInstance.proxy.isHavingEnoughGamesToPlay(null, player, false, false))
		{
			Commands.appInstance.sgBsn.startSinglePlayerGame(sender);	
		}
		else
		{
			send(Commands.NOT_HAVEING_ENOUGH_GAMES, params, sender);
		}
			
	}
}
