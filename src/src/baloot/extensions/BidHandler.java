package src.baloot.extensions;
import src.baloot.beans.GameBean;


import src.baloot.exce.DataValidation;
import src.baloot.exce.Validations;
import src.baloot.utils.Apps;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
 
public class BidHandler extends BaseClientRequestHandler{

	public void handleClientRequest(User sender, ISFSObject params)
	{
		Apps.showLog("**********BidHandler**************"+sender.getName());
		
		String player = sender.getName();
		String bidSelected = params.getUtfString("bidSelected");
		String gameTrumpSuit = params.getUtfString("gameTrumpSuit");
		String tableId = params.getUtfString("tableId");
		String gameId = params.getUtfString("gameId");
		int roundCount = params.getInt("roundCount");
		int bidRoundCount = params.getInt("bidRoundCount");
		
		// Get game bean
		GameBean gameBean = Apps.getGameBean(tableId);
		DataValidation.bidHandler(bidSelected, gameTrumpSuit, tableId, gameBean);
		
		if(gameBean != null)
		{
			if(gameBean.gameId.equals(gameId))
			{
				if(gameBean.getRoundCount() == roundCount)
				{
					if(gameBean._roundBean._bidRoundCount == bidRoundCount)
					{
						if(gameBean.isRematchPhase)
						{
							// Add Log 
							String var = "Handler BidHandler bidSelected "+bidSelected+"  gameTrumpSuit "+gameTrumpSuit;				
							Validations.raisePlayerRequestInRemathcPhaseException(gameBean, player, var);
						}
						else
						{
							gameBean.getBidInstance().bidSelectedRequest(gameBean, player, bidSelected, gameTrumpSuit);
						}
					}
					else
					{
						Validations.raiseBidRoundCountNotMatchException(gameBean, player, gameId, roundCount, bidRoundCount, "BidHandler");
					}
				}
				else
				{
					Validations.raiseRoundCountNotMatchException(gameBean, player, gameId, roundCount, bidRoundCount, "BidHandler");
				}			
			}
			else
			{
				String var = " gameId "+gameBean.gameId+"  clientGameId "+gameId+" clietnTableId "+tableId+" bidSelected "+bidSelected
								+" gameTrumpSuit "+gameTrumpSuit+"  BidHandler";
				
				Validations.raiseGameIdNotMatchException(gameBean, player, var);
			}
		}
		else
		{
			trace(" gameBean_is_null_in_BidHandler gameBean "+gameBean+" player "+player+" tableId "+tableId);
		}
			
	}
	
}
