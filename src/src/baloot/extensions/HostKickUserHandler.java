/**
 * 
 */
package src.baloot.extensions;

import java.util.ArrayList;

import java.util.Collection;
import java.util.Iterator;

import src.baloot.exce.DataValidation;
import src.baloot.exce.Validations;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class HostKickUserHandler extends BaseClientRequestHandler
{
	public void handleClientRequest(User sender, ISFSObject params)
	{
		Apps.showLog("*** HostKickUserHandler******* "+sender.getName());

		// Exception validations
		
		Collection<String> users =  params.getUtfStringArray("kickUsers");
		String tableId = params.getUtfString("tableId");
		
		DataValidation.HostKickUserHandler(users, tableId);
		
		Iterator<String> iterator = users.iterator();
		
		ArrayList<User> kickUsers = new ArrayList<User>();
		ArrayList<String> players = new ArrayList<String>();
		
		while(iterator.hasNext())
		{
			String player = iterator.next();
			User user = Apps.getUserByName(player);
			
			if(user != null)
			{
				kickUsers.add(user);
				players.add(player);
			}
		}	
		
		send(Commands.HOST_KICK_USER, params, kickUsers);	
		
		Validations.riaseHostKickUserException(tableId, players, sender.getName());
		
		for(User user : kickUsers)
		{
			Commands.appInstance.handleClientRequest(Commands.JOIN_LEAVE, user, params);
		}
		// call the leave table for kick users
	}
}
