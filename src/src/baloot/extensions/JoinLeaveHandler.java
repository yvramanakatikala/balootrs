
package src.baloot.extensions;

import java.util.ArrayList;


import src.baloot.beans.GameBean;


import src.baloot.beans.PlayerBean;
import src.baloot.constants.Constant;
import src.baloot.exce.DataValidation;
import src.baloot.exce.Validations;
import src.baloot.exce.beans.LogBean;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class JoinLeaveHandler extends BaseClientRequestHandler {
	
	public synchronized void handleClientRequest(User sender, ISFSObject params)
	{
		Apps.showLog("******** JoinLeaveHandler ********"+sender.getName());

		
		String tableId = params.getUtfString("tableId");
		boolean isPrivateTable = params.getBool("isPrivateTable");
		boolean isJoin = params.getBool("isJoin");
		boolean isGuestUser = Apps.isGuestUser(sender.getName());
		boolean isPlaying = false;
		
		//System.out.println("  %%%%%%%%%% IP ADDERSS"+sender.getIpAddress());
		
		DataValidation.userJoinedHandler(tableId, isPrivateTable, isJoin);
		
		String player = sender.getName();
		
		GameBean gameBean = Apps.getGameBean(tableId);
		Room room = null;
		room = Apps.getRoomByName(tableId);
			
		if(!isGuestUser)
		{
			isPlaying = Commands.appInstance.proxy.isPlayingGame(player);
		}
		
		if(isJoin && isPlaying)
		{
			ISFSObject sss = Commands.appInstance.proxy.getCurrentTalbeId(player);
			String playingTableId = sss.getUtfString("tableId");
			
			params.putUtfString("playingTableId", playingTableId);
			
			send(Commands.TRY_TO_JOIN_SECOND_TABLE, params, sender);
			
			Validations.raisePlayerTryToJoinInToSecondTableException(gameBean, player, playingTableId);
		}
		else if(isJoin && gameBean == null && isPrivateTable && !player.equals(tableId))
		{
			send(Commands.HOST_LEFT, params, sender);
		}
		else if(isJoin && gameBean == null)
		{
			if(isPrivateTable)
			{
				Apps.createGameRoom(tableId);
				room = Apps.getRoomByName(tableId);
				
				Apps.joinGameRoom(sender, room);
				
				gameBean = new GameBean();
				gameBean.tableId = tableId;
				
				gameBean._players.add(player);
				gameBean.setPrivateTable(isPrivateTable);
				gameBean.host = player;
				gameBean.isHostJoined = true;
				
				PlayerBean pb = new PlayerBean();
				pb._playerId = player;
				pb._aiPlayer = player;
				pb.isHost = true;
				pb.position = 0;
				
				gameBean._playersBeansList.add(pb);			
				
				// Add the current gameBean to games list
				Commands.appInstance.getGames().put(tableId, gameBean);
				gameBean.incrementTicket();
				// Add Log					
				String var = " player "+player+" tableId "+tableId;
				LogBean lb = new LogBean();
				lb.setValues(Constant.LOG, "P3", player, "UserJoin", var);
				gameBean.addLog(lb);
				gameBean.writeGameLog();
				
				// Start the quick Start timer			
				if(gameBean.isPrivateTable())
				{
					gameBean.tableType = Constant.PRIVATE;
					gameBean.startTimer(Constant.QUICK_START_TIME_FOR_PRIVATE_TABLE, player, Commands.QUICK_START_PRIVATE_TABLE);
				}
				else
				{
					gameBean.tableType = Constant.PUBLIC;
					gameBean.startTimer(Constant.QUICK_START_TIME, player, Commands.QUICK_START_GAME);
				}
				
				gameBean.isQuickStartPhase = true;
				// Prepare Object 
				params.putUtfString("tableId", gameBean.tableId);
				params.putUtfString("gameId", gameBean.gameId);
				params.putUtfStringArray("players", gameBean._players);
				params.putBool("isSpectator", false);
				params.putUtfString("joinedUser", player);
				params.putBool("rematchPhase", false);
				params.putLong("remainingQuickStartTime", gameBean.getRemainingQuickStartTime());
				params.putUtfString("gameStartTime", gameBean.gameStartedTimeForClient);
				params.putInt("ticket", gameBean.getTicket() );
				params.putIntArray("positions", gameBean.getPositions());
					
				// Update Lobby
				Commands.appInstance.ulBsn.updateLobby(gameBean);
				
				// User joined
				send(Commands.USER_JOINED, params, room.getUserList());
				
				
				Commands.appInstance.proxy.updatePlayingStatusTrue(gameBean, player);			
			}
			else
			{
				if(Commands.appInstance.proxy.isHavingEnoughGamesToPlay(null, player, isPrivateTable, false))
				{
					Apps.createGameRoom(tableId);
					room = Apps.getRoomByName(tableId);
					
					Apps.joinGameRoom(sender, room);
					
					gameBean = new GameBean();
					gameBean.tableId = tableId;
					
					gameBean._players.add(player);
					gameBean.setPrivateTable(isPrivateTable);
					gameBean.host = player;
					gameBean.isHostJoined = true;
					
					PlayerBean pb = new PlayerBean();
					pb._playerId = player;
					pb._aiPlayer = player;
					pb.isHost = true;
					pb.position = 0;
					
					gameBean._playersBeansList.add(pb);			
					
					// Add the current gameBean to games list
					Commands.appInstance.getGames().put(tableId, gameBean);
					gameBean.incrementTicket();
					// Add Log					
					String var = " player "+player+" tableId "+tableId;
					LogBean lb = new LogBean();
					lb.setValues(Constant.LOG, "P3", player, "UserJoin", var);
					gameBean.addLog(lb);
					gameBean.writeGameLog();
					
					// Start the quick Start timer			
					if(gameBean.isPrivateTable())
					{
						gameBean.tableType = Constant.PRIVATE;
						gameBean.startTimer(Constant.QUICK_START_TIME_FOR_PRIVATE_TABLE, player, Commands.QUICK_START_PRIVATE_TABLE);
					}
					else
					{
						gameBean.tableType = Constant.PUBLIC;
						gameBean.startTimer(Constant.QUICK_START_TIME, player, Commands.QUICK_START_GAME);
					}
					
					gameBean.isQuickStartPhase = true;
					// Prepare Object 
					params.putUtfString("tableId", gameBean.tableId);
					params.putUtfString("gameId", gameBean.gameId);
					params.putUtfStringArray("players", gameBean._players);
					params.putBool("isSpectator", false);
					params.putUtfString("joinedUser", player);
					params.putBool("rematchPhase", false);
					params.putLong("remainingQuickStartTime", gameBean.getRemainingQuickStartTime());
					params.putUtfString("gameStartTime", gameBean.gameStartedTimeForClient);
					params.putInt("ticket", gameBean.getTicket() );
					params.putIntArray("positions", gameBean.getPositions());
						
					// Update Lobby
					Commands.appInstance.ulBsn.updateLobby(gameBean);
					
					// User joined
					send(Commands.USER_JOINED, params, room.getUserList());
					
					
					Commands.appInstance.proxy.updatePlayingStatusTrue(gameBean, player);
					
				
				}
				else
				{
					send(Commands.NOT_HAVEING_ENOUGH_GAMES, params, sender);
				}
			}			
		}
		else if(gameBean != null)
		{
			gameBean.getJoinLeaveInstance().joinOrLeaveUser(tableId, isPrivateTable, isJoin, sender, params);
		}
		else
		{
			params.putUtfStringArray("initialPlayers", new ArrayList<String>());
			params.putUtfString("command", "LeaveTable");
			Commands.appInstance.send(Commands.LEAVE_TABLE, params, sender);
		}
	
	}
}
