/**
 * 
 */
package src.baloot.extensions;

import java.util.ArrayList;

import src.baloot.beans.GameBean;
import src.baloot.exce.Validations;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class RematchHandler extends BaseClientRequestHandler{
	
	public synchronized void handleClientRequest(User sender, ISFSObject params)
	{
		Apps.showLog("***** RematchHandler **** "+sender.getName());
		
		String tableId = params.getUtfString("tableId");
		String gameId = params.getUtfString("gameId");
		String player = sender.getName();
		
		GameBean gameBean = Apps.getGameBean(tableId);
		
		if(gameBean != null)
		{
			if(gameBean.gameId.equals(gameId))
			{
				Room room = Apps.getRoomByName(tableId);
				
				if(gameBean.isRematchPhase)
				{
					if(gameBean.rematchPlayers.size() < 4)
					{
						if(gameBean._players.contains(player) && !gameBean.rematchPlayers.contains(player))
						{
							if(gameBean.isPrivateTable())
							{
								gameBean.rematchPlayers.add(player);
								gameBean.incrementTicket();
								
								gameBean.getGameLogInstance().userJoinInRematchPhase(gameBean, player);
							}
							else
							{
								if(Commands.appInstance.proxy.isHavingEnoughGamesToPlay(gameBean, player, false, false))
								{
									gameBean.rematchPlayers.add(player);
									gameBean.incrementTicket();
									
									gameBean.getGameLogInstance().userJoinInRematchPhase(gameBean, player);
								}
								else
								{
									send(Commands.NOT_HAVEING_ENOUGH_GAMES, params, sender);
									
									params.putBool("isJoin", false);
									params.putBool("isPrivateTable", gameBean.isPrivateTable());
									
									Commands.appInstance.handleClientRequest(Commands.JOIN_LEAVE, sender, params);
								}
							}
						}
						else
						{
							Validations.raiseSpetatorJoinRequestInRematchPhaseException(gameBean, player);
						}
					}
					
					if(gameBean.rematchPlayers.size() == 4)
					{
						gameBean.getRematchInstance().startRematchGame(gameBean);
					}
					
					if(gameBean.isPrivateTable() && gameBean.rematchPlayers.size() == 1)
					{
						if( Commands.appInstance.proxy.isHavingEnoughGamesToPlay(gameBean, player, true, true))
						{
							send(Commands.REMATCH, params, getRemainingUsersWithOutSpectators(room, sender, gameBean));
							
							send(Commands.REMATCH_FOR_SPECTATOR, params, getSpectators(room, sender, gameBean));
						}
						else
						{
							send(Commands.NOT_HAVEING_ENOUGH_GAMES, params, sender);
							Commands.appInstance.sgBsn.freeTable(gameBean);
						}
					}
				}
				else
				{
					Validations.raiseRematchRequestNotInRematchPhaseException(gameBean, player);
				}
			}
			else
			{
				String var = " gameId "+gameBean.gameId+" clientgameId "+gameId+" clienttalbeId "+tableId+"  roundCount "+gameBean.getRoundCount()
								+" trickCount "+gameBean._roundBean.trickCount+"   RematchHandler";
				Validations.raiseGameIdNotMatchException(gameBean, player, var);
			}
		}
		else
		{
			send(Commands.CLOSE_ROOM, params, sender);
		}
	}
	
	private  ArrayList<User> getRemainingUsersWithOutSpectators(Room room, User user, GameBean gameBean)
	{
		ArrayList<User> al = new ArrayList<User>();
		
		if(room != null)
		{
			for(User sender : room.getUserList())
			{
				if(!sender.getName().equals(user.getName()) && gameBean._players.contains(sender.getName()))
				{
					al.add(sender);
				}
			}
		}
		
		return al;
	}
	
	private  ArrayList<User> getSpectators(Room room, User user, GameBean gameBean)
	{
		ArrayList<User> al = new ArrayList<User>();
		
		if(room != null)
		{
			for(User sender : room.getUserList())
			{
				if(!sender.getName().equals(user.getName()) && gameBean.getSpecatatores().contains(sender.getName()))
				{
					al.add(sender);
				}
			}
		}
		
		return al;
	}
}
