/**
 * 
 */
package src.baloot.extensions;

import com.smartfoxserver.v2.entities.Room;


import com.smartfoxserver.v2.entities.User;


import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;


import src.baloot.beans.GameBean;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

public class ForceLoginHandler extends BaseClientRequestHandler
{
	//private ScheduledExecutorService worker = Executors.newSingleThreadScheduledExecutor();
	
	ISFSObject params = null;
	User user = null;
	User sender = null;
	
	public void handleClientRequest(User sender, ISFSObject params)
	{
		Apps.showLog("*********ForceLoginHandler********* "+sender.getName());
			
		this.params = params;
		this.sender = sender;
		String username = params.getUtfString("user");
		
		user = Apps.getUserByName(username);		
		
		if(user != null)
		{
			// this will not reach the client. because user is not logged in the client view (only dummy user is logged in client view)
			params.putBool("logout", true);
			send(Commands.FORCE_LOGIN, params, user);			
			//worker.schedule(task, 100, TimeUnit.MILLISECONDS);
			
			writeGameLog(user, sender.getName());
	    	Commands.appInstance.getApi().disconnectUser(user);			
	    	params.putBool("logout", false);
			
			send(Commands.FORCE_LOGIN, params, sender);
		}
		else
		{
			params.putBool("logout", false);
			send(Commands.FORCE_LOGIN, params, sender);
		}
		
	}
	
	public void writeGameLog(User realUser, String dummyUser)
	{
		Room room = realUser.getLastJoinedRoom();
		
		if(room != null)
		{
			GameBean gameBean = Apps.getGameBean(room.getName());
					
			if(gameBean != null)
			{
				gameBean.getGameLogInstance().forceLoginUserDisconneted(gameBean, realUser.getName(), dummyUser);
			}
		}
	}
	
	/*Runnable task = new Runnable() 
	{
	    public void run() {
	    	
	    	Apps.showLog("FORCE LOGIN inside run");
	    	
	    	Commands.appInstance.getApi().disconnectUser(user);			
	    	params.putBool("logout", false);
			
			send(Commands.FORCE_LOGIN, params, sender);
	    }
	 };*/	
}

