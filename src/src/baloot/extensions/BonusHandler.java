package src.baloot.extensions;
import java.util.ArrayList;



import src.baloot.beans.GameBean;
import src.baloot.exce.DataValidation;
import src.baloot.exce.Validations;
import src.baloot.utils.Apps;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class BonusHandler extends BaseClientRequestHandler{
	
	public void handleClientRequest(User sender, ISFSObject params)
	{
		Apps.showLog("*********BonusHandler********** "+sender.getName());
		
		// Exception validations
				
		String tableId = params.getUtfString("tableId");
		String command = params.getUtfString("command");
		String gameId = params.getUtfString("gameId");
		int roundCount = params.getInt("roundCount");
		int trickCount = params.getInt("trickCount");
		String player = sender.getName();		
		ArrayList<Integer> bonusCards = new ArrayList<Integer>();

		// Get game bean
		GameBean gameBean = Apps.getGameBean(tableId);
		
		DataValidation.bonusHandler(bonusCards, command, gameBean);
		
		if(gameBean != null)
		{
			if(!gameBean.isRematchPhase)
			{
				if(gameBean.gameId.equals(gameId))
				{
					if(gameBean.getRoundCount() == roundCount)
					{
						if(gameBean._roundBean.trickCount == trickCount)
						{
							if(gameBean._roundBean.trickCount == 2)
							{
								gameBean.getDiscardInstance().bonusRequest(command, bonusCards, "NA", player, gameBean, false);
							}
							else
							{
								Validations.raiseBonusRequestNotInSecondTrickException(gameBean, player, command);
							}
						}
						else
						{
							Validations.raiseTrickCountNotMatchException(gameBean, player, gameId, roundCount, trickCount, "BonusHandler");
						}
					}
					else
					{
						Validations.raiseRoundCountNotMatchException(gameBean, player, gameId, roundCount, trickCount, "BonusHandler");
					}
				}
				else
				{
					String var = " gameId "+gameBean.gameId+"  clientGameId "+gameId+" command "+command
									+" roundCount "+gameBean.getRoundCount()+" trickCount "+gameBean._roundBean.trickCount
									+" clietnTableId "+tableId+" clietnRoundCount "+roundCount+"  clienttrickCount "+trickCount+"  BonusHandler";
			
					Validations.raiseGameIdNotMatchException(gameBean, player, var);
				}
			}
			else
			{
				String var = " player "+player+" command "+command+" isRematchPhase "+gameBean.isRematchPhase
								+" roundCount "+gameBean.getRoundCount()+" trickCount "+gameBean._roundBean.trickCount+"  BonusHandler ";
				Validations.raisePlayerRequestInRemathcPhaseException(gameBean, player, var);
			}
		}
		
	}	
}
