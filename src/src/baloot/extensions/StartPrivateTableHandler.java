/**
 * 
 */
package src.baloot.extensions;

import src.baloot.beans.GameBean;

import src.baloot.constants.Constant;
import src.baloot.exce.beans.LogBean;
import src.baloot.utils.Apps;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class StartPrivateTableHandler extends BaseClientRequestHandler
{
	public void handleClientRequest(User sender, ISFSObject params)
	{
		Apps.showLog("******StartPrivateTableHandler****** "+sender.getName());
		
		String tableId = params.getUtfString("tableId");
		String player = sender.getName();
		
		GameBean gameBean = Apps.getGameBean(tableId);
		
		if(gameBean != null)
		{
			// Add log
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P1", player, "StartPrivateTable_by_player", "");
			gameBean.addLog(lb);
			lb = null;
			
			gameBean.initGame();
		}
	
	}
}
