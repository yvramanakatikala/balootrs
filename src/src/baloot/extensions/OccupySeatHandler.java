

package src.baloot.extensions;


import src.baloot.beans.GameBean;
import src.baloot.exce.Validations;
import src.baloot.model.OccupySeat;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class OccupySeatHandler extends BaseClientRequestHandler{
	
	public void handleClientRequest(User sender, ISFSObject params)
	{
		Apps.showLog("*******OccupySeatHandler******* "+sender.getName());
		// Exception validations
		
		Integer playerPos = params.getInt("playerPos");	
		String tableId = params.getUtfString("tableId");
		String gameId = params.getUtfString("gameId");
		
		//DataValidation.OccupySeatHandler(playerPos, tableId);
		
		String player = sender.getName();
		
		GameBean gameBean = Apps.getGameBean(tableId);		
		
		if(gameBean != null)
		{
			gameBean.getGameLogInstance().occupieSeat(gameBean, playerPos, gameId, tableId, player);
			
			if(gameBean.gameId.equals(gameId))
			{
				if(!(gameBean.getOccupySeatInstance().isLock()))
				{
					try
					{
						gameBean.getOccupySeatInstance().occupySeat(gameBean, playerPos, player, params, gameId, tableId);gameBean.getOccupySeatInstance().isLock();
					}catch(Exception e)
					{
						e.printStackTrace();
					}
					finally
					{
						gameBean.getOccupySeatInstance().setLock(false);
					}					
				}
				else
				{
					
					params.putUtfString("extensionCommand", Commands.OCCUPY_SEAT);
					send(Commands.RESOURCE_BUSY_TRY_AGAIN, params, sender);					
					gameBean.getGameLogInstance().resouceBusyTryAgain(gameBean, playerPos, gameId, tableId, player, params);
					
					//trace("   Lock_is_not_available isLocak "+gameBean.getOccupySeatInstance().isLock()+" player "+player+" pos "+playerPos);
					//trace("   Lock_is_not_available isLocak "+gameBean.getOccupySeatInstance().isLock()+" player "+player+" pos "+playerPos);
					//trace("   Lock_is_not_available isLocak "+gameBean.getOccupySeatInstance().isLock()+" player "+player+" pos "+playerPos);
					//trace("   Lock_is_not_available isLocak "+gameBean.getOccupySeatInstance().isLock()+" player "+player+" pos "+playerPos);
					//trace("   Lock_is_not_available isLocak "+gameBean.getOccupySeatInstance().isLock()+" player "+player+" pos "+playerPos);
				}				
			}
			else
			{
				String var = " gameId "+gameBean.gameId+" clientGameId "+gameId+"  clientTableId "+tableId+" position "+playerPos
								+"  player "+player+ "  OccupySeatHandler";
				Validations.raiseGameIdNotMatchException(gameBean, player, var);
			}
		}
		else
		{
			// game completed already
			send(Commands.CLOSE_ROOM, params, sender);
		}

	}
	
	
}
