/**
 * 
 */
package src.baloot.extensions;

import java.util.ArrayList;


import java.util.concurrent.ConcurrentHashMap;

import javax.swing.Timer;

import src.baloot.aispan.AISpan;
import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerProfileBean;
import src.baloot.beans.TableBean;
import src.baloot.bsn.DisconnectBsn;
import src.baloot.bsn.MedalsBsn;
import src.baloot.bsn.StartGameBsn;
import src.baloot.bsn.UpdateLobbyBsn;
import src.baloot.classes.AI;
import src.baloot.classes.DataClass;
import src.baloot.classes.GameConfigVariables;
import src.baloot.constants.Constant;
import src.baloot.events.BuddyAddEventHandler;
import src.baloot.events.BuddyRemoveEventHandler;
import src.baloot.events.LoginEventHandler;
import src.baloot.events.LogoutEventHandler;
import src.baloot.events.ServerReadyEventHandler;
import src.baloot.events.UserDisconnectedEventHandler;
import src.baloot.events.UserJoinRoomEventHandler;
import src.baloot.logs.LoadLogFile;
import src.baloot.logs.MainExtensionLog;
import src.baloot.ping.PingBsn;
import src.baloot.ping.UserObject;
import src.baloot.ping.UserPingHandler;
import src.baloot.services.BuddyListRequestHandler;
import src.baloot.services.BuddyListResponseHandler;
import src.baloot.services.ChangePasswordHandler;
import src.baloot.services.ChangePhotoUrlHandler;
import src.baloot.services.ChatHandler;
import src.baloot.services.ClientLogHandler;
import src.baloot.services.DeviceTypeHandler;
import src.baloot.services.GetAllUsersDetailsHandler;
import src.baloot.services.GetDeviceTypeHandler;
import src.baloot.services.GetFaceBookUserDetailsHandler;
import src.baloot.services.GetOtherUserDetailsHandler;
import src.baloot.services.GetUserDetailsHandler;
import src.baloot.services.GetUserWonGamesCountHandler;
import src.baloot.services.SetEmailVisibleHandler;
import src.baloot.services.SoundStatusHandler;
import src.baloot.services.TransferGamesHandler;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;
import src.baloot.utils.MedalsProxy;
import src.baloot.utils.SQLProxy;

import com.smartfoxserver.v2.SmartFoxServer;
import com.smartfoxserver.v2.api.CreateRoomSettings;
import com.smartfoxserver.v2.components.signup.SignUpAssistantComponent;
import com.smartfoxserver.v2.core.SFSEventType;
import com.smartfoxserver.v2.entities.SFSRoomRemoveMode;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.extensions.SFSExtension;


public class BalootExtension extends SFSExtension
{	
	private ConcurrentHashMap<String,GameBean> games = new ConcurrentHashMap<String,GameBean>();
	public  ConcurrentHashMap<String, PlayerProfileBean> playerProfiles = new ConcurrentHashMap<String, PlayerProfileBean>();
	
	private ConcurrentHashMap<String, Integer> playerIds = new ConcurrentHashMap<String, Integer>();
	private ConcurrentHashMap<String, UserObject> userObjs = new ConcurrentHashMap<String, UserObject>();
	
	public ArrayList<TableBean> publicTables = new ArrayList<TableBean>();	
	public ArrayList<TableBean> privateTables = new ArrayList<TableBean>();
	public ArrayList<String> guestUsers = new ArrayList<String>();
	public ArrayList<User> fakePlayers = new ArrayList<User>();
	
	
	public PingBsn pingBsn = new PingBsn();
	public SQLProxy proxy= null;
	public DataClass dc = null;	
	public UpdateLobbyBsn ulBsn = null;
	private DisconnectBsn disconnectionInstance = new DisconnectBsn();
	/** Private table Count */
	public Integer privateTableNo = 0;	
	public AI ai = new AI();	
	public MedalsBsn medalsBsn = null;	
	public MedalsProxy medalsProxy = null;
	public ArrayList<String> suits = new ArrayList<String>();	
	public GameConfigVariables gameCV = new GameConfigVariables();
	public StartGameBsn sgBsn = new StartGameBsn();
	public MainExtensionLog meLog = new MainExtensionLog();
	public AISpan aiSpan = new AISpan();
	
	public SignUpAssistantComponent sac;
	//private Timer timer;
	//private boolean isLock = false;
	//public int poolSize = 10;
	
	
	public SmartFoxServer sfs = SmartFoxServer.getInstance();
	
	
	public void init()
	{
		Apps.showLog("SmartfoxServer2x Started");
		Apps.showLog("Baloot Extension");
		
		//SmartFoxServer.getInstance().getEventManager().setThreadPoolSize(5); 		
		
		medalsBsn = new MedalsBsn(getParentZone());
		medalsProxy = new MedalsProxy(getParentZone());
		
		Commands.appInstance = this;
		
		proxy = new SQLProxy(getParentZone());
		proxy.getPublicTables();	
		
        
        
        LoadLogFile.load(); 
        meLog.init();       
       
        
		// REGISTER REQUEST HANDLERS
		
		addRequestHandler(Commands.Bonus, BonusHandler.class);
		addRequestHandler(Commands.BID, BidHandler.class);	
		addRequestHandler(Commands.BUDDYLIST_REQUEST, BuddyListRequestHandler.class);
		addRequestHandler(Commands.BUDDYLIST_RESPONSE, BuddyListResponseHandler.class);
		
		addRequestHandler(Commands.CREATE_PRIVATE_TABLE, CreatePrivateTableHandler.class);		
		addRequestHandler(Commands.CHAT, ChatHandler.class);
		
		
		addRequestHandler(Commands.DISCARD, DiscardHandler.class);
		
		addRequestHandler(Commands.FORCE_LOGIN, ForceLoginHandler.class);
		
		addRequestHandler(Commands.GET_LOBBY, GetLobbyHandler.class);
		
		addRequestHandler(Commands.HOST_KICK_USER, HostKickUserHandler.class);
		
		addRequestHandler(Commands.INVITE_PLAYER, InvitePlayerHandler.class);
		
		addRequestHandler(Commands.OCCUPY_SEAT, OccupySeatHandler.class);
		
		addRequestHandler(Commands.REMATCH, RematchHandler.class);	
		
		addRequestHandler(Commands.START_PRIVATE_TABLE, StartPrivateTableHandler.class);
		addRequestHandler(Commands.SINGLE_PLAYER_GAME, SinglePlayerGameHandler.class);
		
		addRequestHandler(Commands.JOIN_LEAVE, JoinLeaveHandler.class);		
		addRequestHandler(Commands.USER_PING, UserPingHandler.class);
		
		
		// for exception handling and php services 
		addRequestHandler(Commands.CLIENT_LOG, ClientLogHandler.class);
		addRequestHandler(Commands.CHANGE_PASSWORD, ChangePasswordHandler.class);
		addRequestHandler(Commands.CHANGE_PHOTO_URL, ChangePhotoUrlHandler.class);
		
		addRequestHandler(Commands.DEVICE_TYPE, DeviceTypeHandler.class);
		
		addRequestHandler(Commands.GET_USER_DETAILS, GetUserDetailsHandler.class);
		addRequestHandler(Commands.GET_OTHER_USER_DETAILS, GetOtherUserDetailsHandler.class);
		addRequestHandler(Commands.GET_ALL_USERS_DETAILS, GetAllUsersDetailsHandler.class);
		addRequestHandler(Commands.GET_FACE_BOOK_USER_DETAILS, GetFaceBookUserDetailsHandler.class);
		addRequestHandler(Commands.GET_USER_WON_GAMES_COUNT, GetUserWonGamesCountHandler.class);
		addRequestHandler(Commands.GET_DEVICE_TYPE, GetDeviceTypeHandler.class);
		
		addRequestHandler(Commands.SET_EMAIL_VISIBLE, SetEmailVisibleHandler.class);
		addRequestHandler(Commands.SOUND_STATUS, SoundStatusHandler.class);
		
		addRequestHandler(Commands.TRANSFER_GAMES, TransferGamesHandler.class);
		
		
		// REGISTER EVENT HANDLERS
		
		addEventHandler(SFSEventType.USER_LOGIN, LoginEventHandler.class);
		addEventHandler(SFSEventType.USER_DISCONNECT, UserDisconnectedEventHandler.class);
		addEventHandler(SFSEventType.USER_LOGOUT, LogoutEventHandler.class);
		addEventHandler(SFSEventType.USER_JOIN_ROOM, UserJoinRoomEventHandler.class);
		addEventHandler(SFSEventType.BUDDY_ADD, BuddyAddEventHandler.class);
		addEventHandler(SFSEventType.SERVER_READY, ServerReadyEventHandler.class);
		addEventHandler(SFSEventType.BUDDY_REMOVE, BuddyRemoveEventHandler.class);
		
		
		createLobbyRoom(Constant.LOBBY);
		createAdminRoom("AdminRoom");	
		
		// Lobby Class instance
		ulBsn = new UpdateLobbyBsn();
		dc = new DataClass();
		
		suits.add("clubs");
		suits.add("diamonds");
		suits.add("hearts");
		suits.add("spades");
		
		
		// startTimer(Apps.getRandomNumber(Constant.FAKE_GAME_INIT_TIME));
	}

	public ConcurrentHashMap<String, GameBean> getGames(){
		return games;
	}
	
	public ConcurrentHashMap<String, Integer> getPlayerIds(){
		return playerIds;
	}
	
	public ConcurrentHashMap<String, UserObject> getUserObjs() {
		return userObjs;
	}
	public void setUserObjs(ConcurrentHashMap<String, UserObject> userObjs) {
		this.userObjs = userObjs;
	}
	
	public DisconnectBsn getDisconnectionInstance()
	{
		return disconnectionInstance;
	}
	

	/*public boolean isLock() {
		return isLock;
	}

	
	 * This bit is update locally ie start and stop timer methods.
	 * thats why the following method is no where called
	 
	public void setLock(boolean isLock) {
		this.isLock = isLock;
	}
	
	public void startTimer(int seconds)
	{		
		stopTimer();		
			
		timer = new Timer(seconds*1000, new FakeGameCheckingListener());
		timer.start();	
		isLock = false;
		
		String var = "   delayTime "+seconds+"  isLock "+Commands.appInstance.isLock();
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", Constant.SYSTEM, "TimerStart", var);
		FakeGameLog.addLog(lb);				
	}
	
	public void stopTimer()
	{		
		if(timer != null)
		{
			isLock = true;
			timer.stop();
			timer = null;
			
			String var = "  isLock "+Commands.appInstance.isLock();
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P0", Constant.SYSTEM, "TimerStop", var);
			FakeGameLog.addLog(lb);
		}
	}*/

	public void createAdminRoom(String roomname)
	{		
		try
		{
			CreateRoomSettings settings = new CreateRoomSettings();
		    settings.setName(roomname);	    
		    settings.setMaxUsers(50);
		    settings.setHidden(false);
		    settings.setGroupId(Constant.ADMIN_GROUP);	    	    	    
		    getApi().createRoom(getParentZone(), settings, null);
		    
		}catch(Exception e)
		{
			//e.printStackTrace();
		}
	}
	
	public void createLobbyRoom(String roomname)
	{	
		try
		{
			CreateRoomSettings settings = new CreateRoomSettings();
		    settings.setName(roomname);	    
		    settings.setMaxUsers(50);
		    settings.setHidden(false);
		    settings.setGroupId(Constant.LOBBY_GROUP);	    	    	    
		    getApi().createRoom(getParentZone(), settings, null);
		    
		}catch(Exception e)
		{
			//e.printStackTrace();
		}
	}
	
	public void createNewLobbyRoom(String roomname)
	{		
		try
		{
			CreateRoomSettings settings = new CreateRoomSettings();
		    settings.setName(roomname);
		    settings.setMaxUsers(100);
		    settings.setHidden(false);
		    settings.setGroupId(Constant.LOBBY_GROUP);
		    settings.setAutoRemoveMode(SFSRoomRemoveMode.WHEN_EMPTY);
		    getApi().createRoom(getParentZone(), settings, null);
		    
		}catch(Exception e)
		{
			//e.printStackTrace();
		}
	}
	
	/*public void calculatePoolSize()
	{
		int size = fakePlayers.size();
		int poolSize = 4;
		
		if(size > 6 && size <= 10)
		{
			poolSize = 4;
		}
		else if(size > 10 && size <= 20)
		{
			poolSize = 6;
		}
		else if(size > 20 && size <= 30)
		{
			poolSize = 8;
		}
		else if(size > 30 && size <= 40)
		{
			poolSize = 10;
		}
		else if(size > 40 && size <= 60)
		{
			poolSize = 14;
		}
		else if(size > 60 && size <= 80)
		{
			poolSize = 16;
		}
		else if(size > 80 && size <= 100)
		{
			poolSize = 18;
		}
		else if(size > 100)
		{
			poolSize = 20;
		}
		
		this.poolSize = poolSize;
	}*/
	
	public void destroy(){
		super.destroy();
		
		// Remove Request Handlers
		
		removeRequestHandler(Commands.Bonus);
		removeRequestHandler(Commands.BID);		
		removeRequestHandler(Commands.BUDDYLIST_REQUEST);
		removeRequestHandler(Commands.BUDDYLIST_RESPONSE);
		
		removeRequestHandler(Commands.CREATE_PRIVATE_TABLE);
		removeRequestHandler(Commands.CHAT);
		
		
		removeRequestHandler(Commands.DISCARD);
		
		removeRequestHandler(Commands.FORCE_LOGIN);
		
		removeRequestHandler(Commands.GET_LOBBY);
		removeRequestHandler(Commands.GET_LOBBY_ROOM);
		removeRequestHandler(Commands.GET_USER_STATUS);
		
		removeRequestHandler(Commands.HOST_KICK_USER);
		
		removeRequestHandler(Commands.INVITE_PLAYER);
		
		removeRequestHandler(Commands.OCCUPY_SEAT);
		removeRequestHandler(Commands.REMATCH);

		removeRequestHandler(Commands.SINGLE_PLAYER_GAME);
		
		removeRequestHandler(Commands.JOIN_LEAVE);		
		removeRequestHandler(Commands.USER_PING);
		
		// for exception handling and php services 
		removeRequestHandler(Commands.CLIENT_LOG);
		removeRequestHandler(Commands.CHANGE_PASSWORD);
		removeRequestHandler(Commands.CHANGE_PHOTO_URL);
		
		removeRequestHandler(Commands.GET_USER_DETAILS);
		removeRequestHandler(Commands.GET_OTHER_USER_DETAILS);
		removeRequestHandler(Commands.GET_ALL_USERS_DETAILS);
		removeRequestHandler(Commands.GET_FACE_BOOK_USER_DETAILS);
		removeRequestHandler(Commands.GET_USER_WON_GAMES_COUNT);
		removeRequestHandler(Commands.SET_EMAIL_VISIBLE);
		removeRequestHandler(Commands.SOUND_STATUS);
		removeRequestHandler(Commands.TRANSFER_GAMES);
		
		
		
		// Remove Event handlers
		removeEventHandler(SFSEventType.USER_DISCONNECT);
		removeEventHandler(SFSEventType.USER_LOGIN);
		removeEventHandler(SFSEventType.USER_LOGOUT);
		removeEventHandler(SFSEventType.USER_JOIN_ROOM);
	}	
}
