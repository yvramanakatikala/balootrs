/**
 * 
 */
package src.baloot.extensions;
import java.util.ArrayList;
import java.util.Collection;

import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerBean;
import src.baloot.beans.PlayerRoundBean;
import src.baloot.exce.DataValidation;
import src.baloot.exce.Validations;
import src.baloot.utils.Apps;
import src.baloot.utils.CalculateScores;
import src.baloot.utils.Commands;
import src.baloot.utils.GameApps;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class DiscardHandler extends BaseClientRequestHandler {
	
	public void handleClientRequest(User sender, ISFSObject params)
	{
		Apps.showLog("*********DiscardHandler************ "+sender.getName());
				
		String player = sender.getName();
		Integer cardId = params.getInt("cardId"); 		
		String tableId = params.getUtfString("tableId");
		boolean isDeclaredHighest = params.getBool("isDeclaredHighest");
		boolean isBonus = params.getBool("isBonus");		
		ISFSArray bonus = params.getSFSArray("bonus");
		String command = params.getUtfString("command"); 
		String gameId = params.getUtfString("gameId");
		int roundCount = params.getInt("roundCount");
		int trickCount = params.getInt("trickCount");
		
	 	// Exception validations
		DataValidation.discardHandler(cardId, tableId, isBonus, bonus, command);
		
 		Room room = null;		
		room = Apps.getRoomByName(tableId);
		String suit = GameApps.getSuit(cardId);
		
		GameBean gameBean = Apps.getGameBean(tableId);
		
		if(gameBean != null)
        {
			if(gameBean.gameId.equals(gameId))
			{
				if(gameBean.getRoundCount() == roundCount)
				{
					if(gameBean._roundBean.trickCount == trickCount)
					{
						gameBean.getGameLogInstance().discardLog(gameBean, player, isDeclaredHighest, cardId, isBonus, bonus.size(), command);
						
						if(!gameBean.isRematchPhase && gameBean._roundBean.isRemainingCardsDistributed && !gameBean.isRoundCompletedResultTime)
						{
							if(player.equals(gameBean._roundBean.getTurn()))
							{
								PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(player);
								
								PlayerBean pb = gameBean.getPlayerBean(player);
								
								boolean sign = prb._cards.contains(cardId);
								
								if(sign)						
								{		
									prb.isDeclaredHighest = isDeclaredHighest;
									// Stop the Event Delay Timer
									gameBean.stopTimer();
									
									// handling bonus condition.
									if(isBonus)
									{
										for(int i=0;i<bonus.size();i++)
										{
											ISFSObject sfso = bonus.getSFSObject(i);
											
											Collection<Integer> cards = sfso.getIntArray("cards");
											String bonusType = sfso.getUtfString("bonusType"); 							
											ArrayList<Integer> bonusCards = Apps.getListFromCollection(cards);
											
											gameBean.getDiscardInstance().bonusRequest(command, bonusCards, bonusType, player, gameBean, false);
										}
									}
									
									if(isDeclaredHighest) 
									{			
										CalculateScores cs = new CalculateScores();
										boolean isHighest = cs.isHighest(cardId, gameBean, suit);
										
										// player declared highest.
										params.putUtfString("player", player);
										params.putUtfString("gender", Apps.getGender(player));
										params.putBool("isHighest", isHighest);
										
										send(Commands.HIGHEST_CARD, params, room.getUserList());
										
										
										if(gameBean._roundBean._trumpSuitType.equals(suit) || !isHighest)
										{
											addBonusScores(gameBean, player);							
											gameBean.getGameLogInstance().highestFailed(gameBean, player, cardId, isDeclaredHighest, suit, isHighest);
											gameBean.getDiscardInstance().roundCompleted(gameBean, player, cardId, "", false, true, false);
										}
										else
										{
											if(!pb.isAutoPlay)
											{
												gameBean.getDiscardInstance().discardRequest(gameBean, player, cardId, suit, "Client");
											}
											else
											{
												String var = " player "+player+" Handler DiscardHandler cardid "+cardId+" isDeclaredHighest "+isDeclaredHighest
																+" isAutoplay "+pb.isAutoPlay+" isAI "+pb._isAI+" isFakePlayer "+pb.isFakePlayer;
												Validations.raisePlayerActionAgainstAIActionException(gameBean, player, var);
											}
										}
									}
									else
									{
										if(gameBean._roundBean._doubleType.equals(Commands.CLOSED_DOUBLE) 
												&& gameBean._roundBean._trumpSuitType.equals(suit) 
												&& gameBean._roundBean.openCardsList.size() == 0
												&& gameBean.getDiscardInstance().isHavingOtherSuitCards(prb, gameBean._roundBean._trumpSuitType ))
										{
											// Add Log 
											String var = " suit "+suit+" cardId "+cardId;
											gameBean.getGameLogInstance().closedDoubleFailed(gameBean, player, var);
											
											addBonusScores(gameBean, player);
											gameBean.getDiscardInstance().roundCompleted(gameBean, player, cardId, "", false, false, true);
										}
										else
										{				
											if(!pb.isAutoPlay)
											{
												gameBean.getDiscardInstance().discardRequest(gameBean, player, cardId, suit, "Client" );
											}									
											else
											{
												String var = " player "+player+" Handler DiscardHandler cardid "+cardId+" isDeclaredHighest "+isDeclaredHighest
																+" isAutoplay "+pb.isAutoPlay+" isAI "+pb._isAI+" isFakePlayer "+pb.isFakePlayer;
												Validations.raisePlayerActionAgainstAIActionException(gameBean, player, var);
											}
										}
									}
								}
								else
								{
									Validations.raiseDiscarededCardNotAvailableException(gameBean, player, isDeclaredHighest, cardId, prb);
								}
							}
							else
							{
								Validations.raiseDiscardNotInTurnActionInException(gameBean, player, isDeclaredHighest, cardId, isBonus, bonus.size(), command);
							}
						}
						else
						{
							String var = " player "+player+" cardId "+cardId+"  isDeclaredHighest "+isDeclaredHighest+" isBonus "+isBonus+"  bonussize "+bonus.size()
									+" command "+command+" isRematchPahse "+ gameBean.isRematchPhase+" isRematchPhase"+"  isRoundCompletedResultTime "+gameBean.isRoundCompletedResultTime;
							Validations.raisePlayerRequestInRemathcPhaseException(gameBean, player, var);
						}
					}
					else
					{
						Validations.raiseTrickCountNotMatchException(gameBean, player, gameId, roundCount, trickCount, "DiscardHandler");
					}
				}
				else
				{
					Validations.raiseRoundCountNotMatchException(gameBean, player, gameId, roundCount, trickCount, "DiscardHandler");
				}				
			}
			else
			{
				String var = " gameId "+gameBean.gameId+"  clientGameId "+gameId+" command "+command+" roundCount "+gameBean.getRoundCount()
						+" trickCount "+gameBean._roundBean.trickCount+" isDeclaredHighest "+isDeclaredHighest+" isBonus "+isBonus
						+" clietnRoundCount "+roundCount+"  clienttrickCount "+trickCount+"  DiscardHandler";
				
				Validations.raiseGameIdNotMatchException(gameBean, player, var);
			}			
        }
		else
		{
			trace("Baloot : Hiscard handler Gamebean not found");
		}
		
	}
	
	private void addBonusScores(GameBean gameBean, String player)
	{
		int pos = -1;
		pos = gameBean.getPlayerPosition(player); 
		
		int teampoints = 0;
		
		int team1Bonus = 0;
		int team2Bonus = 0;
		int team1BalootBonus = 0;
		int team2BalootBonus = 0;
		
		// Get the Bonus
		ArrayList<Integer> teamBonus = new CalculateScores().getTeamBonusScores(gameBean);
		ArrayList<Integer> balootBonus = new CalculateScores().getBalootBonus(gameBean);
		
		team1Bonus = teamBonus.get(0);
		team2Bonus = teamBonus.get(1);
		team1BalootBonus = balootBonus.get(0);
		team2BalootBonus = balootBonus.get(1);
		
		// Double the score and Add the bonus
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{
			teampoints = (16)*(gameBean._roundBean.doubleCount+1) + team1Bonus + team2Bonus;
		}
		else
		{
			teampoints = (26)*(gameBean._roundBean.doubleCount+1) + team1Bonus + team2Bonus;	
		}
		
		
		if(pos == 0 || pos == 2)
		{						
			gameBean._playersBeansList.get(0)._points.add(0+team1BalootBonus);
			gameBean._playersBeansList.get(2)._points.add(0+team1BalootBonus);
			gameBean._playersBeansList.get(0)._totalPoints += 0+team1BalootBonus;
			gameBean._playersBeansList.get(2)._totalPoints += 0+team1BalootBonus;	
			
			gameBean._playersBeansList.get(1)._points.add(teampoints+team2BalootBonus);
			gameBean._playersBeansList.get(3)._points.add(teampoints+team2BalootBonus);
			gameBean._playersBeansList.get(1)._totalPoints += teampoints+team2BalootBonus;
			gameBean._playersBeansList.get(3)._totalPoints += teampoints+team2BalootBonus;	
			
			
		}
		else
		{
			gameBean._playersBeansList.get(0)._points.add(teampoints+team1BalootBonus);
			gameBean._playersBeansList.get(2)._points.add(teampoints+team1BalootBonus);
			gameBean._playersBeansList.get(0)._totalPoints += teampoints+team1BalootBonus;
			gameBean._playersBeansList.get(2)._totalPoints += teampoints+team1BalootBonus;	
			
			gameBean._playersBeansList.get(1)._points.add(0+team2BalootBonus);
			gameBean._playersBeansList.get(3)._points.add(0+team2BalootBonus);
			gameBean._playersBeansList.get(1)._totalPoints += 0+team2BalootBonus;
			gameBean._playersBeansList.get(3)._totalPoints += 0+team2BalootBonus;
			 
		}		
			
		
		calculateGamePoints(gameBean, player);
	}
	
	/**
	 * This method is used to calculate the game points and bonus points by the requirement of Eyad.
	 * @param gameBean
	 * @param lostPlayer
	 */
	public void calculateGamePoints(GameBean gameBean, String lostPlayer)
	{
		ArrayList<String> lostPlayers = new ArrayList<String>();
		lostPlayers.add(lostPlayer);
		lostPlayers.add(gameBean.getPartner(lostPlayer));
		
		int gamePoints = 0;
		
		/*// Double the score and Add the bonus
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{
			gamePoints = (162)*(gameBean._roundBean.doubleCount+1);
		}
		else
		{
			gamePoints = (130)*(gameBean._roundBean.doubleCount+1);	
		}*/
		
		
		// Eyad requirement : dont double the detailed points
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{
			gamePoints = 162;
		}
		else
		{
			gamePoints = 130;	
		}
		
		
		// storing detailed game points points.		
		if(GameApps.getTeamOnePlayers(gameBean).contains(lostPlayer))
		{			
			gameBean._roundBean.teamTwoGamePoints = gamePoints;			
		}
		else
		{
			gameBean._roundBean.teamOneGamePoints = gamePoints;
		}
		
		
		// adding bonus points.
		
		int team1Bonus = 0;
		int team2Bonus = 0;
		int team1BalootBonus = 0;
		int team2BalootBonus = 0;
		
		// Get the Bonus
		ArrayList<Integer> teamBonus = GameApps.getTeamBonusPoints(gameBean);
		ArrayList<Integer> balootBonus = GameApps.getBalootBonusPoints(gameBean);
		
		team1Bonus = teamBonus.get(0);
		team2Bonus = teamBonus.get(1);
		team1BalootBonus = balootBonus.get(0);
		team2BalootBonus = balootBonus.get(1);
		
		// As per Eyad requirement dont double the detailed bonus points.	
		Integer doubleCnt = gameBean._roundBean.doubleCount+1;
		if(gameBean._roundBean.isDouble)
		{
			team1Bonus = team1Bonus/doubleCnt;
			team2Bonus = team2Bonus/doubleCnt;
		}		
		
		if(GameApps.getTeamOnePlayers(gameBean).contains(gameBean._playersBeansList.get(1)._playerId))
		{
			gameBean._roundBean.teamOneBonusPoints = team2BalootBonus + team2Bonus;
			gameBean._roundBean.teamTwoBounsPoints = team1BalootBonus + team1Bonus;
		}
		else
		{
			gameBean._roundBean.teamOneBonusPoints = team1BalootBonus + team1Bonus;
			gameBean._roundBean.teamTwoBounsPoints = team2BalootBonus + team2Bonus;
		}
	}
}
