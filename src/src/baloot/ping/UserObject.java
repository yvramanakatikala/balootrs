package src.baloot.ping;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.Date;

import javax.swing.Timer;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

import src.baloot.constants.Constant;
import src.baloot.exce.beans.LogBean;
import src.baloot.listeners.BidActionListener;
import src.baloot.logs.UserLogs;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

public class UserObject implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String userName;
	private String tableId;
	private boolean isPlaying = false;
	private boolean isUserPingEnabled = true;
	private int pingCount = 0;
	
	private Timer loginMedalTimer;
	private Date lastUpdateTime = null;	
	int delayTime = 5*1000;	
	
	public void startLoginMedalTimer(String player, int medalId, int count)
	{
		stopLoginMedalTimer(player);			
			
		loginMedalTimer = new Timer(delayTime, new LoginMedalLister(player, medalId, count, this));
		loginMedalTimer.start();
		
		lastUpdateTime = Apps.getCurrentTime();
		
		String var = " StartLoginMedalTimer Started  delayTime "+delayTime;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", player, "StartLoginMedalTimer", var);
		
		UserLogs.addUserLog(lb, "NA");		    
	}
	
	public void stopLoginMedalTimer(String player)
	{
		if(loginMedalTimer != null)
		{			
			String var = " StopLoginMedalTimer delatyTime "+loginMedalTimer.getDelay()/1000+" runningTime "+Apps.getTimeDifference(lastUpdateTime);
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P0", player, "StopLoginMedalTimer", var);
			
			UserLogs.addUserLog(lb, "NA");
			
			try{ loginMedalTimer.stop();}catch(Exception e){e.printStackTrace();}			
			
			loginMedalTimer = null;
		}		
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	
	public boolean isPlaying() {
		return isPlaying;
	}
	public void setPlaying(boolean isPlaying) {
		this.isPlaying = isPlaying;
		
		if(!isPlaying)
		{
			tableId = null;
		}
	}
	public boolean isUserPingEnabled() {
		return isUserPingEnabled;
	}
	public void setUserPingEnabled(boolean isUserPingEnabled) {
		this.isUserPingEnabled = isUserPingEnabled;
	}
	public int getPingCount() {
		return pingCount;
	}
	public void setPingCount(int pingCount) {
		this.pingCount = pingCount;
	}
}


class LoginMedalLister implements ActionListener
{		
	String player;
	int medalId;
	int count;
	UserObject userObj;
	
	public LoginMedalLister(String player, int medalId, int count, UserObject userObj)
	{				
		this.player = player;
		this.medalId = medalId;
		this.count = count;
		this.userObj = userObj;
	}
	
	public void actionPerformed(ActionEvent e) 
	{
		userObj.stopLoginMedalTimer(player);		
		
        ISFSObject sfso = new SFSObject();	
		
		sfso.putInt("medalId", medalId);
		sfso.putInt("count", count);
		sfso.putUtfString("player", player);
		
		User user = Apps.getUserByName(player);
		
		if(user != null)
		{			
			Commands.appInstance.send(Commands.WON_MEDAL, sfso, user);
			
			String var = " LoginMedalSend  player "+player+" medalId "+medalId+" count "+count;
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P0", player, "LoginMedalSend", var);
			
			UserLogs.addUserLog(lb, "NA");
		}
		else
		{
			String var = " LoginMedalNotSend  player "+player+" medalId "+medalId
							+" count "+count+" userObj "+userObj;
			
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P0", player, "LoginMedalNotSend", var);
			
			UserLogs.addUserLog(lb, "NA");
		}		
	}
}

