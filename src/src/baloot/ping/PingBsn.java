/**
 * 
 */
package src.baloot.ping;

import java.io.Serializable;
import java.util.HashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import src.baloot.exce.Validations;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;

/**
 * @author Suuresh
 *
 */
public class PingBsn implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public HashMap<String, Long> connectedUsers = new HashMap<String, Long>();
	// Keeps a reference to the task execution
    public ScheduledFuture<?> taskHandle;
    public boolean isServerPingEnabled = true;
    public Boolean pauseTaskRunner = false;
	
	public void addConnectedUser(User user)
	{
		Apps.showLog(" AddConnectedUser ");
		if(isServerPingEnabled)
		{
			//add the user to connected user list
			if( taskHandle == null || taskHandle.isDone() || taskHandle.isCancelled())
			{
				if( taskHandle != null )
				{
					taskHandle.cancel(true);
				}
					
		       //Schedule the task to run every second, with no initial delay
				taskHandle = Commands.appInstance.sfs.getTaskScheduler().scheduleAtFixedRate(new TaskRunner(), 0, 2, TimeUnit.SECONDS);
			}
			
			if( connectedUsers.containsKey(user.getName()))
			{
				updateConnectedUserLatestTime(user);
			}
			else
			{
				Commands.appInstance.trace("Adding Connect User:"+user.getName());
				connectedUsers.put(user.getName(), System.currentTimeMillis());
			}
		}
	}
	
	public void updateConnectedUserLatestTime(User user)
	{
		if( isServerPingEnabled )
		{
			connectedUsers.put( user.getName(), System.currentTimeMillis() );
		}
	}
	
	public void removeConnectedUser(User user)
	{
		if( isServerPingEnabled )
		{
			pauseTaskRunner = false;
			Validations.disconnectedByServer(user);
			Commands.appInstance.getApi().disconnectUser(user);		
		}
	}
}
