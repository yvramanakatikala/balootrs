package src.baloot.ping;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;



public class TaskRunner implements Runnable
{     
    public void run()
    {  
    	if( !Commands.appInstance.pingBsn.pauseTaskRunner )
    	{	
	        Iterator<Entry<String, Long>> it = Commands.appInstance.pingBsn.connectedUsers.entrySet().iterator();
	        User userToRemove = null;
	        
	        while (it.hasNext()) 
	        {
	            long currentTime = System.currentTimeMillis();
	            Map.Entry<String, Long>  pairs = (Map.Entry<String, Long> )it.next();
	            
	            
	            User user = Commands.appInstance.getParentZone().getUserByName( (String)pairs.getKey() );
	            //System.out.println( (currentTime - (Long)( pairs.getValue())  )/1000 );
	           

	            if( ( currentTime - (Long)( pairs.getValue()) )/1000 > Commands.appInstance.gameCV.getServerPingTimeOut())
	            {
	            	it.remove();  // avoids a ConcurrentModificationException
	            	userToRemove = user;
	            }
	        }
	        
	        if( userToRemove != null )
	        {
	        	Commands.appInstance.pingBsn.pauseTaskRunner = true;
	        	
	        	UserObject userObj = Commands.appInstance.getUserObjs().get(userToRemove.getName());
	        	
	        	if(userObj != null && userObj.isUserPingEnabled())
	        	{
	        		Commands.appInstance.pingBsn.removeConnectedUser(userToRemove);
	        	}
	        	
	        	userToRemove = null;
	        }	       
    	}       
    }    
}