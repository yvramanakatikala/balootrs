package src.baloot.ping;

import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class UserPingHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User user, ISFSObject params) 
	{
		//Apps.showLog("********* UserPingHandler ********** "+user.getName());
		
		String player = user.getName();
		boolean isUserPingEnabled = false;
		 
		Commands.appInstance.pingBsn.updateConnectedUserLatestTime(user);
		
        UserObject userObj = Commands.appInstance.getUserObjs().get(player);
        
        try
        {
        	isUserPingEnabled = params.getBool("isUserPingEnabled");
        	
        }catch(Exception e)
        {
        	e.printStackTrace();
        }
        
        if(userObj != null)
        {
        	userObj.setUserPingEnabled(isUserPingEnabled);
        }
        
		if(isUserPingEnabled)
		{
			Commands.appInstance.send(Commands.SERVER_PING, params, user);			
		}
	}
	


	

	

}
