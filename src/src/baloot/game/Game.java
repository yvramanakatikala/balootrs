package src.baloot.game;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import src.baloot.exce.beans.ExcBean;
import src.baloot.exce.beans.LogBean;
import src.baloot.logs.GameLog;
import src.baloot.model.Bid;
import src.baloot.model.Bonus;
import src.baloot.model.Discard;
import src.baloot.model.JoinLeave;
import src.baloot.model.OccupySeat;
import src.baloot.model.Rematch;
import src.baloot.model.ReplaceUser;
import src.baloot.model.SyncData;
import src.baloot.utils.Apps;


public abstract class Game extends Log implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public String gameId = "G"+System.currentTimeMillis();
	public String tableId;
	public String gameStartedTimeForClient = Apps.getDateString();
	private int leaveTableUsersCount = 0;
	private int disconnectionCount = 0;
	private int aiPlayersCount = 0;
	private int autoPlayCount = 0;
	private Date gameStartedTime = Apps.getCurrentTime();
	private String gameStartedTimeString;
	private int wrongRoundNoCount = 0;
	private int notInTurnCount = 0;
	private int turnNotMatch = 0;
	
	public String logData;
	private boolean isGameLogFileCreated = false;
	private String gameLogFilePath;	
	private int ticket = 1000;
	public final Integer maxPlayers = 4;
	
	private final Discard discardInstance = new Discard();
	private final GameLog gameLogInstance = new GameLog();
	private final Bid bidInstance = new Bid();
	private final Bonus bonusInstance = new Bonus();
	private final JoinLeave joinLeaveInstance = new JoinLeave();
	private final Rematch rematchInstance = new Rematch();
	private final ReplaceUser replaceUserInstance = new ReplaceUser();
	private final SyncData syncDataInstance = new SyncData();
	private final OccupySeat occupySeatInstance = new OccupySeat();
	
	public ArrayList<String> leaveTablesUsers = new ArrayList<String>();
	public ArrayList<String> winners = new ArrayList<String>();
	public ArrayList<String> loosers = new ArrayList<String>();
	public ArrayList<String> disconnectUsers = new ArrayList<String>();
	public Boolean isRematchTimerStarted = false;
	
		
	public void incrementTicket()
	{
		ticket++;
	}	
	public int getTicket()
	{
		return ticket;
	}
	
	public GameLog getGameLogInstance()
	{
		return gameLogInstance;
	}
	
	public Bid getBidInstance()
	{
		return bidInstance;
	}
	
	public Bonus getBonusInstance(){
	
		return bonusInstance;
	}
	
	public JoinLeave getJoinLeaveInstance()
	{
		return joinLeaveInstance;
	}
	
	public int getLeaveTableUsersCount() {
		return leaveTableUsersCount;
	}

	public void setLeaveTableUsersCount(int leaveTableUsersCount) {
		this.leaveTableUsersCount = leaveTableUsersCount;
	}

	public int getDisconnectionCount() {
		return disconnectionCount;
	}

	public void setDisconnectionCount(int disconnectionCount) {
		this.disconnectionCount = disconnectionCount;
	}
	
	public int getAiPlayersCount() {
		return aiPlayersCount;
	}

	public void setAiPlayersCount(int aiPlayersCount) {
		this.aiPlayersCount = aiPlayersCount;
	}

	public int getAutoPlayCount() {
		return autoPlayCount;
	}

	public void setAutoPlayCount(int autoPlayCount) {
		this.autoPlayCount = autoPlayCount;
	}

	public Date getGameStartedTime() {
		return gameStartedTime;
	}

	public void setGameStartedTime(Date gameStartedTime) {
		this.gameStartedTime = gameStartedTime;
	}

	public String getGameStartedTimeString() {
		return gameStartedTimeString;
	}

	public void setGameStartedTimeString(String gameStartedTimeString) {
		this.gameStartedTimeString = gameStartedTimeString;
	}

	public int getWrongRoundNoCount() {
		return wrongRoundNoCount;
	}

	public void setWrongRoundNoCount(int wrongRoundNoCount) {
		this.wrongRoundNoCount = wrongRoundNoCount;
	}

	public int getNotInTurnCount() {
		return notInTurnCount;
	}

	public void setNotInTurnCount(int notInTurnCount) {
		this.notInTurnCount = notInTurnCount;
	}

	public int getTurnNotMatch() {
		return turnNotMatch;
	}

	public void setTurnNotMatch(int turnNotMatch) {
		this.turnNotMatch = turnNotMatch;
	}
	public boolean isGameLogFileCreated() {
		return isGameLogFileCreated;
	}

	public void setGameLogFileCreated(boolean isGameLogFileCreated) {
		this.isGameLogFileCreated = isGameLogFileCreated;
	}

	public String getGameLogInstanceFilePath() {
		return gameLogFilePath;
	}

	public void setGameLogFilePath(String gameLogFilePath) {
		this.gameLogFilePath = gameLogFilePath;
	}
	
	public synchronized void addLog(LogBean lb)
	{
		String curTime = Apps.getDateString();
	        
		addDataToLogData(ticket+" | "+ lb.getLogType()+" | "+lb.getPriority()+" | "+curTime+" | "+lb.getFrom()+" | "+lb.getPlayer()+" | "+lb.getAction() +" | "+lb.getVariables());
		lb = null;
	}
	
	public synchronized void addExc(ExcBean eb)
	{
		String curTime = Apps.getDateString();
		
		addDataToLogData(ticket+" | "+eb.getLogType()+" | "+eb.getPriority()+" | "+curTime+" | "+"server"+" | "+eb.getPlayer()+" | "+eb.getAction()+" | "+eb.getVariable());
		writeGameLog();
	}
	public synchronized void addDataToLogData(String str)
	{
		if(logData != null)
		{
			logData +="--";
			logData +=str;
		}
		else
		{
			logData = str;
		}
		
		// TODO : comment below line in main build.
		//writeGameLog();
	}
	
	public synchronized void writeGameLog()
	{
		try{
			
			if(logData != null)
			{
				writeLogs(this, logData);
			}		
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		logData = null;
	}
	
	public synchronized Discard getDiscardInstance()
	{
		return discardInstance;
	}
	public synchronized Rematch getRematchInstance()
	{
		return rematchInstance;
	}
	
	public synchronized ReplaceUser getReplaceUserInstance()
	{
		return replaceUserInstance;
	}
	
	public synchronized SyncData getSyncDataInstance()
	{
		return syncDataInstance;
	}
	
	public OccupySeat getOccupySeatInstance() {
		return occupySeatInstance;
	}
		
}
