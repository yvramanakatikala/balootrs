package src.baloot.game;


import java.io.BufferedWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

public class Log implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public static void writeLogs(Game game, String fileData)throws Exception
	{
		FileWriter fileWriter = null;
		BufferedWriter logWriter = null;
		
		if(!game.isGameLogFileCreated())
		{
			String logPath = Apps.getZonePath()+"/"+Apps.getMonth()+"/"+Apps.getDayString()+"/"+Apps.getHour()+"/"+game.gameId+".txt";
			Path path = Paths.get(logPath);
		
			Commands.appInstance.meLog.filePaths.put(game.gameId, logPath);
			
			File logFile = new File(logPath);
			
			Files.createDirectories(path.getParent());
			Files.createFile(path);				
			
			game.setGameLogFilePath(logPath);
			game.setGameLogFileCreated(true);
			
			fileWriter = new FileWriter(logFile, true); 				
			logWriter = new BufferedWriter(fileWriter);
		}
		else
		{
			File logFile = new File(game.getGameLogInstanceFilePath());
			
			fileWriter = new FileWriter(logFile, true); 				
			logWriter = new BufferedWriter(fileWriter); 
		}
		
		// writing the data into file.
		
		String []data = fileData.split("--");
		
		for(int i=0;i<data.length;i++)
		{
			logWriter.append(data[i]);
			logWriter.flush();
			logWriter.newLine();
		}
		
		logWriter.close();
		fileWriter.close();
	}
}
