package src.baloot.game;

import java.util.Date;

public abstract class Player {
	
		// for missing time
		private Date lastDisconnectedTime;
		private long gameMissingTime = 0;
		private Date leavingTime;
		
		private Date lastTurnMissedTime;
		private long turnMissedTime;
		
		private int noofMissedActions = 0;
		
		private int disconnectionCount = 0;
		private int rejoinsCount = 0;
		private int onlineMissedActions = 0;
		private int offlineMissedActions = 0;
		private int clientOnlineMissedActions = 0;
		private int rtRedCount = 0;
		private int fpsRedCount = 0;
		private int refresh = 0;
		private int roundsCount = 0;
		
		private int rejoinYesCount = 0;
		
		private boolean isOffLineGameMissedUser = false;

		public Date getLastDisconnectedTime() {
			return lastDisconnectedTime;
		}

		public void setLastDisconnectedTime(Date lastDisconnectedTime) {
			this.lastDisconnectedTime = lastDisconnectedTime;
		}

		public long getGameMissingTime() {
			return gameMissingTime;
		}

		public void setGameMissingTime(long gameMissingTime) {
			this.gameMissingTime = gameMissingTime;
		}

		public Date getLeavingTime() {
			return leavingTime;
		}

		public void setLeavingTime(Date leavingTime) {
			this.leavingTime = leavingTime;
		}

		public Date getLastTurnMissedTime() {
			return lastTurnMissedTime;
		}

		public void setLastTurnMissedTime(Date lastTurnMissedTime) {
			this.lastTurnMissedTime = lastTurnMissedTime;
		}

		public long getTurnMissedTime() {
			return turnMissedTime;
		}

		public void setTurnMissedTime(long turnMissedTime) {
			this.turnMissedTime = turnMissedTime;
		}

		public int getNoofMissedActions() {
			return noofMissedActions;
		}

		public void setNoofMissedActions(int noofMissedActions) {
			this.noofMissedActions = noofMissedActions;
		}

		public int getDisconnectionCount() {
			return disconnectionCount;
		}

		public void setDisconnectionCount(int disconnectionCount) {
			this.disconnectionCount = disconnectionCount;
		}

		public int getRejoinsCount() {
			return rejoinsCount;
		}

		public void setRejoinsCount(int rejoinsCount) {
			this.rejoinsCount = rejoinsCount;
		}

		public int getOnlineMissedActions() {
			return onlineMissedActions;
		}

		public void setOnlineMissedActions(int onlineMissedActions) {
			this.onlineMissedActions = onlineMissedActions;
		}

		public int getOfflineMissedActions() {
			return offlineMissedActions;
		}

		public void setOfflineMissedActions(int offlineMissedActions) {
			this.offlineMissedActions = offlineMissedActions;
		}

		public int getClientOnlineMissedActions() {
			return clientOnlineMissedActions;
		}

		public void setClientOnlineMissedActions(int clientOnlineMissedActions) {
			this.clientOnlineMissedActions = clientOnlineMissedActions;
		}

		public int getRtRedCount() {
			return rtRedCount;
		}

		public void setRtRedCount(int rtRedCount) {
			this.rtRedCount = rtRedCount;
		}

		public int getFpsRedCount() {
			return fpsRedCount;
		}

		public void setFpsRedCount(int fpsRedCount) {
			this.fpsRedCount = fpsRedCount;
		}

		public int getRefresh() {
			return refresh;
		}

		public void setRefresh(int refresh) {
			this.refresh = refresh;
		}

		public int getRoundsCount() {
			return roundsCount;
		}

		public void setRoundsCount(int roundsCount) {
			this.roundsCount = roundsCount;
		}

		public int getRejoinYesCount() {
			return rejoinYesCount;
		}

		public void setRejoinYesCount(int rejoinYesCount) {
			this.rejoinYesCount = rejoinYesCount;
		}

		public boolean isOffLineGameMissedUser() {
			return isOffLineGameMissedUser;
		}

		public void setOffLineGameMissedUser(boolean isOffLineGameMissedUser) {
			this.isOffLineGameMissedUser = isOffLineGameMissedUser;
		}
		
		

}
