package src.baloot.constants;

public class BeltType {
	
	public static final String WHITE = "White";
	public static final String YELLOW = "Yellow";
	public static final String ORANGE = "Orange";
	public static final String RED = "Red";
	public static final String GREEN = "Green";
	public static final String BLACK = "Black";
	public static final String SILVER = "Silver";
	public static final String GOLD = "Gold";
	public static final String FIRE = "Fire";
	public static final String VIOLET1 = "Violet";
}
