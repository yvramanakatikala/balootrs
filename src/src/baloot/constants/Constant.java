package src.baloot.constants;

import java.io.Serializable;

public class Constant implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public static int QUICK_START_TIME = 6;	
	
	public static int QUICK_START_TIME_FOR_PRIVATE_TABLE = 120;	
	
	public static int BIDDING_TIME = 7;
	
	public static int BIDDING_TIME_AFTER_GAMETYPE_SELECTION = 5;
	
	public static int SELECT_HOKOM_SUIT_TIME = 7;
	
	public static int DISCARD_TIME = 10;	
	
	public static int AI_ACT_TIME_FOR_BIDDING = 1;
	
	public static int AI_ACT_TIME_FOR_DISCARD = 1;
	
	public static int REMATCH_TIME = 15;
	
	public static int NEXT_ROUND_START_TIME = 8;
	
	public static int GAME_END_POINTS = 152;
	
	public static int FAKE_GAME_INIT_TIME = 20;
	
	public static final int FREE_GAMES_COUNT = 4;
	
	
	public static final String GAME_GROUP = "GameGroup";
	public static final String LOBBY_GROUP = "LobbyGroup";
	public static final String ADMIN_GROUP = "AdminGroup";
	public static final String LOBBY = "Lobby";
	// Log Types
	public static final String GAME_LOG = "GAME_LOG";
	public static final String SERVER_LOG = "SERVER_LOG";
	public static final String CLIENT_LOG = "CLIENT_LOG";	
	
	// Server Client Logs
	public static final String WAR = "War";
	public static final String INF = "Inf";
	public static final String ERR = "Err";
	public static final String EXC = "Exc";
	
	// For gamelogs
	public static final String LOG = "Log";
	
	public static final String ACTIVATED = "Activated";
	public static final String PUBLIC = "Public";	
	public static final String PRIVATE = "Private";
	public static final String SINGLE_PLAYER = "SinglePlayer";	
	public static final String TRUE = "true";	
	public static final String FALSE = "false";
	
	public static final String RUNNING = "Running";
	
	public static final String CLIENT = "client";
	public static final String SERVER = "server";
	public static final String SYSTEM = "system";
	
	public static final String BUDDY_LIST = "BuddyList";
	public static final String ONLINE_BUDDY_LIST = "OnlineBuddyList";
	public static final String ALL_PLAYERS = "AllPlayers";
	public static final String ONLINE_PLAYERS = "OnlinePlayers";
	public static final String TOP_PLAYERS = "TopPlayers";
	
	public static final String MALE = "male";
	public static final String FE_MALE = "female";
	
	public static final String LOST = "lost";
	public static final String WON = "won";
	
	public static final String GAME_SOUND = "GameSound";
	public static final String BACK_GROUND_SOUND = "BackGroundSound";
			
}
