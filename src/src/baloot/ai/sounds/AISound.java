package src.baloot.ai.sounds;

import java.util.ArrayList;

import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerBean;
import src.baloot.beans.PlayerRoundBean;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;
import src.baloot.utils.GameApps;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class AISound {	
	
	public static void sendSound(GameBean gameBean, Room room, String player, String soundType, String command)
	{
		
		if(player == null)
		{
			player = "NA";
		}
		
		ISFSObject sfso = new SFSObject();
		
		sfso.putUtfString("soundType", soundType);
		sfso.putUtfString("player", player);
		Commands.appInstance.send(Commands.AI_SOUND, sfso, room.getUserList());
		gameBean.getGameLogInstance().aiSound(gameBean, sfso, command);
	}
	
	public static void sendTrickCompletedSound(GameBean gameBean, Room room, String trickWinner, int card)
	{
		String soundType = SoundType.NO_SOUND;
		PlayerBean pb = gameBean.getPlayerBean(trickWinner);
		
		if(trickWinner != null)
		{
			//if(isAISoundTime(gameBean, trickWinner))
			if(Apps.isAI(trickWinner))
			{
				if(gameBean._roundBean._gameType.equals(Commands.HOKOM) && gameBean._roundBean._trumpSuitType.equals(Apps.getSuit(card)))
				{
					soundType = SoundType.TRICK_COMPLETED_BY_HOKOM_CARD_SOUND;
				}
				else
				{
					soundType = SoundType.TRICK_COMPLETED_SOUND;
				}
				
				if(pb != null)
				{
					//Integer count = pb.aiSoundCount.get(SoundType.TRICK_COMPLETED_SOUND);
					Integer count = gameBean.aiRoundBasedSoundCount.get(SoundType.TRICK_COMPLETED_SOUND);
					
					if(count == null || count == 0)
					{
						gameBean.trickCompletedSoundPlayedCount++;
						soundType = soundType+"_"+gameBean.trickCompletedSoundPlayedCount%6;
						//pb.aiSoundCount.put(SoundType.TRICK_COMPLETED_SOUND, 1);
						gameBean.aiRoundBasedSoundCount.put(SoundType.TRICK_COMPLETED_SOUND, 1);
						sendSound(gameBean, room, trickWinner, soundType, Commands.SAN);
					}
					else if(count == 1)
					{
						//pb.aiSoundCount.put(SoundType.TRICK_COMPLETED_SOUND, 2);
						gameBean.aiRoundBasedSoundCount.put(SoundType.TRICK_COMPLETED_SOUND, 2);
					}
					else if(count == 2)
					{
						//gameBean.trickCompletedSoundPlayedCount++;
						//soundType = soundType+"_"+gameBean.trickCompletedSoundPlayedCount%6;
						//pb.aiSoundCount.put(SoundType.TRICK_COMPLETED_SOUND, 3);
						gameBean.aiRoundBasedSoundCount.put(SoundType.TRICK_COMPLETED_SOUND, 3);
						//sendSound(gameBean, room, trickWinner, soundType, Commands.SAN);
					}
					else if(count == 3 && gameBean._roundBean.trickCount != 8)
					{
						//pb.aiSoundCount.put(SoundType.TRICK_COMPLETED_SOUND, 0);
						gameBean.aiRoundBasedSoundCount.put(SoundType.TRICK_COMPLETED_SOUND, 0);
					}
				}
				
			}
		}	
						
	}
	
	public static void sendSanSelectSound(GameBean gameBean, Room room, String player)
	{
		String soundType = SoundType.NO_SOUND;
		PlayerBean pb = gameBean.getPlayerBean(player);
		
		
		if(isAISoundTime(gameBean, player))				
		{
			if(isOtherTeamSelectHOKOM(gameBean, player))
			{
				soundType =  SoundType.HOKOM_TO_SAN_SOUND;
				
				
				// checking count...				
				if(pb != null)
				{
					Integer count = pb.aiSoundCount.get(SoundType.HOKOM_TO_SAN_SOUND);
					
					if(count == null || count == 0)
					{
						pb.aiSoundCount.put(SoundType.HOKOM_TO_SAN_SOUND, 1);
						sendSound(gameBean, room, gameBean.getPartner(player), soundType, Commands.SAN);
					}
					else if(count == 1)
					{
						pb.aiSoundCount.put(SoundType.HOKOM_TO_SAN_SOUND, 0);						
					}
				}
			}
			else
			{
				soundType =  SoundType.SAN_SELECTED_SOUND;
				
				
				// checking count...				
				if(pb != null)
				{
					Integer count = pb.aiSoundCount.get(SoundType.SAN_SELECTED_SOUND);
					
					if(count == null || count == 0)
					{
						pb.aiSoundCount.put(SoundType.SAN_SELECTED_SOUND, 1);
						
						sendSound(gameBean, room, gameBean.getPartner(player), soundType, Commands.SAN);
					}
					else if(count == 1)
					{
						pb.aiSoundCount.put(SoundType.SAN_SELECTED_SOUND, 2);						
					}
					else if(count == 2)
					{
						pb.aiSoundCount.put(SoundType.SAN_SELECTED_SOUND, 0);					
					}
				}
			}			
		}		
	}
	
	public static void sendBonusSound(GameBean gameBean, Room room, String player, String bonusType)
	{
		String soundType = SoundType.NO_SOUND;
		PlayerBean pb = gameBean.getPlayerBean(player);
		
		
		if(isAISoundTime(gameBean, player))	
		{
			if(bonusType.equals("sira"))
			{
				soundType = SoundType.BONUS_SIRA_SOUND;
				
				// checking count...				
				if(pb != null)
				{
					Integer count = pb.aiSoundCount.get(SoundType.BONUS_SIRA_SOUND);
					
					if(count == null || count == 0)
					{
						pb.aiSoundCount.put(SoundType.BONUS_SIRA_SOUND, 1);
						
						sendSound(gameBean, room, gameBean.getPartner(player), soundType, Commands.SAN);
					}
					else if(count == 1)
					{
						pb.aiSoundCount.put(SoundType.BONUS_SIRA_SOUND, 2);						
					}
					else if(count == 2)
					{
						pb.aiSoundCount.put(SoundType.BONUS_SIRA_SOUND, 0);						
					}
				}
				
			}
			else if(bonusType.equals("50"))
			{
				soundType = SoundType.BONUS_50_SOUND;
				
				
				// checking count...				
				if(pb != null)
				{
					Integer count = pb.aiSoundCount.get(SoundType.BONUS_50_SOUND);
					
					if(count == null || count == 0)
					{
						pb.aiSoundCount.put(SoundType.BONUS_50_SOUND, 1);
												
						sendSound(gameBean, room, gameBean.getPartner(player), soundType, Commands.SAN);
					}
					else if(count == 1)
					{
						pb.aiSoundCount.put(SoundType.BONUS_50_SOUND, 2);	
												
						
						sendSound(gameBean, room, gameBean.getPartner(player), soundType, Commands.SAN);
					}
					else if(count == 2)
					{
						pb.aiSoundCount.put(SoundType.BONUS_50_SOUND, 0);						
					}
				}
			}
			else if(bonusType.equals("100"))
			{
				soundType = SoundType.BONUS_100_SOUND; 		
								
				sendSound(gameBean, room, gameBean.getPartner(player), soundType, Commands.Bonus);
			}
			else if(bonusType.equals("400"))
			{
				soundType = SoundType.BONUS_400_SOUND;
								
				sendSound(gameBean, room, gameBean.getPartner(player), soundType, Commands.Bonus);
			}			
		}		
	}
	
	
	public static void sendGameCompletedSound(GameBean gameBean, Room room)
	{
		String soundType = SoundType.GAME_COMPLETED_SOUND;		
				
		sendSound(gameBean, room, "NA", soundType, Commands.GAME_COMPLETED);
	}
	
	public static void roundCompletedSound(GameBean gameBean, Room room)
	{
		String soundType = SoundType.NO_SOUND;		
		boolean isSoundPlayed = false;
		
		if(isEightTricksWonBySameTeam(gameBean))
		{
			if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
			{
				soundType = SoundType.ALL_TRICKS_WON_IN_HOKOM;
			}
			else if(gameBean._roundBean._gameType.equals(Commands.SAN))
			{
				soundType = SoundType.ALL_TRICKS_WON_IN_SAN;
			}
			
			if(soundType.equals(SoundType.ALL_TRICKS_WON_IN_HOKOM) || soundType.equals(SoundType.ALL_TRICKS_WON_IN_SAN))
			{
				isSoundPlayed = true;
				
				ArrayList<String> winners = getEightTricksWonTeam(gameBean);
				
				if(winners.size() == 2)
				{
					if(Apps.isAI(winners.get(0)) || Apps.isAI(winners.get(1)))
					{
						String player = "NA";
						
						if(Apps.isAI(winners.get(0)))
						{
							player = winners.get(0);
						}
						else
						{
							player = winners.get(1);
						}
												
						sendSound(gameBean, room, player, soundType, Commands.ROUND_COMPLETED);
					}
				}
				
			}			
		}
		
		
		if(Apps.isAI(gameBean.getPartner(gameBean._roundBean._bidWonPerson)))
		{
			if(!GameApps.getCurrentRoundHighestScoreTeam(gameBean).contains(gameBean._roundBean._bidWonPerson))
			{
				soundType = SoundType.TEAMMATE_MAKE_DECISION_AND_LOST;
				
				PlayerBean bidWonBean = gameBean.getPlayerBean(gameBean._roundBean._bidWonPerson);
				PlayerBean aiBean = gameBean.getPlayerBean(gameBean.getPartner(gameBean._roundBean._bidWonPerson));
				
				if(bidWonBean != null && aiBean != null)
				{
					bidWonBean.teammateWonBidAndLostRoundCount++;
					aiBean.teammateWonBidAndLostRoundCount++;
					
					if(bidWonBean.teammateWonBidAndLostRoundCount == 3 || aiBean.teammateWonBidAndLostRoundCount == 3)
					{
						soundType = SoundType.TEAMMATE_MAKE_DECISION_AND_LOST_3_TIMES;
					}
					
					if(bidWonBean.teammateWonBidAndLostRoundCount == 4 || aiBean.teammateWonBidAndLostRoundCount == 4)
					{
						soundType = SoundType.TEAMMATE_MAKE_DECISION_AND_LOST_4_TIMES;
					}				
					
					sendSound(gameBean, room, gameBean.getPartner(gameBean._roundBean._bidWonPerson), soundType, Commands.ROUND_COMPLETED);
				}				
				
				isSoundPlayed = true;
			}
		}
		
		if(isBothTeamsScoresAbove100(gameBean))
		{
			soundType = SoundType.BOTH_TEAMS_SCORE_ABOVE_100;
						
			ArrayList<String> teamOne = GameApps.getTeamOnePlayers(gameBean);
			ArrayList<String> teamTwo = GameApps.getTeamTwoPlayers(gameBean);
			
			if(teamOne.size() == 2)
			{
				if(Apps.isAI(teamOne.get(0)) || Apps.isAI(teamOne.get(1)))
				{
					String player = "NA";
					
					if(Apps.isAI(teamOne.get(0)))
					{
						player = teamOne.get(0);
					}
					else
					{
						player = teamOne.get(1);
					}
										
					sendSound(gameBean, room, player, soundType, Commands.ROUND_COMPLETED);
				}
			}
			
			if(teamTwo.size() == 2)
			{
				if(Apps.isAI(teamTwo.get(0)) || Apps.isAI(teamTwo.get(1)))
				{
					String player = "NA";
					
					if(Apps.isAI(teamTwo.get(0)))
					{
						player = teamTwo.get(0);
					}
					else
					{
						player = teamTwo.get(1);
					}
										
					sendSound(gameBean, room, player, soundType, Commands.ROUND_COMPLETED);
				}
			}
			
			
			isSoundPlayed = true;
		}
		
		if(isScoreTooLow(gameBean))
		{
			soundType = SoundType.SCORE_LESS_THAN_60_AND_OTHER_SCORE_GREATER_THAN_120;
			isSoundPlayed = true;			
			
			ArrayList<String> lowScoreTeam = getLowScoreTeam(gameBean);
			
			if(lowScoreTeam.size() == 2)
			{
				if(Apps.isAI(lowScoreTeam.get(0)) || Apps.isAI(lowScoreTeam.get(1)))
				{
					String player = "NA";
					
					if(Apps.isAI(lowScoreTeam.get(0)))
					{
						player = lowScoreTeam.get(0);
					}
					else
					{
						player = lowScoreTeam.get(1);
					}
										
					sendSound(gameBean, room, player, soundType, Commands.ROUND_COMPLETED);
				}
			}
		}		
	}
	
	public static void doubleSound(GameBean gameBean, Room room, String player)
	{
		String soundType = SoundType.NO_SOUND;	
		String aiPlayer = "NA";
		
		ArrayList<String> team = new ArrayList<String>();
		
		if(GameApps.getTeamOnePlayers(gameBean).contains(player))
		{
			team = GameApps.getTeamTwoPlayers(gameBean);
		}		
		else
		{
			team = GameApps.getTeamOnePlayers(gameBean);
		}
		
		if(team.size() > 0 && Apps.isAI(team.get(0)))
		{
			soundType = SoundType.OPPONENT_MAKE_DOUBLE;	
			
			aiPlayer = team.get(0);
		}
		
		if(team.size() > 1 && Apps.isAI(team.get(1)))
		{
			soundType = SoundType.OPPONENT_MAKE_DOUBLE;	
			
			aiPlayer = team.get(1);
		}
				
		sendSound(gameBean, room, aiPlayer, soundType, Commands.DOUBLE);
	}
	
	public static void checkPlayerWonTrickInSeries(GameBean gameBean, Room room, String player)
	{
		String soundType = SoundType.NO_SOUND;
		
		if(gameBean._roundBean.trickCount == 0)
		{
			PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(player);
			
			if(prb != null)
			{
				prb.continuousTrickWonCount++;
				gameBean._roundBean.preTrickWinner = player;
			}
		}
		else if(gameBean._roundBean.isAnyPlayerWonFourTricksInSeries)
		{
			gameBean._roundBean.isAnyPlayerWonFourTricksInSeries = false;
			
			PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(player);
			
			if(prb != null)
			{
				prb.continuousTrickWonCount++;
				gameBean._roundBean.preTrickWinner = player;
			}
		}
		else
		{
			if(player.equals(gameBean._roundBean.preTrickWinner))
			{
				PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(player);
				
				if(prb != null)
				{
					prb.continuousTrickWonCount++;
					
					
					if(prb.continuousTrickWonCount == 3)
					{
						prb.continuousTrickWonCount = 0;
						gameBean._roundBean.isAnyPlayerWonFourTricksInSeries = true;
						/*if((Apps.isAI(player) && Apps.isAI(gameBean.getPartner(player)))
								|| (!Apps.isAI(player) && Apps.isAI(gameBean.getPartner(player))))	*/
						
						if(Apps.isAI(player))
						{							
							soundType = SoundType.FOUR_TRICKS_WON_IN_SERIES;							
							
							sendSound(gameBean, room, player, soundType, Commands.TRICK_COMPLETED);
						}
					}										
				}				
			}
			else
			{
				gameBean._roundBean.preTrickWinner = player;
				
				for(PlayerRoundBean prb : gameBean._roundBean._playerRoundBeanObjs)
				{
					prb.continuousTrickWonCount = 0;
				}
			}
		}
	}
	
	public static void joinOnAISound(GameBean gameBean, Room room, String player)
	{
		String soundType = SoundType.NO_SOUND;		
		
		if(isAISoundTime(gameBean, player))	
		{
			
			soundType = SoundType.JOIN_ON_AI_SOUND;
						
			sendSound(gameBean, room, gameBean.getPartner(player), soundType, SoundType.JOIN_ON_AI_SOUND);
		}
	}
	
	public static void show400BonusSound(GameBean gameBean, String bonusType, String player)
	{
		Room room = Apps.getRoomByName(gameBean.tableId);
		String soundType = SoundType.NO_SOUND;
		String aiPlayer = "NA";
		
		ArrayList<String> team = new ArrayList<String>();
		
		if(GameApps.getTeamOnePlayers(gameBean).contains(player))
		{
			team = GameApps.getTeamTwoPlayers(gameBean);
		}		
		else
		{
			team = GameApps.getTeamOnePlayers(gameBean);
		}
		
		if(team.size() > 0 && Apps.isAI(team.get(0)))
		{
			soundType = SoundType.SHOW_400_BONUS_SOUND;	
			
			aiPlayer = team.get(0);
			
		}
		
		if(team.size() > 1 && Apps.isAI(team.get(1)))
		{
			soundType = SoundType.SHOW_400_BONUS_SOUND;	
			
			aiPlayer = team.get(1);
		}		
		
		sendSound(gameBean, room, aiPlayer, soundType, Commands.Bonus);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static boolean isScoreTooLow(GameBean gameBean)
	{
		if((getTeamOneScore(gameBean) < 60 || getTeamTwoScore(gameBean) < 60) 
				&& (getTeamOneScore(gameBean) > 120 || getTeamTwoScore(gameBean) > 120))
		{
			return true;
		}
		
		return false;
	}	
	
	public static ArrayList<String> getLowScoreTeam(GameBean gameBean)
	{
		if(getTeamOneScore(gameBean) < 60 )
		{
			return GameApps.getTeamOnePlayers(gameBean);
		}
		else
		{
			return GameApps.getTeamTwoPlayers(gameBean);
		}
	}
	
	public static boolean isBothTeamsScoresAbove100(GameBean gameBean)
	{
		int one = getTeamOneScore(gameBean);
		
		int two = getTeamTwoScore(gameBean);
		
		if(one > 100 && two > 100)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static int getTeamOneScore(GameBean gameBean)
	{
		return GameApps.getTeamOnecurrentScore(gameBean)+GameApps.getTeamOnePreviousScore(gameBean);
	}
	
	public static int getTeamTwoScore(GameBean gameBean)
	{
		return GameApps.getTeamTwocurrentScore(gameBean)+GameApps.getTeamTwoPreviousScore(gameBean);
	}
	
	
	
	public static boolean isEightTricksWonBySameTeam(GameBean gameBean)
	{
		
		if(GameApps.getTeamOneTricksWonCount(gameBean) > 7 || GameApps.getTeamTwoTricksWonCount(gameBean) > 7)
		{
			return true;
		}	
		
		return false;
	}
	
	public static ArrayList<String> getEightTricksWonTeam(GameBean gameBean)
	{
		if(GameApps.getTeamOneTricksWonCount(gameBean) > 7)
		{
			return GameApps.getTeamOnePlayers(gameBean);
		}
		else
		{
			return GameApps.getTeamTwoPlayers(gameBean);
		}		
	}
	
	
	public static boolean isOtherTeamSelectHOKOM(GameBean gameBean, String player)
	{
		if(gameBean._roundBean.isGameTypeSelected && gameBean._roundBean.bidSelected.equals(Commands.HOKOM))
		{
			if(!gameBean.getPartner(player).equals(gameBean._roundBean._bidSelectedPerson))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean isAISoundTime(GameBean gameBean, String player)
	{
		if((Apps.isAI(player) && Apps.isAI(gameBean.getPartner(player)))
				|| (!Apps.isAI(player) && Apps.isAI(gameBean.getPartner(player))))	
		{
			return true;
		}
		
		return false;
	}
}
