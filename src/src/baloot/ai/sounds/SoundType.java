package src.baloot.ai.sounds;

public class SoundType {
	
	public static final String NO_SOUND = "NoSound";
	
	public static final String TRICK_COMPLETED_SOUND = "TrickCompletedSound";
	
	public static final String TRICK_COMPLETED_BY_HOKOM_CARD_SOUND = "TrickCompletedByHokomCardSound";
	
	public static final String SAN_SELECTED_SOUND = "SanSelectedSound";
	
	public static final String HOKOM_TO_SAN_SOUND = "HokomToSanSound";
	
	public static final String BONUS_SIRA_SOUND = "BonusSiraSound";
	
	public static final String BONUS_50_SOUND = "Bonus50Sound";
	
	public static final String BONUS_100_SOUND = "Bonus100Sound";
	
	public static final String BONUS_400_SOUND = "Bonus400Sound";
	
	public static final String GAME_COMPLETED_SOUND = "GameCompletedSound";
	
	public static final String TEAMMATE_MAKE_DECISION_AND_LOST_3_TIMES = "TeammateMakeDecisionAndLost3Times";
	
	public static final String TEAMMATE_MAKE_DECISION_AND_LOST_4_TIMES = "TeammateMakeDecisionAndLost4Times";
	
	public static final String TEAMMATE_MAKE_DECISION_AND_LOST = "TeammateMakeDecisionAndLost";
	
	public static final String 	ALL_TRICKS_WON_IN_SAN = "AllTricksWonInSan";
	
	public static final String ALL_TRICKS_WON_IN_HOKOM = "AllTricksWonInHokom";
	
	public static final String OPPONENT_MAKE_DOUBLE = "OpponentMakeDouble"; 
	
	public static final String FOUR_TRICKS_WON_IN_SERIES = "FourTricksWonInSeries";
	
	public static final String BOTH_TEAMS_SCORE_ABOVE_100 = "BothTeamsScoreAbove100";
	
	public static final String JOIN_ON_AI_SOUND = "JoinOnAiSound";
	
	public static final String SCORE_LESS_THAN_60_AND_OTHER_SCORE_GREATER_THAN_120 = "ScoreLessThan60AndOtherScoreGreaterThan120";
	
	public static final String SHOW_400_BONUS_SOUND = "Show400BonusSound";
	
}
