/**
 * 
 */
package src.baloot.classes;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

/**
 * @author MSKumar
 *
 */
public class DataClass {

	public ArrayList<ArrayList<String>> sunObjects = new ArrayList<ArrayList<String>>();
	public ArrayList<ArrayList<String>> hokomObjects = new ArrayList<ArrayList<String>>();
	
	
	/** Constructer */
	
	public DataClass(){
		sunObjects = readFile(Commands.SAN);
		hokomObjects = readFile(Commands.HOKOM);
	}
	
	private static ArrayList<ArrayList<String>> readFile(String filename){
		

		ArrayList<ArrayList<String>> objects = new ArrayList<ArrayList<String>>();        
		
		try {
			 
			File fXmlFile = new File("src/baloot/xml/"+filename+".xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
	 
			Apps.showLog("Root element :" + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("case");
			Apps.showLog("-----------------------");
	 
			for (int temp = 0; temp < nList.getLength(); temp++) {
	 
			   Node nNode = nList.item(temp);
			   if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	 
			      Element eElement = (Element) nNode;
			      ArrayList<String> obj = new ArrayList<String>();
			      
			     // AppMethods.showLog("Combination 1: " + getTagValue("combination1", eElement));
			     // AppMethods.showLog("Combination 2: " + getTagValue("combination2", eElement));
		         // AppMethods.showLog("Combination 3: " + getTagValue("combination3", eElement));
			     // AppMethods.showLog("Combination 4: " + getTagValue("combination4", eElement));
			     // AppMethods.showLog("Combination 5: " + getTagValue("combination5", eElement));
		         // AppMethods.showLog("Combination 6: " + getTagValue("combination6", eElement));
			     // AppMethods.showLog("Percentage   : " + getTagValue("per", eElement));
			      obj.add(getTagValue("combination1", eElement));
			      obj.add(getTagValue("combination2", eElement));
			      obj.add(getTagValue("combination3", eElement));
			      obj.add(getTagValue("combination4", eElement));
			      obj.add(getTagValue("combination5", eElement));
			      obj.add(getTagValue("combination6", eElement));
			      obj.add(getTagValue("per", eElement));
			      objects.add(obj);      
			     
			   }
			   
			}
		  } catch (Exception e) {
			e.printStackTrace();
		  }
		  
		  
		  return objects;
		//  AppMethods.showLog(objects);
	}

	private static String getTagValue(String sTag, Element eElement) {
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes(); 
        Node nValue = (Node) nlList.item(0); 
        return nValue.getNodeValue();
  }
	
	
	
}
