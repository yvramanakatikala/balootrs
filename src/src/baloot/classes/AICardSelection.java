/**
 * 
 */
package src.baloot.classes;

import java.util.ArrayList;
import java.util.Random;

import src.baloot.beans.GameBean;
import src.baloot.beans.MessageBean;
import src.baloot.beans.PlayedCard;
import src.baloot.beans.PlayerRoundBean;
import src.baloot.bsn.AIBsn;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

public class AICardSelection
{
	public Integer playAIHokom(GameBean gameBean, String player)
	{
		showLog("**************** Play Hokom Player"+ player +"***********");
		Integer card = 0;	
		
		//showLog(player);
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		
		if(prbObj._cards.size() == 1)
		{
			card = prbObj._cards.get(0);
		}
		else if(gameBean._roundBean.openCardsList.size() == 0)
		{
			// Hokom Playing Mode
			showLog("Hokom Playing mode");
			if(checkPlayerMadeHokomDecision(gameBean, player))
			{
				showLog("Person won bid");
				//showLog("c"+checkAnyHokomSuitCards(player));
				if(checkAnyHokomSuitCards(gameBean, player))
				{
					// Check all the cards are hokom cards
					if(checkAllCardsAreHokomCards(gameBean, player))
					{
						ArrayList<Integer> suitCards = new ArrayList<Integer>();
						suitCards = getRoundSuitCards(prbObj._cards, gameBean._roundBean._trumpSuitType);
						showLog("All cards are hokom cards");
						if(haveStrongestCard(gameBean, gameBean._roundBean._trumpSuitType, suitCards, Commands.HOKOM))
						{
							// having strongest card // play strongest
							prbObj._cards = Commands.appInstance.ai.sortHokomorder(prbObj._cards);
							card = prbObj._cards.get(0);
						}
						else
						{
							// not having strongest card // play weakest
							prbObj._cards = Commands.appInstance.ai.sortHokomorder(prbObj._cards);
							card = prbObj._cards.get(prbObj._cards.size()-1);						
						}
					}
					else
					{
						showLog("All cards are not hokom cards");
						
						if(gameBean._roundBean._doubleType.equals(Commands.CLOSED_DOUBLE))
						{
							// Play H80
							card = playAIForHokomH80(gameBean, player);
						}
						else
						{
							// Check the no of hokom cards in hand +
							// The cards are played in previous round +
							// the cards you know with team mate  == 8
							ArrayList<Integer> cardsinhand = getRoundSuitCards(prbObj._cards, gameBean._roundBean._trumpSuitType);
							ArrayList<Integer> cards = getRoundSuitCards(gameBean._roundBean._cardsPlayed, gameBean._roundBean._trumpSuitType);
							ArrayList<Integer> knowncards = getRoundSuitCards(getTeammateKnownCards(gameBean,player), gameBean._roundBean._trumpSuitType);
							
							ArrayList<Integer> allcards = cardsinhand;
							
							
							
							for(int i=0;i<cards.size();i++)
							{
								boolean sign =true;
								for(int j=0;j<allcards.size();j++)
								{
									if(cards.get(i) == allcards.get(j))
										sign = false;
								}
								if(sign)
									allcards.add(cards.get(i));
							}
							for(int i=0;i<knowncards.size();i++)
							{
								boolean sign =true;
								for(int j=0;j<allcards.size();j++)
								{
									if(knowncards.get(i) == allcards.get(j))
										sign = false;
								}
								if(sign)
									allcards.add(knowncards.get(i));
							}
							
							
							
							if( allcards.size() == 8)
							{
								// H 80
								showLog("Know all the hokom cards");
								card = playAIForHokomH80(gameBean, player);
							}
							else
							{
								showLog("Not Know all the hokom cards");
								// Check p2 and p4 did not played hokom suit cards
								if(checkOpponentNotPlayedHokomSuitCardsInLastHokomSuitRound(gameBean, player))
								{
									// Not played hokom 
									// H 80
									showLog("Opponent not played hokom cards in last hokom");
									card = playAIForHokomH80(gameBean, player);
								}
								else
								{
									// Played the hokom suit cards
									showLog("Opponent played hokom cards");
									// Check Do u have any strongest hokom suit card
									ArrayList<Integer> suitcards = getRoundSuitCards(prbObj._cards, gameBean._roundBean._trumpSuitType);
									if(haveStrongestCard(gameBean,gameBean._roundBean._trumpSuitType, suitcards, Commands.HOKOM))
									{
										
										showLog("**************** Having strongest card***********");
										// Have Strongest card								
										suitcards = Commands.appInstance.ai.sortHokomorder(suitcards);
										card = suitcards.get(0);
									}
									else
									{
										showLog("**************** Not Having strongest card***********");
										suitcards = Commands.appInstance.ai.sortHokomorder(suitcards);
										if(suitcards.size()>=2)
										{
											showLog("**************** Not Having strongest card play weakest***********");
											// Play weakest card
											card = suitcards.get(suitcards.size()-1);
											
											//showLog("card "+card);
										}
										else
										{
											showLog("Check U R TM having strongest Hokom suit card");
											// Check U R TM having strongest Hokom suit card 
											ArrayList<Integer> teamcards = getTeammateKnownCards(gameBean, player);
											int no = getStrongestCard(gameBean, gameBean._roundBean._trumpSuitType, Commands.HOKOM);
											boolean sign = false;
											for(int i=0;i<teamcards.size();i++)
											{
												if(teamcards.get(i) == no)
												{
													sign =true;
													break;
												}
											}
											if(sign)
											{
												// Play weakest card
												suitcards = Commands.appInstance.ai.sortHokomorder(suitcards);
												card = suitcards.get(suitcards.size()-1);
											}
											else
											{
												// H 80
												card = playAIForHokomH80(gameBean, player);
											}
										}
									}
								}
							}
						}
					}
				}
				else
				{
					// H 80
					card = playAIForHokomH80(gameBean, player);
				}
			}
			else 
			{
				showLog("Player did not won bid check for teammate");
				// Check u r teammate won bid
				if(checkPlayerMadeHokomDecision(gameBean, getPartner(gameBean, player)))
				{
					// team mate won the bid
					showLog("Team mate won bid");
					if(checkAnyHokomSuitCards(gameBean, player))
					{
						if(checkAllCardsAreHokomCards(gameBean, player))
						{
							
							// All cards are hokom cards
							showLog("All cards are hokom cards");
							ArrayList<Integer> suitcards = getRoundSuitCards(prbObj._cards, gameBean._roundBean._trumpSuitType);
							if(haveStrongestCard(gameBean,gameBean._roundBean._trumpSuitType, suitcards, Commands.HOKOM))							
							{
								// having strongest card // play strongest
								prbObj._cards = Commands.appInstance.ai.sortHokomorder(prbObj._cards);
								card = prbObj._cards.get(0);
							}
							else
							{
								// not having strongest card // play weakest
								prbObj._cards = Commands.appInstance.ai.sortHokomorder(prbObj._cards);
								card = prbObj._cards.get(prbObj._cards.size()-1);						
							}
						}
						else
						{							
							if(gameBean._roundBean._doubleType.equals(Commands.CLOSED_DOUBLE))
							{
								card = playAIForHokomH80(gameBean, player);
							}
							else
							{							
								// Not all cards are hokom cards
								ArrayList<Integer> cardsinhand = getRoundSuitCards(prbObj._cards, gameBean._roundBean._trumpSuitType);
								ArrayList<Integer> cards = getRoundSuitCards(gameBean._roundBean._cardsPlayed, gameBean._roundBean._trumpSuitType);
								ArrayList<Integer> knowncards = getRoundSuitCards(getTeammateKnownCards(gameBean, player), gameBean._roundBean._trumpSuitType);
								
								showLog("Cards In Hand"+ cardsinhand.toString());
								
								ArrayList<Integer> suitcards = cardsinhand;
								
								for(int i=0;i<cards.size();i++)
								{
									boolean sign =true;
									for(int j=0;j<suitcards.size();j++)
									{
										if(cards.get(i) == suitcards.get(j))
											sign = false;
									}
									if(sign)
										suitcards.add(cards.get(i));
								}
								for(int i=0;i<knowncards.size();i++)
								{
									boolean sign =true;
									for(int j=0;j<suitcards.size();j++)
									{
										if(knowncards.get(i) == suitcards.get(j))
											sign = false;
									}
									if(sign)
										suitcards.add(knowncards.get(i));
								}
								
								if(suitcards.size() == 8)
								{
									// H 80
									showLog("Know all the hokom cards");
									card = playAIForHokomH80(gameBean, player);
								}
								else
								{
									showLog("Not Know all the hokom cards");
									// Check p2 and p4 did not played hokom suit cards
									if(checkOpponentNotPlayedHokomSuitCardsInLastHokomSuitRound(gameBean, player))
									{
										// Not played hokom 
										// H 80
										showLog("Not Opponent played hokom cards");
										card = playAIForHokomH80(gameBean, player);
									}
									else
									{
										// Play the strongest hokom card
										showLog("Opponent played Hokom cards");
										showLog("Cards In Hand"+ cardsinhand.toString());
										cardsinhand = getRoundSuitCards(prbObj._cards, gameBean._roundBean._trumpSuitType);
										showLog("Cards In Hand"+ cardsinhand.toString());
										cardsinhand = Commands.appInstance.ai.sortHokomorder(cardsinhand);
										card = cardsinhand.get(0);
									}
								}
							}
						}
					}
					else
					{
						// H 80
						card = playAIForHokomH80(gameBean, player);
					}					
				}
				else
				{
					// Team mate did not won bid
					showLog("Team mate did not won bid");
					if(checkAllCardsAreHokomCards(gameBean, player))
					{
						// yes all the cards are hokom cards
						ArrayList<Integer> suitcards = getRoundSuitCards(prbObj._cards, gameBean._roundBean._trumpSuitType);
						if(haveStrongestCard(gameBean,gameBean._roundBean._trumpSuitType, suitcards, Commands.HOKOM))						
						{
							// having strongest card // play strongest
							prbObj._cards = Commands.appInstance.ai.sortHokomorder(prbObj._cards);
							card = prbObj._cards.get(0);
						}
						else
						{
							// not having strongest card // play weakest
							prbObj._cards = Commands.appInstance.ai.sortHokomorder(prbObj._cards);
							card = prbObj._cards.get(prbObj._cards.size()-1);						
						}
					}
					else
					{
						// not all the cards are hokom cards
						// Call H 80 function					
						card = playAIForHokomH80(gameBean, player);
					}
				}				
			}			
		}
		else
		{
			// Hokom Following Mode			
			showLog("Hokom Following Mode");
			
			// Player Hokom Suit Cards
			PlayerRoundBean teamPbObj = gameBean._roundBean.getPlayerRoundBeanObject(getPartner(gameBean, player));
			ArrayList<Integer> suitCards = getRoundSuitCards(prbObj._cards, gameBean._roundBean._trumpSuitType);
			suitCards = Commands.appInstance.ai.sortHokomorder(suitCards);
			
			// open cards with round suit cards
			ArrayList<Integer> openCards = new ArrayList<Integer>();
			for(int i=0;i<gameBean._roundBean.openCardsList.size();i++)
			{
				if(getSuit(gameBean._roundBean.openCardsList.get(i)).equals(gameBean._roundBean._trumpSuitType))
				{
					openCards.add(gameBean._roundBean.openCardsList.get(i));
				}
			}
			openCards = Commands.appInstance.ai.sortHokomorder(openCards);
			
			
			//Check Current suit is Hokom suit
			if(gameBean._roundBean.currentSuit.equals(gameBean._roundBean._trumpSuitType))
			{
				showLog("Round suit is Hokom suit");
				// Check any hokom suit cards
				if(checkAnyHokomSuitCards(gameBean, player))
				{
					showLog("Having hokom suit cards");
					// Check player made hokom decision
					if(checkPlayerMadeHokomDecision(gameBean, player))
					{
						showLog("Player made hokom decsion");
						// Check any Strongest cards remaining
						ArrayList<Integer> suitcards = getRoundSuitCards(prbObj._cards, gameBean._roundBean._trumpSuitType);
						if(haveStrongestCard(gameBean,gameBean._roundBean._trumpSuitType, suitcards, Commands.HOKOM))						
						{
							showLog("Having strongest card");
							//Check Team Mate Played before
							if(teammateBeforeOrNot(gameBean))
							{
								showLog("Team mate played before");
								// Check Are U the last Player
								if(gameBean._roundBean.openCardsList.size()==3)
								{
									showLog("Last Player");
									// Check u r TM winning the round
									// get the winning card 
									// get the teammate card done on above									
									if(teamPbObj._currentCard == openCards.get(0))
									{
										// Team mate winning
										showLog("Team mate winning current round");
										// Play the weakest hokom suit
										suitCards = Commands.appInstance.ai.sortHokomorder(suitCards);
										card = suitCards.get(suitCards.size()-1);
									}
									else
									{
										showLog("Team mate not winning current round");
										// Play the least strongest hokom possible										
										card = getLeastStrongestPossible(suitCards, openCards);										
									}
								}
								else
								{
									showLog("Not Last Player");
									// Check u r TM Played Second Strongest card									
									if(teamPbObj._currentCard == getSecondStrongestCard(gameBean, gameBean._roundBean._trumpSuitType, Commands.HOKOM))
									{
										showLog("Played Second strongest card");
										// Play the weakest hokom suit card
										suitCards = Commands.appInstance.ai.sortHokomorder(suitCards);
										card = suitCards.get(suitCards.size()-1);
									}
									else
									{
										showLog("Not Played Second strongest card");
										// Play the Strongest hokom suit card
										suitCards = Commands.appInstance.ai.sortHokomorder(suitCards);
										card = suitCards.get(0);
									}
								}
							}
							else
							{
								showLog("Team mate not played");								
								// Play the weakest hokom suit card
								suitCards = Commands.appInstance.ai.sortHokomorder(suitCards);
								card = suitCards.get(0);
							}
						}
						else
						{
							showLog("Not Having strongest card");
							// Check u r team mate played before
							if(teammateBeforeOrNot(gameBean))
							{
								showLog("TM Played");
								// check did his TM Played the strongest card of Hokom suit
								if( teamPbObj._currentCard == getStrongestCard(gameBean, gameBean._roundBean._trumpSuitType, Commands.HOKOM))
								{
									showLog("Played the weakest hokom suit card");
									// Play the weakest hokom suit card
									suitCards = Commands.appInstance.ai.sortHokomorder(suitCards);
									card = suitCards.get(suitCards.size()-1);
								}
								else
								{
									showLog("Did not Played the strongest card");
									// N 124
									card = playAIForHokomN124(gameBean, player);
								}
							}
							else
							{
								showLog("TM did not played");
								// Did u have any strongest hokom suit card Used in this round
								// N 124
								card = playAIForHokomN124(gameBean, player);
							}
						}
					}
					else
					{
						showLog("Player not made hokom decsion");						
						// Check u r TM Made Decision
						if(checkPlayerMadeHokomDecision(gameBean, getPartner(gameBean, player)))
						{
							showLog("Team mate made hokom decision");
							// Check u r TM Played Before
							if(teammateBeforeOrNot(gameBean))
							{
								showLog("Team mate played");
								// check did his TM Played the strongest card of Hokom suit
								if( teamPbObj._currentCard == getStrongestCard(gameBean, gameBean._roundBean._trumpSuitType, Commands.HOKOM))
								{
									showLog("Tm played the strongest");
									// N 122
									card = playAIForHokomN122(gameBean, player);
								}
								else
								{
									showLog("Tm did not played the strongest");
									// N 124
									card = playAIForHokomN124(gameBean, player);
								}
							}
							else
							{
								showLog("Team mate not played");
								// N 124
								card = playAIForHokomN124(gameBean, player);
							}
						}
						else
						{
							showLog("Team mate did not made hokom decision");							
							// Check Team mate Player before u
							if(teammateBeforeOrNot(gameBean))
							{
								showLog("Team mate played before");
								// Check his TM played the strongest card
								if( teamPbObj._currentCard == getStrongestCard(gameBean, gameBean._roundBean._trumpSuitType, Commands.HOKOM))
								{
									showLog("Played the strongest card");
									// N 122
									card = playAIForHokomN122(gameBean, player);
								}
								else
								{
									showLog("Did not Played the strongest card");									
									card = getLeastStrongestPossible(suitCards, openCards);							
								}
							}
							else
							{
								showLog("Team mate did not played before");
								// Check do u have strongest and second strongest cards
								if(checkFirstSecondStrongestCards(gameBean, player, gameBean._roundBean._trumpSuitType, Commands.HOKOM))
								{
									showLog("Having first and second strongest cards");
									// Play the strongest hokom suit card
									suitCards = Commands.appInstance.ai.sortHokomorder(suitCards);
									card = suitCards.get(0);
								}
								else
								{
									showLog("Not Having first and second strongest cards");									
									card = getWeakestHokomPossible(suitCards, openCards);	
								}
							}							
						}
					}
				}
				else
				{
					showLog("Does not have hukom suit cards");
					// Check u r Team mate won the bid
					if(checkPlayerMadeHokomDecision(gameBean, getPartner(gameBean, player)))
					{
						showLog("Team mate made hokom decision");
						// Check team mate played before or not
						if(teammateBeforeOrNot(gameBean))
						{
							showLog("Team mate played before");
							// Check u r the last person on that round
							if(gameBean._roundBean.openCardsList.size() == 3)
							{
								showLog("Last Player for that round");
								// Check U r TM winning this round
								openCards = Commands.appInstance.ai.sortHokomorder(openCards);
								if(teamPbObj._currentCard == openCards.get(0))
								{
									showLog("Team mate played the strongest card");
									// N 131
									card = playAIForHokomN131(gameBean, player);
								}
								else
								{
									showLog("Team mate did not played the strongest card");
									// N 151 
									card = playAIForHokomN151(gameBean, player);
								}
							}
							else
							{
								showLog("Not Last Player for that round");
								// Check u TM used the strongest hokom suit
								if( teamPbObj._currentCard == getStrongestCard(gameBean, gameBean._roundBean._trumpSuitType, Commands.HOKOM))
								{
									showLog("TM played the strongest hokom suit");
									// N 131
									card = playAIForHokomN131(gameBean, player);
								}
								else
								{
									showLog("TM did not played the strongest hokom suit");
									// N 151
									card = playAIForHokomN151(gameBean, player);
								}								
							}
						}
						else
						{
							showLog("Team mate did not played before");
							// Check the T M Having Strongest Hokom suit card
							ArrayList<Integer> TMKnownCards = getRoundSuitCards(getTeammateKnownCards(gameBean, player), gameBean._roundBean._trumpSuitType);
							boolean sign = false;
							for(int i=0;i<TMKnownCards.size();i++)
							{
								if(TMKnownCards.get(i) == getStrongestCard(gameBean, gameBean._roundBean._trumpSuitType, Commands.HOKOM))
								{
									sign = true;
									break;
								}
							}
							if(sign)
							{
								showLog("TM Having Strongest card");
								// N 131
								card = playAIForHokomN131(gameBean, player);
							}
							else
							{
								showLog("TM Does not having strongest card");
								// N 151
								card = playAIForHokomN151(gameBean, player);
							}
						}
					}
					else
					{
						showLog("Team mate did not made hokom decision");
						// Check U R TM Played before You
						if(teammateBeforeOrNot(gameBean))
						{
							// Team mate Played							
							// Check u r the last person on that round
							if(gameBean._roundBean.openCardsList.size() == 3)
							{
								// Did U R TM wining this round
								openCards = Commands.appInstance.ai.sortHokomorder(openCards);
								if(teamPbObj._currentCard == openCards.get(0))
								{
									// N 158
									card = playAIForHokomN158(gameBean, player);
								}
								else
								{ 
									// Not Used the strongest card
									// N 151
									card = playAIForHokomN151(gameBean, player);
								} 
							}
							else
							{
								// Did U R TM Used strongest hokom suit card 
								//openCards = Commands.appInstance.ai.sortHokomorder(openCards);
								if(teamPbObj._currentCard == getStrongestCard(gameBean, gameBean._roundBean._trumpSuitType, Commands.HOKOM))
								{
									// N 158
									card = playAIForHokomN158(gameBean, player);
								}
								else
								{
									// Not Used the strongest card
									// N 151
									card = playAIForHokomN151(gameBean, player);
								}
							}
						}
						else
						{
							// Team mate not played
							// Check the T M Having Strongest Hokom suit card
							ArrayList<Integer> TMKnownCards = getRoundSuitCards(getTeammateKnownCards(gameBean, player), gameBean._roundBean._trumpSuitType);
							boolean sign = false;
							for(int i=0;i<TMKnownCards.size();i++)
							{
								if(TMKnownCards.get(i) == getStrongestCard(gameBean, gameBean._roundBean._trumpSuitType, Commands.HOKOM))
								{
									sign = true;
									break;
								}
							}
							if(sign)
							{
								// N 158
								card = playAIForHokomN158(gameBean, player);
							}
							else
							{
								// N 151
								card = playAIForHokomN151(gameBean, player);								
							}							
						}						
					}
				}
			}
			else
			{				
				showLog("Round suit is not Hokom suit");				
				// Do U Have any card of Round Suit
				ArrayList<Integer> roundSuitCards = getRoundSuitCards(prbObj._cards, gameBean._roundBean.currentSuit);
				roundSuitCards = Commands.appInstance.ai.sortSunorder(roundSuitCards);
				if(roundSuitCards.size() > 0)
				{
					showLog("Having Round suit cards");
					// Do u have strongest card remaining for that suit.
					if(haveStrongestCard(gameBean, gameBean._roundBean.currentSuit, roundSuitCards, Commands.SAN))
					{
						showLog("Having strongest card");
						// Is there any opponent played the hokom suit card
						if(checkOpponentPlayedHokomSuitCard(gameBean, player))
						{
							showLog("Opponent played the hokom suit card");
							// Check U r TM winning this round
							openCards = Commands.appInstance.ai.sortHokomorder(openCards);
							if(teamPbObj._currentCard == openCards.get(0))
							{
								// Team mate winning play the strngest
								card = roundSuitCards.get(0);
							}
							else
							{
								// Play the weakest card
								card = roundSuitCards.get(roundSuitCards.size()-1);								
							}
						}
						else
						{
							showLog("Opponent did not played the hokom suit card");
							// Play the strongest
							card = roundSuitCards.get(0);
						}
					}
					else
					{
						showLog("Not Having strongest card");
						// check u r TM winning this round						
						if(checkYourTMWinningThisRound(gameBean,player))
						{
							// Team mate winning play the strongest
							card = roundSuitCards.get(0);
						}
						else
						{
							// Play the weakest card
							card = roundSuitCards.get(roundSuitCards.size()-1);		
						}					
					}					
				}
				else
				{
					showLog("Not Having Round Suit Cards");
					// Do u Have any hokom suit cards					
					if(checkAnyHokomSuitCards(gameBean, player))
					{
						showLog("Having hokom suit cards");
						// Check u r team mate start playing the round						
						if(gameBean._roundBean.openCardsList.size() == 2 && teamPbObj._currentCard != -1)
						{
							showLog("Team mate start playing the round");
							// Check TM Used Ace
							if(teamPbObj._currentCard%8 == 1)
							{
								showLog("TM Used Ace");
								// Ace used	
								// L 186
								if(checkOpponentPlayedHokomSuitCard(gameBean, player) )
								{
									showLog("Opponent Played hokom suit card");
									// N 186 
									card = playAIForHokomN186(gameBean,player);
								}
								else
								{
									showLog("Opponent did not Played hokom suit card");
									// N 189
									card = playAIForHokomN189(gameBean, player);									
								}
							}
							else
							{
								showLog("Did u r team mate play the strongest card and used highest option");
								// Did u r team mate play the strongest card and used highest option
								// Get the strongest card
								
								Integer strongestcard = 0;								
								strongestcard = getStrongestCard(gameBean, gameBean._roundBean.currentSuit, Commands.SAN);								
								
								showLog("Team mate Said Highest  "+teamPbObj.isDeclaredHighest);								
								showLog(teamPbObj._currentCard+"== "+strongestcard);
								
								if(teamPbObj._currentCard == strongestcard && teamPbObj.isDeclaredHighest)
								{
									// L 186
									if(checkOpponentPlayedHokomSuitCard(gameBean, player))
									{
										showLog("Opponent Played hokom suit card");
										// N 186 
										card = playAIForHokomN186(gameBean, player);
									}
									else
									{
										showLog("Opponent did not Played hokom suit card");
										// N 189
										card = playAIForHokomN189(gameBean, player);									
									}
								}
								else
								{
									// Check Opponent use any Hokom suit card
									if(checkOpponentPlayedHokomSuitCard(gameBean, player))
									{
										showLog("Opponent Played hokom suit card");
										// N 186 
										card = playAIForHokomN186(gameBean, player);
									}
									else
									{
										showLog("Opponent did not Played hokom suit card");
										// play the weakest hokom suit card
										card = 	suitCards.get(suitCards.size()-1);						
									}
								}
								
							}
						}
						else
						{
							showLog("Team mate not start playing the round");
							
							// Does u r TM winning the round
							if(checkYourTMWinningThisRound(gameBean, player))
							{
								// N 189
								card = playAIForHokomN189(gameBean, player);
							}
							else
							{
								/*// L 186
								if(checkOpponentPlayedHokomSuitCard(player))
								{
									showLog("Opponent Played hokom suit card");
									// N 186 
									card = playAIForHokomN186(player);
								}
								else
								{
									showLog("Opponent did not Played hokom suit card");
									// N 189
									card = playAIForHokomN189(player);									
								}*/
								
								
								// M 195 to X 195	
								if(gameBean._roundBean._bidWonPerson.equals(player) ||  gameBean._roundBean._bidWonPerson.equals(getPartner(gameBean, player)))
								{
									if(checkOtherTeamPlayedHokomCard(gameBean,player))
									{
										showLog("Played hokom card");
										ArrayList<Integer> openCards1 = getRoundSuitCards(gameBean._roundBean.openCardsList, gameBean._roundBean._trumpSuitType);
										card = getLeastStrongestPossible(suitCards, openCards1);
										if(card == 0)
											card = playAIForHokomN151(gameBean, player);
										
										// if u r strongest hokom suit card == strongest remaining play strongest
										/*if(haveStrongestCard(gameBean._roundBean._trumpSuitType, suitCards, Commands.HOKOM))
										{
											showLog("Having strongest hokom suit card");
											// Play the strongest hokom suit card
											card = 	suitCards.get(0);
										}
										else
										{
											showLog("Not Having strongest hokom suit card");
											ArrayList<Integer> otherSuitCards = getExcludeHokomCards(prbObj._cards);
											if(otherSuitCards.size()>0)
											{
												// Having other suit cards
												otherSuitCards = ai.sortSunorder(otherSuitCards);
												// Play the weakest card
												card = otherSuitCards.get(otherSuitCards.size()-1); 
											}
											else
											{
												//Play the strongest hokom suit card									
												card = 	suitCards.get(suitCards.size()-1);
											}
										}*/
									}								
									else
									{
										// Play the strongest hokom suit card		
										showLog("Get the hokom cards 12");
										card = 	suitCards.get(0);
									}	
								}
								else
								{
									suitCards = Commands.appInstance.ai.sortHokomorder(suitCards);
									showLog("Get the hokom cards played by opponents");
									if(haveStrongestCard(gameBean,gameBean._roundBean._trumpSuitType, suitCards, Commands.HOKOM))
									{									
										// Get the hokom cards played by opponents
										ArrayList<Integer> oppHokomCards = new ArrayList<Integer>();
										for(int i=0;i<gameBean._roundBean._playerRoundBeanObjs.size();i++)
										{
											if(gameBean._roundBean._playerRoundBeanObjs.get(i)._playerId.equals(player) || getPartner(gameBean, player).equals(gameBean._roundBean._playerRoundBeanObjs.get(i)._playerId))
											{
												// Do nothing
											}
											else
											{
												// Get the current Played card
												if(getSuit((gameBean._roundBean._playerRoundBeanObjs.get(i)._currentCard)).equals(gameBean._roundBean._trumpSuitType))
												{
													// Hokom Suit
													oppHokomCards.add(gameBean._roundBean._playerRoundBeanObjs.get(i)._currentCard);
												}
											}
										}
										
										if(oppHokomCards.size()>0)
										{
											// Play the next strongest hokom card
											for(int i=suitCards.size()-1;i>0;i--)
											{
												oppHokomCards.add(suitCards.get(i));
												Commands.appInstance.ai.sortHokomorder(oppHokomCards);
												
												for(int j=0;j<suitCards.size();j++)
												{
													if(oppHokomCards.get(0) == suitCards.get(j))
													{
														card = oppHokomCards.get(0);
														break;
													}													
												}
												if(card != 0)
													break;
											}	
										}											
										
										if(oppHokomCards.size() == 0 || card == 0)
										{
											showLog("Play the weakest hokom card");
											// Play the weakest hokom card
											card = suitCards.get(suitCards.size()-1);
										}
									}
									else
									{
										showLog("Play the Strongest 121 hokom card");
										card = suitCards.get(0);
									}									
								}
							}							
						}						
					}
					else
					{
						showLog("Not having hokom suit cards");
						//Does u r TM winning the round by playing strongest card
						if(checkYourTMWinningThisRound(gameBean, player))
						{
							// N 189
							card = playAIForHokomN189(gameBean, player);
						}
						else
						{
							// N 151
							card = playAIForHokomN151(gameBean, player);
						}
					}
				}
			}			
		}
				
		
		showLog("Player Cards :"+prbObj._cards);
		
		if(card == 0)
		{
			showLog("Hokom card selection Fail");
			ArrayList<Integer> cards = getExcludeHokomCards(gameBean, prbObj._cards);
			cards = Commands.appInstance.ai.sortSunorder(cards);
			card = cards.get(cards.size()-1);
		}
		showLog("Final Card :"+card);
		
		return card;
	}// Play AI For HOKOM Function END
	
	
	/*private boolean checkAnyOnePlayedHokomCard()
	{
		for(int i=0;i<gameBean._roundBean._openCard.size();i++)
		{
			if(getSuit(gameBean._roundBean._openCard.get(i)).equals(gameBean._roundBean._trumpSuitType))
			{
				return true;
			}
		}
		return false;
	}*/
	
	private boolean checkOtherTeamPlayedHokomCard(GameBean gameBean, String player)
	{
		for(int i=0;i<gameBean._roundBean._playerRoundBeanObjs.size();i++)
		{
			PlayerRoundBean prBean = gameBean._roundBean._playerRoundBeanObjs.get(i);
			if(prBean._playerId.equals(player) || prBean._playerId.equals(getPartner(gameBean, player)))
			{
				// Do nothing
			}
			else
			{
				if(prBean._currentCard !=  0 && getSuit(prBean._currentCard).equals(gameBean._roundBean._trumpSuitType))
				{
					return true;
				}
			}
		}
		return false;
	}
	
	
	private boolean checkYourTMWinningThisRound(GameBean gameBean, String Player)
	{
		boolean sign = false;
		ArrayList<Integer> openCards = getRoundSuitCards(gameBean._roundBean.openCardsList, gameBean._roundBean._trumpSuitType);
		PlayerRoundBean teamPbObj = gameBean._roundBean.getPlayerRoundBeanObject(getPartner(gameBean, Player));
		
		for(int i=0;i<gameBean._roundBean.openCardsList.size();i++)
		{
			if(getSuit(gameBean._roundBean.openCardsList.get(i)).equals(gameBean._roundBean._trumpSuitType))
			{
				sign = true;
				break;
			}
		}
		
		if(sign)
		{
			openCards = Commands.appInstance.ai.sortHokomorder(openCards);
		}
		else
		{
			openCards = new ArrayList<Integer>();
			for(int i=0;i<gameBean._roundBean.openCardsList.size();i++)
			{
				if(getSuit(gameBean._roundBean.openCardsList.get(i)).equals(gameBean._roundBean.currentSuit))
				{
					openCards.add(gameBean._roundBean.openCardsList.get(i));
				}
			}
			openCards = Commands.appInstance.ai.sortSunorder(openCards);
		}
		
		if(teamPbObj._currentCard == openCards.get(0))
		{
			// Team mate winning play the strongest			
			return true;
		}
		else
		{
			// Play the weakest card
			return false;											
		}		
	}
	
	
	private Integer playAIForHokomN189(GameBean gameBean, String player)
	{
		showLog("playAIForHokomN189");
		Integer card = 0;	
		showLog(gameBean._roundBean._bidWonPerson);
		showLog(player+"=="+getPartner(gameBean, player));
		// Are U Or U R TM made the decision
		if(gameBean._roundBean._bidWonPerson.equals(player) || gameBean._roundBean._bidWonPerson.equals(getPartner(gameBean, player)))
		{
			//N 131
			card = playAIForHokomN131(gameBean, player);
		}
		else
		{
			//N 158
			card = playAIForHokomN158(gameBean, player);
		}
		
		return card;
	}
	
	private Integer playAIForHokomN186(GameBean gameBean, String player)
	{
		showLog("playAIForHokomN186");
		Integer card = 0;		
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		// Do U have any strongest hokom suit card than the one used in the round
		ArrayList<Integer> hokomSuitcards = getRoundSuitCards(prbObj._cards, gameBean._roundBean._trumpSuitType);
		hokomSuitcards = Commands.appInstance.ai.sortHokomorder(hokomSuitcards);
		ArrayList<Integer> roundCards = getRoundSuitCards(gameBean._roundBean.openCardsList, gameBean._roundBean._trumpSuitType);
		roundCards = Commands.appInstance.ai.sortHokomorder(roundCards);
		
		if(checkYouHaveStrongestHokomSuitCardUsedInthisRound(hokomSuitcards, roundCards))
		{			 
			card = getLeastStrongestPossible(hokomSuitcards, roundCards);	
		}
		else
		{
			// Not having strongest hokom suit card 
			// N 151
			card = playAIForHokomN151(gameBean, player);
		}
		return card;
	}
	
	
	// Check any Opponent played the hokom Suit Card
	
	private boolean checkOpponentPlayedHokomSuitCard(GameBean gameBean, String player)
	{
		// team mate Player been
		PlayerRoundBean teamPrbObj = gameBean._roundBean.getPlayerRoundBeanObject(getPartner(gameBean, player));
		
		for(int i=0;i<gameBean._roundBean.openCardsList.size();i++)
		{
			if(getSuit(gameBean._roundBean.openCardsList.get(i)).equals(gameBean._roundBean._trumpSuitType) && gameBean._roundBean.openCardsList.get(i) != teamPrbObj._currentCard )
				return true;
		}
		return false;
	}
	
	private ArrayList<Integer> getExcludeHokomCards(GameBean gameBean, ArrayList<Integer> cards)
	{
		int n1=0;
		int n2=0;
		if(gameBean._roundBean._trumpSuitType.equals("clubs")){n1=1;n2=8;}		
		else if(gameBean._roundBean._trumpSuitType.equals("diamonds")){n1=9;n2=16;}
		else if(gameBean._roundBean._trumpSuitType.equals("hearts")){n1=17;n2=24;}
		else if(gameBean._roundBean._trumpSuitType.equals("spades")){n1=25;n2=32;}
		
		ArrayList<Integer> _cards = new ArrayList<Integer>();		
		for(int i=0;i<cards.size();i++)
		{
			Integer card = cards.get(i);			
			if(card>=n1 && card<=n2)
			{
				// Do nothing
			}
			else
			{
				_cards.add(card);
			}						
		}
		return _cards;
	}
	
	private Integer playAIForHokomN158(GameBean gameBean, String player)
	{
		showLog("playAIForHokomN158");
		Integer card = 0;		
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);

		// Exclude Hokom suit cards		
		ArrayList<Integer> exCards = getExcludeHokomCards(gameBean, prbObj._cards);
		
		// Get the 10 card alone and not having highest
		card = getTenCardAloneAndNotHighest(gameBean, exCards);
		if(card == 0 )
		{
			// Check u having 10 and opp having ace of that suit
			card = checkAnyTenWithOpponentHavingAce(gameBean, player);
			if(card == 0)
			{
				// Do u have any 10 and Ace
				card = checkHavingTenAndAce(exCards);
				if(card == 0)
				{
					// Get the 10 card
					card = getTenCard(exCards);
					if(card == 0)
					{
						exCards = Commands.appInstance.ai.sortSunorder(exCards);
						// Do U Have any Ace
						if(isHavingAce(exCards))
						{
							showLog("Having Ace");
							
							showLog("Cards"+exCards);
							// Play the strongest card not having Ace
							for(int i=0;i<exCards.size();i++)
							{
								if(exCards.get(i)%8 != 1)
								{
									card = exCards.get(i);
									break;
								}								
							}
							// all cards are Aces compulsory we need to play ace
							if(card == 0)
							{
								card = exCards.get(0);
							}
						}
						else
						{
							showLog("Not Having Ace");
							// Play the weakest card on hand
							if(exCards.size()>0)
								card = exCards.get(exCards.size()-1);
							else
							{
								prbObj._cards = Commands.appInstance.ai.sortHokomorder(prbObj._cards);
								card = prbObj._cards.get(prbObj._cards.size()-1);
							}
						}
					}
				}				
			}
		}		
		return card;
	}
	
	private boolean isHavingAce(ArrayList<Integer> cards)
	{
		for(int i=0;i<cards.size();i++)
		{
			if(cards.get(i)%8 == 1)
			{
				return true;
			}
		}
		return false;
	}
	
	private Integer getTenCard(ArrayList<Integer> cards)
	{
		Integer card =0;
		for(int i=0;i<cards.size();i++)
		{
			if(cards.get(i)%8 == 5)
			{
				card = cards.get(i);
				break;
			}
		}
		return card;
	}
	
	
	private Integer playAIForHokomN151(GameBean gameBean, String player)
	{
		showLog("playAIForHokomN151");
		// Not having hokom suit cards
		Integer card = 0;
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		ArrayList<Integer> heartsCards = new ArrayList<Integer>();
		ArrayList<Integer> clubsCards = new ArrayList<Integer>();
		ArrayList<Integer> diamondsCards = new ArrayList<Integer>();
		ArrayList<Integer> spadesCards = new ArrayList<Integer>();
		
		for(int i=0;i<prbObj._cards.size();i++)
		{
			if(prbObj._cards.get(i)>=1 && prbObj._cards.get(i)<=8)
			{
				clubsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=9 && prbObj._cards.get(i)<=16)
			{
				diamondsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=17 && prbObj._cards.get(i)<=24)
			{
				heartsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=25 && prbObj._cards.get(i)<=32)
			{
				spadesCards.add(prbObj._cards.get(i));
			}
		}
		
		clubsCards = Commands.appInstance.ai.sortSunorder(clubsCards);
		diamondsCards = Commands.appInstance.ai.sortSunorder(diamondsCards);
		heartsCards = Commands.appInstance.ai.sortSunorder(heartsCards);
		spadesCards = Commands.appInstance.ai.sortSunorder(spadesCards);
		
		if(clubsCards.size()>=3)
		{
			card = getWeakestCardTenwithTwocards(clubsCards);
		}
		
		if(card == 0 && diamondsCards.size()>=3)
		{
			card = getWeakestCardTenwithTwocards(diamondsCards);
		}
		
		if(card == 0 && heartsCards.size()>=3)
		{
			card = getWeakestCardTenwithTwocards(heartsCards);
		}
		
		if(card == 0 && spadesCards.size()>=3)
		{
			card = getWeakestCardTenwithTwocards(spadesCards);
		}
		
		if(card == 0)
		{
			// Check for second condition
			// have any 10 with strongest and having another card
			if(clubsCards.size()>=2)
			{
				if((clubsCards.get(0)%8 == 5)&&(clubsCards.get(0) == getStrongestCard(gameBean, "clubs", Commands.SAN)))
				{
					card = clubsCards.get(clubsCards.size()-1);					
				}
			}
			if(card == 0 && diamondsCards.size()>=2)
			{
				if((diamondsCards.get(0)%8 == 5)&&(diamondsCards.get(0) == getStrongestCard(gameBean, "diamonds", Commands.SAN)))
				{
					card = diamondsCards.get(diamondsCards.size()-1);					
				}
			}
			if(card == 0 && heartsCards.size()>=2)
			{
				if((heartsCards.get(0)%8 == 5)&&(heartsCards.get(0) == getStrongestCard(gameBean, "hearts", Commands.SAN)))
				{
					card = heartsCards.get(heartsCards.size()-1);					
				}
			}
			if(card == 0 && spadesCards.size()>=2)
			{
				if((spadesCards.get(0)%8 == 5)&&(spadesCards.get(0) == getStrongestCard(gameBean, "spades", Commands.SAN)))
				{
					card = spadesCards.get(spadesCards.size()-1);					
				}
			}
		}
		
		if(card == 0)
		{
			// Check for Third condition
			// Check for ace and not having 10 
			
			if(clubsCards.size()>=2)
			{
				if(clubsCards.size()>2)
				{
					card = clubsCards.get(clubsCards.size()-1);
				}
				else
					card = getWeakestCardSuitHavingAceNothavingTen(clubsCards);				
			}
			
			if(card == 0 && diamondsCards.size()>=2)
			{
				if(diamondsCards.size()>2)
				{
					card = diamondsCards.get(diamondsCards.size()-1);
				}
				else
					card = getWeakestCardSuitHavingAceNothavingTen(diamondsCards);
			}
			if(card == 0 && heartsCards.size()>=2)
			{
				if(heartsCards.size()>2)
				{
					card = heartsCards.get(heartsCards.size()-1);
				}
				else
					card = getWeakestCardSuitHavingAceNothavingTen(heartsCards);
			}
			if(card == 0 && spadesCards.size()>=2)
			{
				if(spadesCards.size()>2)
				{
					card = spadesCards.get(spadesCards.size()-1);
				}
				else
					card = getWeakestCardSuitHavingAceNothavingTen(spadesCards);
			}			
		}
		
		if(card == 0)
		{
			// Check for Fourth condition
			ArrayList<Integer> nonHokomCards = getExcludeHokomCards(gameBean, prbObj._cards);
			if(nonHokomCards.size()!= 0)
			{
				nonHokomCards = Commands.appInstance.ai.sortSunorder(nonHokomCards);
				card = nonHokomCards.get(nonHokomCards.size()-1);
			}
			else
			{
				prbObj._cards = Commands.appInstance.ai.sortSunorder(prbObj._cards);
				card = prbObj._cards.get(prbObj._cards.size()-1);
			}
		}		
		return card;
	}
	
	private Integer getWeakestCardSuitHavingAceNothavingTen(ArrayList<Integer> cards)
	{
		Integer card = 0;
		for(int i=0;i<cards.size();i++)
		{
			if(cards.get(i)%8 == 1)
			{
				boolean sign = false; 
				for(int j=0;j<cards.size();j++)
				{
					if(cards.get(j)%8 == 5)
					{
						sign = true;
						break;
					}
				}
				if(!sign)
					card = cards.get(cards.size()-1);
				break;
			}
		}
		return card;
	}
	private Integer getWeakestCardTenwithTwocards(ArrayList<Integer> cards)
	{
		Integer card =0;
		for(int i=0;i<cards.size();i++)
		{
			if(cards.get(i)%8 == 5)
			{
				card = cards.get(cards.size()-1);
				break;
			}
		}
		return card;		
	}
	
	private Integer playAIForHokomN131(GameBean gameBean, String player)
	{		
		showLog("playAIForHokomN131");
		Integer card = 0;		
		//Check have any 10 alone and not highest
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		// Exclude Hokok suit cards
		ArrayList<Integer> exCards = getExcludeHokomCards(gameBean, prbObj._cards);
		card = getTenCardAloneAndNotHighest(gameBean, exCards);
		if(card != 0) 
		{
			// Play that card
		}
		else
		{
			// Check Having 10 and Opponent having Ace of that suit
			card = checkAnyTenWithOpponentHavingAce(gameBean, player);
			if(card == 0)
			{
				// Check Having 10 and Ace
				card = checkHavingTenAndAce(exCards);
				if(card == 0)
				{
					// Check do u have any strongest card suit					
					// Play Messaging Mode
					card = playAICardSelectionMessagingMode(gameBean, player);			
				}				
			}			
		}		
		return card;
	}
	
	
	private Integer checkHavingTenAndAce(ArrayList<Integer> cards)
	{		
		//PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		
		ArrayList<Integer> heartsCards = new ArrayList<Integer>();
		ArrayList<Integer> clubsCards = new ArrayList<Integer>();
		ArrayList<Integer> diamondsCards = new ArrayList<Integer>();
		ArrayList<Integer> spadesCards = new ArrayList<Integer>();
		
		for(int i=0;i<cards.size();i++)
		{
			if(cards.get(i)>=1 && cards.get(i)<=8)
			{
				clubsCards.add(cards.get(i));
			}
			else if(cards.get(i)>=9 && cards.get(i)<=16)
			{
				diamondsCards.add(cards.get(i));
			}
			else if(cards.get(i)>=17 && cards.get(i)<=24)
			{
				heartsCards.add(cards.get(i));
			}
			else if(cards.get(i)>=25 && cards.get(i)<=32)
			{
				spadesCards.add(cards.get(i));
			}
		}
		
		Integer card = 0;
		card = havingTenAndAce(clubsCards);
		
		if( card == 0 )
		{
			card = havingTenAndAce(diamondsCards);
			if(card == 0)
			{
				card = havingTenAndAce(heartsCards);
				if(card == 0)
				{
					card = havingTenAndAce(spadesCards);
					return card;
				}
				else
					return card;
			}
			else 
				return card;
		}
		else 
			return card;		
	}
	
	private Integer havingTenAndAce(ArrayList<Integer> cards)
	{
		Integer card = 0;
		if(cards.size()>0)
		{		
			int count = 0;
			for(int i=0;i<cards.size();i++)
			{
				if(cards.get(i)%8 == 1 || cards.get(i)%8 == 5 )
				{
					count++;				
				}
			}
			
			if(count == 2)
			{
				cards = Commands.appInstance.ai.sortSunorder(cards);
				card = cards.get(0);
			}
			return card;
		}
		else
			return card;
	}
	
	
	private Integer getTenCardAloneAndNotHighest(GameBean gameBean, ArrayList<Integer> cards)
	{		
		ArrayList<Integer> heartsCards = new ArrayList<Integer>();
		ArrayList<Integer> clubsCards = new ArrayList<Integer>();
		ArrayList<Integer> diamondsCards = new ArrayList<Integer>();
		ArrayList<Integer> spadesCards = new ArrayList<Integer>();
		
		for(int i=0;i<cards.size();i++)
		{
			if(cards.get(i)>=1 && cards.get(i)<=8)
			{
				clubsCards.add(cards.get(i));
			}
			else if(cards.get(i)>=9 && cards.get(i)<=16)
			{
				diamondsCards.add(cards.get(i));
			}
			else if(cards.get(i)>=17 && cards.get(i)<=24)
			{
				heartsCards.add(cards.get(i));
			}
			else if(cards.get(i)>=25 && cards.get(i)<=32)
			{
				spadesCards.add(cards.get(i));
			}
		}
		
		Integer card = 0;
		if(clubsCards.size() == 1)
		{
			// Check for only Ten and not highest
			if(checkOnlyTenAndNotHighest(gameBean, clubsCards.get(0)))
				card = clubsCards.get(0);			
		}
		
		if(card == 0 && diamondsCards.size() == 1)
		{
			if(checkOnlyTenAndNotHighest(gameBean, diamondsCards.get(0)))
				card = diamondsCards.get(0);
			if(card == 0 && heartsCards.size() == 1)
			{
				if(checkOnlyTenAndNotHighest(gameBean, heartsCards.get(0)))
					card = heartsCards.get(0);
				if(card == 0 && spadesCards.size() == 1)
				{
					if(checkOnlyTenAndNotHighest(gameBean, spadesCards.get(0)))
						card = spadesCards.get(0);
					return card;
				}
				else
					return card;
			}
			else 
				return card;
		}
		else 
			return card;
		
	}
	
	private boolean checkOnlyTenAndNotHighest(GameBean gameBean, Integer no)
	{
		String type = "";
		if(getSuit(no).equals(gameBean._roundBean._trumpSuitType))		
			type = Commands.HOKOM;		
		else
			type = Commands.SAN;
		
		if(no%8 == 5 && (getStrongestCard(gameBean, getSuit(no), type))!= no )
		{
			return true;
		}
		return false;
	}
	
	
	
	private Integer playAIForHokomN122(GameBean gameBean, String player)
	{
		Integer card = 0;
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		ArrayList<Integer> suitCards = getRoundSuitCards(prbObj._cards, gameBean._roundBean._trumpSuitType);
		suitCards = Commands.appInstance.ai.sortHokomorder(suitCards);
		// Do you have second strongest card
		if(haveSecondStrongestCard(gameBean, gameBean._roundBean._trumpSuitType, prbObj._cards, Commands.HOKOM))
		{
			showLog("Having second strongest card");
			// Play the weakest hokom suit card
			card = suitCards.get(suitCards.size()-1);
		}
		else 
		{
			showLog("Not Having second strongest card");
			// Play the strongest hokom suit card
			card = suitCards.get(0);
		}
		return card;
	}
	
	
	private Integer playAIForHokomN124(GameBean gameBean, String player)
	{
		showLog("playAIForHokomN124");
		Integer card = 0;
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		//Did u have any strongest hokom suit card Used in this round
		ArrayList<Integer> openCards = new ArrayList<Integer>();
		for(int i=0;i<gameBean._roundBean.openCardsList.size();i++)
		{
			if(getSuit(gameBean._roundBean.openCardsList.get(i)).equals(gameBean._roundBean._trumpSuitType))
			{
				openCards.add(gameBean._roundBean.openCardsList.get(i));
			}
		}
		openCards = Commands.appInstance.ai.sortHokomorder(openCards);
		ArrayList<Integer> suitCards = getRoundSuitCards(prbObj._cards, gameBean._roundBean._trumpSuitType);
		suitCards = Commands.appInstance.ai.sortHokomorder(suitCards);
		if(checkYouHaveStrongestHokomSuitCardUsedInthisRound(suitCards, openCards))
		{
			showLog("Having Stronger card");
			// Play the strongest card on hand
			card = suitCards.get(0);
		}
		else
		{
			showLog("Not having Stronger card");
			// Play the weakest card
			card = suitCards.get(suitCards.size()-1);
		}
		return card;
	}
	
	
	private Integer playAIForHokomH80(GameBean gameBean, String player)
	{
		showLog("playAIForHokomH80");
		Integer card = 0;
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		// Checking for ace
		
		for(int i=0;i<prbObj._cards.size();i++)
		{
			if(prbObj._cards.get(i)%8 == 1 && getSuit(prbObj._cards.get(i)).equals(gameBean._roundBean._trumpSuitType))
			{
				// do nothing
			}
			else if(prbObj._cards.get(i)%8 == 1)
			{
				card = prbObj._cards.get(i);
				break;
			}
		}
		
		if(card != 0)
		{
			// Play Ace
			// do nothing ace is played
			showLog("Play Ace");
		}
		else
		{
			// Check any Strongest card having non hokom suit
			ArrayList<Integer> strongestcards = new ArrayList<Integer>();
			for(int i=0;i<Commands.appInstance.suits.size();i++)
			{
				if(Commands.appInstance.suits.get(i).equals(gameBean._roundBean._trumpSuitType))
				{
					// do nothing
				}
				else
				{
					Integer cardno = getStrongestCard(gameBean, Commands.appInstance.suits.get(i), Commands.SAN); 
					if( cardno != -1)
					{
						strongestcards.add(cardno);
					}
				}
			}
			
			strongestcards = Commands.appInstance.ai.sortSunorder(strongestcards);
			prbObj._cards = Commands.appInstance.ai.sortSunorder(prbObj._cards);
			
			for(int i=0;i<prbObj._cards.size();i++)
			{
				for(int j=0;j<strongestcards.size();j++)
				{
					if(prbObj._cards.get(i) == strongestcards.get(j))
					{
						card = prbObj._cards.get(i);
						break;
					}
				}
				if(card != 0)
					break;
			}
			
			if(card != 0)
			{
				// Play Strongest card
				showLog("Play the strongest card");
				// Use Highest Option
				AIBsn aiext = new AIBsn();
				aiext.anounceHighest(gameBean, player);
				aiext = new AIBsn();
				aiext = null;				
				showLog("%%%%%%% Highest Card %%%%%%%%%%");
				
			}
			else
			{
				// Check u r team mate having any strongest card
				// get all the strongest cards
				Integer cardno = getStrongestCard(gameBean, gameBean._roundBean._trumpSuitType, Commands.HOKOM); 
				if( cardno != -1)
				{
					strongestcards.add(cardno);
				}
			
				// get u r team mate cards
				ArrayList<Integer> teammateCards = getTeammateKnownCards(gameBean, player);
				
				// Exclude Hokom suit cards for team mate cards
				
				// Check team mate having strongest cards
				ArrayList<Integer> teammatesStrongestCards = getTeammatestStrongestCards(getExcludeHokomCards(gameBean, teammateCards), strongestcards);
			
				if(teammatesStrongestCards.size() == 0)
				{
					showLog("Team mate not having strong cards");
					// Check P4 Message
					card = checkHokomP4Message(gameBean, player);
				}
				else 
				{
					showLog("team mate having strongest cards");
					for(int i=0;i<teammatesStrongestCards.size();i++)
					{
						ArrayList<Integer> suitCards = getRoundSuitCards(prbObj._cards, getSuit(teammatesStrongestCards.get(i)));
						if(suitCards.size()>=2)
						{
							if(getSuit(suitCards.get(i)).equals(gameBean._roundBean._trumpSuitType))
								suitCards = Commands.appInstance.ai.sortHokomorder(suitCards);
							else
								suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
							card = suitCards.get(0);
							break;
						}
					}
					
					if(card == 0)
					{
						// Check P4 Message
						card = checkHokomP4Message(gameBean, player);
					}
				}
				
			}
			
		}		
		return card;
	}
	
	
	private Integer checkHokomP4Message(GameBean gameBean, String player)
	{
		showLog("checkHokomP4Message");
		Integer card = 0;
		
		PlayerRoundBean tmPrbObj = gameBean._roundBean.getPlayerRoundBeanObject(getPartner(gameBean, player));		
		PlayerRoundBean playerprbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		String suit = "";
		// Check for Periority 4 Message
		Integer no = checkPeriorityMessage(tmPrbObj,4);
		if(no != -1)
		{			
			ArrayList<String> outputSuitArray = new ArrayList<String>();
			
			if(tmPrbObj._hisMessagesForTeammate.get(no)._message.equals("SCM"))
			{
				if(tmPrbObj._hisMessagesForTeammate.get(no)._messageCardSuit.equals("clubs") || tmPrbObj._hisMessagesForTeammate.get(no)._messageCardSuit.equals("spades") )
				{
					outputSuitArray.add("diamonds");
					outputSuitArray.add("hearts");
					//go for diamonds
					if(getRoundSuitCards(playerprbObj._cards, "diamonds").size()>0)
						suit = "diamonds";
					else											
						suit = "hearts";
				}
				else if(tmPrbObj._hisMessagesForTeammate.get(no)._messageCardSuit.equals("diamonds") || tmPrbObj._hisMessagesForTeammate.get(no)._messageCardSuit.equals("hearts") )
				{
					outputSuitArray.add("clubs");
					outputSuitArray.add("spades");
					//go for clubs
					if(getRoundSuitCards(playerprbObj._cards, "clubs").size()>0)
						suit = "clubs";
					else											
						suit = "spades";
				}
			}
			else if(tmPrbObj._hisMessagesForTeammate.get(no)._message.equals("OCM"))
			{
				
				if(tmPrbObj._hisMessagesForTeammate.get(no)._messageCardSuit.equals("spades"))
				{
					if(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("clubs"))
					{
						outputSuitArray.add("hearts");
						outputSuitArray.add("diamonds");
					}
					else if(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("diamonds"))
					{
						outputSuitArray.add("clubs");
						outputSuitArray.add("hearts");
					}
					else if(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("hearts"))
					{
						outputSuitArray.add("clubs");
						outputSuitArray.add("diamonds");
					}						
				}
				else if(tmPrbObj._hisMessagesForTeammate.get(no)._messageCardSuit.equals("clubs"))
				{
					if(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("spades"))
					{							
						outputSuitArray.add("diamonds");
						outputSuitArray.add("hearts");
					}
					else if(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("diamonds"))
					{
						outputSuitArray.add("spades");
						outputSuitArray.add("hearts");
					}
					else if(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("hearts"))
					{
						outputSuitArray.add("spades");
						outputSuitArray.add("diamonds");
					}						
				}
				else if(tmPrbObj._hisMessagesForTeammate.get(no)._messageCardSuit.equals("hearts"))
				{
					if(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("diamonds"))
					{							
						outputSuitArray.add("clubs");
						outputSuitArray.add("spades");
					}
					else if(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("spades"))
					{
						outputSuitArray.add("diamonds");
						outputSuitArray.add("clubs");
					}
					else if(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("clubs"))
					{
						outputSuitArray.add("diamonds");
						outputSuitArray.add("spades");
					}						
				}
				else if(tmPrbObj._hisMessagesForTeammate.get(no)._messageCardSuit.equals("diamonds"))
				{
					if(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("hearts"))
					{							
						outputSuitArray.add("spades");
						outputSuitArray.add("clubs");
					}
					else if(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("spades"))
					{
						outputSuitArray.add("hearts");
						outputSuitArray.add("clubs");
					}
					else if(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("clubs"))
					{
						outputSuitArray.add("hearts");
						outputSuitArray.add("spades");
					}						
				}
				/*if(tmPrbObj._hisMessagesForTeammate.get(no)._messageCardSuit.equals("clubs"))
				{
					outputSuitArray.add("spades");
					outputSuitArray.add(getSuitType(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit));
					// go for spades
					if(getRoundSuitCards(playerprbObj._cards, "spades").size()>0)
						suit = "spades";
					else											
						suit = getSuitType(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit);															
				}
				else if(tmPrbObj._hisMessagesForTeammate.get(no)._messageCardSuit.equals("spades"))
				{
					outputSuitArray.add("clubs");
					outputSuitArray.add(getSuitType(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit));
					// go for clubs
					if(getRoundSuitCards(playerprbObj._cards, "clubs").size()>0)
						suit = "clubs";
					else											
						suit = getSuitType(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit);															
				}
				else if(tmPrbObj._hisMessagesForTeammate.get(no)._messageCardSuit.equals("diamonds"))
				{
					outputSuitArray.add("hearts");
					outputSuitArray.add(getSuitType(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit));
					// go for hearts
					if(getRoundSuitCards(playerprbObj._cards, "hearts").size()>0)
						suit = "hearts";
					else											
						suit = getSuitType(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit);															
				}
				else if(tmPrbObj._hisMessagesForTeammate.get(no)._messageCardSuit.equals("hearts"))
				{
					outputSuitArray.add("diamonds");
					outputSuitArray.add(getSuitType(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit));
					// go for diamonds
					if(getRoundSuitCards(playerprbObj._cards, "diamonds").size()>0)
						suit = "diamonds";
					else											
						suit = getSuitType(tmPrbObj._hisMessagesForTeammate.get(no)._roundSuit);															
				}		*/		
			}
			
			String finalSuit = null;
			for(int i=0;i<outputSuitArray.size();i++)
			{
				suit = outputSuitArray.get(i);
				if(suit.equals(gameBean._roundBean._trumpSuitType))
					continue;
				
				ArrayList<Integer> suitCards =getRoundSuitCards(playerprbObj._cards, suit); 
					
				suitCards = Commands.appInstance.ai.sortSunorder(suitCards);				
				if(suitCards.size()>0)
				{
				
					boolean isTeamMateNotFollowedSuit = false;
					ArrayList<String> gameSuits = getCurrentRoundPlayedCardSuits(gameBean._roundBean.openCardsList);
					for(int j=0;j<gameSuits.size();j++)
					{
						String teamMatePlayedSuit = "nill";
						if(suit.equals(gameSuits.get(j)))
						{
							// Do nothing
							// continue;
						}
						else
						{
							if(j == tmPrbObj._playedcards.size())
							{
								teamMatePlayedSuit = getSuit(tmPrbObj._currentCard);								
							}
							else
							{
								teamMatePlayedSuit = getSuit(tmPrbObj._playedcards.get(j));
							}
						}
						if(teamMatePlayedSuit.equals(suit))
						{
							// dO NOTHING
						}
						else
						{
							isTeamMateNotFollowedSuit = true;
							break;
						}					
					}
				
					// Do you know that your Teammate did not follow the Suit at any previous round
							
					if(isTeamMateNotFollowedSuit)
						continue;
					else
					{
					
						ArrayList<Integer> opponentRoundSuitCards= new ArrayList<Integer>();
						for(int j=0;j<gameBean._players.size();j++)
						{
							if(gameBean._players.get(i).equals(tmPrbObj._playerId) || gameBean._players.get(i).equals(playerprbObj._playerId))
							{
								// Do nothing
							}
							else
							{
								ArrayList<Integer> knowncards = getRoundSuitCards(getTeammateKnownCards(gameBean, gameBean._players.get(j)), suit);
								for(int k=0;k<knowncards.size();k++)
								{
									opponentRoundSuitCards.add(knowncards.get(k));
								}
							}
						}
						ArrayList<Integer> teamRoundSuitCards = new ArrayList<Integer>();
						teamRoundSuitCards = getRoundSuitCards(getTeammateKnownCards(gameBean, tmPrbObj._playerId), suit);
						ArrayList<Integer> knowncards = getRoundSuitCards(playerprbObj._cards, suit);
						for(int k=0;k<knowncards.size();k++)
						{
							teamRoundSuitCards.add(knowncards.get(k));
						}
					
					
						// Written for SAN & not checked for HOKOM.
						teamRoundSuitCards = Commands.appInstance.ai.sortSunorder(teamRoundSuitCards);
						opponentRoundSuitCards = Commands.appInstance.ai.sortSunorder(opponentRoundSuitCards);
					
						if(opponentRoundSuitCards.size() == 0 || opponentRoundSuitCards.get(0)%8 == 0)
						{							
							// select suit as per AI-159
							finalSuit = outputSuitArray.get(i);
							break;
						}
						else 
						{
							ArrayList<Integer> newArray = opponentRoundSuitCards;
							for(int k=0;k<teamRoundSuitCards.size();k++)
							{
								newArray.add(teamRoundSuitCards.get(k));
							}				
						
							// Writter for SAN & not checked for HOKOM.
							newArray = Commands.appInstance.ai.sortSunorder(newArray);							
							if(opponentRoundSuitCards.get(0)  == newArray.get(0))
							{
								continue;
							}
							else
							{
								// select suit as per AI-159
								finalSuit = outputSuitArray.get(i);
								break;
							}
						}
					}
				}
			}
			if(finalSuit != null)
			{
				ArrayList<Integer> suitCards = getRoundSuitCards(playerprbObj._cards, finalSuit);
			
				suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
			
				if(suitCards.size()>0)
				{
					// Play the strongest card
					card = suitCards.get(suitCards.size()-1);				
				}
				else
				{
					// Play weakest card
					ArrayList<Integer> cards = getExcludeHokomCards(gameBean, playerprbObj._cards);
					cards = Commands.appInstance.ai.sortSunorder(cards);
					card = cards.get(cards.size()-1);				
				}				
			}		
			else 
			{
				card = playAIHokomCardSelectionR91(gameBean, player);
				
				/*if(gameBean._roundBean._doubleType.equals(Commands.CLOSED_DOUBLE))
				{		
					AppMethods.showLog("Closed Double block");
					AppMethods.showLog("Know all the hokom cards");
					card = playAIForHokomH80(player);			
				}
				else 
				{
					// have any hokom suit cards
					// R 91
					card = playAIHokomCardSelectionR91(player);						
				}*/			
			}
		}
		else
		{
			showLog("there is no priority 4 message");			
			// get the players team mate played the non hokom suit card as a playing mode
			Integer cardid = getTeammateNonHokomcardPlayedInHokomMode(gameBean, player);
			if(cardid != 0)
			{
				// Played that suit
				ArrayList<Integer> suitcards = getRoundSuitCards(playerprbObj._cards, getSuit(cardid));
				if(suitcards.size()>0)
				{
					//Play the strongest card
					suitcards = Commands.appInstance.ai.sortSunorder(suitcards);
					card = suitcards.get(0);
				}
				else
				{
					// R 91
					card = playAIHokomCardSelectionR91(gameBean, player);
				}
			}
			else
			{
				// Not Played any suit
				// have any hokom suit cards
				// R 91
				card = playAIHokomCardSelectionR91(gameBean, player);
			}
		}
		
		return card;
	}
	
	private Integer playAIHokomCardSelectionR91(GameBean gameBean, String player)
	{
		showLog("playAIHokomCardSelectionR91");
		Integer card = 0;
		
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		if(checkAnyHokomSuitCards(gameBean, player))
		{
			// having Hokom suit cards
			// 
			for(int i=0;i<Commands.appInstance.suits.size();i++)
			{
				if(gameBean._roundBean._trumpSuitType.equals(Commands.appInstance.suits.get(i)))
				{
					// Do nothing
				}
				else
				{				
					ArrayList<Integer> cards = getRoundSuitCards(prbObj._cards, Commands.appInstance.suits.get(i));
					{
						if(cards.size() == 1 && !isPlayedThatSuit(prbObj, Commands.appInstance.suits.get(i)))
						{
							card = cards.get(0);
						}
					}
					if(card != 0)
						break;
				}
			}
			
			if(card == 0)
			{
				// T 94  
				card = playAIHokomCardSelectionT94(gameBean, player);
			}
		}
		else
		{
			// not having hokom suit cards
			// T 94  
			card = playAIHokomCardSelectionT94(gameBean, player);
		}
		return card;
	}
	
	private Integer playAIHokomCardSelectionT94(GameBean gameBean, String player)
	{
		showLog("playAIHokomCardSelectionT94");
		Integer card = 0;		
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		prbObj._cards = Commands.appInstance.ai.sortSunorder(prbObj._cards);	
		// compare all the cards highest values and play the highest suit weakest card
		ArrayList<Integer> heartsCards = new ArrayList<Integer>();
		ArrayList<Integer> clubsCards = new ArrayList<Integer>();
		ArrayList<Integer> diamondsCards = new ArrayList<Integer>();
		ArrayList<Integer> spadesCards = new ArrayList<Integer>();
		
		for(int i=0;i<prbObj._cards.size();i++)
		{
			if(prbObj._cards.get(i)>=1 && prbObj._cards.get(i)<=8)
			{
				clubsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=9 && prbObj._cards.get(i)<=16)
			{
				diamondsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=17 && prbObj._cards.get(i)<=24)
			{
				heartsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=25 && prbObj._cards.get(i)<=32)
			{
				spadesCards.add(prbObj._cards.get(i));
			}
		}
		
		clubsCards = Commands.appInstance.ai.sortSunorder(clubsCards);
		diamondsCards = Commands.appInstance.ai.sortSunorder(diamondsCards);
		heartsCards = Commands.appInstance.ai.sortSunorder(heartsCards);
		spadesCards = Commands.appInstance.ai.sortSunorder(spadesCards);
		
		
		
		ArrayList<Integer> highestcards = new ArrayList<Integer>();
		Integer n1 = 0;
		Integer n2 = 0;
		if(clubsCards.size()>=2)
		{
			if(gameBean._roundBean._trumpSuitType.equals("clubs")){
				// donothing
				n1=1;n2=8;
			}
			else
				highestcards.add(clubsCards.get(0));	
		}		
		
		if(diamondsCards.size()>=2)
		{
			if(gameBean._roundBean._trumpSuitType.equals("diamonds")){		
			// donothing
				n1=9;n2=16;
			}
			else
				highestcards.add(diamondsCards.get(0));
		}
		if(heartsCards.size()>=2)
		{
			if(gameBean._roundBean._trumpSuitType.equals("hearts")){
			// donothing
			n1=17;n2=24;
			}
			else
				highestcards.add(heartsCards.get(0));
		}
		if(spadesCards.size()>=2)
		{
			if(gameBean._roundBean._trumpSuitType.equals("spades")){
				// donothing
				n1=25;n2=32;
			}
			else
				highestcards.add(spadesCards.get(0));
		}
		
		String suit = "null";		
		if(highestcards.size()>1)
		{
			// Check highest suit
			highestcards = Commands.appInstance.ai.sortSunorder(highestcards); 	
			suit = getSuit(highestcards.get(0));
		}
		else if(highestcards.size() == 1)
		{
			suit = getSuit(highestcards.get(0));			
		}
			
		if(suit.equals("null"))
		{
			// pick the weakest card of non hokom suit
			ArrayList<Integer> cards = new ArrayList<Integer>();
			for(int i=0;i<prbObj._cards.size();i++)
			{
				if(prbObj._cards.get(i)>=n1 && prbObj._cards.get(i)<=n2)
				{
					// donothing
				}
				else
					cards.add(prbObj._cards.get(i));
				
			}
			cards = Commands.appInstance.ai.sortSunorder(cards);
			card = cards.get(cards.size()-1);
		}
		else
		{
			// pick the suit weakest card
			if(suit.equals("clubs"))
				card = clubsCards.get(clubsCards.size()-1);
			else if(suit.equals("diamonds"))
				card = diamondsCards.get(diamondsCards.size()-1);
			else if(suit.equals("hearts"))
				card = heartsCards.get(heartsCards.size()-1);
			else if(suit.equals("spades"))
				card = spadesCards.get(spadesCards.size()-1);
		}		
		return card;
	}
	
	public boolean checkYouHaveStrongestHokomSuitCardUsedInthisRound(ArrayList<Integer> suitCards, ArrayList<Integer> openCards)
	{
		Integer card = 0;
		suitCards = Commands.appInstance.ai.sortHokomorder(suitCards);
		for(int i=suitCards.size()-1;i>=0;i--)
		{
			openCards.add(suitCards.get(i));
			//showLog(sendCards);
			openCards = Commands.appInstance.ai.sortHokomorder(openCards);
			
			for(int j=0;j<suitCards.size();j++)
			{
				if(openCards.get(0) == suitCards.get(j))
				{
					card = suitCards.get(j);
					break;
				}
			}
			if(card != 0)
				break;
		}
		
		if(card != 0)
			return true;		
		return false;
	}
	
	public Integer getLeastStrongestPossible(ArrayList<Integer> suitCards , ArrayList<Integer> openCards)
	{
		Integer card = 0;
		suitCards = Commands.appInstance.ai.sortHokomorder(suitCards);
		for(int i=suitCards.size()-1;i>=0;i--)
		{
			openCards.add(suitCards.get(i));
			//showLog(sendCards);
			openCards = Commands.appInstance.ai.sortHokomorder(openCards);
			
			for(int j=0;j<suitCards.size();j++)
			{
				if(openCards.get(0) == suitCards.get(j))
				{
					card = suitCards.get(j);
					break;
				}
			}
			if(card != 0)
				break;
		}
		
		if(card == 0)
			card = suitCards.get(suitCards.size()-1);
		
		return card;		
	}
	
	// Play the weakest hokom possible
	public Integer getWeakestHokomPossible(ArrayList<Integer> suitCards , ArrayList<Integer> openCards)
	{
		Integer card = 0;
		suitCards = Commands.appInstance.ai.sortHokomorder(suitCards);
		for(int i=suitCards.size()-1;i>=0;i--)
		{
			openCards.add(suitCards.get(i));
			//showLog(sendCards);
			openCards = Commands.appInstance.ai.sortHokomorder(openCards);
			
			for(int j=0;j<suitCards.size();j++)
			{
				if(openCards.get(0) == suitCards.get(j))
				{
					card = suitCards.get(j);
					break;
				}
			}
			if(card != 0)
				break;
		}
		
		if(card == 0)
			card = suitCards.get(suitCards.size()-1);
		
		return card;		
	}
	
	
	private boolean isPlayedThatSuit(PlayerRoundBean prbobj, String suit)
	{
		boolean sign = false;
		for(int i=0;i<prbobj._playedcards.size();i++)
		{
			if(getSuit(prbobj._playedcards.get(i)).equals(suit))
			{
				sign = true;
				break;
			}
		}
		return sign;
	}
	
	private Integer getTeammateNonHokomcardPlayedInHokomMode(GameBean gameBean, String player)
	{
		Integer card = 0;
		// Get Team mates cards 
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(getPartner(gameBean, player));
		for(int i=0;i<prbObj._firstPlayedCard.size();i++)
		{
			if(getSuit(prbObj._firstPlayedCard.get(i)).equals(gameBean._roundBean._trumpSuitType))
			{
				// Do nothing 
			}
			else
			{
				card = prbObj._firstPlayedCard.get(i);
				break;
			}
		}		
		return card;
	}
	
	
	/*private String getSuitType(String suit)
	{
		if(suit.equals("clubs"))
			return "spades";
		else if(suit.equals("spades"))
			return "clubs";
		else if(suit.equals("diamonds"))
			return "hearts";
		else if(suit.equals("hearts"))
			return "diamonds";
		
		return null;
	}*/
	
	
	private ArrayList<Integer> getTeammatestStrongestCards(ArrayList<Integer> teammatecards, ArrayList<Integer> strongestCards)
	{
		ArrayList<Integer> cards = new ArrayList<Integer>();
			
		for(int i=0;i<teammatecards.size();i++)
		{
			for(int j=0;j<strongestCards.size();j++)
			{
				if(teammatecards.get(i) == strongestCards.get(j))
				{
					cards.add(teammatecards.get(i));
					break;
				}
			}			
		}		
		return cards;
	}
	
	private boolean checkOpponentNotPlayedHokomSuitCardsInLastHokomSuitRound(GameBean gameBean, String player)
	{		
		showLog("checkOpponentNotPlayedHokomSuitCardsInLastHokomSuitRound");
		
		boolean played = false;
		
		if(gameBean._roundBean._noofHokomSuitTricks == 0)			
		{
			return false;
		}
		else
		{			
			for(int i=0;i<gameBean.maxPlayers;i++)
			{
				if(gameBean._players.get(i).equals(player) || gameBean._players.get(i).equals(getPartner(gameBean, player)))
				{
					// Do nothing
				}
				else
				{
					showLog("Size"+gameBean._roundBean._lastHokomSuitTrick.size());
					PlayedCard pc1 = new PlayedCard();
					PlayedCard pc2 = new PlayedCard();
					for(int j=0;j<gameBean._roundBean._lastHokomSuitTrick.size();j++)
					{
						showLog(gameBean._roundBean._lastHokomSuitTrick.get(j)._playerId);
						
						
						if(gameBean._players.get(i).equals(gameBean._roundBean._lastHokomSuitTrick.get(j)._playerId))
						{
							pc1 = gameBean._roundBean._lastHokomSuitTrick.get(j);
							for(int k=0;k<gameBean._roundBean._lastHokomSuitTrick.size();k++)
							{
								if(getPartner(gameBean, gameBean._players.get(i)).equals(gameBean._roundBean._lastHokomSuitTrick.get(k)._playerId))
								{
									pc2 = gameBean._roundBean._lastHokomSuitTrick.get(k);
								}
							}
						}											
					}	
					
					if(gameBean._roundBean._trumpSuitType.equals(gameBean._roundBean.getSuit(pc1._cardid)) || gameBean._roundBean._trumpSuitType.equals(gameBean._roundBean.getSuit(pc2._cardid)))
					{
						played = false;
						showLog("Bool1"+player);
					}					
					else
					{
						played = true;				
						showLog("Bool2"+player);
					}
					break;
				}			
			}		
			
			if(gameBean._roundBean._noofHokomSuitTricks >= 2)
				played = true;			
		}	
		showLog("Bool"+player);
		return played;
	}
	
	private ArrayList<Integer> getTeammateKnownCards(GameBean gameBean, String player)
	{
		// Get the team mate known cards
		ArrayList<Integer> knowncards = getBonusWinCards(gameBean, getPartner(gameBean, player));
		if(gameBean._roundBean._openCardTaker.equals(player))
		{
			knowncards.add(gameBean._roundBean._openCardId);
		}		
		return knowncards;
	}
	
	private boolean checkPlayerMadeHokomDecision(GameBean gameBean, String player)
	{
		if(gameBean._roundBean._gameType.equals(Commands.HOKOM) && gameBean._roundBean._bidWonPerson.equals(player))
			return true;
		return false;
	}
	
	private boolean checkAnyHokomSuitCards(GameBean gameBean, String player)
	{
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		for(int i=0;i<prbObj._cards.size();i++)
		{
			if(getSuit(prbObj._cards.get(i)).equals(gameBean._roundBean._trumpSuitType))
			{
				return true;
			}
		}
		return false;
	}
	
	private boolean checkAllCardsAreHokomCards(GameBean gameBean, String player)
	{
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		ArrayList<Integer> suitCards = getRoundSuitCards(prbObj._cards, gameBean._roundBean._trumpSuitType);
		if(suitCards.size() == prbObj._cards.size())
			return true;		
		else 
			return false;
	}
	
	
	/** HOKOM Functions Ending*/
	
	
	
	/** SAN Function Starting */
	//public Integer playAISan(){
	public Integer playAISan(GameBean gameBean, String player)
	{	
		Integer card = 0;		
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		PlayerRoundBean tmprbObj = gameBean._roundBean.getPlayerRoundBeanObject(getPartner(gameBean, player));
		
		ArrayList<Integer> playercards = prbObj._cards;
		showLog("*****************  Play AI SAN Player "+player+    "**********************");
		//showLog("Player Cards"+playercards);
		
		//showLog("Opencard Size "+gameBean._roundBean._openCard.size());
		
		if(prbObj._cards.size() == 1)
		{
			card = prbObj._cards.get(0);
		}
		else if(gameBean._roundBean.openCardsList.size() == 0)
		{
			// Playing Mode
			showLog("Playing Mode");
			// Check for p1 message 
			// Check partners message
			
			Integer no = checkPeriorityMessage(tmprbObj, 1); 
			if( no != -1)
			{
				// Message
				showLog(" P1 Message");
				ArrayList<Integer> suitCards = getRoundSuitCards(playercards, tmprbObj._hisMessagesForTeammate.get(no)._messageCardSuit);
				suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
				if(suitCards.size()>0)
				{
					// Having Same Suit Cards
					showLog("Having Same Suit Cards");
					// Play Strongest Card of that suit
					card = suitCards.get(0);
				}
				else
				{
					// Not Having Same Suit Cards
					showLog("Not Having Same Suit Cards");
					 card = getStrongestCardRemainingCards(gameBean, player, Commands.SAN);
				}
			}
			else
			{
				// No Message
				showLog(" No P1 Message");
				card = getStrongestCardRemainingCards(gameBean, player, Commands.SAN);
			}			
		}
		else
		{
			// Following mode
			showLog(" Following mode");			
			ArrayList<Integer> suitCards = getRoundSuitCards(playercards, gameBean._roundBean.currentSuit);		
			if(suitCards.size() == 0)
			{
				showLog("No Round Suit Cards");
				
				if(playercards.size() == 1)
				{
					card = playercards.get(0);
				}
				else
				{
					// Is Team mate playing Before
					if(teammateBeforeOrNot(gameBean))
					{
						showLog("playing before");
						// check partner before are not					
						
						if(tmprbObj._currentCard == getStrongestCard(gameBean, gameBean._roundBean.currentSuit, Commands.SAN))
						{
							// Used strongest Card
							showLog("Used strongest Card");
							// Do u have any 10
							// 3
							card = playAICardSelectionThird(gameBean,player);
						}
						else
						{
							// Not Used strongest Card
							showLog("Not Used strongest Card");					 
							// 4
							card = playAICardSelectionFourth(gameBean,player);
						}
					}
					else
					{
						showLog("Not Playing before");			
						// Check your team mate having strongest card
						
						// Get the teammates card
						// Check his team mate said bonus and having highest cards
						ArrayList<Integer> bonuscards = getBonusWinCards(gameBean, getPartner(gameBean, player));
						
						//showLog("Before "+bonuscards);
						
						if(gameBean._roundBean._openCardTaker.equals(getPartner(gameBean, player)))
						{
							bonuscards.add(gameBean._roundBean._openCardId);
						}
						//showLog("After "+bonuscards);
						
						if(playercards.size()>0)
						{
							showLog("Having cards");
							bonuscards = getRoundSuitCards(bonuscards, gameBean._roundBean.currentSuit);
							//showLog(bonuscards.toString());
							if(bonuscards.size()>0)
							{
								//Having suit Cards
								showLog("Having suit Cards");
								// Check the highest suit Card is with him or not
								bonuscards = Commands.appInstance.ai.sortSunorder(bonuscards);
								if(getStrongestCard(gameBean, gameBean._roundBean.currentSuit, Commands.SAN) == bonuscards.get(0))
								{
									// TM Having Highest Card
									showLog("TM Having Highest Card");
									// 3
									card = playAICardSelectionThird(gameBean,player);
								}
								else
								{
									showLog("TM Not Having Highest Card");
									// Check Do U Have any A,10,K
									// 4
									card = playAICardSelectionFourth(gameBean,player);								
								}
							}
							else
							{
								// No suit Cards
								showLog("No suit Cards");
								// 4
								card = playAICardSelectionFourth(gameBean,player);
							}
						}
						else
						{
							showLog("Not Having cards");
							// 4
							card = playAICardSelectionFourth(gameBean,player);
						}						
					}
				}
				
			}
			else
			{
				showLog("Round Suit Cards");
				if(suitCards.size()== 1)
				{
					card = suitCards.get(0);
				}
				else
				{
					showLog("Check is First Round Or Second Round");
					if(gameBean._roundBean.trickCount == 0)
					{
						showLog("First Round");
						//showLog(teammateBeforeOrNot(player));
						
						if(checkAceAndBonus(gameBean, player))
						{
							// Play Strongest
							showLog("Play Strongest");
							suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
							card = suitCards.get(0);
						}
						else							
						{
							showLog("playAIForSANF172");
							// F172
							card = playAIForSANF172(gameBean,player);
						}
					}
					else
					{
						showLog("Not First Round");
						// F172
						card = playAIForSANF172(gameBean,player);
					}					
				}			
			}
		
		}
		
		
		if(card == 0)
		{
			showLog("San Card Selection Failed Check Again");			
			ArrayList<Integer> suitCards = getRoundSuitCards(playercards, gameBean._roundBean.currentSuit);	
			if(suitCards.size()>0)
			{
				card = suitCards.get(suitCards.size()-1);
			}
			else
			{			
				// Play weakest
				prbObj._cards = Commands.appInstance.ai.sortSunorder(prbObj._cards);
				card = prbObj._cards.get(prbObj._cards.size()-1);
			}
		}
		showLog("Player Cards "+prbObj._cards);
		showLog("Final Card "+ card);		
		return card;
		
	}// Play AI SAN Main Function
	
	private Integer getLeastStrongestToWinRoundFromCards(GameBean gameBean, ArrayList<Integer> suitCards)
	{
		int card =0;
		suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
		
		ArrayList<Integer> openCardsArray = new ArrayList<Integer>();
		for(int i=0;i<gameBean._roundBean.openCardsList.size();i++)
		{
			openCardsArray.add(gameBean._roundBean.openCardsList.get(i));
		}
		
		for (int i=suitCards.size()-1; i>=0; i--)
		{
		  
			openCardsArray.add(suitCards.get(i));
			openCardsArray = Commands.appInstance.ai.sortSunorder(openCardsArray);
		  
			if(openCardsArray.get(0) == suitCards.get(i))
			{
				// Play least Stronges card
				card = suitCards.get(i);
				break;
			}
			else
			{
				openCardsArray.remove(suitCards.get(i));
			}		  
		}		
		return card;				
	}
	
	private Integer playAIForSANF172(GameBean gameBean, String player)
	{
		Integer card =0;
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		ArrayList<Integer> suitCards = getRoundSuitCards(prbObj._cards, gameBean._roundBean.currentSuit);	
		// have strongestCard
		showLog("have strongestCard");
		if(haveStrongestCard(gameBean,gameBean._roundBean.currentSuit,suitCards, Commands.SAN))
		{
			if(gameBean._roundBean.openCardsList.size()==3)
			{
				card = getLeastStrongestToWinRoundFromCards(gameBean, suitCards);
			}
			else
			{
				// Play Strongest
				showLog("Play Strongest");
				suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
				card = suitCards.get(0);
			}
		}
		else
		{
			if(teammateBeforeOrNot(gameBean))
			{
				showLog("Before Playerd"); 
				// Check partner before played
				PlayerRoundBean tmPrbobj = gameBean._roundBean.getPlayerRoundBeanObject(getPartner(gameBean, player));
				
				
				if(tmPrbobj._currentCard == getStrongestCard(gameBean, gameBean._roundBean.currentSuit, Commands.SAN))
				{
					// Used strongest Card
					showLog("Used strongest Card");
					if(haveSecondStrongestCard(gameBean,gameBean._roundBean.currentSuit, suitCards, Commands.SAN))
					{
						// Having Second Strongest card
						showLog("Having Second Strongest card");
						//Play Weakest card
						suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
						card = suitCards.get(suitCards.size()-1);
					}
					else
					{
						// Not Having Second Strongest card
						showLog("Not Having Second Strongest card");
						//Play Strongest card
						suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
						card = suitCards.get(0);
					}
				}
				else
				{
					// Not Used strongest card					
					//showLog("Not Used strongest card");
					showLog("playAIForSANJ184");
					// J 184
					card = playAIForSANJ184(gameBean,player, suitCards);
				}			
				
			}
			else
			{
				//showLog("Not Before Playerd");
				showLog("playAIForSANJ184");
				// J 184
				card = playAIForSANJ184(gameBean,player, suitCards);
			}			
		}
		return card;
	}
	
	private Integer playAIForSANJ184(GameBean gameBean, String player, ArrayList<Integer>suitCards)
	{
		showLog("playAIForSANJ184");
		Integer card = 0;		
		
		// Get the teammates card
		// Check his team mate said bonus and having highest cards
		ArrayList<Integer> cards = getBonusWinCards(gameBean,getPartner(gameBean,player));
		
		//showLog("Before "+cards);
		
		if(gameBean._roundBean._openCardTaker.equals(getPartner(gameBean,player)))
		{
			cards.add(gameBean._roundBean._openCardId);
		}
		//showLog("After "+cards);
		
		if(cards.size()>0)
		{
			showLog("Having cards");
			cards = getRoundSuitCards(cards, gameBean._roundBean.currentSuit);
			showLog(cards.toString());
			if(cards.size()>0)
			{
				//Having suit Cards
				showLog("Having suit Cards");
				// Check the highest suit Card is with him or not
				cards = Commands.appInstance.ai.sortSunorder(cards);
				if(getStrongestCard(gameBean,gameBean._roundBean.currentSuit, Commands.SAN) == cards.get(0))
				{
					// TM Having Highest Card
					showLog("TM Having Highest Card");
					// Check for second Highest
					if(haveSecondStrongestCard(gameBean,gameBean._roundBean.currentSuit, suitCards, Commands.SAN))
					{
						showLog("Having second Highest Card");
						//Play Strongest card
						suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
						card = suitCards.get(0);
					}
					else
					{
						showLog("Not Having second Highest Card");
						card = playAIForSANL188(gameBean, player, suitCards);
						if(card == 0)
						{
							//Play Weakest card
							showLog("Play weakest card");
							suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
							card = suitCards.get(suitCards.size()-1);
						}
					}
				}
				else
				{
					showLog("TM Not Having Highest Card");
					card = playAIForSANL188(gameBean, player, suitCards);
					if(card == 0)
					{
						//Play Weakest card
						showLog("Play weakest card");
						suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
						card = suitCards.get(suitCards.size()-1);
					}
				}
			}
			else
			{
				// No suit Cards
				showLog("No suit Cards");
				//Play Weakest card
				showLog("Play weakest card");
				suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
				card = suitCards.get(suitCards.size()-1);
			}
		}
		else
		{
			showLog("Not Having cards");
			//Play Weakest card
			/** Changed the flow from eyad */
			
			// Check r u the last player
			if(gameBean._roundBean.openCardsList.size() == 3)
			{
				showLog("last user");
				suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
				ArrayList<Integer> openCards = new ArrayList<Integer>();
				for(int i=0;i<gameBean._roundBean.openCardsList.size();i++)
				{
					openCards.add(gameBean._roundBean.openCardsList.get(i));
				}
				openCards.add(suitCards.get(0));
				openCards = Commands.appInstance.ai.sortSunorder(openCards);
				if(suitCards.get(0) == openCards.get(0))
				{
					// Play Strongest card
					suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
					card = suitCards.get(0);
				}
				else
				{
					// Play weakest card
					suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
					card = suitCards.get(suitCards.size()-1);
				}
			}
			else
			{
				//showLog("not last user");
				//showLog("Play weakest card");
				suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
				card = suitCards.get(suitCards.size()-1);
			}
			
			/*showLog("Play weakest card");
			suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
			card = suitCards.get(suitCards.size()-1);*/
		}		
		return card;
	}
	
	private Integer playAIForSANL188(GameBean gameBean, String player, ArrayList<Integer>suitCards)
	{
		ArrayList<Integer> suitOpenCardsArray = getRoundSuitCards(gameBean._roundBean.openCardsList, gameBean._roundBean.currentSuit);
		suitOpenCardsArray = Commands.appInstance.ai.sortSunorder(suitOpenCardsArray);
		suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
		 
		 
		 // Check if you are last player
		 if(gameBean._roundBean.openCardsList.size() == 3)
		 {		  
			 if(suitCards.size() != 0)
				 suitOpenCardsArray.add(suitCards.get(0));
			  
			 suitOpenCardsArray = Commands.appInstance.ai.sortSunorder(suitOpenCardsArray);
			  
			 if(suitOpenCardsArray.get(0) == suitCards.get(0))
			 {		   
				 return suitCards.get(0);					  
			 }
			 else 
			 {
				 return suitCards.get(suitCards.size()-1);
			 }
		 }
		 else
		 {
			 if(suitCards.size() == 1)
			 {
				 return suitCards.get(0);
			 }
			 else if(suitCards.size() == 2)
			 {
				 return suitCards.get(suitCards.size()-1);
			 }
			 else 
			 {
			   return suitCards.get(suitCards.size()-2);
			 }
		 }
	}
	
	
	private ArrayList<Integer> getStrongestCards(GameBean gameBean, String type)
	{
		ArrayList<Integer> strongestCards = new ArrayList<Integer>();
		Integer no = getStrongestCard(gameBean,"hearts", type);
		if(no != -1)
		{
			strongestCards.add(no);
		}
		no = getStrongestCard(gameBean,"clubs", type);
		if(no != -1)
		{
			strongestCards.add(no);
		}
		no = getStrongestCard(gameBean,"diamonds", type);
		if(no != -1)
		{
			strongestCards.add(no);
		}
		no = getStrongestCard(gameBean,"spades", type);
		if(no != -1)
		{
			strongestCards.add(no);
		}
		return strongestCards;

	}
	
	private ArrayList<Integer> getSecondStrongestCards(GameBean gameBean, String type)
	{
		ArrayList<Integer> strongestCards = new ArrayList<Integer>();
		Integer no = getSecondStrongestCard(gameBean,"hearts", type);
		if(no != -1)
		{
			strongestCards.add(no);
		}
		no = getSecondStrongestCard(gameBean,"clubs", type);
		if(no != -1)
		{
			strongestCards.add(no);
		}
		no = getSecondStrongestCard(gameBean,"diamonds", type);
		if(no != -1)
		{
			strongestCards.add(no);
		}
		no = getSecondStrongestCard(gameBean,"spades", type);
		if(no != -1)
		{
			strongestCards.add(no);
		}
		return strongestCards;

	}
	
	
	private Integer getStrongestCardRemainingCards(GameBean gameBean, String player, String type)
	{
		showLog(" getStrongestCardRemainingCards");
		Integer card =0;	
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		ArrayList<Integer> strongestCards = getStrongestCards(gameBean,type);		
		for(int i= 0;i<strongestCards.size();i++)
		{
			boolean sign = false;
			for(int j=0;j<prbObj._cards.size();j++)
			{
				if(strongestCards.get(i) == prbObj._cards.get(j))
				{
					sign = true;
					card = strongestCards.get(i);
					break;
				}
			}
			if(sign)
			{				
				//showLog("Play the strongest card");
				break;
			}
		}
		
		if(card == 0)
		{
			// Check for p2 Message
			String suit = checkPeriorityTwoMessage(gameBean,player); 
			if( suit.equals("null"))
			{
				// Not having p2 Message
				showLog("Not Having P2 Message");
				
				// Check for p3 message
				
				//card = playPeriorityThreeMessage(player);
				card = checkISFirstRound(gameBean,player);
				
			}
			else
			{
				
				// Having P2 Message 
				showLog("Having P2 Message");
				// Do u have any card of that suit
				ArrayList<Integer> suitCards = getRoundSuitCards(prbObj._cards, suit);
				suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
				if(suitCards.size()>0)
				{
					// Having Same Suit Cards
					showLog("Having Same Suit Cards");
					// Play Strongest Card of that suit
					card = suitCards.get(0);
				}
				else
				{
					// Not Having Same Suit Cards
					showLog("Not Having Same Suit Cards");
					// Check for p3 message					
					//card = playPeriorityThreeMessage(player);
					card = checkISFirstRound(gameBean,player);
				}				 
			}
		}		
		return card;
	}
	
	private Integer playPeriorityThreeMessage(GameBean gameBean,String player)
	{
		showLog(" playPeriorityThreeMessage");
		Integer card = 0;
		// Checking for partners message
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(getPartner(gameBean,player));
		PlayerRoundBean playerPrbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		
		
		Integer no = checkPeriorityMessage(prbObj,3); 
		if( no != -1)
		{
			// Message
			showLog(" P3 Message");
			MessageBean messageBean = new MessageBean();
			messageBean = prbObj._hisMessagesForTeammate.get(no);
			
			// Check opponent is having any strong card
			// Opponents bonus cards
			
			ArrayList<Integer> opponentsBonusCards =  new ArrayList<Integer>();
			for(int i=0;i<gameBean._players.size();i++)
			{
				if(gameBean._players.get(i).equals(player) || gameBean._players.get(i).equals(getPartner(gameBean,player)))
				{
					// Not opponents
				}
				else
				{
					ArrayList<Integer> cards = getBonusWinCards(gameBean,gameBean._players.get(i));
					for(int j=0;j<cards.size();j++)
					{
						opponentsBonusCards.add(cards.get(j));
					}						
				}
			}
			
			if(gameBean._roundBean._openCardTaker.equals(player) || gameBean._roundBean._openCardTaker.equals(getPartner(gameBean,player)))
			{
				// Not opponents
			}
			else
			{
				opponentsBonusCards.add(gameBean._roundBean._openCardId);
			}
			
			
			Integer strongestCard = getStrongestCard(gameBean,messageBean._message, Commands.SAN);
			
			boolean sign = false;			
			for(int j=0;j<opponentsBonusCards.size();j++)
			{
				if(strongestCard == opponentsBonusCards.get(j))
				{
					sign = true;						 
					break;
				}					
			}
				
			
			if(sign)
			{
				// Yes Having Strong Card
				// Check p4 Message
				card = playPeriourityFourMessage(gameBean,player);
			}
			else
			{
				// Not having Strongest Card
				ArrayList<Integer> suitCards = getRoundSuitCards(playerPrbObj._cards, prbObj._hisMessagesForTeammate.get(no)._messageCardSuit);
				suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
				if(suitCards.size()>0)
				{
					// Having Same Suit Cards
					showLog("Having Same Suit Cards");
					// Play Strongest Card of that suit
					card = suitCards.get(0);
				}
				else
				{
					// Not Having Same Suit Cards
					showLog("Not Having Same Suit Cards");
					// Check p4 Message
					card = playPeriourityFourMessage(gameBean, player);
				}
			}		
		}
		else
		{
			// No Message
			showLog(" No P3 Message");
			// Check p4 message
			card = playPeriourityFourMessage(gameBean,player);
			
		}		
		return card;
	}
	
	private Integer playPeriourityFourMessage(GameBean gameBean, String player)
	{
		Integer card = 0;	
		// Check for partners message
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(getPartner(gameBean,player));	
		PlayerRoundBean playerprbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		
		Integer no = checkPeriorityMessage(prbObj,4);
		
		if( no != -1)
		{
			// P4 Message
			showLog("P4 Message");
			// Check for no of p4 Messages
			Integer count = 0;
			String suit = "";
			ArrayList<String> suitArray = new ArrayList<String>();
			
			for(int i=0;i<prbObj._hisMessagesForTeammate.size();i++)
			{
				if(prbObj._hisMessagesForTeammate.get(i)._messagePeriority == 4)
				{
					count++;
				}
			}
			
			showLog("Messages Count "+count);
								
			
			if(count >=2)
			{
				// More Messages
				MessageBean firstMessageBean = new MessageBean();
				MessageBean secondMessageBean = new MessageBean();
				int messageCount = 0;
				for(int i=0;i<prbObj._hisMessagesForTeammate.size();i++)
				{
					if(prbObj._hisMessagesForTeammate.get(i)._messagePeriority == 4)
					{
						if(messageCount == 0)
						{
							firstMessageBean = prbObj._hisMessagesForTeammate.get(i);
							messageCount++;
						}
						else
						{
							secondMessageBean = prbObj._hisMessagesForTeammate.get(i);
							messageCount++;
						}
					}
					if(messageCount == 2)
						break;
				}
				
				if(firstMessageBean._messageCardSuit.equals(secondMessageBean._messageCardSuit))
				{
					showLog("Equal Suits");
					// Equal suits
					ArrayList<Integer> cards = new ArrayList<Integer>();
					Integer num = firstMessageBean._messageCard%8;
					if(num == 0)
						num = 8;
					cards.add(num);
					num = secondMessageBean._messageCard%8;
					if(num == 0)
						num = 8;
					cards.add(num);
					cards = Commands.appInstance.ai.sortSunorder(cards);
					
					num = firstMessageBean._messageCard%8;
					
					showLog("Cards To Order"+ cards);
					
					if(num == 0)
						num = 8;
					
					if(cards.get(0) != num)
					{
						
						// increasing
						// Same Suit
						suit = secondMessageBean._messageCardSuit;
						
					}
					else
					{
						// decreasing
						// Same color other suit
						if(secondMessageBean._messageCardSuit.equals("clubs"))
							suit = "spades";
						else if(secondMessageBean._messageCardSuit.equals("spades"))
							suit = "clubs";
						else if(secondMessageBean._messageCardSuit.equals("diamonds"))
							suit = "hearts";
						else if(secondMessageBean._messageCardSuit.equals("hearts"))
							suit = "diamonds";						
					}					
					suitArray.add(suit);
					
				}
				else
				{
					showLog("Different  Suits");
					
					if((firstMessageBean._messageCardSuit.equals("diamonds") && secondMessageBean._messageCardSuit.equals("hearts")) ||
						(firstMessageBean._messageCardSuit.equals("hearts") && secondMessageBean._messageCardSuit.equals("diamonds")) ||	
						(firstMessageBean._messageCardSuit.equals("clubs") && secondMessageBean._messageCardSuit.equals("spades")) ||
						(firstMessageBean._messageCardSuit.equals("spades") && secondMessageBean._messageCardSuit.equals("clubs")))
					{
						
						showLog("Same color Opposit suit");
						
						if(firstMessageBean._messageCardSuit.equals("clubs"))
						{
							if(getRoundSuitCards(playerprbObj._cards, "diamonds").size()>0)						
								suit = "diamonds";
							else 
								suit = "hearts";
							
						}
						else if(firstMessageBean._messageCardSuit.equals("spades"))
						{
							if(getRoundSuitCards(playerprbObj._cards, "diamonds").size()>0)						
								suit = "diamonds";
							else 
								suit = "hearts";
							
						}
						else if(firstMessageBean._messageCardSuit.equals("diamonds"))
						{
							if(getRoundSuitCards(playerprbObj._cards, "clubs").size()>0)						
								suit = "clubs";
							else 
								suit = "spades";
							
						} 
						else if(firstMessageBean._messageCardSuit.equals("hearts"))
						{
							if(getRoundSuitCards(playerprbObj._cards, "clubs").size()>0)						
								suit = "clubs";
							else 
								suit = "spades";							
						}						
					}
					else
					{
						showLog("Different color Opposit suit");
						
						if(firstMessageBean._messageCardSuit.equals("clubs"))
						{
							if(getRoundSuitCards(playerprbObj._cards, "spades").size()>0)	
							{
								suit = "spades";								
							}
							else if(secondMessageBean._messageCardSuit.equals("diamonds"))
							{
								if(getRoundSuitCards(playerprbObj._cards, "hearts").size()>0)						
									suit = "hearts";
								else
									suit = "diamonds";								
							}
							else if(secondMessageBean._messageCardSuit.equals("hearts"))
							{
								if(getRoundSuitCards(playerprbObj._cards, "diamonds").size()>0)						
									suit = "diamonds";
								else
									suit = "hearts";								
							}							
						}
						else if(firstMessageBean._messageCardSuit.equals("spades"))
						{
							if(getRoundSuitCards(playerprbObj._cards, "clubs").size()>0)	
							{
								suit = "clubs";								
							}
							else if(secondMessageBean._messageCardSuit.equals("diamonds"))
							{
								if(getRoundSuitCards(playerprbObj._cards, "hearts").size()>0)						
									suit = "hearts";
								else
									suit = "diamonds";
							}
							else if(secondMessageBean._messageCardSuit.equals("hearts"))
							{
								if(getRoundSuitCards(playerprbObj._cards, "diamonds").size()>0)						
									suit = "diamonds";
								else
									suit = "hearts";
							}							
						}
						else if(firstMessageBean._messageCardSuit.equals("diamonds"))
						{
							if(getRoundSuitCards(playerprbObj._cards, "hearts").size()>0)						
								suit = "hearts";
							else if(secondMessageBean._messageCardSuit.equals("spades"))
							{
								if(getRoundSuitCards(playerprbObj._cards, "clubs").size()>0)						
									suit = "clubs";
								else
									suit = "spades";
							}
							else if(secondMessageBean._messageCardSuit.equals("clubs"))
							{
								if(getRoundSuitCards(playerprbObj._cards, "spades").size()>0)						
									suit = "spades";
								else
									suit = "clubs";
							}
												
						}
						else if(firstMessageBean._messageCardSuit.equals("hearts"))
						{
							if(getRoundSuitCards(playerprbObj._cards, "diamonds").size()>0)						
								suit = "diamonds";
							else if(secondMessageBean._messageCardSuit.equals("spades"))
							{
								if(getRoundSuitCards(playerprbObj._cards, "clubs").size()>0)						
									suit = "clubs";
								else
									suit = "spades";
							}
							else  if(secondMessageBean._messageCardSuit.equals("clubs"))
							{
								if(getRoundSuitCards(playerprbObj._cards, "spades").size()>0)						
									suit = "spades";
								else
									suit = "clubs";
							}												
						}						
					}
				}
				suitArray.add(suit);
			}
			else
			{
				// Single Message
				showLog("Single Message");
				
				
				
				if(prbObj._hisMessagesForTeammate.get(no)._message.equals("OCM"))
				{
					/*if(prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("clubs"))					
						suit = "spades";					
					else if (prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("spades"))
						suit = "clubs";
					else if (prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("hearts"))
						suit = "diamonds";
					else if (prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("diamonds"))
						suit = "hearts";*/
					
					// New Enhancement from eyad
					// Code changed as per the Eyad
					
					if(prbObj._hisMessagesForTeammate.get(no)._messageCardSuit.equals("spades"))
					{
						if(prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("clubs"))
						{
							suitArray.add("hearts");
							suitArray.add("diamonds");
						}
						else if(prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("diamonds"))
						{
							suitArray.add("clubs");
							suitArray.add("hearts");
						}
						else if(prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("hearts"))
						{
							suitArray.add("clubs");
							suitArray.add("diamonds");
						}						
					}
					else if(prbObj._hisMessagesForTeammate.get(no)._messageCardSuit.equals("clubs"))
					{
						if(prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("spades"))
						{							
							suitArray.add("diamonds");
							suitArray.add("hearts");
						}
						else if(prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("diamonds"))
						{
							suitArray.add("spades");
							suitArray.add("hearts");
						}
						else if(prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("hearts"))
						{
							suitArray.add("spades");
							suitArray.add("diamonds");
						}						
					}
					else if(prbObj._hisMessagesForTeammate.get(no)._messageCardSuit.equals("hearts"))
					{
						if(prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("diamonds"))
						{							
							suitArray.add("clubs");
							suitArray.add("spades");
						}
						else if(prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("spades"))
						{
							suitArray.add("diamonds");
							suitArray.add("clubs");
						}
						else if(prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("clubs"))
						{
							suitArray.add("diamonds");
							suitArray.add("spades");
						}						
					}
					else if(prbObj._hisMessagesForTeammate.get(no)._messageCardSuit.equals("diamonds"))
					{
						if(prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("hearts"))
						{							
							suitArray.add("spades");
							suitArray.add("clubs");
						}
						else if(prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("spades"))
						{
							suitArray.add("hearts");
							suitArray.add("clubs");
						}
						else if(prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("clubs"))
						{
							suitArray.add("hearts");
							suitArray.add("spades");
						}						
					}
				}
				else
				{
					if(prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("clubs") || prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("spades"))
					{
						 // hearts/diamonds
						if(getRoundSuitCards(playerprbObj._cards, "hearts").size()>0)						
							suit = "hearts";						
						else
							suit = "diamonds";							
					}
					else if(prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("hearts") || prbObj._hisMessagesForTeammate.get(no)._roundSuit.equals("diamonds"))
					{
						 // clubs/spades
						if(getRoundSuitCards(playerprbObj._cards, "clubs").size()>0)						
							suit = "clubs";						
						else
							suit = "spades";							
					} 
					suitArray.add(suit);
				}
			}
			
			
			
			showLog("SUit"+suit);
			showLog("Suits "+ suitArray.toString());
			
			String finalSuit = "";
			
			for(int i=0;i<suitArray.size();i++)
			{
				ArrayList<Integer> suitCards = getRoundSuitCards(playerprbObj._cards, suitArray.get(i));
				suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
				
				if(suitCards.size()>0)
				{
					boolean isTeamMateNotFollowedSuit = false;
					ArrayList<String> gameSuits = getCurrentRoundPlayedCardSuits(gameBean._roundBean.openCardsList);
					for(int j=0;j<gameSuits.size();j++)
					{
						String teamMatePlayedSuit = "nill";
						if(suitArray.get(i).equals(gameSuits.get(j)))
						{
							// Do nothing
							// continue;
						}
						else
						{
							if(j == prbObj._playedcards.size())
							{
								teamMatePlayedSuit = getSuit(prbObj._currentCard);								
							}
							else
							{
								teamMatePlayedSuit = getSuit(prbObj._playedcards.get(j));
							}
						}
						
						if(teamMatePlayedSuit.equals(suitArray.get(i)))
						{
							// dO NOTHING
						}
						else
						{
							isTeamMateNotFollowedSuit = true;
							break;
						}				
					}
					
					// Do you know that your Teammate did not follow the Suit at any previous round
					if(isTeamMateNotFollowedSuit)
						continue;
					else
					{
						
						ArrayList<Integer> opponentRoundSuitCards= new ArrayList<Integer>();
						for(int j=0;j<gameBean._players.size();j++)
						{
							if(gameBean._players.get(i).equals(prbObj._playerId) || gameBean._players.get(i).equals(playerprbObj._playerId))
							{
								// Do nothing
							}
							else
							{
								ArrayList<Integer> knowncards = getRoundSuitCards(getTeammateKnownCards(gameBean, gameBean._players.get(j)), suitArray.get(i));
								for(int k=0;k<knowncards.size();k++)
								{
									opponentRoundSuitCards.add(knowncards.get(k));
								}
							}
						}
						ArrayList<Integer> teamRoundSuitCards = new ArrayList<Integer>();
						teamRoundSuitCards = getRoundSuitCards(getTeammateKnownCards(gameBean, prbObj._playerId), suitArray.get(i));
						ArrayList<Integer> knowncards = getRoundSuitCards(playerprbObj._cards, suitArray.get(i));
						for(int k=0;k<knowncards.size();k++)
						{
							teamRoundSuitCards.add(knowncards.get(k));
						}
						
						// Written for SAN & not checked for HOKOM.
						teamRoundSuitCards = Commands.appInstance.ai.sortSunorder(teamRoundSuitCards);
						opponentRoundSuitCards = Commands.appInstance.ai.sortSunorder(opponentRoundSuitCards);
						
						
						if(opponentRoundSuitCards.size() == 0 || opponentRoundSuitCards.get(0)%8 == 0)
						{							
							// select suit as per AI-159
							finalSuit = suitArray.get(i);
							break;
						}
						else 
						{
							ArrayList<Integer> newArray = opponentRoundSuitCards;
							for(int k=0;k<teamRoundSuitCards.size();k++)
							{
								newArray.add(teamRoundSuitCards.get(k));
							}				
							
							// Writter for SAN & not checked for HOKOM.
							newArray = Commands.appInstance.ai.sortSunorder(newArray);							
							if(opponentRoundSuitCards.get(0)  == newArray.get(0))
							{
								continue;
							}
							else
							{
								// select suit as per AI-159
								finalSuit = suitArray.get(i);
								break;
							}
						}
						
					}					
				}
			}
			
			if(finalSuit != null)
			{
				ArrayList<Integer> suitCards = getRoundSuitCards(playerprbObj._cards, finalSuit);
				
				suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
				
				if(suitCards.size()>0)
				{
					// Having Same Suit Cards
					Apps.showLog("Having Same Suit Cards");
					
					// Changed as per Eyad
					if(suitCards.size() == 1)
						card = suitCards.get(0);
					else if(suitCards.get(0)%8 == 5)
					{
						// Check for Having 10 of that suit
						card = suitCards.get(1);
					}
					else 
					{
						// Play Stronges Card of that suit as per AI-159
						card = suitCards.get(0);
					}
				}
				else
				{
					// Not Having Same Suit Cards
					Apps.showLog("Not Having Same Suit Cards");
					
					// Check For First round or second round
					// Flow Changed as per the new file : "Changed from getCardForFirstRoundPlayer" to "getCardOfAnySuitHavingSecondStrongestCardForPlayer"
					// 162
					card =  checkAnySuitHavingSecondStrongestCard(gameBean, playerprbObj._playerId, Commands.SAN);
				}				
			}
			else {
				// Check For First round or second round
				// Flow Changed as per the new file : "Changed from getCardForFirstRoundPlayer" to "getCardOfAnySuitHavingSecondStrongestCardForPlayer"
				// 162
				card = checkAnySuitHavingSecondStrongestCard(gameBean,playerprbObj._playerId, Commands.SAN);			
			}		
		}
		else
		{
			// No P4 Message
			showLog(" NO P4 Message");
			// Check For First round or second round
			//card = checkISFirstRound(player);	
			card = checkAnySuitHavingSecondStrongestCard(gameBean,playerprbObj._playerId, Commands.SAN);	
		}
		return card;
	}
	
	
	private ArrayList<String> getCurrentRoundPlayedCardSuits(ArrayList<Integer> playedCards){
		ArrayList<String> suits = new ArrayList<String>();
		for(int i=0;i<playedCards.size();i++)
		{
			String suit = getSuit(playedCards.get(i));
			boolean sign = false;
			for(int j=0;j<suits.size();j++)
			{
				if(suits.get(j).equals(suit))
				{
					sign = true;
					break;
				}
			}
			if(!sign)
			{
				suits.add(suit);
			}	
		}
		return suits;
	}
	
	
	private Integer checkISFirstRound(GameBean gameBean, String player)
	{
		showLog("checkISFirstRound");
		Integer card=0; 
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		if(gameBean._roundBean.trickCount == 1)
		{
			// First Round and check if partner won the bid
			if(gameBean._roundBean._bidRoundCount == 1 && gameBean._roundBean._bidSelectedPerson.equals(getPartner(gameBean,player)))
			{
				// Middle suit card
				
				ArrayList<Integer> suitCards = getRoundSuitCards(prbObj._cards, getSuit(gameBean._roundBean._openCardId));
				suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
				if(suitCards.size()>0)
				{
					// Play strongest
					card = suitCards.get(0);
				}
				else
				{
					// 162
					//card = checkAnySuitHavingSecondStrongestCard(player, Commands.SAN);
					card = playPeriorityThreeMessage(gameBean,player);
				}				
			}
			else
			{
				//162
				//card = checkAnySuitHavingSecondStrongestCard(player, Commands.SAN);
				card = playPeriorityThreeMessage(gameBean,player);
			}
		}
		else 
		{
			// Not First Round
			// N 162
			//card = checkAnySuitHavingSecondStrongestCard(player, Commands.SAN);
			card = playPeriorityThreeMessage(gameBean,player);
		}
		return card;		
	}
	
	private Integer checkAnySuitHavingSecondStrongestCard(GameBean gameBean, String player, String type)
	{	
		Integer card=0;
		ArrayList<Integer> secondStrongestCards = getSecondStrongestCards(gameBean,type);		
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		
		for(int i= 0;i<secondStrongestCards.size();i++)
		{
			boolean sign = false;
			for(int j=0;j<prbObj._cards.size();j++)
			{
				if(secondStrongestCards.get(i) == prbObj._cards.get(j))
				{
					sign = true;
					break;
				}
			}
			if(sign)
			{
				card = secondStrongestCards.get(i);
				showLog("Play the strongest card");
				break;
			}
		}
		
		if(card != 0)
		{
			// Check do u have any other card of that suit
			ArrayList<Integer> suitCards = getRoundSuitCards(prbObj._cards, getSuit(card));
			suitCards = Commands.appInstance.ai.sortSunorder(suitCards);
			if(suitCards.size()>=2)
			{
				// having 2 cards
				// Play weakest
				card = suitCards.get(suitCards.size()-1);
			}
			else
			{
				// is there any suit that has not been played in previous rounds
				card = anySuitNotBeenPlayed(gameBean,player); 
			}
		}
		else
		{
			// is there any suit that has not been played in previous rounds
			card = anySuitNotBeenPlayed(gameBean,player);
		}		
		return card;
	}
	
	private Integer anySuitNotBeenPlayed(GameBean gameBean, String player)
	{
		Integer card = 0;
		Integer clubsCount = 0;
		Integer spadesCount = 0;
		Integer diamondsCount = 0;
		Integer heartsCount = 0;
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		for(int i=0;i<gameBean._roundBean._cardsPlayed.size();i++)
		{
			if(getSuit(gameBean._roundBean._cardsPlayed.get(i)).equals("clubs"))
			{
				clubsCount++;
			}
			else if(getSuit(gameBean._roundBean._cardsPlayed.get(i)).equals("spades"))
			{
				spadesCount++;
			}
			else if(getSuit(gameBean._roundBean._cardsPlayed.get(i)).equals("diamonds"))
			{
				diamondsCount++;
			}
			else if(getSuit(gameBean._roundBean._cardsPlayed.get(i)).equals("hearts"))
			{
				heartsCount++;
			}
		}	
		
		String suit ="null";
		if(heartsCount == 0)
			suit = "hearts";
		else if(clubsCount == 0)
			suit = "clubs";
		else if(diamondsCount == 0)
			suit = "diamonds";
		else if(spadesCount == 0)
			suit = "spades";
		
		if(suit.equals("null"))
		{
			// Play the weakest
			prbObj._cards = Commands.appInstance.ai.sortSunorder(prbObj._cards);
			card = prbObj._cards.get(prbObj._cards.size()-1);
		}
		else 
		{
			// Do U have any cards of that suit
			ArrayList<Integer> suitcards = getRoundSuitCards(prbObj._cards, suit);
			suitcards = Commands.appInstance.ai.sortSunorder(suitcards);
			if(suitcards.size()>0)
			{
				// Play the weakest of that suit
				card = suitcards.get(suitcards.size()-1); 
			}
			else 
			{
				// Play the weakest of remaining
				prbObj._cards = Commands.appInstance.ai.sortSunorder(prbObj._cards);
				card = prbObj._cards.get(prbObj._cards.size()-1);
			}			
		}		
		return card;
	}
	
	private String checkPeriorityTwoMessage(GameBean gameBean, String player)
	{
		showLog("checkPeriorityTwoMessage");
		String str = "null";
		// Check his team mate said bonus and having highest cards
		// get the partners cards
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(getPartner(gameBean,player));
		PlayerRoundBean playerPrbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		ArrayList<Integer> bonuscards = getBonusWinCards(gameBean,getPartner(gameBean,player));
		
		//showLog("Before "+bonuscards);
		
		if(gameBean._roundBean._openCardTaker.equals(getPartner(gameBean,player)))
		{
			bonuscards.add(gameBean._roundBean._openCardId);
		}
		//showLog("After "+bonuscards);
		int card = 0;
		if(bonuscards.size()>0)
		{		
			for(int i=0;i<prbObj._cards.size();i++)
			{
				for(int j=0;j<bonuscards.size();j++)
				{
					if(prbObj._cards.get(i) == bonuscards.get(j))
					{
						card = prbObj._cards.get(i);
						str = getSuit(card);
						if(getRoundSuitCards(playerPrbObj._cards, str).size()>0)
						{
							break;
						}
						else
						{
							str = "null";
							card = 0;
						}					
					}					
				}
				if(card != 0)
					break;
			}			
		}		
		return str;	
	}
	
	
	
	private Integer checkPeriorityMessage(PlayerRoundBean prbObj, Integer periourity)
	{
		int sign = -1;
		if(prbObj._hisMessagesForTeammate.size()>0)
		{
			for(int i=0; i<prbObj._hisMessagesForTeammate.size();i++)
			{
				if(prbObj._hisMessagesForTeammate.get(i)._messagePeriority == periourity)
					sign = i;
			}
		}
		return sign;
	}
	
	
	private Integer playAICardSelectionFourth(GameBean gameBean, String player)
	{
		showLog("*******playAICardSelectionFourth******");
		Integer card = 0;
		card = checkNotHavingAceTenOrK(gameBean,player);
		if(card == 0)
		{
			// Every suit having A or 10 or K
			showLog("Every suit having A or 10 or K");		
			card = checkNotHavingAceOrTen(gameBean,player);
			if(card == 0)
			{
				showLog("Every suit having A or 10");			
				card = checkNotHavingTenAceCountTwoElsePlayWeak(gameBean,player);				
			}
			else
			{
				showLog("Every suit not having A or 10");
				
			}
		}
		else
		{
			showLog("Every Suit Not Having A or K or 10");
		}
		return card;
	}
	
	private Integer checkNotHavingTenAceCountTwoElsePlayWeak(GameBean gameBean, String player){
	
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		ArrayList<Integer> heartsCards = new ArrayList<Integer>();
		ArrayList<Integer> clubsCards = new ArrayList<Integer>();
		ArrayList<Integer> diamondsCards = new ArrayList<Integer>();
		ArrayList<Integer> spadesCards = new ArrayList<Integer>();
		
		for(int i=0;i<prbObj._cards.size();i++)
		{
			if(prbObj._cards.get(i)>=1 && prbObj._cards.get(i)<=8)
			{
				clubsCards.add(prbObj._cards.get(i));				
			}
			else if(prbObj._cards.get(i)>=9 && prbObj._cards.get(i)<=16)
			{
				diamondsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=17 && prbObj._cards.get(i)<=24)
			{
				heartsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=25 && prbObj._cards.get(i)<=32)
			{
				spadesCards.add(prbObj._cards.get(i));
			}
		}
		//showLog("c"+clubsCards);
		//showLog("c"+diamondsCards);
		//showLog("c"+heartsCards);
		//showLog("c"+spadesCards);
		
		Integer card = 0;
		card = checkNotHavingAceOrTenCountTwo(clubsCards);
		
		if( card == 0 )
		{
			card = checkNotHavingAceOrTenCountTwo(diamondsCards);
			if(card == 0)
			{
				card = checkNotHavingAceOrTenCountTwo(heartsCards);
				if(card == 0)
				{
					card = checkNotHavingAceOrTenCountTwo(spadesCards);					
				}				
			}			
		}		
		if(card == 0)
		{
			// Play Weakest
			prbObj._cards = Commands.appInstance.ai.sortSunorder(prbObj._cards);
			card = prbObj._cards.get(prbObj._cards.size()-1); 
		}
		else 
		{
			showLog("Having cards count = 2 and not having 10 and Ace");
			showLog("Played weakest");
		}
		return card;
	}
	
	private Integer checkNotHavingAceOrTenCountTwo(ArrayList<Integer>cards)
	{
		//showLog("Cards "+cards);
		Integer card = 0;
		if(cards.size()>0)
		{
			boolean sign = true;
			for(int i=0;i<cards.size();i++)
			{
				if(cards.get(i)%8 == 1 || cards.get(i)%8 == 5 )
				{
					sign = false;
					break;
				}
			}
			
			if(sign)
			{
				if(cards.size()>=2)
				{
					cards = Commands.appInstance.ai.sortSunorder(cards);
					card = cards.get(cards.size()-1);	
				}
			}				
		}		
		return card;
	}
	
	
	private Integer checkNotHavingAceOrTen(GameBean gameBean, String player)
	{		
		showLog("checkNotHavingAceOrTen");
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		ArrayList<Integer> heartsCards = new ArrayList<Integer>();
		ArrayList<Integer> clubsCards = new ArrayList<Integer>();
		ArrayList<Integer> diamondsCards = new ArrayList<Integer>();
		ArrayList<Integer> spadesCards = new ArrayList<Integer>();
		
		for(int i=0;i<prbObj._cards.size();i++)
		{
			if(prbObj._cards.get(i)>=1 && prbObj._cards.get(i)<=8)
			{
				clubsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=9 && prbObj._cards.get(i)<=16)
			{
				diamondsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=17 && prbObj._cards.get(i)<=24)
			{
				heartsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=25 && prbObj._cards.get(i)<=32)
			{
				spadesCards.add(prbObj._cards.get(i));
			}
		}
		
		Integer card = 0;
		card = notHavingAceOrTen(heartsCards);
		
		if( card == 0 )
		{
			card = notHavingAceOrTen(clubsCards);
			if(card == 0)
			{
				card = notHavingAceOrTen(diamondsCards);
				if(card == 0)
				{
					card = notHavingAceOrTen(spadesCards);
					return card;
				}
				else
					return card;
			}
			else 
				return card;
		}
		else 
			return card;		
	}
	
	private Integer notHavingAceOrTen(ArrayList<Integer> cards)
	{
		showLog("Cards in notHavingAceOrTen"+cards);
		Integer card = 0;
		if(cards.size()>0)
		{
			boolean sign = true;
			for(int i=0;i<cards.size();i++)
			{
				if(cards.get(i)%8 == 1 || cards.get(i)%8 == 5 )
				{
					sign = false;
					break;
				}
			}			
			
			if(sign)
			{
				cards = Commands.appInstance.ai.sortSunorder(cards);
				card = cards.get(cards.size()-1);
				return card;
			}
			else
				return card;
				
		}
		else
			return card;
	}
	
	
	private Integer checkNotHavingAceTenOrK(GameBean gameBean, String player)
	{
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		ArrayList<Integer> heartsCards = new ArrayList<Integer>();
		ArrayList<Integer> clubsCards = new ArrayList<Integer>();
		ArrayList<Integer> diamondsCards = new ArrayList<Integer>();
		ArrayList<Integer> spadesCards = new ArrayList<Integer>();
		
		for(int i=0;i<prbObj._cards.size();i++)
		{
			if(prbObj._cards.get(i)>=1 && prbObj._cards.get(i)<=8)
			{
				clubsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=9 && prbObj._cards.get(i)<=16)
			{
				diamondsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=17 && prbObj._cards.get(i)<=24)
			{
				heartsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=25 && prbObj._cards.get(i)<=32)
			{
				spadesCards.add(prbObj._cards.get(i));
			}
		}
		
		Integer card = 0;
		card = NotHavingAceTenOrK(heartsCards);
		
		if( card == 0 )
		{
			card = NotHavingAceTenOrK(clubsCards);
			if(card == 0)
			{
				card = NotHavingAceTenOrK(diamondsCards);
				if(card == 0)
				{
					card = NotHavingAceTenOrK(spadesCards);
					return card;
				}
				else
					return card;
			}
			else 
				return card;
		}
		else 
			return card;
	}
	
	private Integer NotHavingAceTenOrK(ArrayList<Integer> cards)	
	{
		Integer card = 0;
		if(cards.size()>0)
		{
			boolean sign = true;
			for(int i=0;i<cards.size();i++)
			{
				if(cards.get(i)%8 == 1 || cards.get(i)%8 == 5 || cards.get(i)%8 == 2)
				{
					sign = false;
					break;
				}
			}
			
			if(sign)
			{
				cards = Commands.appInstance.ai.sortSunorder(cards);
				card = cards.get(cards.size()-1);
				return card;
			}
			else
				return card;
				
		}
		else
			return card;
	}
	
	
	private Integer playAICardSelectionThird(GameBean gameBean, String player)
	{	
		Integer card =0;
		showLog("*******playAICardSelectionThird********* Check any 10 Having");
		if(checkAnyTen(gameBean,player))
		{
			showLog("Having 10");
			// Check which 10 with second strongest and with ace or other card
			card = checkAnyTenWithSecondHighest(gameBean, player);
			if(card == 0)
			{
				// Fail
				showLog("Check for opponent persons ace");
				card = checkAnyTenWithOpponentHavingAce(gameBean, player);
				if(card != 0)
				{
					// Play 10;
					showLog("Play 10");
				}
				else
				{
					// 5
					//  Play Messaging Mode
					showLog("Play Messaging mode");
					card = playAICardSelectionMessagingMode(gameBean, player);
				}
			}
			else
			{
				// Play 10
				showLog("Play 10");			
				
			}						
			
		}
		else
		{
			showLog("Not Having 10");
			// 5
			//  Play Messaging Mode
			showLog("Play Messaging mode");
			card = playAICardSelectionMessagingMode(gameBean, player);
		}
		
		return card;
	}
	
	private Integer playAICardSelectionMessagingMode(GameBean gameBean, String player)
	{
		showLog("playAICardSelectionMessagingMode Player "+player);
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		Integer card = 0;
		if(checkAnyHokomSuitCards(gameBean, player) && gameBean._roundBean._gameType.equals(Commands.HOKOM))
		{
			showLog("Having hokom suit cards");
			// Exclude Hokom suit and round suit			
			// Exclude Hokok suit cards
			ArrayList<Integer> exCards = getExcludeHokomCards(gameBean,prbObj._cards);
			
			ArrayList<Integer> clubs = getRoundSuitCards(exCards, "clubs");
			ArrayList<Integer> diamonds = getRoundSuitCards(exCards, "diamonds");
			ArrayList<Integer> hearts = getRoundSuitCards(exCards, "hearts");
			ArrayList<Integer> spades = getRoundSuitCards(exCards, "spades");
			
			ArrayList<String> suits = new ArrayList<String>();
			
			if(clubs.size()>0)
				suits.add("clubs");
			if(diamonds.size()>0)
				suits.add("diamonds");
			if(hearts.size()>0)
				suits.add("hearts");
			if(spades.size()>0)
				suits.add("spades");
				
			if(suits.size() == 1)
			{
				// single suit
				exCards = Commands.appInstance.ai.sortSunorder(exCards);
				if(checkFirstSecondThirdStrongestCards(gameBean,player, suits.get(0)))
				{
					// having first , second and third highest
					// play strongest					
					card = exCards.get(0);
				}
				else
				{
					// play weakest
					card = exCards.get(exCards.size()-1);
				}
			}
			else if(suits.size() == 2)
			{
				// Two suits		
				// Check the suits does not have strongest card
				ArrayList<Integer> suit1Cards = getRoundSuitCards(exCards, suits.get(0));
				ArrayList<Integer> suit2Cards = getRoundSuitCards(exCards, suits.get(1));
				
				suit1Cards = Commands.appInstance.ai.sortSunorder(suit1Cards);
				suit2Cards = Commands.appInstance.ai.sortSunorder(suit2Cards);
				
				if(suit1Cards.get(0) == getStrongestCard(gameBean,suits.get(0), Commands.SAN))
				{
					if(suit2Cards.get(0) == getStrongestCard(gameBean,suits.get(1), Commands.SAN))
					{
						card = exCards.get(exCards.size()-1);
					}
					else
					{
						// play Strongest
						card = suit2Cards.get(0);
					}
				}
				else
				{
					// Play strongest
					card = suit1Cards.get(0);
				}
				
			}
			else
			{
				showLog("There is no other suit cards");
				prbObj._cards = Commands.appInstance.ai.sortHokomorder(prbObj._cards);
				card = prbObj._cards.get(prbObj._cards.size()-1);
			}
		}
		else
		{		
			card = haveStrongCardRemaining(gameBean,player, gameBean._roundBean._gameType);			
			if(card != 0)
			{
				showLog("Having Strongest Card");
				
				String strongestSuit = getSuit(card);
				card = 0;
				// Play Messaging Mode
				// Do u Have any other suit cards
				if(!haveAnyOtherSuitCards(gameBean,player, strongestSuit))
				{
					showLog("Not having other suit cards");
					// Check there is 1 and 2 and 3 rd strongest cards				
					card = playAIMessagingModeY148(gameBean,player, strongestSuit);				
				}
				else
				{
					// Check is this first message
					//showLog("having other suit cards");
					//showLog("Check for any message");
					if(prbObj._messagesCount == 0)
					{
						showLog("First Message");
						if(checkAnySuitNotHavingStrongestCard(gameBean,player))
						{
							showLog("Not having strongest card on remaining suits");
							showLog("Play First Message");
							String weakestSuit = getWeakestSuit(gameBean,player);
							ArrayList<Integer> cards = getRoundSuitCards(prbObj._cards,weakestSuit);
							card = cards.get(0);
							// Save the message						
							saveMessage(gameBean,player,strongestSuit, card);
							//showLog("card"+card);
						}
						else
						{
							showLog("Every suit having strongest card");
							showLog("Play weakest on hand");						
							prbObj._cards = Commands.appInstance.ai.sortSunorder(prbObj._cards);
							card = prbObj._cards.get(prbObj._cards.size()-1);					
						}
					}
					else
					{
						showLog("Second Message");					
						if(isSameColorMessage(gameBean,player))
						{
							showLog("Same Color Message");
							ArrayList<Integer> sameSuitCards = getRoundSuitCards(prbObj._cards, prbObj._aiMessages.get(0)._messageCardSuit);
							if(sameSuitCards.size()>0)
							{
								//Have same suit cards
								showLog("Have Same Suit Cards");
								// Play Strongest of that suit
								sameSuitCards = Commands.appInstance.ai.sortSunorder(sameSuitCards);
								card = sameSuitCards.get(0);
							}
							else 
							{							
								showLog("Not Having same suit cards");
								if(!haveAnyOtherSuitCards(gameBean, player, prbObj._aiMessages.get(0)._messageCardSuit))
								{
									// False
									// Get the suit
									
									// having same suit cards
									// Check there is 1 and 2 and 3 rd strongest cards								
									showLog("Check there is 1 and 2 and 3 rd strongest cards");
									card = playAIMessagingModeY148(gameBean,player, getSuit(prbObj._cards.get(0)));								
								}
								else
								{
									// True
									if(checkAnySuitNotHavingStrongestCard(gameBean,player))
									{
										showLog("Not having strongest card on remaining suits");
										// Check which suit is not having strongest card
										card = getStrongestCardOfTheSuitNotHavingStrongestCard(gameBean,player);
									}
									else
									{
										showLog("Every suit having strongest card");
										showLog("Play weakest on hand");
										prbObj._cards = Commands.appInstance.ai.sortSunorder(prbObj._cards);
										card = prbObj._cards.get(prbObj._cards.size()-1);						
									}
								}							
							}
						}
						else
						{
							if(isOppositColorMessage(gameBean,player))
							{
								showLog("Opposit Color Message");							
								// Check for other opposite colour suit cards
								ArrayList<Integer> oppColorSuitCards = new ArrayList<Integer>();
								
								if(prbObj._aiMessages.get(0)._roundSuit.equals("diamonds"))
								{
									oppColorSuitCards = getRoundSuitCards(prbObj._cards, "hearts");
								}
								else if( prbObj._aiMessages.get(0)._roundSuit.equals("hearts"))
								{
									oppColorSuitCards = getRoundSuitCards(prbObj._cards, "diamonds");
								}
								else if( prbObj._aiMessages.get(0)._roundSuit.equals("clubs"))
								{
									oppColorSuitCards = getRoundSuitCards(prbObj._cards, "spades");
								}
								else if( prbObj._aiMessages.get(0)._roundSuit.equals("spades"))
								{
									oppColorSuitCards = getRoundSuitCards(prbObj._cards, "clubs");
								}							
								if(oppColorSuitCards.size()>0)
								{
									showLog("Having same suit cards");
									oppColorSuitCards = Commands.appInstance.ai.sortSunorder(oppColorSuitCards);
									if(oppColorSuitCards.get(0) == getStrongestCard(gameBean,prbObj._aiMessages.get(0)._messageCardSuit, Commands.SAN))
									{
										showLog("Strongest card");
										//Check having same suit of the first message cards 
										ArrayList<Integer> firstMessageSuitCards = getRoundSuitCards(prbObj._cards, prbObj._aiMessages.get(0)._messageCardSuit);
										firstMessageSuitCards = Commands.appInstance.ai.sortSunorder(firstMessageSuitCards);
										
										if(firstMessageSuitCards.size()>0)
										{
											// Play strongest card of that suit
											card = firstMessageSuitCards.get(0);
										}
										else
										{
											// play the weakest card of that suit
											card = oppColorSuitCards.get(oppColorSuitCards.size()-1);										
										}									
									}
									else
									{
										// Not Strongest
										showLog("No Stronger cards Play stongest card of that suit");
										card = oppColorSuitCards.get(0);
									}
								}
								else
								{
									showLog("Not having same suit cards");								
									ArrayList<Integer> sameSuitCards = getRoundSuitCards(prbObj._cards, prbObj._aiMessages.get(0)._messageCardSuit);
									if(sameSuitCards.size()>0)
									{
										// Having same suit cards
										showLog("Play stongest cards of that suit");
										sameSuitCards = Commands.appInstance.ai.sortSunorder(sameSuitCards);
										card = sameSuitCards.get(0);
									}
									else
									{									
										card = playAIMessagingModeY148(gameBean,player, getSuit(prbObj._cards.get(0)));									
									}
									
								}
								
							}
							else
							{
								// NO Message							
								card = playAIMessagingModeY148(gameBean,player, getSuit(prbObj._cards.get(0)));							
							}
						}
					}
					
				}
			}
			else
			{
				// Play Strongest card on hand
				showLog("Play Strongest card");			
				prbObj._cards = Commands.appInstance.ai.sortSunorder(prbObj._cards);
				card = prbObj._cards.get(0);
			}
		}
		return card;
	}
	
	
	private Integer playAIMessagingModeY148(GameBean gameBean, String player, String suit)
	{
		showLog("playAIMessagingModeY148");
		Integer card = 0;
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		
		if(gameBean._roundBean._trumpSuitType.equals(getSuit(prbObj._cards.get(0))) && (gameBean._roundBean._gameType.equals(Commands.HOKOM)))
		{			
			prbObj._cards = Commands.appInstance.ai.sortHokomorder(prbObj._cards);
		}
		else
		{			
			prbObj._cards = Commands.appInstance.ai.sortSunorder(prbObj._cards);
		}
		if(checkFirstSecondThirdStrongestCards(gameBean, player, suit))
		{
			// Play Strongest on the hand
			showLog("Play Strongest on the hand");			
			card = prbObj._cards.get(0);
			
		}
		else
		{
			// Play weakest Card on the hand
			showLog("Play weakest Card on the hand");			
			card = prbObj._cards.get(prbObj._cards.size()-1);
		}
		return card;
	}
	
	
	private Integer getStrongestCardOfTheSuitNotHavingStrongestCard(GameBean gameBean, String player)
	{
		Integer card=0;
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);		
		ArrayList<Integer> heartsCards = new ArrayList<Integer>();
		ArrayList<Integer> clubsCards = new ArrayList<Integer>();
		ArrayList<Integer> diamondsCards = new ArrayList<Integer>();
		ArrayList<Integer> spadesCards = new ArrayList<Integer>();
		for(int i=0;i<prbObj._cards.size();i++)
		{
			if(prbObj._cards.get(i)>=1 && prbObj._cards.get(i)<=8)
			{
				clubsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=9 && prbObj._cards.get(i)<=16)
			{
				diamondsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=17 && prbObj._cards.get(i)<=24)
			{
				heartsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=25 && prbObj._cards.get(i)<=32)
			{
				spadesCards.add(prbObj._cards.get(i));
			}
		}
		
		if(clubsCards.size()>0)
		{
			clubsCards = Commands.appInstance.ai.sortSunorder(clubsCards);
			Integer no = getStrongestCard(gameBean,"clubs", Commands.SAN);
			showLog(clubsCards.get(0)+" :"+ no);
			
			if(clubsCards.get(0) != no)
				card = clubsCards.get(0);
		}
		if(diamondsCards.size()>0)
		{
			diamondsCards = Commands.appInstance.ai.sortSunorder(diamondsCards);
			Integer no = getStrongestCard(gameBean,"diamonds", Commands.SAN);
			showLog(diamondsCards.get(0)+" : "+ no);
			if(diamondsCards.get(0) != no)
				card = diamondsCards.get(0);
		}
		if(heartsCards.size()>0)
		{
			heartsCards = Commands.appInstance.ai.sortSunorder(heartsCards);
			Integer no = getStrongestCard(gameBean,"hearts", Commands.SAN);
			showLog(heartsCards.get(0)+" :"+ no);
			if(heartsCards.get(0) != no)
				card = heartsCards.get(0);
		}
		if(spadesCards.size()>0)
		{
			spadesCards = Commands.appInstance.ai.sortSunorder(spadesCards);
			Integer no = getStrongestCard(gameBean,"spades", Commands.SAN);
			showLog(spadesCards.get(0)+" :"+ no);
			if(spadesCards.get(0) != no)
				card = spadesCards.get(0);
		}		
		return card;
	}
	
	
	private boolean isSameColorMessage(GameBean gameBean, String player)
	{
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		if(prbObj._aiMessages.get(0)._message.equals("SCM"))
				return true;
		return false;
	}
	
	private boolean isOppositColorMessage(GameBean gameBean, String player)
	{
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		if(prbObj._aiMessages.get(0)._message.equals("OCM"))
				return true;
		return false;
	}
	
	
	private void saveMessage(GameBean gameBean, String player, String suit, Integer card)
	{
		showLog("Save the message");		
		PlayerRoundBean teamPrbObj = gameBean._roundBean.getPlayerRoundBeanObject(getPartner(gameBean, player));
		MessageBean mb = new MessageBean();
		String teamMatePlyedSuit = getSuit(teamPrbObj._currentCard);
		
		if(teamMatePlyedSuit.equals("clubs") && suit.equals("spades"))
		{
			mb._message = "SCM";
		}
		else if(teamMatePlyedSuit.equals("clubs") && (suit.equals("diamonds") || suit.equals("hearts")))
		{
			mb._message = "OCM";
		}	
		else if(teamMatePlyedSuit.equals("diamonds") && suit.equals("hearts"))
		{
			mb._message = "SCM";
		}
		else if(teamMatePlyedSuit.equals("diamonds") && (suit.equals("clubs") || suit.equals("spades")))
		{
			mb._message = "OCM";
		}
		else if(teamMatePlyedSuit.equals("hearts") && suit.equals("diamonds"))
		{
			mb._message = "SCM";
		}
		else if(teamMatePlyedSuit.equals("hearts") && (suit.equals("clubs") || suit.equals("spades")))
		{
			mb._message = "OCM";
		}
		else if(teamMatePlyedSuit.equals("spades") && suit.equals("clubs"))
		{
			mb._message = "SCM";
		}
		else if(teamMatePlyedSuit.equals("spades") && (suit.equals("diamonds") || suit.equals("hearts")))
		{
			mb._message = "OCM";
		}
		mb._messageCardSuit = getSuit(card);		
		mb._messageCard = card;
		mb._messagePeriority = 4;
		mb._roundSuit = gameBean._roundBean.currentSuit;
		
		for(int i=0; i<gameBean._roundBean._playerRoundBeanObjs.size();i++)
		{
			if(gameBean._roundBean._playerRoundBeanObjs.get(i)._playerId.equals(player))
			{
				gameBean._roundBean._playerRoundBeanObjs.get(i)._messagesCount++;
				gameBean._roundBean._playerRoundBeanObjs.get(i)._aiMessages.add(mb);
			}
		}	
	}	
	
	private String getSuit(Integer no)
	{
		String suit="";
		if(no>=1&&no<=8)
			suit = "clubs";
		else if(no>=9&&no<=16)
			suit = "diamonds";
		else if(no>=17&&no<=24)
			suit = "hearts";
		else if(no>=25&&no<=32)
			suit = "spades";
		return suit;
	}
	/*
	private Integer playOppositColorTOStrongestSuit(String player, String suit)
	{
		Integer card =0;
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		ArrayList<Integer> heartsCards = new ArrayList<Integer>();
		ArrayList<Integer> clubsCards = new ArrayList<Integer>();
		ArrayList<Integer> diamondsCards = new ArrayList<Integer>();
		ArrayList<Integer> spadesCards = new ArrayList<Integer>();
		
		for(int i=0;i<prbObj._cards.size();i++)
		{
			if(prbObj._cards.get(i)>=1 && prbObj._cards.get(i)<=8)
			{
				clubsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=9 && prbObj._cards.get(i)<=16)
			{
				diamondsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=17 && prbObj._cards.get(i)<=24)
			{
				heartsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=25 && prbObj._cards.get(i)<=32)
			{
				spadesCards.add(prbObj._cards.get(i));
			}
		}
		
		if(suit.equals("clubs") || suit.equals("spades"))
		{
			// play hearts, diamonds
			if(gameBean._roundBean._trumpSuitType.equals("hearts"))
				heartsCards = Commands.appInstance.ai.sortHokomorder(heartsCards);
			else			
				heartsCards = Commands.appInstance.ai.sortSunorder(heartsCards);
			
			if(gameBean._roundBean._trumpSuitType.equals("diamonds"))
				diamondsCards = Commands.appInstance.ai.sortHokomorder(diamondsCards);
			else
				diamondsCards = Commands.appInstance.ai.sortSunorder(diamondsCards);
			if(heartsCards.size()>0)
				card = heartsCards.get(0);
			else
				card = diamondsCards.get(0);			
		}
		else
		{
			// play clubs, spades
			
			if(gameBean._roundBean._trumpSuitType.equals("clubs"))
				clubsCards = Commands.appInstance.ai.sortHokomorder(clubsCards);
			else
				clubsCards = Commands.appInstance.ai.sortSunorder(clubsCards);
			
			if(gameBean._roundBean._trumpSuitType.equals("spades"))
				spadesCards = Commands.appInstance.ai.sortHokomorder(spadesCards);
			else
				spadesCards = Commands.appInstance.ai.sortSunorder(spadesCards);		
			
			if(clubsCards.size()>0)
				card = clubsCards.get(0);
			else
				card = spadesCards.get(0);
		}
		return card;
	}*/
	
	private boolean checkAnySuitNotHavingStrongestCard(GameBean gameBean, String player )
	{
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		ArrayList<Integer> heartsCards = new ArrayList<Integer>();
		ArrayList<Integer> clubsCards = new ArrayList<Integer>();
		ArrayList<Integer> diamondsCards = new ArrayList<Integer>();
		ArrayList<Integer> spadesCards = new ArrayList<Integer>();
		
		for(int i=0;i<prbObj._cards.size();i++)
		{
			if(prbObj._cards.get(i)>=1 && prbObj._cards.get(i)<=8)
			{
				clubsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=9 && prbObj._cards.get(i)<=16)
			{
				diamondsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=17 && prbObj._cards.get(i)<=24)
			{
				heartsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=25 && prbObj._cards.get(i)<=32)
			{
				spadesCards.add(prbObj._cards.get(i));
			}
		}
		
		boolean csign = false;
		boolean dsign = false;
		boolean hsign = false;
		boolean ssign = false;
		Integer no = 0;
		if(clubsCards.size()>0)
		{
			if(gameBean._roundBean._trumpSuitType.equals("clubs") && (gameBean._roundBean._gameType.equals(Commands.HOKOM)))
			{
				clubsCards = Commands.appInstance.ai.sortHokomorder(clubsCards);
				no = getStrongestCard(gameBean,"clubs", Commands.HOKOM);
			}
			else
			{
				clubsCards = Commands.appInstance.ai.sortSunorder(clubsCards);
				no = getStrongestCard(gameBean,"clubs", Commands.SAN);
			}			
			if(clubsCards.get(0) != no)
				csign = true;
		}
		if(diamondsCards.size()>0)
		{
			if(gameBean._roundBean._trumpSuitType.equals("diamonds") && (gameBean._roundBean._gameType.equals(Commands.HOKOM)))
			{
				diamondsCards = Commands.appInstance.ai.sortHokomorder(diamondsCards);
				no = getStrongestCard(gameBean,"diamonds", Commands.HOKOM);
			}
			else
			{
				diamondsCards = Commands.appInstance.ai.sortSunorder(diamondsCards);
				no = getStrongestCard(gameBean,"diamonds", Commands.SAN);
			}	
			if(diamondsCards.get(0) != no)
				dsign = true;
		}
		if(heartsCards.size()>0)
		{	
			if(gameBean._roundBean._trumpSuitType.equals("hearts") && (gameBean._roundBean._gameType.equals(Commands.HOKOM)))
			{
				heartsCards = Commands.appInstance.ai.sortHokomorder(heartsCards);
				no = getStrongestCard(gameBean,"hearts", Commands.HOKOM);
			}
			else
			{
				heartsCards = Commands.appInstance.ai.sortSunorder(heartsCards);
				no = getStrongestCard(gameBean,"hearts", Commands.SAN);
			}
			if(heartsCards.get(0) != no)
				hsign = true;
		}
		if(spadesCards.size()>0)
		{	
			if(gameBean._roundBean._trumpSuitType.equals("spades") && (gameBean._roundBean._gameType.equals(Commands.HOKOM)))
			{
				spadesCards = Commands.appInstance.ai.sortHokomorder(spadesCards);
				no = getStrongestCard(gameBean,"spades", Commands.HOKOM);
			}
			else
			{
				spadesCards = Commands.appInstance.ai.sortSunorder(spadesCards);
				no = getStrongestCard(gameBean,"spades", Commands.SAN);
			}
			
			
			if(spadesCards.get(0) != no)
				ssign = true;
		}		
		
		if(csign || dsign || hsign || ssign)
			return true;
		else
			return false;
	}
	
	
	public String getWeakestSuit(GameBean gameBean, String player)
	{
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		
		ArrayList<Integer> heartsCards = new ArrayList<Integer>();
		ArrayList<Integer> clubsCards = new ArrayList<Integer>();
		ArrayList<Integer> diamondsCards = new ArrayList<Integer>();
		ArrayList<Integer> spadesCards = new ArrayList<Integer>();
		
		for(int i=0;i<prbObj._cards.size();i++)
		{
			if(prbObj._cards.get(i)>=1 && prbObj._cards.get(i)<=8)
			{
				clubsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=9 && prbObj._cards.get(i)<=16)
			{
				diamondsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=17 && prbObj._cards.get(i)<=24)
			{
				heartsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=25 && prbObj._cards.get(i)<=32)
			{
				spadesCards.add(prbObj._cards.get(i));
			}
		}
		
		Integer no = 0;
		String suit = "null";
		if(clubsCards.size()>0)
		{
			if(gameBean._roundBean._trumpSuitType.equals("clubs") && (gameBean._roundBean._gameType.equals(Commands.HOKOM)))
			{
				clubsCards = Commands.appInstance.ai.sortHokomorder(clubsCards);
				no = getStrongestCard(gameBean,"clubs", Commands.HOKOM);
			}
			else
			{
				clubsCards = Commands.appInstance.ai.sortSunorder(clubsCards);
				no = getStrongestCard(gameBean,"clubs", Commands.SAN);
			}			
			if(clubsCards.get(0) != no)
				suit = "clubs";
		}
		if(diamondsCards.size()>0 && suit.equals("null"))
		{
			if(gameBean._roundBean._trumpSuitType.equals("diamonds") && (gameBean._roundBean._gameType.equals(Commands.HOKOM)))
			{
				diamondsCards = Commands.appInstance.ai.sortHokomorder(diamondsCards);
				no = getStrongestCard(gameBean,"diamonds", Commands.HOKOM);
			}
			else
			{
				diamondsCards = Commands.appInstance.ai.sortSunorder(diamondsCards);
				no = getStrongestCard(gameBean,"diamonds", Commands.SAN);
			}	
			if(diamondsCards.get(0) != no)
				suit = "diamonds";
		}
		if(heartsCards.size()>0 && suit.equals("null"))
		{	
			if(gameBean._roundBean._trumpSuitType.equals("hearts") && (gameBean._roundBean._gameType.equals(Commands.HOKOM)))
			{
				heartsCards = Commands.appInstance.ai.sortHokomorder(heartsCards);
				no = getStrongestCard(gameBean,"hearts", Commands.HOKOM);
			}
			else
			{
				heartsCards = Commands.appInstance.ai.sortSunorder(heartsCards);
				no = getStrongestCard(gameBean,"hearts", Commands.SAN);
			}
			if(heartsCards.get(0) != no)
				suit = "hearts";
		}
		if(spadesCards.size()>0 && suit.equals("null"))
		{	
			if(gameBean._roundBean._trumpSuitType.equals("spades") && (gameBean._roundBean._gameType.equals(Commands.HOKOM)))
			{
				spadesCards = Commands.appInstance.ai.sortHokomorder(spadesCards);
				no = getStrongestCard(gameBean,"spades", Commands.HOKOM);
			}
			else
			{
				spadesCards = Commands.appInstance.ai.sortSunorder(spadesCards);
				no = getStrongestCard(gameBean,"spades", Commands.SAN);
			}
			
			
			if(spadesCards.get(0) != no)
				suit = "spades";
		}		
		return suit;
	}
	
	
	private boolean checkFirstSecondStrongestCards(GameBean gameBean, String player, String suit, String type)
	{
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		ArrayList<Integer> cards = getRoundSuitCards(gameBean._roundBean._cardsInHand, suit);
		if(type.equals(Commands.HOKOM))
		{
			cards = Commands.appInstance.ai.sortHokomorder(cards);
			prbObj._cards = Commands.appInstance.ai.sortHokomorder(prbObj._cards);
		}
		else
		{
			cards = Commands.appInstance.ai.sortSunorder(cards);
			prbObj._cards = Commands.appInstance.ai.sortSunorder(prbObj._cards);
		}
		if(prbObj._cards.size()>=2)
		{
			if(prbObj._cards.get(0)== cards.get(0) && prbObj._cards.get(1) == cards.get(1))
				return true;
		}
		
		return false;
	}
	
	private boolean checkFirstSecondThirdStrongestCards(GameBean gameBean, String player, String suit)
	{
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		ArrayList<Integer> cards = getRoundSuitCards(gameBean._roundBean._cardsInHand, suit);
		if(gameBean._roundBean._trumpSuitType.equals(getSuit(cards.get(0))) && (gameBean._roundBean._gameType.equals(Commands.HOKOM)))
		{
			cards = Commands.appInstance.ai.sortHokomorder(cards);
			prbObj._cards = Commands.appInstance.ai.sortSunorder(prbObj._cards);
		}
		else
		{
			cards = Commands.appInstance.ai.sortSunorder(cards);
			prbObj._cards = Commands.appInstance.ai.sortSunorder(prbObj._cards);
		}
		if(prbObj._cards.size()>=3 && cards.size() >= 3)
		{
			if(prbObj._cards.get(0)== cards.get(0) && prbObj._cards.get(1) == cards.get(1) && prbObj._cards.get(2)== cards.get(2))
				return true;
		}
		
		return false;
	}
	
	
	private boolean haveAnyOtherSuitCards(GameBean gameBean, String player, String suit)
	{
		showLog("haveAnyOtherSuitCards");
		
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		int n1=0;
		int n2=0;
		if(suit.equals("clubs")){n1=1;n2=8;}		
		else if(suit.equals("diamonds")){n1=9;n2=16;}
		else if(suit.equals("hearts")){n1=17;n2=24;}
		else if(suit.equals("spades")){n1=25;n2=32;}
		
		
		Integer count = 0;
		for(int i=0;i<prbObj._cards.size();i++)
		{
			Integer card = prbObj._cards.get(i);			
			if(card>=n1 && card<=n2)
				count++;			
		}
		showLog(count.toString());
		
		if(count == prbObj._cards.size())
			return false;		
		return true;
	}
	
	private ArrayList<Integer> getHighestCardsInAllSuits(GameBean gameBean, String gameType)
	{
		ArrayList<Integer> highestCards = new ArrayList<Integer>();
		String type = "";
		if(gameType.equals(Commands.SAN) || gameType.equals(Commands.ASHKAL))
			type = Commands.SAN;
		else
		{
			if(gameBean._roundBean._trumpSuitType.equals("clubs"))
				type = Commands.HOKOM;
			else
				type = Commands.SAN;
		}		
		Integer no = getStrongestCard(gameBean,"clubs", type); 
		if( no != -1)
		{
			highestCards.add(no);
		}
		if(gameType.equals(Commands.SAN))
			type = Commands.SAN;
		else
		{
			if(gameBean._roundBean._trumpSuitType.equals("diamonds"))
				type = Commands.HOKOM;
			else
				type = Commands.SAN;
		}		
		no = getStrongestCard(gameBean,"diamonds", type);
		if( no != -1)
		{
			highestCards.add(no);
		}
		if(gameType.equals(Commands.SAN))
			type = Commands.SAN;
		else
		{
			if(gameBean._roundBean._trumpSuitType.equals("hearts"))
				type = Commands.HOKOM;
			else
				type = Commands.SAN;
		}
		no = getStrongestCard(gameBean,"hearts", type);
		if( no != -1)
		{
			highestCards.add(no);
		}
		if(gameType.equals(Commands.SAN))
			type = Commands.SAN;
		else
		{
			if(gameBean._roundBean._trumpSuitType.equals("spades"))
				type = Commands.HOKOM;
			else
				type = Commands.SAN;
		}
		no = getStrongestCard(gameBean,"spades", type);
		if( no != -1)
		{
			highestCards.add(no);
		}
		return highestCards;
	}
	
	
	private Integer haveStrongCardRemaining(GameBean gameBean, String player, String gameType)
	{
		Integer card=0;
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		ArrayList<Integer> highestCards = new ArrayList<Integer>();
		highestCards = getHighestCardsInAllSuits(gameBean,gameType);	
		showLog(highestCards.toString());
		
		for(int i=0;i<prbObj._cards.size();i++)
		{
			for(int j=0;j<highestCards.size();j++)
			{
				
				if(prbObj._cards.get(i) == highestCards.get(j))
				{
					card = prbObj._cards.get(i);
					showLog("Card :"+card);
					break;
				}
			}
			if(card != 0)
				break;			
		}		
		return card;
	}
	
	
	private ArrayList<Integer> getBonusWinCards(GameBean gameBean, String player)
	{
		ArrayList<Integer> cards = new ArrayList<Integer>();
		if(gameBean._roundBean._bonusObjs.size()>0)
		{
			if(gameBean._roundBean._highestBonusPerson.equals(player) || gameBean._roundBean._highestBonusPerson.equals(getPartner(gameBean, player)))
			{
				showLog("***Having Bonus Cards***");
				for(int i=0;i<gameBean._roundBean._bonusObjs.size();i++)
				{
					if(gameBean._roundBean._bonusObjs.get(i)._playerId.equals(player) && gameBean._roundBean._bonusObjs.get(i)._isShowedCards)
					{
						cards = gameBean._roundBean._bonusObjs.get(i).getCards();
					}
				}
			}			
		}		
		return cards;
	}
	
	
	private boolean checkAnyTen(GameBean gameBean, String player)
	{
		boolean sign=false;
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		for(int i=0;i<prbObj._cards.size();i++)
		{
			if(prbObj._cards.get(i)%8 == 5)
			{
				sign = true;
				break;
			}
		}
		return sign;
	}
	private Integer checkAnyTenWithSecondHighest(GameBean gameBean, String player)
	{
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		ArrayList<Integer> heartsCards = new ArrayList<Integer>();
		ArrayList<Integer> clubsCards = new ArrayList<Integer>();
		ArrayList<Integer> diamondsCards = new ArrayList<Integer>();
		ArrayList<Integer> spadesCards = new ArrayList<Integer>();
		
		for(int i=0;i<prbObj._cards.size();i++)
		{
			if(prbObj._cards.get(i)>=1 && prbObj._cards.get(i)<=8)
			{
				clubsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=9 && prbObj._cards.get(i)<=16)
			{
				diamondsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=17 && prbObj._cards.get(i)<=24)
			{
				heartsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=25 && prbObj._cards.get(i)<=32)
			{
				spadesCards.add(prbObj._cards.get(i));
			}
		}
		
		Integer card = 0;
		card = getTenCard(gameBean, clubsCards, "clubs");
		
		if( card == 0 )
		{
			card = getTenCard(gameBean,diamondsCards, "diamonds");
			if(card == 0)
			{
				card = getTenCard(gameBean,heartsCards, "hearts");
				if(card == 0)
				{
					card = getTenCard(gameBean,spadesCards, "spades");
					return card;
				}
				else
					return card;
			}
			else 
				return card;
		}
		else 
			return card;
		
	}
	
	
	
	private Integer checkAnyTenWithOpponentHavingAce(GameBean gameBean, String player)
	{
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(player);
		ArrayList<Integer> heartsCards = new ArrayList<Integer>();
		ArrayList<Integer> clubsCards = new ArrayList<Integer>();
		ArrayList<Integer> diamondsCards = new ArrayList<Integer>();
		ArrayList<Integer> spadesCards = new ArrayList<Integer>();
		
		for(int i=0;i<prbObj._cards.size();i++)
		{
			if(prbObj._cards.get(i)>=1 && prbObj._cards.get(i)<=8)
			{
				clubsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=9 && prbObj._cards.get(i)<=16)
			{
				diamondsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=17 && prbObj._cards.get(i)<=24)
			{
				heartsCards.add(prbObj._cards.get(i));
			}
			else if(prbObj._cards.get(i)>=25 && prbObj._cards.get(i)<=32)
			{
				spadesCards.add(prbObj._cards.get(i));
			}
		}
		Integer card = 0;
		
		card = getTenCardWithOpponentHavingAce(gameBean,clubsCards, "clubs", player);
		
		if( card == 0 )
		{
			card = getTenCardWithOpponentHavingAce(gameBean,diamondsCards, "diamonds", player);
			if(card == 0)
			{
				card = getTenCardWithOpponentHavingAce(gameBean,heartsCards, "hearts", player);
				if(card == 0)
				{
					card = getTenCardWithOpponentHavingAce(gameBean,spadesCards, "spades", player);
					return card;
				}
				else
					return card;
			}
			else 
				return card;
		}
		else 
			return card;		
		
	}
	
	
	private Integer getTenCardWithOpponentHavingAce(GameBean gameBean, ArrayList<Integer> cards, String type, String player)
	{
		Integer card = 0;
		for(int i=0;i<cards.size();i++)
		{
			if(cards.get(i)%8 == 5)
			{
				String opponent = "";
				ArrayList<Integer> oppCards = new ArrayList<Integer>();
				if(gameBean._roundBean._bonusObjs.size()>0)
				{				
					for(int j=0;j<gameBean.maxPlayers;j++)
					{
						if(gameBean._players.get(j).equals(player) || gameBean._players.get(j).equals(getPartner(gameBean,player)))
						{
							// Do nothing
						}
						else
						{							
							opponent = gameBean._players.get(j);
							if(gameBean._roundBean._highestBonusPerson.equals(opponent) || gameBean._roundBean._highestBonusPerson.equals(getPartner(gameBean,opponent)))
							{
								for(int k=0;k<gameBean._roundBean._bonusObjs.size();k++)
								{
									if(gameBean._roundBean._bonusObjs.get(k)._playerId.equals(opponent) || gameBean._roundBean._bonusObjs.get(k)._playerId.equals(getPartner(gameBean,opponent)))
									{
										if(gameBean._roundBean._bonusObjs.get(k)._isShowedCards)
										{
											for(int l=0;l<gameBean._roundBean._bonusObjs.get(k).getCards().size();l++)
											{
												oppCards.add(gameBean._roundBean._bonusObjs.get(k).getCards().get(l));
											}
										}
									}
								}
								
								// Check for opencard taker
								if(gameBean._roundBean._openCardTaker.equals(opponent) || gameBean._roundBean._openCardTaker.equals(getPartner(gameBean,opponent)))
								{
									oppCards.add(gameBean._roundBean._openCardId);
								}							
							}
						}
					}
				}
				
				
				for(int j=0;j<oppCards.size();j++)
				{
					if(oppCards.get(j)%8 == 1)
					{
						if(type.equals("clubs") && oppCards.get(j) == 1)
						{
							card = cards.get(i);
						}
						else if(type.equals("diamonds") && oppCards.get(j) == 9)
						{
							card = cards.get(i);
						}
						else if(type.equals("hearts") && oppCards.get(j) == 17)
						{
							card = cards.get(i);
						}
						else if(type.equals("spades") && oppCards.get(j) == 25)
						{
							card = cards.get(i);
						}
					}
				}
				break;
			}			
		}			
		return card;
	}
	
	
	
	private Integer getTenCard(GameBean gameBean, ArrayList<Integer> cards, String type)
	{
		Integer card = 0;
		for(int i=0;i<cards.size();i++)
		{
			if(cards.get(i)%8 == 5)
			{
				
				if(cards.size() == 1)
				{
					//showLog("Only 10");
					if(haveSecondStrongestCard(gameBean,type, cards, Commands.SAN))
					{
						// Play 10
						card = cards.get(i); 
					}					
				}				
			}
		}
		return card;
	}
	
	private boolean checkAceAndBonus(GameBean gameBean, String player)
	{	
		// Check partner said bonus and thrown ace
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(getPartner(gameBean,player));
		Integer currentCard = prbObj._currentCard; 
		showLog("checkAceAndBonus : current Card "+currentCard);
		if(currentCard%8 == 1 && saidBonus(gameBean,getPartner(gameBean,player)))
		{
			//showLog("Ace");
			return true;
		}
		else
		{
			//showLog("Not Ace");
			return false;
		}
	}
	
	private boolean saidBonus(GameBean gameBean, String player)
	{
		if(gameBean._roundBean._bonusObjs.size()>0)
		{	
			boolean sign = false;
			for(int i=0;i<gameBean._roundBean._bonusObjs.size();i++)
			{
				if(gameBean._roundBean._bonusObjs.get(i)._playerId.equals(player))
				{
					sign = true;
					break;
				}
			}			
			return sign;
		}
		else
		{
			return false;
		}
	}
	
	private boolean haveStrongestCard(GameBean gameBean, String suit, ArrayList<Integer>cards, String type)
	{
		if(type.equals(Commands.SAN))
			cards = Commands.appInstance.ai.sortSunorder(cards);		
		else
			cards = Commands.appInstance.ai.sortHokomorder(cards);
				
		if(cards.get(0) ==  getStrongestCard(gameBean, suit, type))
			return true;		
		else					
			return false;
		
	}
	
	private boolean haveSecondStrongestCard(GameBean gameBean, String suit,ArrayList<Integer>cards, String type)
	{
		if(type.equals(Commands.SAN))
			cards = Commands.appInstance.ai.sortSunorder(cards);		
		else
			cards = Commands.appInstance.ai.sortHokomorder(cards);	
		//getStrongestCard(suit);
		if(cards.get(0) ==  getSecondStrongestCard(gameBean, suit, type))
		{
			//showLog("Having Strongest");
			return true;
		}
		else
		{
			//showLog("Not Having Strongest");
			return false;
		}
	}
	

	
	private Integer getStrongestCard(GameBean gameBean, String suit, String type)
	{
		ArrayList<Integer> arr = getRoundSuitCards(gameBean._roundBean._cardsInHand, suit);	
		
		if(type.equals(Commands.SAN))
			arr = Commands.appInstance.ai.sortSunorder(arr);
		else
			arr = Commands.appInstance.ai.sortHokomorder(arr);
			
		if(arr.size()>0)
		{
			//showLog("Highest Card"+arr.get(0));
			return arr.get(0);
		}
		else
		{
			//showLog("No Cards");
			return -1;
		}		
	}
	
	private Integer getSecondStrongestCard(GameBean gameBean, String suit, String type)
	{
		ArrayList<Integer> arr = getRoundSuitCards(gameBean._roundBean._cardsInHand, suit);		
		if(type.equals(Commands.SAN))
			arr = Commands.appInstance.ai.sortSunorder(arr);
		else
			arr = Commands.appInstance.ai.sortHokomorder(arr);
		if(arr.size()>1)
		{
			showLog("Highest Card"+arr.get(0));
			return arr.get(1);
		}
		else
		{
			showLog("No Cards");
			return -1;
		}		
	}
	
	
	
	
	private ArrayList<Integer> getRoundSuitCards(ArrayList<Integer> cards, String suit)
	{
		int n1=0;
		int n2=0;
		if(suit.equals("clubs")){n1=1;n2=8;}
		else if(suit.equals("diamonds")){n1=9;n2=16;}
		else if(suit.equals("hearts")){n1=17;n2=24;}
		else if(suit.equals("spades")){n1=25;n2=32;}
		
		ArrayList<Integer> suitCards = new ArrayList<Integer>();
		for(int i=0;i<cards.size();i++)
		{			
			if(cards.get(i)>=n1 && cards.get(i)<=n2)
				suitCards.add(cards.get(i));				
		}
		//showLog("Round Suit Cards "+ suitCards);
		return suitCards;		
	}
	
	// Is team mate playing before
	private boolean teammateBeforeOrNot(GameBean gameBean)
	{		
		if(gameBean._roundBean.openCardsList.size()>=2)
		{
			//showLog("Played");
			return true;
		}
		else
		{
			//showLog("Not played");
			return false;
		}	
	}
	
	
	private String getPartner(GameBean gameBean, String player)
	{
		String teammate = "";
		for(int i=0; i<gameBean.maxPlayers;i++)
		{
			if(gameBean._players.get(i).equals(player))
			{
				if(i==0)
					teammate = gameBean._players.get(2);
				else if(i == 1)
					teammate = gameBean._players.get(3);
				else if(i == 2)
					teammate = gameBean._players.get(0);
				else if(i == 3)
					teammate = gameBean._players.get(1);					
			}
		}
		return teammate;
	}
	
	public String getHokomSuitInSecondRound(String suit)
	{
		String str = "";
		ArrayList<String> suitList = new ArrayList<String>();
		for(int i=0;i<Commands.appInstance.suits.size();i++)
		{
			if(Commands.appInstance.suits.get(i).equals(suit))
			{}
			else
			{
				suitList.add(Commands.appInstance.suits.get(i));
				break;
			}
		}
		
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(suitList.size());
		str = suitList.get(randomInt);
		return str;
	}
	
	private void showLog(String str){
		//AppMethods.showLog(str);
		//System.out.println(str);
	}
	
	
	
}
