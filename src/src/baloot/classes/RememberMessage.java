/**
 * 
 */
package src.baloot.classes;

import src.baloot.beans.GameBean;
import src.baloot.beans.MessageBean;
import src.baloot.beans.PlayerRoundBean;
import src.baloot.utils.Apps;

/**
 * @author MSKumar
 *
 */
public class RememberMessage  
{
	GameBean gameBean = null;
	String player="";
	Integer cardid=0;
	String suit="";
	
	public RememberMessage(GameBean gameBean, String playername, Integer card, String suittype)
	{
		this.gameBean = gameBean;
		player = playername;
		cardid = card;
		suit = suittype;
	}
	
	public void saveMessage()
	{
		Apps.showLog("Messaging system");
		
		PlayerRoundBean prbObj = gameBean._roundBean.getPlayerRoundBeanObject(getPartner(player));
		MessageBean mb = null;
		if(prbObj._currentCard != -1)
		{
			// Team mate played
			String teammatePlayedSuit = getSuit(prbObj._currentCard);
			
			int firstPlayedColor = 0;
			int secondPlayedColor = 0;
			
			if(teammatePlayedSuit.equals("clubs") || teammatePlayedSuit.equals("spades"))
				firstPlayedColor = 0;
			else 
				firstPlayedColor = 1;
			
			if(suit.equals("clubs") || suit.equals("spades"))
				secondPlayedColor = 0;
			else 
				secondPlayedColor = 1;
			
			
			if(cardid%8 == 1)
			{
				if(suit.equals(teammatePlayedSuit))
				{
					// No message
				}
				else 
				{
					// Priority one message
					mb = new MessageBean();
					mb._message= "null";
					mb._messageCard = cardid;
					mb._messageCardSuit = suit;
					mb._roundSuit = gameBean._roundBean.currentSuit;
					mb._messagePeriority = 1;
				}					
			}
			else if(cardid%8 == 5 && prbObj._currentCard%8 == 1)
			{
				if(teammatePlayedSuit.equals(suit))
				{
					// Priority three message 
					mb = new MessageBean();
					mb._message= "null";
					mb._messageCard = cardid;
					mb._messageCardSuit = suit;
					mb._roundSuit = gameBean._roundBean.currentSuit;
					mb._messagePeriority = 3;
				}
				else 
				{
					// Priority four message						
					mb = new MessageBean();
					if(firstPlayedColor == secondPlayedColor)
						mb._message= "SCM";
					else
						mb._message= "OCM";
					mb._messageCard = cardid;
					mb._messageCardSuit = suit;
					mb._roundSuit = gameBean._roundBean.currentSuit;
					mb._messagePeriority = 4;
				}
			}
			else
			{
				
				Apps.showLog("Register P4 Message");
				
				if(suit.equals(teammatePlayedSuit))
				{
					// Do nothing
				}
				else
				{
					// Priority four message
					mb = new MessageBean();
					if(firstPlayedColor == secondPlayedColor)
						mb._message= "SCM";
					else
						mb._message= "OCM";
					mb._messageCard = cardid;
					mb._messageCardSuit = suit;
					mb._roundSuit = gameBean._roundBean.currentSuit;
					mb._messagePeriority = 4;					
				}
			}
			
			
			if(mb != null)
			{			
				// Add message to messages list
				for(int i=0;i<gameBean._roundBean._playerRoundBeanObjs.size();i++)
				{
					if(gameBean._roundBean._playerRoundBeanObjs.get(i)._playerId.equals(player))
					{
						gameBean._roundBean._playerRoundBeanObjs.get(i)._hisMessagesForTeammate.add(mb);
					}
				}
			}
			
		}
		else
		{
			// Team mate not played
			// no message
		}		
	}
	
	private String getSuit(Integer no)
	{
		String suit="";
		if(no>=1&&no<=8)
			suit = "clubs";
		else if(no>=9&&no<=16)
			suit = "diamonds";
		else if(no>=17&&no<=24)
			suit = "hearts";
		else if(no>=25&&no<=32)
			suit = "spades";
		return suit;
	}
	
	private String getPartner(String player)
	{
		
		Apps.showLog("Player"+player);
		Apps.showLog("Player"+gameBean._players.toString());
		
		String teammate = "";
		for(int i=0; i<gameBean.maxPlayers;i++)
		{
			if(gameBean._players.get(i).equals(player))
			{
				if(i==0)
					teammate = gameBean._players.get(2);
				else if(i == 1)
					teammate = gameBean._players.get(3);
				else if(i == 2)
					teammate = gameBean._players.get(0);
				else if(i == 3)
					teammate = gameBean._players.get(1);					
			}
		}
		return teammate;
	}
}
