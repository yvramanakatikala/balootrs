/**
 * 
 */
package src.baloot.classes;

import java.util.ArrayList;

import java.util.Collection;
import java.util.Collections;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;

import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerBean;
import src.baloot.constants.Constant;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

public class AI
{
	public void checkPlayerTurnBiddingorDiscard(GameBean gameBean, String player)
	{
		
		boolean isPlayerTurn = false;
		String command = "";		
		boolean isAI = false;
		
		showLog("checkPlayerTurnBiddingorDiscard");
		showLog("Player "+player);
		
		PlayerBean pb = gameBean.getPlayerBean(player);
		if(pb != null)
		{
			if(pb._isAI || pb.isAutoPlay || pb.isFakePlayer)
			{
				isAI = true;
			}
			
			gameBean.getGameLogInstance().checkPlayerTurnBiddingorDiscard(gameBean, pb);
		}
				
		if(isAI)
		{
			if(gameBean._roundBean.isRemainingCardsDistributed)
			{
				showLog("Discard Turn "+gameBean._roundBean.getTurn());
				// Game
				if(player.equals(gameBean._roundBean.getTurn()))
				{
					isPlayerTurn = true;
					command = Commands.DISCARD;
				}
			}
			else			
			{
				showLog("Bidding Turn "+gameBean._roundBean.getTurnForBid());
				// Bidding
				if(player.equals(gameBean._roundBean.getTurnForBid()))
				{
					isPlayerTurn = true;
					command = Commands.BID;
				}
			}
			
			showLog("Not his turn");		
			
			if(isPlayerTurn)
			{				
				if(command.equals(Commands.BID))
				{
					gameBean.getGameLogInstance().aiBid(gameBean, player, command);
    				
					gameBean._roundBean.startTimer(Constant.AI_ACT_TIME_FOR_BIDDING, command, gameBean, player);
				}
				else if(command.equals(Commands.DISCARD))				
				{
					gameBean.getGameLogInstance().aiDiscard(gameBean, player, command);
    				
					gameBean._roundBean.startTimer(Constant.AI_ACT_TIME_FOR_DISCARD, command, gameBean, player);
				}
			}
		}
		else
		{
			showLog("User Alive");	
		}
	}
	
	public void checkPlayerDiscardForTrickCompletion(GameBean gameBean, String player)
	{
		boolean isAI = false;
		PlayerBean pb = gameBean.getPlayerBean(player);
		
		if(pb != null)
		{
			if(pb._isAI || pb.isAutoPlay || pb.isFakePlayer)
			{
				isAI = true;
			}
			
			gameBean.getGameLogInstance().checkPlayerTurnBiddingorDiscard(gameBean, pb);
		}
				
		if(isAI)
		{
			gameBean.getGameLogInstance().aiDiscard(gameBean, player, Commands.DISCARD);    				
			gameBean._roundBean.startTimer(Constant.AI_ACT_TIME_FOR_DISCARD + 1, Commands.DISCARD, gameBean, player);
		}
	}
	
	public boolean checkIsAIUser(GameBean gameBean, String player)
	{
		boolean isAIUser = false;			
		boolean checkAI = false;
		
		//showLog("checkPlayersTurnBiddingOrGame");
		//showLog("Player "+player);
		
		for(int i=0;i<gameBean._playersBeansList.size();i++)
		{
			if((gameBean._playersBeansList.get(i)._isAI && player.equals(gameBean._playersBeansList.get(i)._playerId)) ||
					(gameBean._playersBeansList.get(i).isFakePlayer && player.equals(gameBean._playersBeansList.get(i)._playerId)))
			{
				checkAI = true;
				break;
			}			
		}
		
		if(checkAI)
		{
			if(gameBean._roundBean.isRemainingCardsDistributed)
			{
				//showLog("Discard Turn"+gameBean._roundBean.getTurn());
				// Game
				if(player.equals(gameBean._roundBean.getTurn()))
				{
					isAIUser = true;					
				}
			}
			else
			{
				//showLog("Bidding Turn"+gameBean._roundBean.getTurnForBid());
				// Bidding
				if(player.equals(gameBean._roundBean.getTurnForBid()))
				{
					isAIUser = true;					
				}
			}
		}		
		return isAIUser;
	}
	
	public String getBidSelected(GameBean gameBean, String player)
	{
		String str = Commands.PASS;
				
		SFSArray arr = gameBean._roundBean.getBidEnableBits(player);
		
		//showLog(arr);		
		ISFSObject obj = null;
		
		for(int i=0;i<arr.size();i++)
		{
			obj = arr.getSFSObject(i);
			if(obj.getUtfString("playerId").equals(player))
				break;
		}
		if(obj == null)
		{
			//showLog("User Not Found");
		}
		else
		{
			Collection<Integer> bits = obj.getIntArray("enableBidBits");
			
			Object bitsArr[] = bits.toArray();
			
			Integer per = 0;
			
			ArrayList<Integer> cards = new ArrayList<Integer>();
			Integer opencard = 0;
			Integer playerPos = 0;
			for(int i=0;i<gameBean._roundBean._playerRoundBeanObjs.size();i++)
			{
				if(gameBean._roundBean._playerRoundBeanObjs.get(i)._playerId.equals(player))
				{
					cards = gameBean._roundBean._playerRoundBeanObjs.get(i)._cards;
					opencard = gameBean._roundBean._openCardId;
					playerPos = i;
					break;
				}
			}
			
			int passPer = 60;
			int no = ((Integer)bitsArr[4]).intValue();
			
			
			if(no == 1 && player.equals(gameBean.shuffler) && !gameBean._roundBean._gameType.equals(Commands.SAN))// Ashkal
			{
				// Get Per				
				per = getAshkalPercentage(cards, opencard, gameBean, playerPos);
				
				 		 		
				if(per >= passPer) 	
				{
					if(gameBean._roundBean._openCardId %8 == 1)
					{ 				
						if(gameBean._roundBean._gameType.equals("null"))
						{
							return Commands.SAN;
						}
						else
							per = 0;
					} 			
					else 
					{ 		
						ArrayList<Integer> sorted_cards = new ArrayList<Integer>();
						sorted_cards = sortSunorder(cards);
						
						// Add opencard
						sorted_cards.add(opencard);						
						
						if(getBonusPercentage(sorted_cards, Commands.SAN, gameBean._roundBean._trumpSuitType) != 0)
						{
							per = 0;
						}						 			
					} 		
				} 		
				else 			
					per = 0;
				
				
				if(per>=passPer)
					str = Commands.ASHKAL;
			}
			
			if(no == 0 || per<passPer) // San
			{				
				// FIRSR 70
				// Second 60
				// Check for San
				no = ((Integer)bitsArr[0]).intValue();
				if(no == 1)
				{
					// Get per
					per = getSunPercentage(cards, opencard, gameBean, playerPos);
					if(gameBean._roundBean._bidRoundCount == 0)					
						passPer = 70;					
					else 
						passPer = 60;
					if(per>=passPer)
						str = Commands.SAN;
				}
			}
			
			if( no == 0 || per < passPer) // Hokom
			{
				passPer = 55;
				no = ((Integer)bitsArr[1]).intValue();
				// Check For Hokom
				if(no == 1)
				{
					// Get per
					per = getHokomPercentage(cards, opencard, gameBean, playerPos);
					if(per>=passPer)
						str = Commands.HOKOM;
				}
			}		
		}			
		return str;
	}
	
	private Integer getSunPercentage(ArrayList<Integer> cards, Integer opencard, GameBean gameBean, Integer playerPos)
	{
		Integer per = 0;
		
		ArrayList<Integer> sorted_cards = new ArrayList<Integer>();
		sorted_cards = sortSunorder(cards);
		per = getPercentage(prepareObject(sorted_cards,opencard, gameBean._roundBean._trumpSuitType, Commands.SAN),Commands.appInstance.dc.sunObjects);	// First step
		//showLog("Percentage 1:"+per);
		per += getBonusPercentage(sorted_cards, Commands.SAN, gameBean._roundBean._trumpSuitType);
		//showLog("Percentage 2:"+per);
		per += getDecisionPeriodPercentage(Commands.SAN, gameBean);
		//showLog("Percentage 3:"+per);
		per += getScorePercentage(playerPos, gameBean, Commands.SAN);
		//showLog("Percentage 4:"+per);
		per += getTeammateDecisionPercentage(playerPos, gameBean);
		//showLog("Percentage 5:"+per);
		return per;
	}
	
	private Integer getHokomPercentage(ArrayList<Integer> cards, Integer opencard, GameBean gameBean, Integer playerPos)
	{
		Integer per = 0;
		
		ArrayList<Integer> sorted_cards = new ArrayList<Integer>();		
		sorted_cards = sortHokomorder(cards);		
		per += getPercentage(prepareObject(sorted_cards,opencard,gameBean._roundBean._trumpSuitType, Commands.HOKOM),Commands.appInstance.dc.hokomObjects);	// First step		
		//showLog("Percentage 1:"+per);
		per += getBonusPercentage(sorted_cards, Commands.HOKOM, gameBean._roundBean._trumpSuitType);
		//showLog("Percentage 2:"+per);
		per += getDecisionPeriodPercentage(Commands.HOKOM, gameBean);
		//showLog("Percentage 3:"+per);
		per += getScorePercentage(playerPos, gameBean, Commands.HOKOM);
		//showLog("Percentage 4:"+per);
		per += getTeammateDecisionPercentage(playerPos, gameBean);
		//showLog("Percentage 5:"+per);
		
		//showLog("Total Percentage"+ per);
		return per;
	}
	
	private Integer getAshkalPercentage(ArrayList<Integer> cards, Integer opencard, GameBean gameBean, Integer playerPos)
	{
		Integer per = 0;
		
		ArrayList<Integer> sorted_cards = new ArrayList<Integer>();
		sorted_cards = sortSunorder(cards);

		per = getPercentage(prepareObject(sorted_cards,opencard, gameBean._roundBean._trumpSuitType, Commands.SAN),Commands.appInstance.dc.sunObjects);
		
		return per;
	}
	
	
	
	public ArrayList<Integer> sortHokomorder(ArrayList<Integer> alist)
	{
		ArrayList<Integer> arr = new ArrayList<Integer>();
		int hokomHighestPos[] = {4,6,1,5,2,3,7,0};
		
		for(int j=0;j<hokomHighestPos.length;j++)
		{			
			for(int i=0;i<alist.size();i++)
			{				
				if(alist.get(i)%8 == hokomHighestPos[j])
				{						
					arr.add(alist.get(i));			
				}
			}
		}		
		return arr;
	}
	
	public ArrayList<Integer> sortSunorder(ArrayList<Integer> alist)
	{
		ArrayList<Integer> arr = new ArrayList<Integer>();
		int sunHighestPos[] = {1,5,2,3,4,6,7,0};
		
		for(int j=0;j<sunHighestPos.length;j++)
		{				
			for(int i=0;i<alist.size();i++)
			{				
				if(alist.get(i)%8 == sunHighestPos[j])
				{						
					arr.add(alist.get(i));					
				}
			}			
		}
		//showLog("Sun Order"+arr);
		return arr;
	}
	
	private ArrayList<String> prepareObject(ArrayList<Integer> alist, Integer opencard, String suit, String gameType)
	{
		ArrayList<String> arr = new ArrayList<String>();
		
		int spade = 0;
		int heart = 0;
		int diamond = 0;
		int club = 0;
		
		alist.add(opencard);
		
		if(gameType.equals(Commands.HOKOM))
			alist = sortHokomorder(alist);
		else
			alist = sortSunorder(alist);		
		
		if(suit.equals("clubs"))
			club = 1;		
		else if(suit.equals("spades"))
			spade = 1;
		else if(suit.equals("hearts"))
			heart = 1;
		else
			diamond = 1;
		
		//showLog(alist);
		
		for(int i=0;i<alist.size();i++)
		{			
			if(alist.get(i)>=1 && alist.get(i)<=8)
			{
				arr.add(addString(alist.get(i),club));
			}
			else if(alist.get(i)>=9 && alist.get(i)<=16)
			{				
				arr.add(addString(alist.get(i),diamond));
			}				
			else if(alist.get(i)>=17 && alist.get(i)<=24)
			{
				arr.add(addString(alist.get(i),heart));
			}				
			else if(alist.get(i)>=25 && alist.get(i)<=32)
			{			
				arr.add(addString(alist.get(i),spade));
			}			
		}
		
		return arr;
	}
	private String addString( Integer card, Integer cardrank)
	{
		String str = "";
		if(card%8 == 1){
			str = "A,"+cardrank.toString();
		}
		else if(card%8 == 2){
			str = "K,"+cardrank.toString();
		}
		else if(card%8 == 3){
			str = "Q,"+cardrank.toString();
		}
		else if(card%8 == 4){
			str = "J,"+cardrank.toString();
		}
		else if(card%8 == 5){
			str = "10,"+cardrank.toString();
		}
		else if(card%8 == 6){
			str = "9,"+cardrank.toString();
		}
		else if(card%8 == 7){
			str = "8,"+cardrank.toString();
		}
		else if(card%8 == 0){
			str = "7,"+cardrank.toString();
		}
		return str;
		
	}

	private Integer getPercentage(ArrayList<String> obj, ArrayList<ArrayList<String>> sunObj)
	{
		Integer per=0;
		
		for(int i=0;i<sunObj.size();i++)
		{
			ArrayList<String> ob = sunObj.get(i);			
			boolean objFound = false;
			for(int j=0;j<obj.size();j++)
			{				
				String str1 = obj.get(j).toString();
				String a[] = str1.split(",");
				//showLog(a[0]);
				String str2 = ob.get(j).toString();
				String b[] = str2.split(",");
				//showLog(b[0]);
				boolean val1;
				boolean val2;
				
				if(b[0].equals("X"))
				{					
					val1 = true;
				}
				else
				{
					if(a[0].equals(b[0]))
					{
						val1 = true;
					}
					else
					{
						val1 = false;
					}					
				}
				if(b[1].equals("x"))
				{					
					val2 = true;
				}
				else
				{
					if(a[1].equals(b[1]))
					{
						val2 = true;
					}
					else
					{
						val2 = false;
					}
				}
				
				if(val1 && val2)
				{
					objFound = true;					
				}
				else
				{
					objFound = false;
					break;
				}
			}
			
			if(objFound)
			{
				per = Integer.parseInt(ob.get(ob.size()-1));
				break;
			}
		}	
		showLog("1 :"+per);
		return per;	
	}
	
	private Integer getBonusPercentage(ArrayList<Integer> cards, String gameType, String suit)
	{
		Integer per = 0;
		String bonusType= "";
		
		if(gameType.equals(Commands.SAN) || gameType.equals(Commands.ASHKAL))
		{
			if(checkFourHundredBonus(cards))
				bonusType = "400";			
			else if(checkHundredBonus(cards))
				bonusType = "100";
			else
				bonusType = checkSiraBonus(cards);				
			
			if(bonusType.equals("400"))
				per = 100; 
			else if(bonusType.equals("100"))
				per = 40;
			else if(bonusType.equals("50"))
				per = 20;
			else if(bonusType.equals("sira"))
				per = 10;
			
		}
		else if(gameType.equals(Commands.HOKOM))
		{
			if(checkBalootBonus(cards, suit))
				bonusType = "baloot";			
			else if(checkHundredBonus(cards))
				bonusType = "100";
			else
				bonusType = checkSiraBonus(cards);				
			
			if(bonusType.equals("baloot"))
				per = 15; 
			else if(bonusType.equals("100"))
				per = 30;
			else if(bonusType.equals("50"))
				per = 15;
			else if(bonusType.equals("sira"))
				per = 10;
		}
		//showLog("2 :"+per);
		return per;
	}

	public String getBonusType(ArrayList<Integer> cards, String gameType, String suit)
	{		
		String bonusType= "null";
		if(gameType.equals(Commands.SAN) || gameType.equals(Commands.ASHKAL))
		{
			if(checkFourHundredBonus(cards))
				bonusType = "400";			
			else if(checkHundredBonus(cards))
				bonusType = "100";
			else
				bonusType = checkSiraBonus(cards);			
		}
		else if(gameType.equals(Commands.HOKOM))
		{
			if(checkBalootBonus(cards, suit))
				bonusType = "baloot";			
			if(checkHundredBonus(cards))
				bonusType = "100";
			else
				bonusType = checkSiraBonus(cards);			
		}		
		return bonusType;
	}

	public ArrayList<Integer> getSiraCards(ArrayList<Integer>arr, String type)
	{
		ArrayList<Integer> bonusCards = new ArrayList<Integer>();
		ArrayList<Integer> clubs = new ArrayList<Integer>();
		ArrayList<Integer> diamond = new ArrayList<Integer>();
		ArrayList<Integer> heart = new ArrayList<Integer>();
		ArrayList<Integer> spades = new ArrayList<Integer>();
		
		Collections.sort(arr);
		for(int i=0;i<arr.size();i++)
		{
			if(arr.get(i)>=1 && arr.get(i)<=8)
				clubs.add(arr.get(i));
			else if(arr.get(i)>=9 && arr.get(i)<=16)
				diamond.add(arr.get(i));
			else if(arr.get(i)>=17 && arr.get(i)<=24)
				heart.add(arr.get(i));
			else if(arr.get(i)>=25 && arr.get(i)<=32)
				spades.add(arr.get(i));
		}
		//showLog(clubs.size());
		//showLog(clubs);
		int count = 0;
		if(type.equals("sira"))
		{
			count = 2;
		}
		else if(type.equals("50"))
		{
			count = 3;
		}
		else if(type.equals("100"))
		{
			count = 4;
		}
		
		if(clubs.size()>=3)
		{
			bonusCards = getSeries(clubs,count);
		}
		else if(diamond.size()>=3)
		{
			bonusCards = getSeries(diamond,count);
		}
		else if(heart.size()>=3)
		{
			bonusCards = getSeries(heart,count);
		}
		else if(spades.size()>=3)
		{
			bonusCards = getSeries(spades,count);
		}
		
		showLog("AI Selected Bonus Cards  "+bonusCards);
		
		return bonusCards;
	}
	
	private static ArrayList<Integer> getSeries(ArrayList<Integer> cards, int count)
	{		
		ArrayList<Integer> bonusCards = new ArrayList<Integer>();
		int no=0;
		boolean sign = false;
		for(int i=0;i<cards.size()-1;i++)
		{				
			if(cards.get(i)+1 == cards.get(i+1) && !sign)
			{				
				bonusCards.add(cards.get(i));
				no++;				
				if(bonusCards.size() == count)
				{
					bonusCards.add(cards.get(i+1));
					break;
				}
			}
			else
			{
				if(bonusCards.size() != count)
				{
					bonusCards = new ArrayList<Integer>();
					no=0;
				}
			}
			
			if(no == count)
			{
				sign = true;
				no++;
				bonusCards.add(cards.get(i+1));
			}
		}	
		
		if(bonusCards.size() != count+1)
			bonusCards = new ArrayList<Integer>();
		
		return bonusCards;
	}
	
	public ArrayList<Integer> getHundredCards(ArrayList<Integer> cards)
	{
		ArrayList<Integer> bonusCards = new ArrayList<Integer>();
		
		Collections.sort(cards);		
		int count1 =0;
		int count2 =0;
		int count3 =0;
		int count4 =0;
		int count5 =0;
		for(int i=0;i<cards.size();i++)
		{
			switch(cards.get(i)%8)
			{
				case 1: count1++; break;
				case 2: count2++; break;
				case 3: count3++; break;
				case 4: count4++; break;
				case 5: count5++; break;			
			}
		}
		int no = 0;
		
		if(count1 == 4)
		{
			no = 1;
		}
		else if(count2 == 4)
		{
			no = 2;
		}
		else if(count3 == 4)
		{
			no = 3;	
		}
		else if(count4 == 4)
		{
			no = 4;	
		} 
		else if(count5 ==4)
		{
			no = 5;
		}
		
		for(int i=0;i<cards.size();i++)
		{
			if(cards.get(i)%8 == no)
			{
				bonusCards.add(cards.get(i));			
			}
		}
		return bonusCards;
	}
	
	
	public ArrayList<Integer> getFourHundredCards(ArrayList<Integer> cards)
	{
		ArrayList<Integer> bonusCards = new ArrayList<Integer>();
		for(int i=0;i<cards.size();i++)
		{
			if(cards.get(i)%8 == 1)
			{
				bonusCards.add(cards.get(i));			
			}
		}
		return bonusCards;
	}
	
	public ArrayList<Integer> getBalootCards(ArrayList<Integer> cards, String suit)
	{
		ArrayList<Integer> bonusCards = new ArrayList<Integer>();
		if(suit.equals("clubs"))
		{
			bonusCards.add(2);bonusCards.add(3);
		}
		else if(suit.equals("diamonds"))
		{
			bonusCards.add(10);bonusCards.add(11);
		}
		else if(suit.equals("hearts"))
		{
			bonusCards.add(18);bonusCards.add(19);
		}		
		else if(suit.equals("spades"))
		{
			bonusCards.add(26);bonusCards.add(27);
		}			
		return bonusCards;
	}
	
	private boolean checkFourHundredBonus(ArrayList<Integer> arr)
	{
		Collections.sort(arr);		
		int count =0;
		for(int i=0;i<arr.size();i++)
		{
			if(arr.get(i)%8 == 1 )
			{					
				count++;				
			}
		}
		if(count == 4)
		{
			return true;
		}
		return false;
	}
	
	private boolean checkHundredBonus(ArrayList<Integer> arr)
	{		
		Collections.sort(arr);		
		int count1 =0;
		int count2 =0;
		int count3 =0;
		int count4 =0;
		int count5 =0;
		for(int i=0;i<arr.size();i++)
		{
			switch(arr.get(i)%8)
			{
				case 1: count1++; break;
				case 2: count2++; break;
				case 3: count3++; break;
				case 4: count4++; break;
				case 5: count5++; break;			
			}
		}					
		if(count1 == 4 || count2 == 4 || count3 == 4 || count4 == 4 || count5 ==4)
		{
			return true;
		}
		return false;
	}
	
	private String checkSiraBonus(ArrayList<Integer> arr)
	{
		ArrayList<Integer> clubs = new ArrayList<Integer>();
		ArrayList<Integer> diamond = new ArrayList<Integer>();
		ArrayList<Integer> heart = new ArrayList<Integer>();
		ArrayList<Integer> spades = new ArrayList<Integer>();
		
		Collections.sort(arr);
		for(int i=0;i<arr.size();i++)
		{
			if(arr.get(i)>=1 && arr.get(i)<=8)
				clubs.add(arr.get(i));
			else if(arr.get(i)>=9 && arr.get(i)<=16)
				diamond.add(arr.get(i));
			else if(arr.get(i)>=17 && arr.get(i)<=24)
				heart.add(arr.get(i));
			else if(arr.get(i)>=25 && arr.get(i)<=32)
				spades.add(arr.get(i));
		}
		//showLog(clubs.size());
		//showLog(clubs);
		if(clubs.size()>=3)
		{
			return checkSeries(clubs);
		}
		else if(diamond.size()>=3)
		{
			return checkSeries(diamond);
		}
		else if(heart.size()>=3)
		{
			return checkSeries(heart);
		}
		else if(spades.size()>=3)
		{
			return checkSeries(spades);
		}		
		
		return "null";
	}
	
	/** Checking the series cards it will return the sira/50/100 */
	
	private String checkSeries(ArrayList<Integer> arr)
	{
		int count = 0;
		String type = "null";
		for(int i=0;i<arr.size()-1;i++)
		{			
			if(arr.get(i)+1 == arr.get(i+1))
			{
				count++;				
				if(count == 2)
					type = "sira";
				else if(count == 3)
					type = "50";
				else if(count == 4)
					type = "100";
			}
			else
			{				
				count = 0;
			}
		}		
		return type;		
	}
	
	private boolean checkBalootBonus(ArrayList<Integer> arr, String suit)
	{		
		ArrayList<Integer> balootCards = new ArrayList<Integer>();
		
		if(suit.equals("clubs"))
		{
			balootCards.add(2);
			balootCards.add(3);
		}
		else if(suit.equals("diamonds"))
		{
			balootCards.add(10);
			balootCards.add(11);
		}
		else if(suit.equals("hearts"))
		{
			balootCards.add(18);
			balootCards.add(19);
		}
		else
		{
			balootCards.add(26);
			balootCards.add(27);
		}
		
		int cardsCount=0;
		for(int i=0;i<arr.size();i++)
		{
			for(int j=0;j<balootCards.size();j++)
			{
				if(arr.get(i) == balootCards.get(j))
				{
					cardsCount++;
				}
			}				
		}
		
		if(cardsCount == 2)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private Integer getDecisionPeriodPercentage(String suit, GameBean gameBean)
	{
		Integer per = 0;
		
		if(gameBean._roundBean._bidRoundCount == 1)			
		{
			if(suit.equals(Commands.SAN))
				per = -10; 
		}
		//showLog("3 :"+per);
		return per;
	}
	
	private Integer getScorePercentage(Integer playerPos, GameBean gameBean, String gameType)
	{
		Integer per=0;			
		Integer opponentPos = 0;
		
		if(playerPos == 0 || playerPos == 2)
			opponentPos = 1;
		else if(playerPos == 1 || playerPos == 3)
			opponentPos = 0;
		
		Integer team1Points = 0;
		Integer team2Points = 0;
		
		team1Points = gameBean._playersBeansList.get(playerPos)._totalPoints;
		team2Points = gameBean._playersBeansList.get(opponentPos)._totalPoints;
		
		if(team1Points<50 && (team2Points<101 && team2Points>130))
		{
			if(gameType.equals(Commands.SAN) || gameType.equals(Commands.HOKOM)|| gameType.equals(Commands.ASHKAL))
				per = -10;			
		}
		else if(team1Points<50 && team2Points>131)
		{
			if(gameType.equals(Commands.SAN) || gameType.equals(Commands.ASHKAL) )
				per = -25;
			else if(gameBean.equals(Commands.HOKOM))
				per = -20;
			
		}
		else if((team1Points>50 && team1Points<100) && (team2Points>101 && team2Points<130))
		{
			if(gameType.equals(Commands.SAN) || gameType.equals(Commands.HOKOM)|| gameType.equals(Commands.ASHKAL))
				per = -5;			
		}
		else if((team1Points>50 && team1Points<100) && (team2Points>131))
		{
			if(gameType.equals(Commands.SAN) || gameType.equals(Commands.ASHKAL) )
				per = -25;
			else if(gameBean.equals(Commands.HOKOM))
				per = -20;
		}
		else if((team1Points>101 && team1Points<130) && (team2Points>131))
		{
			if(gameType.equals(Commands.SAN) || gameType.equals(Commands.ASHKAL) )
				per = -25;
			else if(gameBean.equals(Commands.HOKOM))
				per = -20;
		}
		else if(team1Points>131 && team2Points>131)
		{
			if(gameType.equals(Commands.SAN) || gameType.equals(Commands.HOKOM)|| gameType.equals(Commands.ASHKAL))
				per = -20;	
		}
		//showLog("4 :"+per);
		return per;
	}	
	/** Getting the percentage by comparing the partners decision*/
	private Integer getTeammateDecisionPercentage(Integer playerPos, GameBean gameBean){
		Integer per = 0;
		Integer partnerPos = 0;		
		if(playerPos == 0)
			partnerPos=2;
		else if(playerPos == 1)
			partnerPos = 3;
		else if(playerPos == 2)
			partnerPos = 0;
		else if(playerPos == 3)
			partnerPos = 1;
		
		if(gameBean._roundBean._playerRoundBeanObjs.get(partnerPos)._bidSelected.equals(Commands.HOKOM))
		{
			per = -20;
		}
		if(gameBean._roundBean._playerRoundBeanObjs.get(playerPos)._bidSelected.equals(Commands.HOKOM))
		{
			per = -20;
		}
		//showLog("5 :"+per);
		return per;
	}
	
	private void showLog(String str){		
		Apps.showLog(str);
	}
	
	
	
}// End class
