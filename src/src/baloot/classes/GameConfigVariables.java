package src.baloot.classes;

import src.baloot.utils.Commands;

public class GameConfigVariables {	

	private boolean isAllowIP = false;
	private boolean isDynamicXmlLoading = true;
	private boolean writeServerExceptionns = true;
	private boolean writeClientExceptions = true;
	private boolean writeClientLogsIntoGame = true;
	private boolean serverPingEnabled = true;
	private boolean dataValidation = false;
	private boolean aiSpan = false;
	private int clientThrottlingTime = 60;
	private int clientLogPerMinute = 10;
	private int serverThrottlingTime = 60;
	private int pingCountToCheckPlayingTables = 10;
	private int serverPingTimeOut = 10;
	private String serverBuildVersion = "0.0";
	
	
	public boolean isAllowIP() {
		return isAllowIP;
	}
	public void setAllowIP(boolean isAllowIP) {
		this.isAllowIP = isAllowIP;
	}

	public boolean isWriteServerExceptionns() {
		return writeServerExceptionns;
	}

	public void setWriteServerExceptionns(boolean writeServerExceptionns) {
		this.writeServerExceptionns = writeServerExceptionns;
	}

	public boolean isWriteClientExceptions() {
		return writeClientExceptions;
	}

	public void setWriteClientExceptions(boolean writeClientExceptions) {
		this.writeClientExceptions = writeClientExceptions;
	}

	public int getClientThrottlingTime() {
		return clientThrottlingTime;
	}

	public void setClientThrottlingTime(String clientThrottlingTime) {
		this.clientThrottlingTime = Integer.parseInt(clientThrottlingTime);
	}

	public int getServerThrottlingTime() {
		return serverThrottlingTime;
	}

	public void setServerThrottlingTime(String serverThrottlingTime) {
		this.serverThrottlingTime = Integer.parseInt(serverThrottlingTime);
	}

	public int getClientLogPerMinute() {
		return clientLogPerMinute;
	}

	public void setClientLogPerMinute(String clientLogPerMinute) {
		this.clientLogPerMinute = Integer.parseInt(clientLogPerMinute);
	}

	public boolean isWriteClientLogsIntoGame() {
		return writeClientLogsIntoGame;
	}

	public void setWriteClientLogsIntoGame(boolean writeClientLogsIntoGame) {
		this.writeClientLogsIntoGame = writeClientLogsIntoGame;
	}

	public int getPingCountToCheckPlayingTables() {
		return pingCountToCheckPlayingTables;
	}

	public void setPingCountToCheckPlayingTables(String pingCountToCheckPlayingTables) {
		this.pingCountToCheckPlayingTables =Integer.parseInt(pingCountToCheckPlayingTables);
	}

	public boolean isDynamicXmlLoading() {
		return isDynamicXmlLoading;
	}

	public void setDynamicXmlLoading(boolean isDynamicXmlLoading) {
		this.isDynamicXmlLoading = isDynamicXmlLoading;
	}

	public int getServerPingTimeOut() {
		return serverPingTimeOut;
	}

	public void setServerPingTimeOut(String serverPingTimeOut) {
		this.serverPingTimeOut = Integer.parseInt(serverPingTimeOut);
	}

	public boolean isServerPingEnabled() {
		return serverPingEnabled;
	}
	
	public boolean isDataValidation() {
		return dataValidation;
	}
	public void setDataValidation(boolean dataValidation) {
		this.dataValidation = dataValidation;
	}

	public void setServerPingEnabled(boolean serverPingEnabled) {
		this.serverPingEnabled = serverPingEnabled;
		
		Commands.appInstance.pingBsn.isServerPingEnabled = serverPingEnabled;
	}

	public String getServerBuildVersion() {
		return serverBuildVersion;
	}

	public void setServerBuildVersion(String serverBuildVersion) {
		this.serverBuildVersion = serverBuildVersion;
	}
	public boolean isAiSpan() {
		return aiSpan;
	}
	public void setAiSpan(boolean aiSpan) {
		this.aiSpan = aiSpan;
	}
	
}
