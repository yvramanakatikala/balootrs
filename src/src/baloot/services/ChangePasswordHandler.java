package src.baloot.services;

import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class ChangePasswordHandler extends BaseClientRequestHandler{
	
	@Override
	public void handleClientRequest(User sender, ISFSObject params) 
	{
		Apps.showLog("*********ChangePasswordHandler********* "+sender.getName());
		
		String newPassword = params.getUtfString("newPassword");
		String userName = sender.getName(); 
				
		int userID = Commands.appInstance.proxy.getUserID(userName);
		
		Commands.appInstance.proxy.updatePassword(userID, newPassword);
		
		newPassword = Commands.appInstance.proxy.getPassword(userID);
		
		params.putUtfString("newPassword", newPassword);		
		
		send(Commands.CHANGE_PASSWORD, params, sender);
		
		/*params = Commands.appInstance.proxy.getUserDetails(Apps.getPlayerId(sender.getName()));
		
		send(Commands.GET_USER_DETAILS, params, sender);*/
		
		Commands.appInstance.handleClientRequest(Commands.GET_USER_DETAILS, sender, params);
	}
}
