package src.baloot.services;

import java.util.ArrayList;

import java.util.List;

import src.baloot.constants.Constant;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.buddylist.Buddy;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class GetAllUsersDetailsHandler extends BaseClientRequestHandler
{
	@Override
	public void handleClientRequest(User sender, ISFSObject params) 
	{
		Apps.showLog("******* GetAllUsersDetailsHandler *******"+sender.getName());
		
		String command = params.getUtfString("command");
		int pageNo = 0;
		pageNo = params.getInt("pageNo");
		String player = sender.getName();
		ISFSArray sfsArr = new SFSArray();
		int minRange = 0;
		int maxRange = 10;
		
		maxRange = pageNo*10;
		minRange = maxRange - 10;
		ArrayList<String> usersList = new ArrayList<String>();
		
		ISFSArray allPlayers =  new SFSArray();
		
		if(Constant.BUDDY_LIST.equals(command))
		{
			ArrayList<Buddy> list = (ArrayList<Buddy>)Commands.appInstance.getParentZone().getBuddyListManager().getBuddyList(player).getBuddies();
			
			for(Buddy buddy : list)
			{
				usersList.add(buddy.getName());
				allPlayers.addSFSObject(getUserStatusObject(buddy.getName()));
			}
			
			if(maxRange > usersList.size())
			{
				maxRange = usersList.size();
			}
			
			for(int i=minRange; i<maxRange;i++)
			{
				String userName = usersList.get(i);
				sfsArr.addSFSObject(getUserStatusObject(userName));
			}
		}
		else if(Constant.ONLINE_BUDDY_LIST.equals(command))
		{
			ArrayList<Buddy> list = (ArrayList<Buddy>)Commands.appInstance.getParentZone().getBuddyListManager().getBuddyList(player).getBuddies();
			
			for(Buddy buddy : list)
			{
				if(Apps.getUserByName(buddy.getName()) != null)
				{
					usersList.add(buddy.getName());
					allPlayers.addSFSObject(getUserStatusObject(buddy.getName()));
				}				
			}
			
			if(maxRange > usersList.size())
			{
				maxRange = usersList.size();
			}
			
			for(int i=minRange; i<maxRange;i++)
			{
				String userName = usersList.get(i);
				sfsArr.addSFSObject(getUserStatusObject(userName));
			}
		}
		else if(Constant.ALL_PLAYERS.equals(command))
		{
			usersList = Commands.appInstance.proxy.getAllUsersList();
			
			for(String name: usersList)
			{
				allPlayers.addSFSObject(getUserStatusObject(name));
			}
			
			
			if(maxRange > usersList.size())
			{
				maxRange = usersList.size();
			}
			
			for(int i=minRange; i<maxRange;i++)
			{
				String userName = usersList.get(i);
				sfsArr.addSFSObject(getUserStatusObject(userName));
			}
		}
		else if(Constant.ONLINE_PLAYERS.equals(command))
		{
			List<User> list = Apps.getAllUsers();
			
			for(User user: list)
			{
				allPlayers.addSFSObject(getUserStatusObject(user.getName()));
			}
			
			if(maxRange > list.size())
			{
				maxRange = list.size();
			}
			
			for(int i=minRange; i<maxRange;i++)
			{
				String userName = list.get(i).getName();
				sfsArr.addSFSObject(getUserStatusObject(userName));
			}
		}
		else if(Constant.TOP_PLAYERS.equals(command))
		{
			ISFSArray topList = Commands.appInstance.proxy.getTopPlayersList();
							
			
			if(maxRange > topList.size())
			{
				maxRange = topList.size();
			}
			
			for(int i=minRange; i<maxRange;i++)
			{
				ISFSObject sfso = topList.getSFSObject(i);
				sfsArr.addSFSObject(sfso);
			}
			
			params.putSFSArray("allPlayers", topList);
			params.putSFSArray("allUsersDetails", sfsArr);
			send(Commands.GET_TOP_PLAYERS_LIST, params, sender);
			
		}
		
		if(!Constant.TOP_PLAYERS.equals(command))
		{
			params.putSFSArray("allPlayers", allPlayers);
			params.putSFSArray("allUsersDetails", sfsArr);
			send(Commands.GET_ALL_USERS_DETAILS, params, sender);
		}
	}
	
	public ISFSObject getUserStatusObject(String userName)
	{
		ISFSObject sfso = new SFSObject();
		
		sfso.putUtfString("userName", userName);
		sfso.putInt("status", getStatus(userName));
		
		return sfso;
	}
	
	public int getStatus(String userName)
	{
		int status = 0;
		User user = Apps.getUserByName(userName);
		
		if(user != null)
		{
			List<User> list = Apps.getLobbyUsers();
			
			if(list.contains(user))
			{
				status = 1;
			}
			
			list = Apps.getGameGroupUsers();
			
			if(list.contains(user))
			{
				status = 2;
			}
		}
		
		return status;
	}
}
