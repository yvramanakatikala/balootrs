package src.baloot.services;

import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class DeviceTypeHandler extends BaseClientRequestHandler {
	@Override
	public void handleClientRequest(User sender, ISFSObject params) 
	{
        Apps.showLog("*********DeviceTypeHandler********* "+sender.getName());
		
		String deviceType = params.getUtfString("deviceType");
		String userName = sender.getName();	
		
		if(Apps.isGuestUser(userName))
		{
				Commands.appInstance.proxy.storeGuestUserDeviceType(userName, deviceType);
		}
		else
		{
			Commands.appInstance.proxy.storeUserDeviceType(userName, deviceType);
		}
	}

}
