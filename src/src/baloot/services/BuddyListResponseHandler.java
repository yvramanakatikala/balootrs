package src.baloot.services;


import src.baloot.utils.Apps;

import src.baloot.utils.Commands;

import com.smartfoxserver.v2.api.ISFSBuddyApi;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class BuddyListResponseHandler extends BaseClientRequestHandler 
{
	public void handleClientRequest(User sender, ISFSObject params)
	{
		Apps.showLog("********BuddyResponse Handler********* "+sender.getName());	
		
		String player = sender.getName();
		String requestedPlayer = params.getUtfString("buddyResponseReceiver");
		boolean isBuddyRequestAccepted = params.getBool("isBuddyRequestAccepted");
		User requestedUser = Apps.getUserByName(requestedPlayer);
		
		send(Commands.BUDDYLIST_RESPONSE, params, sender );	
		if(requestedUser != null)
		{
			send(Commands.BUDDYLIST_RESPONSE, params, requestedUser );
		}
		
		if(isBuddyRequestAccepted)
		{
			ISFSBuddyApi buddyApi = Commands.appInstance.sfs.getAPIManager().getBuddyApi();
		
			try
			{
				buddyApi.addBuddy(sender, requestedPlayer, false, true, true);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			try
			{
				buddyApi.addBuddy(requestedUser, player, false, true, true);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}	
	}
}

