package src.baloot.services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import src.baloot.beans.GameBean;
import src.baloot.constants.Constant;
import src.baloot.logs.ExcSummaryLog;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class ClientLogHandler extends BaseClientRequestHandler
{

	@Override
	public void handleClientRequest(User sender, ISFSObject params) 
	{
		//Appmethods.showLog("******* ClientLogHandler *******"+sender.getName());
		
		String tableId = params.getUtfString("tableId");
		String logType = params.getUtfString("logType");
		String priority = params.getUtfString("priority");
		String gameId = params.getUtfString("gameId");
		String action = params.getUtfString("action");
		String variables = params.getUtfString("variables");
		String player = sender.getName();
		
		GameBean gameBean = null;
		
		if(tableId != null)
		{
			gameBean = Apps.getGameBean(tableId);
		}
		
		
		if(action != null && logType != null && logType.equals(Constant.EXC))
		{
			ExcSummaryLog.incrementClientExcTypeCount(action);
		}
		
		if(logType.equals(Constant.LOG) && Commands.appInstance.meLog.preLogData.isNewClientLog(player, action))
		{
			writeData(gameBean, gameId, player, action, variables, logType, priority, tableId, params);
		}
		else if(Commands.appInstance.meLog.preExcData.isNewClientExc(player, action))
		{
			writeData(gameBean, gameId, player, action, variables, logType, priority, tableId, params);
		}
	}
	
	public void writeData(GameBean gameBean, String gameId, String player, String action, String variables, String logType, String priority, String tableId, ISFSObject params)
	{
		if(gameBean != null && Commands.appInstance.gameCV.isWriteClientLogsIntoGame())
		{
			gameBean.getGameLogInstance().addClietnLog(gameBean, gameId, player, action, variables, logType, priority);
		}
		
		if(gameBean == null)
		{
			String data = "AfterGameEnd  "+logType+" | "+Apps.getDateString()+" | "+"client"+" | "+player+" | "+action+" | "+variables;
			writeLogAfterGameEnd(gameId, data);
		}
		
	    String current_time = Apps.getDateString();
	    
	    if(Commands.appInstance.gameCV.isWriteClientExceptions() && logType != null && logType.equals(Constant.EXC))
	    {
	    	String log = logType+" | "+gameId+" | "+priority+" | "+current_time+" | "+player+" | "+"client"+" | "+action+" | "+variables;
		    Commands.appInstance.meLog.addClientLog(log);
	    }
	    
	    if(priority != null && priority.equals("P0") && logType.equals(Constant.EXC))
	    {
	    	String log = logType+" | "+gameId+" | "+priority+" | "+current_time+" | "+player+" | "+"client"+" | "+action+" | "+variables;
		    Commands.appInstance.meLog.addClientP0Log(log);
		    
	    }
	}
	
	public void writeLogAfterGameEnd(String gameId, String data)
	{
		String filePath = Commands.appInstance.meLog.filePaths.get(gameId);
		BufferedWriter logWriter;
		
		if(filePath != null)
		{
			//System.out.println("DFDFDFDFD gameId "+gameId+" filePath "+filePath);
			
			File logFile = new File(filePath);
			
			try 
			{
				FileWriter fileWriter = new FileWriter(logFile, true); 				
				logWriter = new BufferedWriter(fileWriter); 
				logWriter.newLine();
				
				logWriter.append(data);
				logWriter.flush();
				logWriter.close();
			}
			catch(IOException e) 
			{
	            e.printStackTrace();
	        }
		}
	}
}
