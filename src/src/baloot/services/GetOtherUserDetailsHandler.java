package src.baloot.services;

import src.baloot.utils.Apps;

import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class GetOtherUserDetailsHandler extends BaseClientRequestHandler
{
	@Override
	public void handleClientRequest(User sender, ISFSObject params) 
	{
		Apps.showLog("******* GetUserDetailsHandler *******"+sender.getName());
		
		String userName = params.getUtfString("userName");
		
		params = Commands.appInstance.proxy.getUserDetails(userName, params);
		
		send(Commands.GET_OTHER_USER_DETAILS, params, sender);
	}

}
