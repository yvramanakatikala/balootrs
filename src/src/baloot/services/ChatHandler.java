/**
 * 
 */
package src.baloot.services;

import src.baloot.bsn.UpdateLobbyBsn;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class ChatHandler extends BaseClientRequestHandler{
	
	public void handleClientRequest(User sender, ISFSObject params)
	{
		// Exception validations
		
        String tableId = params.getUtfString("tableId");
        String player = sender.getName();
        
        params.putUtfString("sender", sender.getName());
        params.putUtfString("gender", Apps.getGender(sender.getName()));
        
        int points = Commands.appInstance.proxy.getUserPoints(player);
        params.putInt("points", points);
        Room room = Apps.getRoomByName(tableId);
        
        if(room != null)
        {
        	if(tableId.equals("Lobby"))
        	{
    	        send(Commands.CHAT, params, UpdateLobbyBsn.getLobbyUsers());
        	}
        	else
        	{
        		send(Commands.CHAT, params, room.getUserList());
        	}
        }
        
	}
}
