package src.baloot.services;


import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class BuddyListRequestHandler extends BaseClientRequestHandler 
{
	public void handleClientRequest(User sender, ISFSObject params)
	{
		Apps.showLog("******BuddyListRequestHandler******* "+sender.getName());
			
		User receiver = Commands.appInstance.getApi().getUserByName(params.getUtfString("buddyRequestReceiver"));
		
		if(receiver != null)
		{
			send(Commands.BUDDYLIST_REQUEST, params, receiver );	
		}
	}
}
