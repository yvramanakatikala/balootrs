package src.baloot.services;

import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class SetEmailVisibleHandler extends BaseClientRequestHandler
{
	@Override
	public void handleClientRequest(User sender, ISFSObject params) 
	{
		Apps.showLog("******* SetEmailVisibleHandler *******"+sender.getName());
		
		
		int emailVisible = params.getInt("emailVisible");
		
		Commands.appInstance.proxy.setEmailVisible(sender.getName(), emailVisible);
		
	}

}
