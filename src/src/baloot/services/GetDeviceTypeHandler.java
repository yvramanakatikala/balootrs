package src.baloot.services;

import java.util.ArrayList;
import java.util.Collection;

import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class GetDeviceTypeHandler extends BaseClientRequestHandler {
	@Override
	public void handleClientRequest(User sender, ISFSObject params)
	{
        Apps.showLog("******* GetDeviceTypeHandler *******"+sender.getName());
		
		Collection<String> players = params.getUtfStringArray("players");
		ArrayList<String> list = new ArrayList<String>();
		
		Object arr[] = players.toArray();				
		for(int i=0;i<arr.length;i++)
		{
			String no = ((String)arr[i]);
			list.add(no);			
		}	
		
		params.putSFSArray("playersDeviceTypes", Commands.appInstance.proxy.getPlayersDeviceTypeList(list));
		
		send(Commands.GET_DEVICE_TYPE, params, sender);
		
	}

}
