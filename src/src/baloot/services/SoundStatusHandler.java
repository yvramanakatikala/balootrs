package src.baloot.services;

import src.baloot.constants.Constant;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class SoundStatusHandler  extends BaseClientRequestHandler{
	
	public void handleClientRequest(User sender, ISFSObject params)
	{
		String player = sender.getName();
		
		String soundType = params.getUtfString("soundType");
		String status = params.getUtfString("status");
		
		if(soundType.equals(Constant.GAME_SOUND))
		{
			Commands.appInstance.proxy.updateGameSoundStatus(player, status);
		}
		else if(soundType.equals(Constant.BACK_GROUND_SOUND))
		{
			Commands.appInstance.proxy.updateBackGroundSoundStatus(player, status);
		}	
		else
		{
			trace(" NO_command_matched_in_SoundStatusHandler");
		}
	}

}
