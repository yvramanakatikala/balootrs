package src.baloot.services;

import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class GetFaceBookUserDetailsHandler extends BaseClientRequestHandler{
	
	@Override
	public void handleClientRequest(User sender, ISFSObject params) 
	{
		Apps.showLog("*********GetFaceBookUserDetailsHandler********* "+sender.getName());
		
		String userName = params.getUtfString("userName");
		
		boolean isRegisteredUser = false;
		String password = "password";
		
		if(Commands.appInstance.proxy.getUserID(userName) != -1)
		{
			isRegisteredUser = true;
			password = Commands.appInstance.proxy.getPassword(Commands.appInstance.proxy.getUserID(userName));
		}
		
		params.putBool("isRegisteredUser", isRegisteredUser);
		params.putUtfString("password", password);
		
		send(Commands.GET_FACE_BOOK_USER_DETAILS, params, sender);
	}
}
