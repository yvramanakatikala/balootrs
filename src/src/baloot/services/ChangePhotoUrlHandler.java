package src.baloot.services;

import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class ChangePhotoUrlHandler extends BaseClientRequestHandler{
	
	@Override
	public void handleClientRequest(User sender, ISFSObject params) 
	{
		Apps.showLog("*********ChangePhotoUrlHandler********* "+sender.getName());
		
		String photoUrl = params.getUtfString("photoUrl");
		String userName = sender.getName();
		
		Commands.appInstance.proxy.updatePhotoUrl(Apps.getPlayerId(userName), photoUrl);
		
		/*params = Commands.appInstance.proxy.getUserDetails(Apps.getPlayerId(userName));
		
		send(Commands.GET_USER_DETAILS, params, sender);*/
		
		Commands.appInstance.handleClientRequest(Commands.GET_USER_DETAILS, sender, params);
		
	}

}
