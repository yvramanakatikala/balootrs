package src.baloot.services;

import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class TransferGamesHandler extends BaseClientRequestHandler{
	
	@Override
	public void handleClientRequest(User sender, ISFSObject params) 
	{
		Apps.showLog("****TransferGamesHandler***** "+sender.getName());
		
		String reciever = params.getUtfString("reciever");
		int gamesCount = params.getInt("gamesCount");
		String player = sender.getName();
		
		Commands.appInstance.proxy.transferGames(Apps.getPlayerId(player), Apps.getPlayerId(reciever), gamesCount);
		
		/*params = Commands.appInstance.proxy.getUserDetails(Apps.getPlayerId(player));
		
		send(Commands.GET_USER_DETAILS, params, sender);*/
		
		Commands.appInstance.handleClientRequest(Commands.GET_USER_DETAILS, sender, params);
		
		User user = Apps.getUserByName(reciever);
		
		if(user != null)
		{
			/*params = Commands.appInstance.proxy.getUserDetails(Apps.getPlayerId(reciever));
			
			send(Commands.GET_USER_DETAILS, params, user);*/
			
			Commands.appInstance.handleClientRequest(Commands.GET_USER_DETAILS, user, params);
		}
	}
}
