package src.baloot.services;

import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class GetUserDetailsHandler extends BaseClientRequestHandler
{
	@Override
	public void handleClientRequest(User sender, ISFSObject params) 
	{
		Apps.showLog("******* GetUserDetailsHandler *******"+sender.getName());
		
		String player = sender.getName();
		boolean isGuestUser = Apps.isGuestUser(player);		
		
		params = Commands.appInstance.proxy.getUserDetails(player, params);
		
		params.putBool("isGuestUser", isGuestUser);
		
		send(Commands.GET_USER_DETAILS, params, sender);
	}
}
