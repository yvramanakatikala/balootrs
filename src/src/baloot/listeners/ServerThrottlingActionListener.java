package src.baloot.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import src.baloot.logs.ExcSummaryLog;
import src.baloot.serialize.SerializeGame;
import src.baloot.utils.Commands;


import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;

public class ServerThrottlingActionListener implements ActionListener
{
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		ISFSArray sfsArr = Commands.appInstance.meLog.getPreviousServerData();
		
		for(int i=0; i<sfsArr.size();i++)
		{
			ISFSObject sfso = sfsArr.getSFSObject(i);
			
			String preTime = sfso.getUtfString("time");
			
			if(Commands.appInstance.meLog.preExcData.isLessThanServerThrottlingTime(preTime))
			{
				// do nothing
			}
			else
			{
				sfsArr.removeElementAt(i);
			}
			
		}
		
		// This is for writing Exc count of server and client.
		
		try
		{
			ExcSummaryLog.writeSummaryLog();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		SerializeGame.serializeSummaryLog();
		
		Commands.appInstance.meLog.writeGameEndLog();
		
	}	
}
