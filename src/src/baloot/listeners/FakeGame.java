package src.baloot.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.Timer;

import src.baloot.beans.GameBean;
import src.baloot.constants.Constant;
import src.baloot.exce.beans.LogBean;
import src.baloot.logs.FakeGameLog;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.User;


public class FakeGame
{
	private Timer timer;
	GameBean gameBean;
	
	public FakeGame(ArrayList<User> list, GameBean gameBean)
	{
		this.gameBean = gameBean;
		//startTimer(list, gameBean);		
	}
	
	/*public void startTimer(ArrayList<User> list, GameBean gameBean)
	{
		stopTimer();	
		int seconds = getRandomNumber(5);
		
				
		timer = new Timer(seconds*1000, new FakeGameListener(list, gameBean, this));
		timer.start();
		
		
		String var = " FakeGame_Timer_Started  delayTime "+seconds+"  isLock "+Commands.appInstance.isLock()+" gameID "+gameBean.gameId;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", Constant.SYSTEM, "FakeGameTimer", var);
		gameBean.addLog(lb);
		gameBean.writeGameLog();
		FakeGameLog.addLog(lb);
	}
	
	public void stopTimer()
	{
		if(timer!= null)
		{
			String var = " FakeGame_Timer_stopped "+"  isLock "+Commands.appInstance.isLock()+" gameID "+gameBean.gameId;
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P0", Constant.SYSTEM, "FakeGameTimer", var);
			gameBean.addLog(lb);
			gameBean.writeGameLog();
			FakeGameLog.addLog(lb);
			
			timer.stop();
		}
	}
	
	public int getRandomNumber(int bound)
	{
		Random generator = new Random();
		
	    int randomInt = generator.nextInt(bound);
	      
	    return (randomInt < 1) ? getRandomNumber(bound) : randomInt;
	}
}



class FakeGameListener implements ActionListener {
	
	ArrayList<User> list;
	GameBean gameBean;
	FakeGame fg; 
	
	public FakeGameListener(ArrayList<User> list, GameBean gameBean, FakeGame fg)
	{
		this.list = list;
		this.gameBean = gameBean;
		this.fg = fg;
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		fg.stopTimer();		
		
		String var = " FakeGameListener ActionPerformed"+"  isLock "+Commands.appInstance.isLock()+" gameID "+gameBean.gameId;
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P0", Constant.SYSTEM, "FakeGameListener", var);
		gameBean.addLog(lb);
		gameBean.writeGameLog();		
		FakeGameLog.addLog(lb);
		
		if(Commands.appInstance.isLock())
		{
			new FakeGame(list, gameBean);
		}
		else
		{
			Commands.appInstance.fakePlayers.addAll(list);
		}
	}	*/
}
