package src.baloot.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;

public class ClientThrottlingActionListener implements ActionListener
{
	@Override
	public void actionPerformed(ActionEvent e)
	{
		
		// for client exceptions
		ISFSArray sfsArr = Commands.appInstance.meLog.getPreviousClientExcData();
		
		for(int i=0; i<sfsArr.size();i++)
		{
			ISFSObject sfso = sfsArr.getSFSObject(i);
			
			String preTime = sfso.getUtfString("time");
			
			if(Commands.appInstance.meLog.preExcData.isLessThanClientThrottlingTime(preTime))
			{
				break;
			}
			else
			{
				sfsArr.removeElementAt(i);
			}			
		}
		
		// for client log data
		ISFSArray sfsLogArr = Commands.appInstance.meLog.getPreviousClenetLogData();
		
		for(int i=0; i<sfsLogArr.size();i++)
		{
			ISFSObject sfso = sfsLogArr.getSFSObject(i);
			
			String preTime = sfso.getUtfString("time");
			
			if(Commands.appInstance.meLog.preExcData.isLessThanClientThrottlingTime(preTime))
			{
				break;
			}
			else
			{
				sfsLogArr.removeElementAt(i);
			}
			
		}
	}
}
