package src.baloot.listeners;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;

import javax.swing.Timer;

import src.baloot.beans.GameBean;
import src.baloot.constants.Constant;
import src.baloot.exce.beans.LogBean;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

public class BidActionListener implements ActionListener
{
	GameBean gameBean;	
	String tableId;
	
	public BidActionListener(String tableId)
	{				
		this.tableId = tableId;
	}
	public void actionPerformed(ActionEvent e) 
	{
		gameBean = Apps.getGameBean(tableId);
		
		if(gameBean != null)
		{				
			Timer timer = gameBean.getTimer();
			
			if(timer != null)
			{
				int delayTime = timer.getDelay()/1000;
				
				String var = " BidActionListener_Timer_Completed  delayTime "+delayTime+"  runningTime "+Apps.getTimeDifference(gameBean.getLastUpdate());
				
				LogBean lb = new LogBean();
				lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "BidTimer", var);
				gameBean.addLog(lb);
				lb = null;	
			}
			
			gameBean.stopTimer();
			
			if(gameBean._roundBean.isGameTypeSelected)
			{
				// bid selected and handle the condition.
				gameBean._roundBean.stopAllPlayerIndividualTimers();
				LogBean lb = new LogBean();
				lb.setValues(Constant.LOG, "P1", Constant.SYSTEM, "BidTimerCompletedSuccessfully", "NoBodySelectsAnyThing");
				gameBean.addLog(lb);
				lb = null;
				
				if(gameBean._roundBean._gameType.equals(Commands.HOKOM) && gameBean._roundBean._bidRoundCount == 2 && gameBean._roundBean.doubleCount == 0)
				{
					gameBean._roundBean.isRemainingAllPlayersPassHokom = true;
					
					gameBean.getBidInstance().selectHokomSuit(gameBean, gameBean._roundBean._bidWonPerson, gameBean._roundBean.bidSelected);
				}
				else
				{
					gameBean.getBidInstance().distributeRemainingCards(gameBean, gameBean._roundBean.bidSelected);
				}
			}
			else
			{
				LogBean lb = new LogBean();
				lb.setValues(Constant.LOG, "P1", Constant.SYSTEM, "BidSelectionFromServer", " bid  pass");
				gameBean.addLog(lb);
				lb = null;
				
				gameBean.getBidInstance().bidSelectedRequest(gameBean, gameBean._roundBean.getTurnForBid(), Commands.PASS, "");
			}
		}
	}
}
