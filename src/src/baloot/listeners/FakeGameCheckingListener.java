package src.baloot.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;

import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerBean;
import src.baloot.beans.TableBean;
import src.baloot.constants.Constant;
import src.baloot.exce.beans.LogBean;
import src.baloot.logs.FakeGameLog;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

public class FakeGameCheckingListener  {
	
	/*@Override
	public void actionPerformed(ActionEvent ae) 
	{
		Commands.appInstance.stopTimer();
		
		try
		{
			if(Commands.appInstance.gameCV.isAiSpan())
			{
				if(Commands.appInstance.fakePlayers.size() > Commands.appInstance.poolSize)
				{
					startGameWithAI();
				}
				else
				{
					String var = " fakePlayersCount "+Commands.appInstance.fakePlayers.size()+" poolSize "+Commands.appInstance.poolSize;
					LogBean lb = new LogBean();
					lb.setValues(Constant.EXC, "P0", Constant.SYSTEM, "LessPoolSizeToStartTheGame", var);
					FakeGameLog.addLog(lb);
				}				
			}			
		}
		catch(Exception e)
		{

			String var = "  isLock "+Commands.appInstance.isLock()+" fakePlayersCount "+Commands.appInstance.fakePlayers.size()+" exception "+e.toString();
			LogBean lb = new LogBean();
			lb.setValues(Constant.EXC, "P0", Constant.SYSTEM, "ExceptionInFakeGameCheckingListener", var);
			FakeGameLog.addLog(lb);
		}
		finally
		{
			Commands.appInstance.startTimer(Apps.getRandomNumber(Constant.FAKE_GAME_INIT_TIME));
		}		
	}
	
	private void startGameWithAI()
	{
		Collections.shuffle(Commands.appInstance.fakePlayers);	
		
		//String tableId = getFreeTable();
		
		String tableId = getRandomFreeTable();
		
		if(tableId != null)
		{
			GameBean gameBean = Apps.getGameBean(tableId);
			
			if(gameBean == null)
			{
				ArrayList<User> fakeList = getFakePlayers();
				
				if(fakeList.size() == 2)
				{
					Apps.createGameRoom(tableId);
					Room room = Apps.getRoomByName(tableId);
					
					gameBean = new GameBean();
					gameBean.tableId = tableId;
					
					for(int i=0;i<2;i++)
					{
						if(i == 0 || i == 1)
						{
							String fakePlayerName = "ramana";
							
							Apps.joinGameRoom(fakeList.get(i), room);
							
							if(i == 0)
							{
								fakePlayerName = fakeList.get(i).getName();
								gameBean.host = fakePlayerName;
							}
							else if(i ==1)
							{
								fakePlayerName = fakeList.get(i).getName();
							}
							
							
							// game logic
							
							gameBean._players.add(fakePlayerName);
							
							PlayerBean pb = new PlayerBean();
							pb._playerId = fakePlayerName;
							pb._aiPlayer = fakePlayerName;					
							pb.position = i;
							pb.isFakePlayer = true;
							
							gameBean._playersBeansList.add(pb);	
							
							if(pb.position == 0)
							{
								pb.isHost = true;
							}
						}					
					}
					
					gameBean.isHostJoined = true;		
					
					// Add the current gameBean to games list
					Commands.appInstance.getGames().put(tableId, gameBean);
					gameBean.incrementTicket();
					
					gameBean.tableType = Constant.PUBLIC;
					
					// Add Log					
					String var = " tableId "+tableId;
					LogBean lb = new LogBean();
					lb.setValues(Constant.LOG, "P0", Constant.SERVER, "StaringFakeGame", var);
					gameBean.addLog(lb);
					gameBean.writeGameLog();	
					
					
					gameBean.initGame();
						
					// Update Lobby
					Commands.appInstance.ulBsn.updateLobby(gameBean);
					
					var = "  isLock "+Commands.appInstance.isLock()+" players "+fakeList+" gameId "+gameBean.gameId;
					lb = new LogBean();
					lb.setValues(Constant.LOG, "P0", Constant.SYSTEM, "FakeGameStartedInListener", var);
					FakeGameLog.addLog(lb);
				}					
			}
			else
			{
				String var = "  isLock "+Commands.appInstance.isLock()+" gameId "+gameBean.gameId;
				LogBean lb = new LogBean();
				lb.setValues(Constant.EXC, "P0", Constant.SYSTEM, "GameFoundWhileStartToFakeGame", var);
				FakeGameLog.addLog(lb);
			}
		}
		else
		{
			String var = "  isLock "+Commands.appInstance.isLock()+" fakePlayersCount "+Commands.appInstance.fakePlayers.size();
			LogBean lb = new LogBean();
			lb.setValues(Constant.EXC, "P0", Constant.SYSTEM, "NoFreeTableAvailable", var);
			FakeGameLog.addLog(lb);
		}	
	}
	
	private String getFreeTable()
	{
		for(int k=0;k<Commands.appInstance.publicTables.size();k++)
		{
			TableBean tb = Commands.appInstance.publicTables.get(k);
			String tableId = tb.getTableId();
			
			GameBean gameBean = Apps.getGameBean(tableId);
			
			if(gameBean == null)
			{
			
				return tableId;
			}				
		}
		
		return null;
	}
	
	private ArrayList<User> getFakePlayers()
	{
		ArrayList<User> fakeList = new ArrayList<User>();
		
		for(User user : Commands.appInstance.fakePlayers)
		{
			fakeList.add(user);
			
			if(fakeList.size() == 2)
			{
				break;
			}
		}
		
		Commands.appInstance.fakePlayers.removeAll(fakeList);
		
		return fakeList;
	}
	
	private String getRandomFreeTable()
	{
		int range = Commands.appInstance.publicTables.size();
		
		Random generator = new Random();
		
	    int randomInt = generator.nextInt(range);	    
	    
	    TableBean tb = Commands.appInstance.publicTables.get(randomInt);
	    
	    GameBean gameBean = Apps.getGameBean(tb.getTableId());	    
	    
	    return gameBean == null ? tb.getTableId() : getFreeTable();	    
	}*/
}
