package src.baloot.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerBean;
import src.baloot.beans.PlayerRoundBean;
import src.baloot.constants.Constant;
import src.baloot.exce.beans.LogBean;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

public class HokomSuitSelectActionListener implements ActionListener
{
	GameBean gameBean;	
	String player;
	String tableId;
	
	public HokomSuitSelectActionListener(String tableId, String player)
	{				
		this.tableId = tableId;
		this.player = player;
	}
	public void actionPerformed(ActionEvent e) 
	{
		gameBean = Apps.getGameBean(tableId);
		
		if(gameBean != null)
		{				
			Timer timer = gameBean.getTimer();
			
			if(timer != null)
			{
				int delayTime = timer.getDelay()/1000;
				
				String var = " HokomSuitSelectActionListener Timer Completed  delayTime "+delayTime+"  runningTime "+Apps.getTimeDifference(gameBean.getLastUpdate());
				
				LogBean lb = new LogBean();
				lb.setValues(Constant.LOG, "P3", Constant.SYSTEM, "HokomSuitSelect", var);
				gameBean.addLog(lb);
				lb = null;	
			}
			
			gameBean.stopTimer();
			
			PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(player);
			
			if(prb != null)
			{
				prb.startTimer(1, Commands.SELECT_HOKOM_SUIT, gameBean, player); 
			}
			else
			{
				// player becomes as ai so get the left player bean.
				
				PlayerBean pb  = gameBean.getLeftPlayerBean(player);
				prb = gameBean._roundBean.getPlayerRoundBeanObject(pb._playerId);
				prb.startTimer(1, Commands.SELECT_HOKOM_SUIT, gameBean, pb._playerId);
			}			
		}
	}
}
