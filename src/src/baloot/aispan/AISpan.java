
package src.baloot.aispan;

import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerBean;
import src.baloot.beans.TableBean;
import src.baloot.constants.Constant;
import src.baloot.exce.beans.LogBean;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

/**
 * This class is only per unit testing purpose. Not for production system.
 * @author Ramana
 *
 */
public class AISpan {
	
	public void initAISpaning()
	{
		for(int k=0;k<1;k++)
		{
			TableBean tb = Commands.appInstance.publicTables.get(k);
			
			String tableId = tb.getTableId();
			Apps.createGameRoom(tableId);
			
			GameBean gameBean = new GameBean();
			gameBean.tableId = tableId;
			
			for(int i=0;i<4;i++)
			{
				if(i == 0 || i == 1)
				{
					String fakePlayerName;
					if(i == 0)
					{
						fakePlayerName = "ramana";
						gameBean.host = fakePlayerName;
					}
					else
					{
						fakePlayerName = "sandhyarani";
					}
					
					
					// game logic
					
					gameBean._players.add(fakePlayerName);
					
					PlayerBean pb = new PlayerBean();
					pb._playerId = fakePlayerName;
					pb._aiPlayer = fakePlayerName;					
					pb.position = i;
					pb.isFakePlayer = true;
					
					gameBean._playersBeansList.add(pb);	
					
					if(pb.position == 0)
					{
						pb.isHost = true;
					}
				}
				else
				{
					String aiName;
					if(i == 2)
					{
						aiName = "AI"+1;
					}
					else
					{
						aiName = "AI"+2;
					}		
					
					
					// game logic
					gameBean._players.add(aiName);
					
					PlayerBean aiBean = new PlayerBean();
					aiBean._playerId = aiName;
					aiBean._aiPlayer = aiName;
					aiBean.position = i;
					aiBean._isAI = true;
					aiBean.gender = "Male";
					
					gameBean._playersBeansList.add(aiBean);	
				}
			}
			
			gameBean.isHostJoined = true;		
			
			// Add the current gameBean to games list
			Commands.appInstance.getGames().put(tableId, gameBean);
			gameBean.incrementTicket();
			
			gameBean.tableType = Constant.PUBLIC;
			
			// Add Log					
			String var = " tableId "+tableId;
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P0", Constant.SERVER, "StaringFakeGame", var);
			gameBean.addLog(lb);
			gameBean.writeGameLog();	
			
			
			gameBean.initGame();
				
			// Update Lobby
			Commands.appInstance.ulBsn.updateLobby(gameBean);			
		}
	}	
}
