/**
 * 
 */
package src.baloot.bsn;

import java.util.ArrayList;


import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerRoundBean;
import src.baloot.classes.NewAICardSelection;
import src.baloot.constants.Constant;
import src.baloot.exce.DataValidation;
import src.baloot.exce.beans.LogBean;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class AIBsn{	
	
	public void anounceHighest(GameBean gameBean, String player)
	{
		// ExceptionValidations 
				
		Room room = Commands.appInstance.getParentZone().getRoomByName(gameBean.tableId);
		
		
		for(int i=0;i<gameBean._roundBean._playerRoundBeanObjs.size();i++)
		{
			if(gameBean._roundBean._playerRoundBeanObjs.get(i)._playerId.equals(player))
			{
				gameBean._roundBean._playerRoundBeanObjs.get(i).isDeclaredHighest = true;				
				break;
			}
		}
		
		ISFSObject sfso = new SFSObject();
		sfso.putUtfString("player",player);
		sfso.putUtfString("gender", Apps.getGender(player) );
		sfso.putInt("ticket", gameBean.getTicket());
		sfso.putInt("roundCount", gameBean.getRoundCount());
		sfso.putInt("trickCount", gameBean._roundBean.trickCount);
		sfso.putBool("isHighest", true);
		
		Commands.appInstance.send(Commands.HIGHEST_CARD,sfso, room.getUserList());	
		
	}	
	
	
	public void playAI(String command, GameBean gameBean, String player)
	{			
		// validations
		
		String var = "Player "+player+" Command :"+command;	
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", player, "PlayAI", var);
		gameBean.addLog(lb);
		lb = null;
		
		Room room = Apps.getRoomByName(gameBean.tableId);
		
		DataValidation.playAI(command, gameBean, player, room);
		
		NewAICardSelection aiCardSelection = new NewAICardSelection();          
		
 		if(command.equals(Commands.SELECT_HOKOM_SUIT))
		{			
			String gameTrumpSuit = aiCardSelection.getHokomSuitInSecondRound(gameBean._roundBean._trumpSuitType);
			
			gameBean.getBidInstance().bidSelectedRequest(gameBean, player, Commands.HOKOM, gameTrumpSuit);
		}		
		else if(command.equals(Commands.BID))
		{						
			String bidSelected = Commands.appInstance.ai.getBidSelected(gameBean, player);			
			String gameTrumpSuit = gameBean._roundBean._trumpSuitType;
			
			gameBean.getBidInstance().bidSelectedRequest(gameBean, player, bidSelected, gameTrumpSuit);
		}
		else if(command.equals(Commands.DISCARD))
		{			
			// Implemented ai logic for getting card id and suit of the card
			Integer cardid = 0; 		
	 		String suit = "null";
	 		
			if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
			{
				cardid = aiCardSelection.playAIHokom(gameBean, player);
				suit = Apps.getSuit(cardid);
			}
			else
			{				
				cardid = aiCardSelection.playAISan(gameBean, player);
				suit = Apps.getSuit(cardid);			
			}			
			
			// Add log
			var = "player "+player+" Card :"+cardid+" trickCount :"+gameBean._roundBean.trickCount;
			lb = new LogBean();
			lb.setValues(Constant.LOG, "P3", player, "AIDiscard", var);
			gameBean.addLog(lb);
			lb = null;
			
			
	 		if(gameBean._roundBean.trickCount == 1)
	 		{
	 			//Check the bonus cards
	 			ArrayList<Integer> cards = new ArrayList<Integer>();
	 			
	 			for(int i=0;i<gameBean._roundBean._playerRoundBeanObjs.size();i++)
	 			{
	 				if(gameBean._roundBean._playerRoundBeanObjs.get(i)._playerId.equals(player))
	 				{
	 					for(int j=0;j<gameBean._roundBean._playerRoundBeanObjs.get(i)._cards.size();j++)
	 					{
	 						cards.add(gameBean._roundBean._playerRoundBeanObjs.get(i)._cards.get(j));
	 					}	 					
	 					break;
	 				}
	 			}
	 			
	 			// Multiple Bonus are implemented
	 			String bonusType = "null";
	 			for(int i=0;i<3;i++)
	 			{
	 				bonusType = Commands.appInstance.ai.getBonusType(cards, gameBean._roundBean._gameType, gameBean._roundBean._trumpSuitType);
	 				if(bonusType.equals("null"))
	 					break;
	 				else
	 				{
	 					ArrayList<Integer> bonuscards = new ArrayList<Integer>();
	 					
	 					if(bonusType.equals("sira"))
	 					{
	 						bonuscards = Commands.appInstance.ai.getSiraCards(cards, "sira");
	 					}
	 					else if(bonusType.equals("50"))
	 					{
	 						bonuscards = Commands.appInstance.ai.getSiraCards(cards, "50");
	 					}
	 					else if(bonusType.equals("100"))
	 					{
	 						bonuscards = Commands.appInstance.ai.getSiraCards(cards, "100");
	 						if(bonuscards.size() == 0)
	 						{
	 							bonuscards = Commands.appInstance.ai.getHundredCards(cards);
	 						} 							
	 					}
	 					else if(bonusType.equals("400"))
	 					{
	 						bonuscards = Commands.appInstance.ai.getFourHundredCards(cards);
	 					}
	 					/*else if(bonusType.equals("baloot"))
	 					{
	 						bonuscards = Commands.appInstance.ai.getBalootCards(cards, gameBean._roundBean._trumpSuitType);
	 					}*/
	 					
	 					// Request for Bonus
	 					gameBean.getDiscardInstance().bonusRequest("requestBonus", bonuscards, bonusType, player, gameBean, true);
	 					
	 					// Remove the remaining cards from the cards list
	 					for(int j=0;j<bonuscards.size();j++)
	 					{
	 						for(int k=0;k<cards.size();k++)
	 						{
	 							if(bonuscards.get(j) == cards.get(k))
	 							{
	 								cards.remove(k);
	 								break;
	 							}
	 						}
	 					}	 						
	 				}
	 			}

	 		}
	 		else if(gameBean._roundBean.trickCount == 2)
	 		{	 			
	 			// Check if bonus is there
	 			if(gameBean._roundBean._bonusObjs.size()>0)
	 			{	 				
	 				if(gameBean._roundBean._highestBonusPerson.equals(player) || gameBean._roundBean._highestBonusPerson.equals(gameBean.getPartner(player)))
	 				{	 			
	 					for(int i=0;i<gameBean._roundBean._bonusObjs.size();i++)
	 					{
	 						if(gameBean._roundBean._bonusObjs.get(i)._playerId.equals(player))
	 						{
	 							if(gameBean._roundBean._bonusObjs.get(i)._bonusType.equals("baloot"))
	 							{
	 								// do nothing
	 							}
	 							else	
	 							{
	 								// Request for show bonus
	 								gameBean.getDiscardInstance().bonusRequest("showBonus", gameBean._roundBean._bonusObjs.get(i)._cards, gameBean._roundBean._bonusObjs.get(i)._bonusType, player, gameBean, true );
	 							}
	 						}
	 					}	 						 					
	 				}	 					 				
	 			}
	 			
	 			/*
	 			 * ramana
	 			 * 
	 			 * checking for requesting the baloot bonus.
	 			 */

	 			if(!gameBean._roundBean.isBalootBonusDeclared)
	 			{	 				
	 				if(gameBean._roundBean._gameType.equals(Commands.HOKOM))
		 			{
		 				ArrayList<Integer> bonusCards = Apps.getBalootBonusCards(gameBean);
		 				
		 				ArrayList<Integer> allCards = new ArrayList<Integer>();
		 				
		 				
		 				PlayerRoundBean prb = gameBean._roundBean.getPlayerRoundBeanObject(player);
		 				
		 				allCards.addAll(prb._cards);
		 				allCards.addAll(prb._playedcards);
		 				
		 				if(allCards.containsAll(bonusCards))
		 				{
		 					gameBean._roundBean.isBalootBonusDeclared = true;
		 					gameBean.getDiscardInstance().bonusRequest("requestBonus", bonusCards, Commands.BALOOT, player, gameBean, true );
		 				}		 				
		 			}
	 			}	 			
	 		}
	 		// Request for discard
	 		gameBean.getDiscardInstance().discardRequest(gameBean, player, cardid, suit, "Server");
		}	
		
	}
}
