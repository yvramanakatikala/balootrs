package src.baloot.bsn;


import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerBean;
import src.baloot.constants.Constant;
import src.baloot.exce.Validations;
import src.baloot.exce.beans.LogBean;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

public class StartGameBsn {
	
	public synchronized void gameCompleted(GameBean gameBean)
	{
		gameBean.stopAllTimers();
		
		Commands.appInstance.proxy.gameCompletedUpdateGameStatus(gameBean, gameBean.gameId);
		
		if(gameBean._roundBean != null)
		{		
			// Save the medal for win over less than 50 points
			if(gameBean._playersBeansList.get(0)._totalPoints >= Constant.GAME_END_POINTS)
			{
				if(gameBean._playersBeansList.get(1)._totalPoints <= 50 )
				{					
					Commands.appInstance.medalsBsn.saveMedal(gameBean._playersBeansList.get(0)._playerId, "winsOverLessthanFiftyPoints", gameBean);
					Commands.appInstance.medalsBsn.saveMedal(gameBean._playersBeansList.get(2)._playerId, "winsOverLessthanFiftyPoints", gameBean);
				}
			}
			else if(gameBean._playersBeansList.get(1)._totalPoints >= Constant.GAME_END_POINTS)
			{
				if(gameBean._playersBeansList.get(0)._totalPoints <= 50 )
				{					
					Commands.appInstance.medalsBsn.saveMedal(gameBean._playersBeansList.get(0)._playerId, "winsOverLessthanFiftyPoints", gameBean);
					Commands.appInstance.medalsBsn.saveMedal(gameBean._playersBeansList.get(2)._playerId, "winsOverLessthanFiftyPoints", gameBean);					
				}
			}		
		}
		
		for(PlayerBean pb : gameBean._playersBeansList)
		{
			int points = 0;
			
			if((pb.position == 0 || pb.position == 2) && gameBean.getWinnersPosition() == 0)
			{
				points = 2;
			}
			else if((pb.position == 1 || pb.position == 3) && gameBean.getWinnersPosition() == 1)
			{
				points = 2;
			}
			
			if(!pb._isAI && !Apps.isGuestUser(pb._playerId))
			{
				Commands.appInstance.proxy.updateUserProfile(gameBean, pb._playerId, points, gameBean.tableId, gameBean.gameId);
			}
		}
		
		String realPlayer = null;
		boolean sign = false;
		for(PlayerBean pb : gameBean._playersBeansList)
		{			
			if(!pb._isAI)
			{
				sign = true;
				realPlayer = pb._playerId;
				break;
			}
		}
		
		setWinnereAndLoosers(gameBean);
		gameBean.isRematchTimerStarted = sign;
		gameBean.getGameLogInstance().gameCompleted(gameBean);
		
		if(sign)
		{	
			LogBean lb = new LogBean();
			lb.setValues(Constant.LOG, "P3", "System", "RealPlayersAvailableForRematch", " realPlayer "+realPlayer);
			gameBean.addLog(lb);

			gameBean.setRematchPlayers();			
			gameBean.startTimer(Constant.REMATCH_TIME, "System", Commands.REMATCH);	
			gameBean.isRematchPhase = true;
		}
		else
		{	
			Commands.appInstance.proxy.gameCompleted(gameBean);
			gameBean.getGameLogInstance().realPlayerNotAvailableToStartRematchTimer(gameBean);
			freeTable(gameBean);
		}
	}
	
	public void freeTable(GameBean gameBean)
	{	
		LogBean lb = new LogBean();
		lb.setValues(Constant.LOG, "P3", "System", "RematchNotStartedCloseRoom", "");
		gameBean.addLog(lb);
		lb = null;
		
		gameBean.isGameCompleted = true;
		gameBean.stopAllTimers();
		Room room = Apps.getRoomByName(gameBean.tableId);		
		
		for(String player : gameBean._players)
		{
			Commands.appInstance.proxy.updatePlayingStatusFalse(gameBean, player);
		}		
		
		if(gameBean.isPrivateTable())
		{
			// Update the lobby for private tables								
			Apps.showLog(" freeTable ");
			String date = Apps.getPrivateTable(gameBean.tableId).getCreatedDate();			
			Commands.appInstance.proxy.closePrivateTable(date);			
			Apps.removePrivateTable(gameBean.tableId);
			
			Commands.appInstance.ulBsn.removeLobbyRoom(gameBean.tableId);
		}
		else
		{
			Commands.appInstance.ulBsn.gameCompletedUpdateLobby(gameBean);
			
			//Apps.showLog("**********************");
			//Apps.showLog("**********************");
			//Apps.showLog("   GAME_COMPLETED_UPDATE_LOBBY ");
			//Apps.showLog("**********************");
			//Apps.showLog("**********************");
			//Apps.showLog("**********************");
		}		
		
		if(room != null)
		{
			Commands.appInstance.send(Commands.CLOSE_ROOM, new SFSObject(), room.getUserList());
			
			//Apps.showLog("********************** ");
			//Apps.showLog("********************** "+room.getSize());
			
			/*for(User user: room.getUserList())
			{
				Apps.showLog("   THIS_IS_CLOSE_ROOM "+user.getName());
			}*/
			//Apps.showLog("   THIS_IS_CLOSE_ROOM ");
			//Apps.showLog("**********************");
			//Apps.showLog("**********************");
			//Apps.showLog("**********************");
		}
		// Remove the room
		if(room != null && !room.getName().equals("Lobby"))
		{
			String var = " "+room.getName()+" closed";
			lb = new LogBean();
			lb.setValues(Constant.LOG, "P3", "System", "RoomRemoved", var);
			gameBean.addLog(lb);
				
			Apps.removeRoom(room);
		}		
		
		gameBean.writeGameLog();
		Commands.appInstance.getGames().remove(gameBean.tableId);
	}
	
	public void sendCurrentTables(String userName)
	{
		User user = Apps.getUserByName(userName);
		
		if(Apps.isGuestUser(userName))
		{
			 if(Commands.appInstance.proxy.isPlayingGame(userName))
			 {
				 //System.out.println("guest_isPlaying  true");
				 //System.out.println("guest_isPlaying  true");
				 
				//String tableId = Commands.appInstance.proxy.getCurrentTalbeId(userName);
				 ISFSObject param = Commands.appInstance.proxy.getCurrentTalbeId(userName);
					
					String tableId = param.getUtfString("tableId");
					
					
				
				if(tableId != null)
				{
					GameBean gameBean = Apps.getGameBean(tableId);
					
					if(gameBean != null)
					{
						
						

						ISFSObject sfso = new SFSObject();
						sfso.putUtfString("tableId", tableId);
						 
						Room room = Apps.getRoomByName(tableId);
						String player = user.getName();
						
						if(user != null && room != null && gameBean._players.contains(userName))
						{
							Apps.joinGameRoom(user, room);
							PlayerBean pb = gameBean.getPlayerBean(player);
							pb.isNewlyOccupiedSeat = true;
							
							gameBean.getGameLogInstance().joinAfterDisconnection(gameBean, pb);
							gameBean.getSyncDataInstance().sendSyncData(gameBean, user.getName());
						}
						else
						{
							Validations.raiseUserorRoomIsNullInSyncData(user, room, gameBean);
						}
					}
					else
					{
						Validations.raiseGameNotAvailableInSyncData(tableId, userName);
					}
				}
			 }
		}
		else if(Commands.appInstance.proxy.isPlayingGame(userName))
		{
			ISFSObject param = Commands.appInstance.proxy.getCurrentTalbeId(userName);
			
			String tableId = param.getUtfString("tableId");
			
			
			/*try			
			{
				tableId = new String(tableId.getBytes("UTF-8"), "UTF-8");
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}*/
			
			
			
			if(!tableId.equals("null"))
			{
				GameBean gameBean = Apps.getGameBean(tableId);
				
				if(gameBean != null)
				{
					ISFSObject sfso = new SFSObject();
					
					sfso.putUtfString("tableId", tableId);
					 
					Room room = Apps.getRoomByName(tableId);
					String player = user.getName();
					
					if(user != null && room != null && gameBean._players.contains(userName))
					{
						Apps.joinGameRoom(user, room);
						PlayerBean pb = gameBean.getPlayerBean(player);
						pb.isNewlyOccupiedSeat = true;
						
						gameBean.getGameLogInstance().joinAfterDisconnection(gameBean, pb);
						gameBean.getSyncDataInstance().sendSyncData(gameBean, user.getName());
					}
					else
					{
						Validations.raiseUserorRoomIsNullInSyncData(user, room, gameBean);
					}
				}
				else
				{
					Validations.raiseGameNotAvailableInSyncData(tableId, userName);
				}
			}
		}
		else
		{
			if(user != null)
			{
				Commands.appInstance.send(Commands.RECONNECTION_COMPLETED, new SFSObject(), user);
			}
		}
	}
	
	public Integer id = 1;
	
	public synchronized void startSinglePlayerGame(User user)
	{
		Apps.showLog(" >>>>>> startGameWithAI >>>>>>>>>> ");
		String player = user.getName();
		
		String roomname = "Baloott"+id;
		id++;
		if(id == 100000)
		{
			id = 1;
		}
		
		Apps.createGameRoom(roomname);		
		Room gameRoom = Apps.getRoomByName(roomname);
		Apps.joinGameRoom(user, gameRoom);
		
		GameBean gameBean = new GameBean();
		gameBean.tableId = roomname;
		gameBean.host = user.getName();
		gameBean.isHostJoined = true;
		gameBean.isSinglePlayer = true;
		gameBean.tableType = Constant.SINGLE_PLAYER;
		gameBean._players.add(player);
		
		PlayerBean pb = new PlayerBean();
		pb._playerId = player;
		pb.isHost = true;
		pb.position = 0;
		
		gameBean._playersBeansList.add(pb);	
		
		gameBean.getGameLogInstance().singlePlayerGame(gameBean, player);
		
		gameBean.initGame();		
		Commands.appInstance.getGames().put(roomname, gameBean);
	}
	
	public void setWinnereAndLoosers(GameBean gameBean)
	{
		if(gameBean.getWinnersPosition() == 0)
		{
			for(PlayerBean pb : gameBean._playersBeansList)
			{
				if(pb.position == 0 || pb.position == 2)
				{
					gameBean.winners.add(pb._playerId);
				}
				else
				{
					gameBean.loosers.add(pb._playerId);
				}
			}
		}
		else if(gameBean.getWinnersPosition() == 1)
		{
			for(PlayerBean pb : gameBean._playersBeansList)
			{
				if(pb.position == 1 || pb.position == 3)
				{
					gameBean.winners.add(pb._playerId);
				}
				else
				{
					gameBean.loosers.add(pb._playerId);
				}
			}
		}
	}
}
