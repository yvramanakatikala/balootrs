/**
 * 
 */
package src.baloot.bsn;

import java.util.ArrayList;



import java.util.Collection;
import java.util.List;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

import src.baloot.beans.GameBean;
import src.baloot.beans.TableBean;
import src.baloot.constants.Constant;
import src.baloot.exce.DataValidation;
import src.baloot.exce.Validations;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

public class UpdateLobbyBsn {

	public void updateLobby(GameBean gameBean)
	{
		// ExceptionValidations		
		
		DataValidation.updateLobby(gameBean);
		
		if(!gameBean.isSinglePlayer)
		{		
			TableBean grb = null;
			Room room = Commands.appInstance.getParentZone().getRoomByName(gameBean.tableId);
			
			for(int i=0;i<Commands.appInstance.publicTables.size();i++)
			{
				if(Commands.appInstance.publicTables.get(i).getTableId().equals(gameBean.tableId))
				{
					grb =  Commands.appInstance.publicTables.get(i);	
					break;
				}
			}
			
			if(grb == null)
			{
				for(int i=0;i<Commands.appInstance.privateTables.size();i++)
				{
					if(Commands.appInstance.privateTables.get(i).getTableId().equals(gameBean.tableId))
					{
						grb =  Commands.appInstance.privateTables.get(i);	
						break;
					}
				}
			}
			
			if(grb != null)
			{			
				if(gameBean.isGameStarted)
				{
					grb.setTeam1Score(gameBean.getPlayerBean(gameBean.initialPlayers.get(0))._totalPoints);
					grb.setTeam2Score(gameBean.getPlayerBean(gameBean.initialPlayers.get(1))._totalPoints);
					
/*					grb.setTeam1Player1(gameBean._playersBeansList.get(0)._playerId);
					grb.setTeam1Player2(gameBean._playersBeansList.get(2)._playerId);
					grb.setTeam2Player1(gameBean._playersBeansList.get(1)._playerId);
					grb.setTeam2Player2(gameBean._playersBeansList.get(3)._playerId);*/
					
					if(gameBean.initialPlayers.size() == 4)
					{
						grb.setTeam1Player1(gameBean.initialPlayers.get(0));
						grb.setTeam1Player2(gameBean.initialPlayers.get(2));
						grb.setTeam2Player1(gameBean.initialPlayers.get(1));
						grb.setTeam2Player2(gameBean.initialPlayers.get(3));
					}
					else
					{
						Validations.raiseLobbyUpdateException(gameBean);
					}
				}
				else
				{			
					grb.clearPreviousData();
					
					if(gameBean._players.size() != 0)
					{
						for(int j=0;j<gameBean._players.size();j++)
						{
							if(j == 0)
								grb.setTeam1Player1(gameBean._players.get(j));
							else if(j == 1)
								grb.setTeam2Player1(gameBean._players.get(j));
							else if(j == 2)
								grb.setTeam1Player2(gameBean._players.get(j));
							else if(j == 3)
								grb.setTeam2Player2(gameBean._players.get(j));
						}	
					}
				}
				
				grb.setNoofSpectators(gameBean.getSpecatatorsCount());
				grb.setIsStarted(gameBean.isGameStarted);
				grb.setSpecators(gameBean.getSpecatatores());
				grb.setHost(gameBean.host);
				
				// Send the Object to room users
				Commands.appInstance.send(Commands.UPDATE_LOBBY, grb.getSFSObject(), room.getUserList() );

				Commands.appInstance.send(Commands.UPDATE_LOBBY, grb.getSFSObject(), getAllUsers());
			}
			else
			{
				Commands.appInstance.trace("************UpdateLobbyBsn : TableBean not found");
			}
		}
		
	}
	
	public void gameCompletedUpdateLobby(GameBean gameBean)
	{		
		String tableId = gameBean.tableId;
		Room room = Apps.getRoomByName(tableId);		
		
		TableBean grb = null;
		for(int i=0;i<Commands.appInstance.publicTables.size();i++)
		{
			if(Commands.appInstance.publicTables.get(i).getTableId().equals(tableId))
			{
				grb =  Commands.appInstance.publicTables.get(i);	
				break;
			}
		}
		
		if(grb == null)
		{
			for(int i=0;i<Commands.appInstance.privateTables.size();i++)
			{
				if(Commands.appInstance.privateTables.get(i).getTableId().equals(tableId))
				{
					grb =  Commands.appInstance.privateTables.get(i);	
					break;
				}
			}
		}
		
		if(grb != null)
		{			
			grb.setTeam1Player1("null");
			grb.setTeam1Player2("null");
			grb.setTeam2Player1("null");
			grb.setTeam2Player2("null");
			grb.setTeam1Score(0);
			grb.setTeam2Score(0);
			grb.setNoofSpectators(0);
			grb.setIsStarted(false);
			
			// Send the Object to room users
			Commands.appInstance.send(Commands.UPDATE_LOBBY, grb.getSFSObject(), room.getUserList());				
			
			Commands.appInstance.send(Commands.UPDATE_LOBBY, grb.getSFSObject(), getAllUsers());
		}
	}
	
	public void addLobbyRoom(TableBean grb)
	{
		
		Commands.appInstance.send(Commands.ADD_LOBBY, grb.getSFSObject(), getAllUsers());
		
	}
	
	public void removeLobbyRoom(String lobbyRoom)
	{
			
		ISFSObject sfso = new SFSObject();
		sfso.putUtfString("roomName", lobbyRoom);
		
		Commands.appInstance.send(Commands.REMOVE_LOBBY, sfso, getAllUsers());
	}
	
	public void sendOnlineUsersCount()
	{
		ISFSObject sfso = new SFSObject();
		
		sfso.putInt("onlineUsersCount", getAllUsers().size());
		
		Commands.appInstance.send(Commands.ONLINE_USERS_COUNT, sfso, getAllUsers());
	}
	
	public  static List<User> getLobbyUsers()
	{
		List<User> listusers = new ArrayList<User>();
	
		Collection<User> users = Commands.appInstance.getParentZone().getUsersInGroup("LobbyGroup");	
		
		Object arr[] = users.toArray();				
		for(int i=0;i<arr.length;i++)
		{
			User no = ((User)arr[i]);
			listusers.add(no);			
		}
		
		return listusers;
	}
	
	public static List<User> getGameGroupUsers()
	{
		List<User> listusers = new ArrayList<User>();
		
		Collection<User> users = Commands.appInstance.getParentZone().getUsersInGroup("GameGroup");	
		
		Object arr[] = users.toArray();				
		for(int i=0;i<arr.length;i++)
		{
			User no = ((User)arr[i]);
			listusers.add(no);			
		}
		
		return listusers;
	}
	
	public static List<User> getAllUsers()
	{
		List<User> listusers = getLobbyUsers();
		
		Collection<User> users = Commands.appInstance.getParentZone().getUsersInGroup("GameGroup");	
		
		Object arr[] = users.toArray();				
		for(int i=0;i<arr.length;i++)
		{
			User no = ((User)arr[i]);
			listusers.add(no);			
		}		
		return listusers;
	}
	
	public Room getLobbyRoom()
	{
		List<Room> lobbyrooms = Commands.appInstance.getParentZone().getRoomListFromGroup(Constant.LOBBY_GROUP);
		
		Room lobbyRoom = null;
		
		for(int i=1;i<=lobbyrooms.size();i++)
		{
			String roomname = "Lobby"+i;			
			lobbyRoom = Commands.appInstance.getParentZone().getRoomByName(roomname);
			
			if(lobbyRoom != null)
			{
				// check
				if(lobbyRoom.getUserList().size() >= 96)
				{
					// check for another room
					Apps.showLog("Room is filled");
				}
				else
				{					
					Apps.showLog("Room is not filled send user to that room");
					break;
				}
			}
			else
			{
				// create room and break;
				Apps.showLog("Room created");
					
				Commands.appInstance.createNewLobbyRoom(roomname);
				
				lobbyRoom = Apps.getRoomByName(roomname);
					
				break;
			}
		}
		
		return lobbyRoom;
	}
	
}
