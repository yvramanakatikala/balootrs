/**
 * 
 */
package src.baloot.bsn;

import src.baloot.beans.GameBean;


import src.baloot.beans.PlayerBean;
import src.baloot.beans.PlayerProfileBean;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.Room;

public class DisconnectBsn
{
	public synchronized void disconnectUser(Room room, String player)
	{		
		// Remove Player from playerIds		5
		Apps.removePlayerId(player);
				
		String tableId = room.getName();				 
		GameBean gameBean = Apps.getGameBean(tableId);
		
		 if(gameBean != null )
		 {			
			gameBean.getGameLogInstance().disconnectionLog(gameBean, player);
			boolean isSpecatatorFound = false;
				
			if(gameBean.getSpecatatores().contains(player))
			{
				gameBean.getSpecatatores().remove(player);
				isSpecatatorFound = true;
				
				gameBean.getGameLogInstance().spectatorDisconnected(gameBean, player, isSpecatatorFound);
				Commands.appInstance.ulBsn.updateLobby(gameBean);							
			}
			 
			if(!isSpecatatorFound && !gameBean.isRematchPhase)
			{		
				for(PlayerBean pb : gameBean._playersBeansList)
				{
					 if(pb._playerId.equals(player))
					 {
						gameBean.getGameLogInstance().userDisconnedted(gameBean, player, isSpecatatorFound);
						 
						 pb.isAutoPlay = true;
						 
						 if(gameBean.isGameStarted && !gameBean.isRoundCompletedResultTime)
						 {
							 Commands.appInstance.ai.checkPlayerTurnBiddingorDiscard(gameBean, player);
						 }
						 
						 checikIsGuestUser(player);
						 break;
					 }
				}
				
				gameBean.disconnectUsers.add(player);
			}
		 }
	}
	
	public void checikIsGuestUser(String player)
	{
		PlayerProfileBean pfb = Apps.getPlayerProfile(player);
		
		if(pfb != null)
		{
			if(pfb.isGuestUser)
			{
				Commands.appInstance.proxy.guestUserDisconnected(player);
				
			}
		}
	}
}
