/**
 * 
 */
package src.baloot.bsn;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import src.baloot.beans.GameBean;
import src.baloot.ping.UserObject;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.Zone;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class MedalsBsn{
	
	Connection con = null;
	
	public MedalsBsn(Zone zone){
		
		try {
			con = zone.getDBManager().getConnection();
			Apps.showLog("SQL Connection Success ");
		} catch (SQLException e) {
			Apps.showLog("SQLProxy SQL Connection Failed :" + e.toString());
		}
	}	
	
	private void sendMedal(Integer medal, Integer count, String player, String roomname)
	{
		Room room = Apps.getRoomByName(roomname); 
		GameBean gameBean = Apps.getGameBean(roomname);
		
		ISFSObject sfso = new SFSObject();	
		
		sfso.putInt("medalId", medal);
		sfso.putInt("count", count);
		sfso.putUtfString("player", player);
		
		Commands.appInstance.send(Commands.WON_MEDAL, sfso, room.getUserList());
		
		if(gameBean != null)
		{
			gameBean.getGameLogInstance().saveMedal(gameBean, player, count, medal);
		}
		
		/*System.out.println("  @@@@@@@@@@@@@@@@@@@@@@@@@@@@ medal "+medal);
		System.out.println("  @@@@@@@@@@@@@@@@@@@@@@@@@@@@ count "+count);
		System.out.println("  @@@@@@@@@@@@@@@@@@@@@@@@@@@@ player "+player);*/
	}
	
	public void updateUserProfile(String player, Integer points, String tableId, GameBean gameBean)throws Exception
	{
		Apps.showLog("*********** updateUserProfile ***********");
		
		Integer wons = 0;
		Integer losts = 0;
		int player_points = 0;
		int newPoints = 0;
		
		if(points == 0)
			losts = 1;		
		else
			wons = 1;
		
		Integer userId = Apps.getPlayerId(player);
		Apps.showLog("userid "+userId);
		
		
		if(userId != null && userId != -1)
		{
			player_points = Commands.appInstance.medalsProxy.getPoints(userId, gameBean);			
			int Totalnoofwins = Commands.appInstance.medalsProxy.getTotalNoofWins(userId, gameBean);
			
			int newPlayerPoints = player_points + points;
			int newTotalnoofwins = Totalnoofwins + wons;
						
			Apps.showLog("Player Points "+ player_points);			
			
			updateTotalNoofWinsMedals(userId, Totalnoofwins, newTotalnoofwins, tableId, player, gameBean);
			
			Commands.appInstance.medalsProxy.updateUserRank(userId, player_points, newPlayerPoints, player, tableId, gameBean);
			
			
			/* NUMBER OF WINS IN A ROW */
						
			if(points != 0)
			{						 
				Commands.appInstance.medalsProxy.updateNoofWinsInaRow(userId, player, tableId, gameBean);
			}
			else
			{
				// Reset
				String sqlUpdate = "update user_data set noof_wins_inarow = 0 where user_id="+userId+";";
				Apps.sqlLog(" MedalBsn >>  update user_data "+sqlUpdate);
								
				try {
					PreparedStatement stmtUpdate = con.prepareStatement(sqlUpdate);
					stmtUpdate.executeUpdate();
					
					stmtUpdate.close();
					
				} catch (Exception e) {
					Apps.sqlErr(" MedalBsn >>  update user_data  "+ e.toString()+" Query "+sqlUpdate, null);
				}
			}	
		}	
		
		
		
		//Formula calculating the new Points
		
		/*if(player_points>=0 && player_points<=54 && points != 0)// for winner
		{
			newPoints = player_points+(points/2);
		}
		else if(player_points>=0 && player_points<=54 && points == 0)// for looser.
		{
			newPoints = player_points;
		}
		else if(player_points >= 55 )
		{
			newPoints = player_points+(points-1);
		}*/
		
		/*
		 * For winners points = 2 and for loosers points = 0.
		 */
		if(player_points >=0 && player_points<=54)// for winner
		{
			newPoints = player_points+(points/2);
		}		
		else if(player_points >= 55 && player_points <= 100)
		{
			newPoints = player_points+(points-1);
		}
		else if(player_points >= 101 && player_points <= 200)
		{
			newPoints = player_points + (points+(points/2) - 2);
		}
		
		
		Apps.showLog("New Points  "+ newPoints);
		
		if(player_points == 0 && newPoints <0)
			newPoints = 0;
		
		if(player_points == 15 && newPoints < 15)
			newPoints = 15;
		
		if(player_points == 35 && newPoints<35)
			newPoints = 35;
				
		if(player_points == 55 && newPoints < 55)
			newPoints = 55;
		
		if(newPoints >= 200)
			newPoints = 200;
		
		Integer count = 1;
		
		String sqlUpdate = "update users set TotalNoofgamesplayed=TotalNoofgamesplayed+"
					+ count	+", Totalnooflost = Totalnooflost+"+losts+",Totalnoofwins=Totalnoofwins+"+wons+",Points="+newPoints+" where UserID="+userId+";";		
		Apps.showLog(sqlUpdate);
		
		
		try {
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.executeUpdate();
			
			pstmt.close();
			
		} catch (Exception e) {
			Apps.sqlErr(" MedalBsn >>  updateUserProfile  "+ e.toString()+" Query "+sqlUpdate, gameBean);
		}

		// Update total no of games played medals
		totalGamePlay(player, gameBean);
		
	}

	public void updateTotalNoofWinsMedals(int userId, int Totalnoofwins, int newTotalnoofwins, String tableId, String player, GameBean gameBean)
	{
		if(Totalnoofwins == 49 && newTotalnoofwins == 50 )
		{				
			sendMedal(Commands.MEDAL_1, 1, player, tableId);
			
			Commands.appInstance.medalsProxy.insertMedalToUser(userId, Commands.MEDAL_1, 1, gameBean);
			
			checkMedal44(player, userId, tableId);										
		}				
		else if(Totalnoofwins == 99 && newTotalnoofwins == 100 )
		{				
			sendMedal(Commands.MEDAL_2,1, player, tableId);
							
			Commands.appInstance.medalsProxy.insertMedalToUser(userId, Commands.MEDAL_2, 1, gameBean);
							
			checkMedal45(player, userId, tableId);
			
		}
		else if(Totalnoofwins == 100 && newTotalnoofwins == 101)
		{
			sendMedal(Commands.MEDAL_51, 1, player, tableId);
			
			Commands.appInstance.medalsProxy.insertMedalToUser(userId, Commands.MEDAL_51, 1, gameBean);			
		}
		else if(Totalnoofwins == 140 && newTotalnoofwins == 141)
		{
			sendMedal(Commands.MEDAL_52, 1, player, tableId);
			
			Commands.appInstance.medalsProxy.insertMedalToUser(userId, Commands.MEDAL_52, 1, gameBean);
		}
		else if(Totalnoofwins == 180 && newTotalnoofwins == 181)
		{
			sendMedal(Commands.MEDAL_53, 1, player, tableId);
			
			Commands.appInstance.medalsProxy.insertMedalToUser(userId, Commands.MEDAL_53, 1, gameBean);
		}
		else if(Totalnoofwins == 199 && newTotalnoofwins == 200 )
		{
			sendMedal(Commands.MEDAL_3, 1, player, tableId);
			
			Commands.appInstance.medalsProxy.insertMedalToUser(userId, Commands.MEDAL_3, 1, gameBean);
			
			checkMedal46(player, userId, tableId);			
		}
		else if(Totalnoofwins == 499 && newTotalnoofwins == 500 )
		{			
			sendMedal(Commands.MEDAL_4, 1, player, tableId);
			
			Commands.appInstance.medalsProxy.insertMedalToUser(userId, Commands.MEDAL_4, 1, gameBean);
			
			checkMedal47(player, userId, tableId);
		}
		else if(Totalnoofwins == 999 && newTotalnoofwins == 1000 )
		{			
			sendMedal(Commands.MEDAL_5,1, player, tableId);
			
			Commands.appInstance.medalsProxy.insertMedalToUser(userId, Commands.MEDAL_5, 1, gameBean);
			
			checkMedal48(player, userId, tableId);
		}
	}
	
	/* No of 32 Cards WON */ /* No of Double Wins */ /* No of wins over less than 50 points */ /* No of san wins */ /* No of hokom wins */
	/* NOOf sign in for days in a row */
	public void saveMedal(String player, String Commandstr, GameBean gameBean)
	{
		String tableId = gameBean.tableId;
		
		if(player.equals("AI1") || player.equals("AI2") || player.equals("AI3"))
		{
			// Do nothing
		}
		else
		{		
			Integer userId = Apps.getPlayerId(player);
			
			if(userId != null && userId != -1)
			{				
				int rowCount = 0;
				int noofthirtytwocardswongames = 0;
				int doubleWins = 0;
				int lessthanfiftypoints = 0;
				int sanWins = 0;
				int hokomWins = 0;
				
				String select = "select * from user_data where user_id="+userId+";";
				
				try
				{
					PreparedStatement pstmt = con.prepareStatement(select);
					ResultSet rs = pstmt.executeQuery();
					
					while (rs.next()) 
					{	
						rowCount = 1;						
						noofthirtytwocardswongames = rs.getInt("noof_thirtytwo_card_wins");
						doubleWins = rs.getInt("noof_double_wins");
						lessthanfiftypoints = rs.getInt("noof_wins_over_lessthan_fifty_points");
						sanWins = rs.getInt("noof_san_wins");
						hokomWins = rs.getInt("noof_hokom_wins");
					}
					
					rs.close();
					pstmt.close();
					
				}catch(Exception e)
				{
					Apps.sqlErr(" MedalsBsn : saveMedal :" + e.toString()+"userId "+userId+"  Commandstr "+Commandstr+"  Query "+select, gameBean);
				}
				
				if(rowCount != 0)
				{					
					if(Commandstr.equals("thirtyTwoCardsWin"))
					{
						noofthirtytwocardswongames++;
						
						if(noofthirtytwocardswongames == 25)
						{							
							sendMedal(Commands.MEDAL_26,1, player, tableId);
							insertMedalToUser(userId, Commands.MEDAL_26, 1, gameBean);							
						}
						else if(noofthirtytwocardswongames == 50)
						{
							sendMedal(Commands.MEDAL_27,1, player, tableId);
							
							insertMedalToUser(userId, Commands.MEDAL_27, 1, gameBean);
						}
						else if(noofthirtytwocardswongames == 100)
						{
							sendMedal(Commands.MEDAL_28,1, player, tableId);
							
							insertMedalToUser(userId, Commands.MEDAL_28, 1, gameBean);
							
							
							checkMedal45(player, userId, tableId);
						}
						
						// Update player Profile
						String sqlUpdate = "update user_data set noof_thirtytwo_card_wins ="+noofthirtytwocardswongames+" where user_id ="+userId+";";		
						//AppMethods.showLog(sqlUpdate);
						updateStatement(sqlUpdate);						
					}
					else if(Commandstr.equals("doubleWins"))
					{						
						doubleWins++;
						
						if(doubleWins == 25)
						{
							sendMedal(Commands.MEDAL_36,1, player, tableId);
							
							insertMedalToUser(userId, Commands.MEDAL_36, 1, gameBean);
							checkMedal47(player, userId, tableId);
						}
						else if(doubleWins == 50)
						{
							sendMedal(Commands.MEDAL_37,1, player, tableId);
							
							insertMedalToUser(userId, Commands.MEDAL_37, 1, gameBean);
							
							checkMedal48(player, userId, tableId);
							
							checkMedal50(player, userId, tableId);								
						}
						// Update player Profile
						String sqlUpdate = "update user_data set noof_double_wins="+doubleWins+" where user_id="+userId+";";		
						//AppMethods.showLog(sqlUpdate);
						updateStatement(sqlUpdate);	
						
					}
					else if(Commandstr.equals("winsOverLessthanFiftyPoints"))
					{						
						lessthanfiftypoints++;
						
						if(lessthanfiftypoints == 25)
						{
							sendMedal(Commands.MEDAL_41,1, player, tableId);
							
							insertMedalToUser(userId, Commands.MEDAL_41, 1, gameBean);
							checkMedal50(player, userId, tableId);
							
						}							
						// Update player Profile
						String sqlUpdate = "update user_data set noof_wins_over_lessthan_fifty_points="+lessthanfiftypoints+" where user_id="+userId+";";		
						//AppMethods.showLog(sqlUpdate);
						updateStatement(sqlUpdate);
					}
					else if(Commandstr.equals("sanWins"))
					{						
						sanWins++;
						if(sanWins == 250)
						{
							sendMedal(Commands.MEDAL_42,1, player, tableId);
							
							insertMedalToUser(userId, Commands.MEDAL_42, 1, gameBean);
							checkMedal50(player, userId, tableId);
						}							
						// Update player Profile
						String sqlUpdate = "update user_data set noof_san_wins="+sanWins+" where user_id="+userId+";";		
						//AppMethods.showLog(sqlUpdate);
						updateStatement(sqlUpdate);
					}
					else if(Commandstr.equals("hokomWins"))
					{						
						hokomWins++;
						
						if(hokomWins == 250)
						{
							sendMedal(Commands.MEDAL_43,1, player, tableId);
							insertMedalToUser(userId, Commands.MEDAL_43, 1, gameBean);
							checkMedal50(player, userId, tableId);
						}							
						// Update player Profile
						String sqlUpdate = "update user_data set noof_hokom_wins="+hokomWins+" where user_id="+userId+";";		
						
						updateStatement(sqlUpdate);
					}			
				}// end of rowCount if block.			
			}
		}
	}
	
	/* END No of 32 Cards WON */ /* End No of Double Wins */
	/* END No of wins over less than 50 points */ /* End  No of san wins */ /* End No of hokom wins */
	/* End NOOf sign in for days in a row */

	/* Number of total game play */
	public void totalGamePlay(String player, GameBean gameBean)
	{
		Integer userId = Apps.getPlayerId(player);
		String tableId = gameBean.tableId;
		int noofgamesplayed = 0;
		
		if(userId != null && userId != -1)
		{
			String sqlSelect = "select * from users where UserID = ?";
			Apps.sqlLog(" MedalsBsn >> totalGamePlay  userId"+userId+"  query "+sqlSelect);
			
			try
			{
				PreparedStatement pstmt = con.prepareStatement(sqlSelect);
				pstmt.setInt(1, userId);			
				ResultSet rs = pstmt.executeQuery();	
				
				while(rs.next())
				{
					noofgamesplayed = rs.getInt("TotalNoofgamesplayed");
				}
				
				rs.close();
				pstmt.close();
				
			}catch(Exception e)
			{
				Apps.sqlErr(" MedalsBsn : totalGamePlay :" + e.toString()+"userId "+userId+"  player "+player+"  Query "+sqlSelect, gameBean);
			}
			
			if(noofgamesplayed == 100)
			{
				sendMedal(Commands.MEDAL_38,1, player, tableId);
				
				insertMedalToUser(userId, Commands.MEDAL_38, 1, gameBean);
								
				checkMedal45(player, userId, tableId);
				
			}
			else if(noofgamesplayed == 250)
			{
				sendMedal(Commands.MEDAL_39,1, player, tableId);
				
				insertMedalToUser(userId, Commands.MEDAL_39, 1, gameBean);
				
				checkMedal46(player, userId, tableId);
				checkMedal47(player, userId, tableId);				
			}
			else if(noofgamesplayed == 500)
			{
				sendMedal(Commands.MEDAL_40,1, player, tableId);
				
				insertMedalToUser(userId, Commands.MEDAL_40, 1, gameBean);
				checkMedal48(player, userId, tableId);
			}
		}
		
	}
	
	
	/***************************/
	/* Combination of medals */
	/***************************/
	/** 1 � �7 � �18 � �32 */
	public void checkMedal44(String player, Integer userId, String room)
	{
				
		try{
		
			String selectStatement = "select * from user_medals where user_id="+userId+" and medal_id in ("+Commands.MEDAL_1+","+Commands.MEDAL_7+","+Commands.MEDAL_18+","+Commands.MEDAL_32+");";
			
			//ResultSet resultSet = Commands.appInstance.proxy.getResultSet(selectStatement);		
			PreparedStatement pstmt = con.prepareStatement(selectStatement);
			ResultSet resultSet = pstmt.executeQuery();
			
			String date = getDateString();
			Integer count = 0;
			int noofrows = 0;
		
			if(resultSet == null)
			{
				// Do nothing
			}
			else
			{
				
				if(resultSet.next())
				{
					do{
					count++;			
					}
					while (resultSet.next());
				}				
				
				if(count == 4)
				{
					// Check for Medal-44				
					selectStatement = "select * from user_medals where user_id="+userId+" and medal_id="+Commands.MEDAL_44+";";
					//AppMethods.showLog(selectStatement);
					//resultSet = Commands.appInstance.proxy.getResultSet(selectStatement);
					PreparedStatement stmt = con.prepareStatement(selectStatement);
					ResultSet rSet = stmt.executeQuery();
					
					noofrows = 0;
					while(rSet.next())
					{
						noofrows++;
					}
					rSet.close();
					stmt.close();
					if(noofrows == 0)
					{
						// send medal Commands.: Medal-44 and Save
						sendMedal(Commands.MEDAL_44,1, player, room);
						
						String sqlInsert ="Insert INTO user_medals(user_id,medal_id,count,created_date) VALUES ("+userId+","+Commands.MEDAL_44+","+1+",'"+date+"');";
						//AppMethods.showLog(sqlInsert);
						insertStatement(sqlInsert);
					}
				}	
			}
			
			resultSet.close();					
			pstmt.close();
		}
		catch(SQLException e)
		{		
			Apps.showLog("checkMedal44 SQL query failed :"+ e.toString() );
		}		
	}
	
	/** �2 � �8 � �19 � �33 � �28 � �38 */
	public void checkMedal45(String player, Integer userId, String room)
	{
		// ExceptionValidations 		
		
		//DataValidation.checkMedal45(player, userId, room);
				
		try{			
		
			String selectStatement = "select * from user_medals where user_id="+userId+" and medal_id in ("+Commands.MEDAL_2+","+Commands.MEDAL_8+","+Commands.MEDAL_19+","+Commands.MEDAL_33+Commands.MEDAL_28+","+Commands.MEDAL_38+");";
			
			//AppMethods.showLog(selectStatement);
			//ResultSet resultSet = Commands.appInstance.proxy.getResultSet(selectStatement);
			PreparedStatement pstmt = con.prepareStatement(selectStatement);
			ResultSet resultSet = pstmt.executeQuery();
			
			String date = getDateString();
			Integer count = 0;
			int noofrows = 0;
			if(resultSet == null)
			{
				// Do nothing
			}
			else
			{			
				if(resultSet.next())
				{
					do{
					count++;			
					}
					while (resultSet.next());
				}			
			
				if(count == 6)
				{
					// Check for Medal-45				
					selectStatement = "select * from user_medals where user_id="+userId+" and medal_id="+Commands.MEDAL_45+";";
					//AppMethods.showLog(selectStatement);
					//resultSet = Commands.appInstance.proxy.getResultSet(selectStatement);
					PreparedStatement stmt = con.prepareStatement(selectStatement);
					ResultSet rSet = stmt.executeQuery();
					
					noofrows = 0;
					while(rSet.next())
					{
						noofrows++;
					}
					rSet.close();
					stmt.close();
					if(noofrows == 0)
					{
						// send medal Commands.: Medal-45 and Save
						sendMedal(Commands.MEDAL_45,1, player, room);
						
						String sqlInsert ="Insert INTO user_medals(user_id,medal_id,count,created_date) VALUES ("+userId+","+Commands.MEDAL_45+","+1+",'"+date+"');";
						//AppMethods.showLog(sqlInsert);
						insertStatement(sqlInsert);
					}
				}
			}
			
			resultSet.close();
			pstmt.close();
			
		}catch(SQLException e){
			Apps.showLog("SQL query failed :"+ e.toString() );
		}		
	}
	
	/** 3 � �9 � �19 � �34 � �29 � �39*/
	public void checkMedal46(String player, Integer userId, String room)
	{
		
		try{			
		
			String selectStatement = "select * from user_medals where user_id="+userId+" and medal_id in ("+Commands.MEDAL_3+","+Commands.MEDAL_9+","+Commands.MEDAL_19+","+Commands.MEDAL_34+Commands.MEDAL_29+","+Commands.MEDAL_39+");";
			//AppMethods.showLog(selectStatement);
			//ResultSet resultSet = Commands.appInstance.proxy.getResultSet(selectStatement);
			PreparedStatement pstmt = con.prepareStatement(selectStatement);
			ResultSet resultSet = pstmt.executeQuery();
			
			String date = getDateString();
			Integer count = 0;
			int noofrows = 0;
			if(resultSet == null)
			{
				// Do nothing
			}
			else
			{			
				if(resultSet.next())
				{
					do{
					count++;			
					}
					while (resultSet.next());
				}				
				if(count == 6)
				{
					// Check for Medal-46				
					selectStatement = "select * from user_medals where user_id="+userId+" and medal_id="+Commands.MEDAL_46+";";
					//AppMethods.showLog(selectStatement);
					//resultSet = Commands.appInstance.proxy.getResultSet(selectStatement);
					PreparedStatement stmt = con.prepareStatement(selectStatement);
					ResultSet rSet = stmt.executeQuery();
					
					noofrows = 0;
					while(rSet.next())
					{
						noofrows++;
					}
					rSet.close();
					stmt.close();
					if(noofrows == 0)
					{
						// send medal Commands.: Medal-46 and Save
						sendMedal(Commands.MEDAL_46,1, player, room);
						
						String sqlInsert ="Insert INTO user_medals(user_id,medal_id,count,created_date) VALUES ("+userId+","+Commands.MEDAL_46+","+1+",'"+date+"');";
						//AppMethods.showLog(sqlInsert);
						insertStatement(sqlInsert);
					}
				}	
			}
			
			resultSet.close();
			pstmt.close();
		}catch(SQLException e){
			Apps.showLog("SQL query failed :"+ e.toString() );
		}
	}
	
	/** �4 � �10 � �20 � �34 � �29 � �39 � �36*/
	public void checkMedal47(String player, Integer userId, String room)
	{				
		try{
			String selectStatement = "select * from user_medals where user_id="+userId+" and medal_id in ("+Commands.MEDAL_4+","+Commands.MEDAL_10+","+Commands.MEDAL_20+","+Commands.MEDAL_34+Commands.MEDAL_29+","+Commands.MEDAL_39+Commands.MEDAL_36+");";
			//AppMethods.showLog(selectStatement);
			//ResultSet resultSet = Commands.appInstance.proxy.getResultSet(selectStatement);
			PreparedStatement pstmt = con.prepareStatement(selectStatement);
			ResultSet resultSet = pstmt.executeQuery();
			
			String date = getDateString();
			Integer count = 0;
			int noofrows = 0;
			if(resultSet == null)
			{
				// Do nothing
			}
			else
			{
			
				if(resultSet.next())
				{
					do{
					count++;			
					}
					while (resultSet.next());
				}				
			
				if(count == 7)
				{
					// Check for Medal-47				
					selectStatement = "select * from user_medals where user_id="+userId+" and medal_id="+Commands.MEDAL_47+";";
					//AppMethods.showLog(selectStatement);
					//resultSet = Commands.appInstance.proxy.getResultSet(selectStatement);
					PreparedStatement stmt = con.prepareStatement(selectStatement);
					ResultSet rSet = stmt.executeQuery();
					
					noofrows = 0;
					while(rSet.next())
					{
						noofrows++;
					}
					rSet.close();
					stmt.close();
					if(noofrows == 0)
					{
						// send medal Commands.: Medal-47 and Save
						sendMedal(Commands.MEDAL_47,1, player, room);
						
						String sqlInsert ="Insert INTO user_medals(user_id,medal_id,count,created_date) VALUES ("+userId+","+Commands.MEDAL_47+","+1+",'"+date+"');";
						//AppMethods.showLog(sqlInsert);
						insertStatement(sqlInsert);
					}
				}	
			}
			
			resultSet.close();
			pstmt.close();
			
		}catch(SQLException e){
			Apps.showLog("SQL query failed :"+ e.toString() );
		}
	}
	
	/** �5 � �11 � �20 � �34 � �30 � �40 � �37 */
	public void checkMedal48(String player, Integer userId, String room)
	{		
		try{
			
		
			String selectStatement = "select * from user_medals where user_id="+userId+" and medal_id in ("+Commands.MEDAL_5+","+Commands.MEDAL_11+","+Commands.MEDAL_20+","+Commands.MEDAL_34+Commands.MEDAL_30+","+Commands.MEDAL_40+Commands.MEDAL_37+");";
			//AppMethods.showLog(selectStatement);
			//ResultSet resultSet = Commands.appInstance.proxy.getResultSet(selectStatement);
			PreparedStatement pstmt = con.prepareStatement(selectStatement);
			ResultSet resultSet = pstmt.executeQuery();
			
			String date = getDateString();
			Integer count = 0;
			int noofrows = 0;
			if(resultSet == null)
			{
				// Do nothing
			}
			else
			{
				
				if(resultSet.next())
				{
					do{
					count++;			
					}
					while (resultSet.next());
				}				
			
				if(count == 7)
				{
					// Check for Medal-48				
					selectStatement = "select * from user_medals where user_id="+userId+" and medal_id="+Commands.MEDAL_48+";";
					//AppMethods.showLog(selectStatement);
					//resultSet = Commands.appInstance.proxy.getResultSet(selectStatement);
					PreparedStatement stmt = con.prepareStatement(selectStatement);
					ResultSet rSet = stmt.executeQuery();
					
					noofrows = 0;
					while(rSet.next())
					{
						noofrows++;
					}
					rSet.close();
					stmt.close();
					if(noofrows == 0)
					{
						// send medal Commands.: Medal-48 and Save
						sendMedal(Commands.MEDAL_48,1, player, room);
						
						String sqlInsert ="Insert INTO user_medals(user_id,medal_id,count,created_date) VALUES ("+userId+","+Commands.MEDAL_48+","+1+",'"+date+"');";
						//AppMethods.showLog(sqlInsert);
						insertStatement(sqlInsert);
					}
				}
			}
			
			resultSet.close();
			pstmt.close();
			
		}catch(SQLException e){
			Apps.showLog("SQL query failed :"+ e.toString() );
		}
	}
	
	/** ��22 � �23 � �24 � �25 */
	public void checkMedal49(String player, Integer userId, String room)
	{		
		try{
			
		
			String selectStatement = "select * from user_medals where user_id="+userId+" and medal_id in ("+Commands.MEDAL_22+","+Commands.MEDAL_23+","+Commands.MEDAL_24+","+Commands.MEDAL_25+");";
			//AppMethods.showLog(selectStatement);
			//ResultSet resultSet = Commands.appInstance.proxy.getResultSet(selectStatement);
			PreparedStatement pstmt = con.prepareStatement(selectStatement);
			ResultSet resultSet = pstmt.executeQuery();
			
			String date = getDateString();
			Integer count = 0;
			int noofrows = 0;
			if(resultSet == null)
			{
				// Do nothing
			}
			else
			{
			
				if(resultSet.next())
				{
					do{
					count++;			
					}
					while (resultSet.next());
				}		
				
				if(count == 4)
				{
					// Check for Medal-49				
					selectStatement = "select * from user_medals where user_id="+userId+" and medal_id="+Commands.MEDAL_49+";";
					//AppMethods.showLog(selectStatement);
					//resultSet = Commands.appInstance.proxy.getResultSet(selectStatement);
					PreparedStatement stmt = con.prepareStatement(selectStatement);
					ResultSet rSet = stmt.executeQuery();
					
					noofrows = 0;
					while(rSet.next())
					{
						noofrows++;
					}
					rSet.close();
					stmt.close();
					
					if(noofrows == 0)
					{
						// send medal Commands.: Medal-49 and Save
						sendMedal(Commands.MEDAL_49,1, player, room);
						
						String sqlInsert ="Insert INTO user_medals(user_id,medal_id,count,created_date) VALUES ("+userId+","+Commands.MEDAL_49+","+1+",'"+date+"');";
						//AppMethods.showLog(sqlInsert);
						insertStatement(sqlInsert);
					}
				}
			}
			
			resultSet.close();
			pstmt.close();
		}catch(SQLException e){
			Apps.showLog("SQL query failed :"+ e.toString() );
		}
	}
	
	/** ��41 � �42 � �43 � �37 */
	public void checkMedal50(String player, Integer userId, String room)
	{	
		
		try{
			String selectStatement = "select * from user_medals where user_id="+userId+" and medal_id in ("+Commands.MEDAL_41+","+Commands.MEDAL_42+","+Commands.MEDAL_43+","+Commands.MEDAL_37+");";
			//AppMethods.showLog(selectStatement);
			//ResultSet resultSet = Commands.appInstance.proxy.getResultSet(selectStatement);
			PreparedStatement pstmt = con.prepareStatement(selectStatement);
			ResultSet resultSet = pstmt.executeQuery();	
			
			String date = getDateString();
			Integer count = 0;
			int noofrows = 0;
			if(resultSet == null)
			{
				// Do nothing
			}
			else
			{
				if(resultSet.next())
				{
					do{
					count++;			
					}
					while (resultSet.next());
				}
				
				if(count == 4)
				{
					// Check for Medal-50				
					selectStatement = "select * from user_medals where user_id="+userId+" and medal_id="+Commands.MEDAL_50+";";
					//AppMethods.showLog(selectStatement);
					//resultSet = Commands.appInstance.proxy.getResultSet(selectStatement);
					PreparedStatement stmt = con.prepareStatement(selectStatement);
					ResultSet rSet = stmt.executeQuery();
					
					
					noofrows = 0;
					while(rSet.next())
					{
						noofrows++;
					}
					
					rSet.close();
					stmt.close();
					if(noofrows == 0)
					{
						// send medal Commands.: Medal-50 and Save
						sendMedal(Commands.MEDAL_50,1, player, room);
						
						String sqlInsert ="Insert INTO user_medals(user_id,medal_id,count,created_date) VALUES ("+userId+","+Commands.MEDAL_50+","+1+",'"+date+"');";
						//AppMethods.showLog(sqlInsert);
						insertStatement(sqlInsert);
					}
				}	
			}
			
			resultSet.close();
			pstmt.close();
			
		}catch(SQLException e){
			Apps.showLog("SQL query failed :"+ e.toString() );
		}
	}
	
	
	public void bonusMedals(String player, ArrayList<String> bonuses, GameBean gameBean)throws Exception
	{		
		Integer userId = Apps.getPlayerId(player);
		String tableId = gameBean.tableId;
		
		if(userId != null && userId != -1)
		{
			String sqlSelect = "select * from user_data where user_id="+userId+";";
			
			PreparedStatement preParedStatement = con.prepareStatement(sqlSelect);
			ResultSet rs = preParedStatement.executeQuery();
			
			while(rs.next())
			{					
				int noofSiras = rs.getInt("noof_sira_bonus");
				int nooffifty = rs.getInt("noof_fifty_bonus");
				int noofhundred = rs.getInt("noof_hundred_bonus");
				int nooffourhundred = rs.getInt("noof_four_hundred_bonus");
				
				for(int i=0;i<bonuses.size();i++)
				{										
					if(bonuses.get(i).equals("sira"))
					{						
						noofSiras++;
						
						Commands.appInstance.medalsProxy.updateNoofSiraBonus(userId, noofSiras, gameBean);
						
						if(noofSiras == 50)
						{
							
							int rowsCount = Commands.appInstance.medalsProxy.getParticularMedalCountofaUser(userId, Commands.MEDAL_22, gameBean);
							
							if(rowsCount == 0)
							{
								sendMedal(Commands.MEDAL_22, 1, player, tableId);
								
								insertMedalToUser(userId, Commands.MEDAL_22, 1, gameBean);
								checkMedal49(player, userId, tableId);
							}
							else
							{
								int count = Commands.appInstance.medalsProxy.getMedalCountofaUser(userId, Commands.MEDAL_22, gameBean);
								
								count++;
								
								sendMedal(Commands.MEDAL_22, count, player, tableId);	
								
								updateMedalsCount(userId, Commands.MEDAL_22, count, gameBean);								
							}
							
						}						
					}
					else if(bonuses.get(i).equals("50"))
					{						
						nooffifty++;
						
						Commands.appInstance.medalsProxy.updateNoofFiftyBonus(userId, nooffifty, gameBean);
						
						if(nooffifty == 30)
						{
							int rowsCount = Commands.appInstance.medalsProxy.getParticularMedalCountofaUser(userId, Commands.MEDAL_23, gameBean);
														
							if(rowsCount == 0)
							{
								sendMedal(Commands.MEDAL_23, 1, player, tableId);
																
								insertMedalToUser(userId, Commands.MEDAL_23, 1, gameBean);
								checkMedal49(player, userId, tableId);
							}
							else
							{
								int count = Commands.appInstance.medalsProxy.getMedalCountofaUser(userId, Commands.MEDAL_23, gameBean);
								
								count++;
								
								sendMedal(Commands.MEDAL_23, count, player, tableId);
								
								updateMedalsCount(userId, Commands.MEDAL_23, count, gameBean);	
								
							}
						}						
					}
					else if(bonuses.get(i).equals("100"))
					{
						noofhundred++;
						
						Commands.appInstance.medalsProxy.updateNoofHundredBonus(userId, noofhundred, gameBean);
						
						if(noofhundred == 20)
						{
							int rowsCount = Commands.appInstance.medalsProxy.getParticularMedalCountofaUser(userId, Commands.MEDAL_24, gameBean);
														
							if(rowsCount == 0)
							{
								sendMedal(Commands.MEDAL_24, 1, player, tableId);
								
								insertMedalToUser(userId, Commands.MEDAL_24, 1, gameBean);
								checkMedal49(player, userId, tableId);
							}
							else
							{
								int count = Commands.appInstance.medalsProxy.getMedalCountofaUser(userId, Commands.MEDAL_24, gameBean);
								
								count++;
								
								sendMedal(Commands.MEDAL_24, count, player, tableId);
								
								updateMedalsCount(userId, Commands.MEDAL_24, count, gameBean);	
								
							}
						}					
					}
					else if(bonuses.get(i).equals("400"))
					{
						nooffourhundred++;
						
						Commands.appInstance.medalsProxy.updateNoofFourHundredBonus(userId, nooffourhundred, gameBean); 
						
						if(nooffourhundred == 10)
						{
							int rowsCount = Commands.appInstance.medalsProxy.getParticularMedalCountofaUser(userId, Commands.MEDAL_25, gameBean);
							
							if(rowsCount == 0)
							{
								sendMedal(Commands.MEDAL_25,1, player, tableId);
								
								insertMedalToUser(userId, Commands.MEDAL_25, 1, gameBean);
								
								checkMedal49(player, userId, tableId);
							}
							else
							{
								int count = Commands.appInstance.medalsProxy.getMedalCountofaUser(userId, Commands.MEDAL_25, gameBean);
								count++;
								
								sendMedal(Commands.MEDAL_25,count, player, tableId);
								
								updateMedalsCount(userId, Commands.MEDAL_25, count, gameBean);	
							}							
						}					
					}
					else if(bonuses.get(i).equals("baloot"))
					{
						// Do nothing
					}
				}			
			}			
			rs.close();
			preParedStatement.close();
		}			
	}	
	
	public void noofSignInForDaysInaRow(String player, String roomName)
	{		
		Integer userId = Apps.getPlayerId(player);
		if(player.equals("AI1") || player.equals("AI2") || player.equals("AI3"))
		{
			// Do nothing
		}
		else
		{			
			if(userId != null && userId != -1)
			{
				int noofsigninfordaysinarow = Commands.appInstance.medalsProxy.getNoofSigninFordaysInarow(userId, null);
				
				//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$ 11");
				//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$  11 ");
				if(noofsigninfordaysinarow == 10)
				{
					int rowsCount = Commands.appInstance.medalsProxy.getParticularMedalCountofaUser(userId, Commands.MEDAL_32, null);
									
					//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$ 22"+rowsCount);
					//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$ 22");
					if(rowsCount == 0)								
					{
						sendMedal(Commands.MEDAL_32, 1, player, roomName);
						
						insertMedalToUser(userId, Commands.MEDAL_32, 1, null);
						
						checkMedal44(player, userId, roomName);
						
						//Here we start one timer in UserObject, 
						//because at this time User is not available.
						//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$");
						//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$");
						UserObject userObj = Apps.getUserObject(player);
						if(userObj != null)
						{
							//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$  55");
							//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$  55");
							userObj.startLoginMedalTimer(player, Commands.MEDAL_32, 1);
						}	
						else
						{
							System.out.println("  rowsCount "+rowsCount);
							System.out.println("  rowsCount "+rowsCount);
						}
					}
				}
				if(noofsigninfordaysinarow == 25)
				{
					int rowsCount = Commands.appInstance.medalsProxy.getParticularMedalCountofaUser(userId, Commands.MEDAL_33, null);
										
					if(rowsCount == 0)
					{
						sendMedal(Commands.MEDAL_33, 1, player, roomName);
						
						insertMedalToUser(userId, Commands.MEDAL_33, 1, null);	
						
						checkMedal45(player, userId, roomName);
					}
				}
				if(noofsigninfordaysinarow == 50)
				{
					int rowsCount = Commands.appInstance.medalsProxy.getParticularMedalCountofaUser(userId, Commands.MEDAL_34, null);
					
					if(rowsCount == 0)
					{
						sendMedal(Commands.MEDAL_34,1, player, roomName);
						insertMedalToUser(userId, Commands.MEDAL_34, 1, null);	
						
						checkMedal46(player, userId, roomName);
						checkMedal47(player, userId, roomName);
						checkMedal48(player, userId, roomName);
					}
				}
				if(noofsigninfordaysinarow == 100)
				{
					int rowsCount = Commands.appInstance.medalsProxy.getParticularMedalCountofaUser(userId, Commands.MEDAL_35, null);
										
					if(rowsCount == 0)
					{
						sendMedal(Commands.MEDAL_35,1, player, roomName);						
						insertMedalToUser(userId, Commands.MEDAL_35, 1, null);								
					}
				}
			}				
		}
	}
	
	private void insertStatement(String sqlInsert) 
	{
		Apps.sqlLog(" MedalBsn >>  insertStatement  "+sqlInsert);
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlInsert);
			
		    pstmt.executeUpdate();	
		    
		    pstmt.close();
		}
		 catch(Exception e){
			 Apps.sqlErr("MedalBsn >>  insertStatement :"+ e.toString()+" Query "+sqlInsert, null);
		}
	}
	
	private void updateStatement(String sqlUpdate) 
	{
		Apps.sqlLog("MedalBsn >> Update Statement :"+ sqlUpdate);
		try {
				PreparedStatement stmtUpdate = con.prepareStatement(sqlUpdate);
				stmtUpdate.executeUpdate();
				
				stmtUpdate.close();
				
			} catch (Exception e) {
				Apps.sqlErr(" MedalBsn >>  updateStatement  "+ e.toString()+" Query "+sqlUpdate, null);
			}
	}
	
	public void insertMedalToUser(int userId, int medalId, int count, GameBean gameBean)
	{
		String date = getDateString();
		
		String sqlInsert ="Insert INTO user_medals(user_id, medal_id, count, created_date) VALUES (?,?,?,?);";
		Apps.sqlLog(" MedalsBsn >> insertMedalToUser  userId "+userId+" medalId "+medalId+"  coiunt "+count+" query "+sqlInsert);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlInsert);
			pstmt.setInt(1, userId);
			pstmt.setInt(2, medalId);
			pstmt.setInt(3, count);
			pstmt.setString(4, date);
			
			pstmt.executeUpdate();
			
			pstmt.close();
			
		}catch(Exception e)
		{
			Apps.sqlErr(" MedalsBsn : insertMedalToUser :" + e.toString()+"userId "+userId+"  medalId "+medalId+" count "+count+"  Query "+sqlInsert, gameBean);
		}		
	}
	
	public void updateMedalsCount(int userId, int medalId, int count, GameBean gameBean)
	{
		String date = getDateString();
		String sqlUpdate = "update user_medals set count = ? modified_date = ? where user_id = ? and medal_id = ?";
		//sqlUpdate ="update user_medals set count="+count+" modified_date='"+date+"', where id="+id+" ;";
		Apps.sqlLog(" MedalsBsn >> updateMedalsCount  userId "+userId+" medalId "+medalId+"  coiunt "+count+" query "+sqlUpdate);
		
		try
		{
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setInt(1, count);
			pstmt.setString(2, date);
			pstmt.setInt(3, userId);
			pstmt.setInt(4, medalId);
			
			pstmt.executeUpdate();
			
			pstmt.close();
			
		}catch(Exception e)
		{
			Apps.sqlErr(" MedalsBsn : updateMedalsCount :" + e.toString()+"userId "+userId+"  medalId "+medalId+" count "+count+"  Query "+sqlUpdate, gameBean);
		}	
	}
	
	public String getDateString()
	{
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Calendar cal = Calendar.getInstance();		
		String date = dateFormat.format(cal.getTime());
		
		return date;
	}
	
}
