package src.baloot.exce;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;

import src.baloot.beans.BonusBean;
import src.baloot.beans.GameBean;
import src.baloot.beans.TableBean;
import src.baloot.beans.PlayerBean;
import src.baloot.beans.PlayerRoundBean;
import src.baloot.constants.Constant;
import src.baloot.exce.beans.ExcBean;
import src.baloot.utils.Commands;

public class DataValidation implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public static void anounceHighest(GameBean gameBean, String player, Room room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);	
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = null;
		}
	}
	
	public static void playAI(String command, GameBean gameBean, String player, Room room)
	{
		
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("command");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(command, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = null;
		}
	}
	
	public static void bonusHandleClientRequest(String command, ArrayList<Integer> cards, String bonusType, String player, GameBean gameBean, Room room , boolean isAI)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("command");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(command, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("cards");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(cards, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("bonusType");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(bonusType, eb);
			
			eb = null;
		}
	}
	
	public static void anounceBalootBonus(GameBean gameBean, String player, ArrayList< Integer> bonusCards, Room room )	
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("bonusCards");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(bonusCards, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = null;
		}
	}
	
	public static void setHighestBonusPerson(GameBean gameBean)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);
			
			eb = null;
		}
	}
	
	public static void getHighestBonusPerson(BonusBean ob1, BonusBean ob2, GameBean gameBean)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("ob1");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(ob1, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("ob2");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(ob2, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);
			
			eb = null;
		}
	}
	
	public static void getPosition(String player, ArrayList<String> arr)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("arr");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(arr, eb);
			
			eb = null;
		}
	}
	
	public static void getHighestCard(ArrayList<Integer> cards)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("cards");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(cards, eb);		
			
			eb = null;
		}
	}
	
	public static void bidhadlerClientRequest(GameBean gameBean, Room room, String player, String bidSelected, String gameTrumpSuit)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("bidSelected");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(bidSelected, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameTrumpSuit");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameTrumpSuit, eb);
			
			eb = null;
		}
	}
	
	public static void distributeRemainingCards(String bidSelected)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("bidSelected");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(bidSelected, eb);
			
			eb = null;
		}
	}
	
	public static void checkPassedAndSendBid(GameBean gameBean, String player, Room room, ISFSObject sfso, String bidSelected )
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("bidSelected");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(bidSelected, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("sfso");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(sfso, eb);
			
			eb = null;
		}
	}
	
	public static void checkIsNumbers(ArrayList<Integer> arr)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("arr");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(arr, eb);	
			
			eb = null;
		}
	}
	
	public static void discardHandleClientRequest(GameBean gameBean, Room room, String player, Integer cardid, String suit, String request )
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("cardid");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(cardid, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("suit");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(suit, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("request");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(request, eb);
			
			eb = null;
		}
	}
	
	public static void checkBalootBonus(Integer cardid, String player, GameBean gameBean, Room room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("cardid");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(cardid, eb);
			
			eb = null;
		}
	}
	
	public static void checkHavingOtherSuitCards(PlayerRoundBean prBean, Integer card, String suit)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("prBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(prBean, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("card");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(card, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("suit");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(suit, eb);
			
			eb = null;
		}
	}
	
	public static void checkSaidBonus(GameBean gameBean, String player)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
					
			eb = null;
		}
	}
	
	public static void showBonusAutomatically(GameBean gameBean, String player, Room room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = null;
		}
	}
	
	public static void checkPartnerSaidShowCards(GameBean gameBean, String player, ISFSObject sfso, Room room, String command )
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("sfso");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(sfso, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("command");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(command, eb);
			
			eb = null;
		}
	}
	
	public static void gameRoundCompleted(GameBean gameBean, String player, Integer cardId, Room room, boolean isBonusFail , boolean isHighestFail, boolean isClosedDoubleFail )
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("cardId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(cardId, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("isBonusFail");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(isBonusFail, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("isHighestFail");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(isHighestFail, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("isClosedDoubleFail");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(isClosedDoubleFail, eb);
			
			eb = null;
		}
	}
	
	public static void disconnect(Room room, String player)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = null;
		}
	}
	
	public static void sendMedal(Integer medal, Integer count, String player, String roomname)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("medal");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(medal, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("count");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(count, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("roomname");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(roomname, eb);
			
			eb = null;
		}
	}
	
	public static void updateUserProfile(String player, Integer points, String room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("points");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(points, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = null;
		}
	}
	
	public static void bonusMedals(String player, ArrayList<String> bonuses, String room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("bonuses");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(bonuses, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = null;
		}
	}
	
	public static void saveMedal(String player, String Commandstr, String room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("Commandstr");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(Commandstr, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = null;
		}
	}
	
	public static void totalGamePlay(String player, String room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = null;
		}
	}
	
	public static void checkMedal44(String player, Integer userId, String room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("userId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(userId, eb);
			
			eb = null;
		}
	}
	
	public static void checkMedal45(String player, Integer userId, String room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("userId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(userId, eb);
			
			eb = null;
		}
	}
	
	public static void checkMedal46(String player, Integer userId, String room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("userId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(userId, eb);
			
			eb = null;
		}
	}
	
	public static void checkMedal47(String player, Integer userId, String room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("userId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(userId, eb);
			
			eb = null;
		}
	}
	
	public static void checkMedal48(String player, Integer userId, String room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("userId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(userId, eb);
			
			eb = null;
		}
	}
	
	public static void checkMedal49(String player, Integer userId, String room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("userId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(userId, eb);
			
			eb = null;
		}
	}
	
	public static void checkMedal50(String player, Integer userId, String room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("userId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(userId, eb);
			
			eb = null;
		}
	}
	
	public static void rematch(GameBean gameBean, Room room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);
			
			eb = null;
		}
	}
	
	public static void closeGame(GameBean gameBean)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();

			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);
			
			eb = null;
		}
	}
	
	public static void replaceUserNameWithAI(GameBean gameBean, PlayerBean pbean)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("pbean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(pbean, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);
			
			eb = null;
		}
	}
	
	public static void rePlaceUser(GameBean gameBean, PlayerBean pbean, String player)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("pbean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(pbean, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("player");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(player, eb);
			
			eb = null;
		}
	}
	
	public static void sinkData(GameBean gameBean, User sender)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("sender");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(sender, eb);
			
			eb = null;
		}
	}
	
	public static void sinkUserData(GameBean gameBean, PlayerBean pb)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("pb");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(pb, eb);
			
			eb = null;
		}
	}
	
	public static void updateLobby(GameBean gameBean)
	{
		
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);
			
			eb = null;
		}
	}
	
	public static void gameCompletedUpdateLobby(String tableId, Room room)
	{
		
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("tableId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(tableId, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = null;
		}
	}
	
	public static void addLobbyRoom(TableBean grb)
	{
		
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("grb");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(grb, eb);
			
			eb = null;
		}
	}
	
	public static void removeLobbyRoom(String lobbyRoom)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("lobbyRoom");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(lobbyRoom, eb);
			
			eb = null;
		}
	}
	
	public static void updateLobbyRoom(TableBean grb, Room room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("grb");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(grb, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = null;
		}
	}
	
	public static void bidHandler(String bidSelected, String gameTrumpSuit, String tableId, GameBean gameBean)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);		
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("tableId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(tableId, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("bidSelected");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(bidSelected, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameTrumpSuit");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameTrumpSuit, eb);
			
			eb = null;
		}
	}
	
	public static void bonusHandler(Collection<Integer> cards, String command, GameBean gameBean)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);		
					
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("cards");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(cards, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("command");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(command, eb);
			
			eb = null;
		}
	}

	public static void createPrivateTableHandler(String command, String tableId, String password, boolean enablePassword, boolean isBlock)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("command");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(command, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("tableId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(tableId, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("password");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(password, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("enablePassword");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(enablePassword, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("isBlock");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(isBlock, eb);
			
			eb = null;
		}
	}
	
	public static void discardHandler(Integer cardId, String tableId, boolean isBonus, ISFSArray bonus, String command)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb.setLogType(Constant.EXC);
			eb.setVariable("cardId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(cardId, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("tableId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(tableId, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("isBonus");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(isBonus, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("bonus");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(bonus, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("command");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(command, eb);
			
			eb = null;
		}
	}
	
	public static void ForceAIHandler(GameBean gameBean, Room room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);
			
			eb = null;
		}
	}
	
	public static void GetLobbyHandler(String command)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("command");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(command, eb);
			
			eb = null;
		}
	}
	
	public static void GetUsersStatusHandler(String command, boolean isUpdate, String searchString, Collection<String> listUsers)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("command");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(command, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("isUpdate");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(isUpdate, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("searchString");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(searchString, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("listUsers");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(listUsers, eb);
			
			eb = null;
		}
	}
	
	public static void HighestCardHandler(GameBean gameBean, Room room)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("room");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(room, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);
			
			eb = null;
		}
	}
	
	public static void HokomSelectedHandler(GameBean gameBean, String tableId)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("tableId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(tableId, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("gameBean");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(gameBean, eb);
			
			eb = null;
		}
	}
	
	public static void HostKickUserHandler(Collection<String> users, String tableId)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("users");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(users, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("tableId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(tableId, eb);
			
			eb = null;
		}
	}
	
	public static void InvitePlayerHandler(String password, Collection<String> invitee)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("password");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(password, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("invitee");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(invitee, eb);
			
			eb = null;
		}
	}
	
	public static void LeaveTableHandler(String tableId)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("tableId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(tableId, eb);
			
			eb = null;
		}
	}
	
	public static void OccupySeatHandler(Integer playerPos, String tableId)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("playerPos");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(playerPos, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("tableId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(tableId, eb);
			
			eb = null;
		}
	}
	
	public static void rematchHandler(String command)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("command");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(command, eb);
			
			eb = null;
		}
	}
	
	public static void userJoinedHandler(String tableId, boolean isPrivateTable, boolean isJoin)
	{
		if(Commands.appInstance.gameCV.isDataValidation())
		{
			ExcBean eb = new ExcBean();
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("tableId");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(tableId, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("isPrivateTable");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(isPrivateTable, eb);
			
			eb = new ExcBean();
			eb.setLogType(Constant.EXC);
			eb.setVariable("isPrivateTable");
			eb.setPriority("P0");		
			PreCondition.checkForAnyNotNull(isPrivateTable, eb);
			
			eb = null;
		}
	}
}
