package src.baloot.exce;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;

import src.baloot.beans.GameBean;
import src.baloot.beans.PlayerBean;
import src.baloot.beans.PlayerProfileBean;
import src.baloot.beans.PlayerRoundBean;
import src.baloot.beans.TableBean;
import src.baloot.constants.Constant;
import src.baloot.exce.beans.ExcBean;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;

public class Validations implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	public static void raiseDuplicateUserException(GameBean gameBean, String player)
	{
		ExcBean eb = new ExcBean();
		eb.setExcMsg("DuplicateUserException");
		eb.setGameID(gameBean.gameId);
		eb.setPlayer(player);
		eb.setPriority("P0");
		eb.setTicket(gameBean.getTicket());
		eb.setAction("DuplicateUserException");
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		
		eb = null;
	}
	
	public static void raiseDuplicateSpectatorException(GameBean gameBean, String player)
	{
		ExcBean eb = new ExcBean();
		eb.setExcMsg("DuplicateSpectatorException");
		eb.setGameID(gameBean.gameId);
		eb.setPlayer(player);
		eb.setPriority("P0");
		eb.setTicket(gameBean.getTicket());
		eb.setAction("DuplicateSpectatorException");
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		
		eb = null;
	}
	
	public static void raiseGameNotAvailableInSyncData(String tableId, String player)
	{
		String var = " GameNotAvailableInSyncData tableId "+tableId+" player "+player;
		ExcBean eb = new ExcBean();
		eb.setExcMsg("GameNotAvailableInSyncData");
		eb.setPlayer(player);
		eb.setPriority("P0");
		eb.setVariable(var);
		eb.setAction("GameNotAvailableInSyncData");
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		eb = null;
	}
	
	public static void raiseUserorRoomIsNullInSyncData(User user, Room room, GameBean gameBean)
	{
		String var = " user "+user+" room "+room+" players "+gameBean._players+" spectators "+gameBean.getSpecatatores();
		ExcBean eb = new ExcBean();
		eb.setExcMsg("UserorRoomIsNullInSyncData");
		eb.setPriority("P0");
		eb.setVariable(var);
		eb.setAction("UserorRoomIsNullInSyncData");
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void riaseHostKickUserException(String tableId, ArrayList<String> players, String host)
	{
		String var = "Host "+host+"  kickUsers "+players+" tableId "+tableId;
		ExcBean eb = new ExcBean();
		eb.setExcMsg("HostKickUserException");
		eb.setLogType(Constant.LOG);
		eb.setPlayer(host);
		eb.setPriority("P1");
		eb.setAction("HostKickUserException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		GameBean gameBean = Apps.getGameBean(tableId);
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void raiseBidRequestAfterDistributionException(GameBean gameBean, String player, String bidSelected, String gameTrumpSuit)
	{
		String var = " isCardsDistributed "+gameBean._roundBean.isRemainingCardsDistributed+" bidSelected "+bidSelected+"  gameTrumpSuit "+gameTrumpSuit;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("BidRequestAfterDistributionException");
		eb.setPlayer(player);
		eb.setPriority("P0");
		eb.setAction("BidRequestAfterDistributionException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
	}
	
	public static void raisePositionNotAvailabeException(GameBean gameBean, String player, int position)
	{
		String var = " player "+player+" position "+position+"   ";
		
		for(PlayerBean pb : gameBean._playersBeansList)
		{
			var = var+"    gamePlayer "+pb._playerId+" fixedPosition "+pb.position;
		}
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("PositionNotAvailabeException");
		eb.setPlayer(player);
		eb.setPriority("P0");
		eb.setAction("PositionNotAvailabeException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
	}
	
	public static void raiseGameStartWithIncompleteDataException(GameBean gameBean)
	{
		String var = " ";
		
		for(PlayerBean pb : gameBean._playersBeansList)
		{
			var = var+"    player "+pb._playerId+" position "+pb.position+"   isAI "+pb._isAI+" isFakePlayer "+pb.isFakePlayer;
		}
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("GameStartWithIncompleteDataException");
		eb.setPriority("P0");
		eb.setAction("GameStartWithIncompleteDataException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void raiseCloseDoubleNotInHokomException(GameBean gameBean, String gameType, String bidSelected, String player)
	{
		String var = " gameType "+gameType+" bidSelected "+bidSelected+" player "+player;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("CloseDoubleNotInHokomException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("CloseDoubleNotInHokomException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void raiseDiscarededCardNotAvailableException(GameBean gameBean, String player, boolean isDeclaredHighest, int cardId, PlayerRoundBean prb)
	{
		String var = " player "+player+"  cardId "+cardId+" cards "+prb._cards.toString()+" isDeclaredHighest "+isDeclaredHighest;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("DiscarededCardNotAvailableException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("DiscarededCardNotAvailableException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void raisePlayerActionAgainstAIActionException(GameBean gameBean, String player, String var)
	{
		ExcBean eb = new ExcBean();
		eb.setExcMsg("PlayerActionAgainstAIActionException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("PlayerActionAgainstAIActionException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void raisePlayerRequestInRemathcPhaseException(GameBean gameBean, String player, String var)
	{
		ExcBean eb = new ExcBean();
		eb.setExcMsg("PlayerRequestInRemathcPhaseException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("PlayerRequestInRemathcPhaseException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void raiseTrickWinnerNotFoundException(GameBean gameBean, String player, String trickWinner)
	{
		String var = "GameStruck player "+player+"  trickWinner "+trickWinner;
	
		ExcBean eb = new ExcBean();
		eb.setExcMsg("TrickWinnerNotFoundException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("TrickWinnerNotFoundException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	public static void raiseTrickCountCardsSizeNotMatchException(GameBean gameBean, PlayerRoundBean prb)
	{
		// Add log			
		String var = "Player "+prb._playerId+" Turn "+gameBean._roundBean.getTurn()+" Played cards size "+prb._playedcards.size()+" trickCount "+gameBean._roundBean.trickCount;
	
		ExcBean eb = new ExcBean();
		eb.setExcMsg("TrickCountCardsSizeNotMatchException");
		eb.setPriority("P0");
		eb.setPlayer(prb._playerId);
		eb.setAction("TrickCountCardsSizeNotMatchException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void raiseRematchRequestNotInRematchPhaseException(GameBean gameBean, String player)
	{
		String var = " player "+player+" isRematchPhase "+gameBean.isRematchPhase;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("RematchRequestNotInRematchPhaseException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("RematchRequestNotInRematchPhaseException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void raiseSpetatorJoinRequestInRematchPhaseException(GameBean gameBean, String player)
	{
		String var = " player "+player+" isRematchPhase "+gameBean.isRematchPhase+" players  "+gameBean._players.toString()
						+" spetators  "+gameBean.getSpecatatores().toString()+" rematchPlayers "+gameBean.rematchPlayers.toString();	
		ExcBean eb = new ExcBean();
		eb.setExcMsg("SpetatorJoinRequestInRematchPhaseException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("SpetatorJoinRequestInRematchPhaseException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void raiseBonusRequestNotInSecondTrickException(GameBean gameBean, String player, String command)
	{
		String var = " command "+command+"  roundCount "+gameBean.getRoundCount()+" trickCount "+gameBean._roundBean.trickCount;
		ExcBean eb = new ExcBean();
		eb.setExcMsg("BonusRequestNotInSecondTrickException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("BonusRequestNotInSecondTrickException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void raiseGameIdNotMatchException(GameBean gameBean, String player, String var)
	{
		ExcBean eb = new ExcBean();
		eb.setExcMsg("GameIdNotMatchException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("GameIdNotMatchException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void raiseRoundCountNotMatchException(GameBean gameBean, String player, String gameId, int roundCount, int trickCount, String handler)
	{
		String var = " gameId "+gameBean.gameId+" roundCount "+gameBean.getRoundCount()+" trickCount "+gameBean._roundBean.trickCount
					+" clientGameId "+gameId+"  clientRoundCount "+roundCount+"  trickCount "+trickCount+"  handler "+handler;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("RoundCountNotMatchException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("RoundCountNotMatchException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void raiseTrickCountNotMatchException(GameBean gameBean, String player, String gameId, int roundCount, int trickCount, String handler)
	{
		String var = " gameId "+gameBean.gameId+" roundCount "+gameBean.getRoundCount()+" trickCount "+gameBean._roundBean.trickCount
				+" clientGameId "+gameId+"  clientRoundCount "+roundCount+"  trickCount "+trickCount+"  handler "+handler;
	
		ExcBean eb = new ExcBean();
		eb.setExcMsg("TrickCountNotMatchException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("TrickCountNotMatchException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
	}
	
	public static void raiseDiscardNotInTurnActionInException(GameBean gameBean, String player, boolean isDeclaredHighest, int cardId, boolean isBonus, int bonusSize, String command)
	{
		String var = " player "+player+" turnPlayer "+gameBean._roundBean.getTurn()+" cardId "+cardId+"  isDeclaredHighest "+isDeclaredHighest+" isBonus "+isBonus+"  bonusSize "
				+bonusSize+" command "+command+" opencards  "+gameBean._roundBean.openCardsList;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("DiscardNotInTurnActionInException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("DiscardNotInTurnActionInException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
	}
	
	public static void raiseBidRoundCountNotMatchException(GameBean gameBean, String player, String gameId, int roundCount, int bidRoundCount, String handler)
	{
		String var = " gameId "+gameBean.gameId+" roundCount "+gameBean.getRoundCount()+" bidRoundCount "+gameBean._roundBean._bidRoundCount
				+" clientGameId "+gameId+"  clientRoundCount "+roundCount+"  clientbidRoundCount "+bidRoundCount+"  handler "+handler;
	
		ExcBean eb = new ExcBean();
		eb.setExcMsg("BidRoundCountNotMatchException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("BidRoundCountNotMatchException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void disconnectedByServer(User user)
	{
		List<Room> joinedRooms = user.getJoinedRooms();
		
		for(Room room : joinedRooms)
		{
			if(room.getGroupId().equals(Constant.GAME_GROUP))
			{
				String tableId = room.getName();
				String player = user.getName();
				String var = "player  "+player+"  tableId "+tableId;
				GameBean gameBean = Apps.getGameBean(tableId);
				
				ExcBean eb = new ExcBean();
				eb.setExcMsg("DisconnectedByServer");
				eb.setPriority("P0");
				eb.setPlayer(player);
				eb.setAction("DisconnectedByServer");
				eb.setVariable(var);
				
				Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
				
				if(gameBean != null)
				{
					gameBean.addExc(eb);
				}
				eb = null;
			}
		}
	}
	
	public static void raiseOccupieSeatPlayerBeanNotAvailableException(GameBean gameBean, String player, PlayerBean pb, int playerPos)
	{
		String var = " PlayerBean "+pb+" position "+playerPos+" isGameStarted "+gameBean.isGameStarted+" requestedPlayer "+player;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("OccupieSeatPlayerBeanNotAvailableException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("OccupieSeatPlayerBeanNotAvailableException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void raiseTwoTimesOccupieSeatInPrivateTableException(GameBean gameBean, String player, int requestedPosition, int occupiedPosition)
	{
		String var = " player "+player+" requestedposition "+requestedPosition+" occupiedPosition "+occupiedPosition;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("TwoTimesOccupieSeatInPrivateTableException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("TwoTimesOccupieSeatInPrivateTableException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void raisePlayerTryToJoinInToSecondTableException(GameBean gameBean, String player, String playingTableId)
	{
		String var = " player "+player+" playingTableId "+playingTableId;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("PlayerTryToJoinInToSecondTableException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("PlayerTryToJoinInToSecondTableException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void raiseLobbyUpdateException(GameBean gameBean)
	{
		String var = " players "+gameBean._players+" initialPlayers "+gameBean.initialPlayers;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("LobbyUpdateException");
		eb.setPriority("P0");
		eb.setPlayer("System");
		eb.setAction("LobbyUpdateException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
		eb = null;
	}
	
	public static void raiseListenerPlayerAndActualPlayerNotMatchException(GameBean gameBean, String listenerPlayer, String actualPlayer, String from, String command)
	{
		String var = "  listenerPlayer "+listenerPlayer+"  actualPlayer "+actualPlayer+"  command  "+command+" from  "+from;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("ListenerPlayerAndActualPlayerNotMatchException");
		eb.setPriority("P0");
		eb.setPlayer("System");
		eb.setAction("ListenerPlayerAndActualPlayerNotMatchException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
	}
	
	public static void raiseNotHavingEnoughGamesToPlayException(GameBean gameBean, String player, int reduceCount, int freeGamesCount, 
			int freeGamesWithIp, int paidGamesCount, boolean isPrivateTable)
	{
		String var = "player :"+player+"  reduceCount "+reduceCount+"  freeGamesCount "+freeGamesCount
				+"  freeGamesWithIp  "+freeGamesWithIp+"  paidGamesCount "+paidGamesCount+" isPrivateTable "+isPrivateTable;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("NotHavingEnoughGamesToPlayException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("NotHavingEnoughGamesToPlayException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
	}
	
	public static void raiseUserIDNULLException(String player, Integer UserID)
	{
		String var = " player "+player+"  UserID "+UserID;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("UserIDNULLException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("UserIDNULLException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
	}
	
	public static void raiseLoginWithSameIPException(User user, String player, String IP)
	{
		String var = " player "+player+"  IP "+IP+"   alreadyLoginUser "+user.getName()+"  ip "+user.getSession().getAddress();
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("LoginWithSameIPException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("LoginWithSameIPException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
	}
	
	public static void raiseLoginWithSameGuestIPException(User user, String player, String IP)
	{
		String var = " player "+player+"  IP "+IP+"   alreadyLoginUser "+user.getName()+"  ip "+user.getSession().getAddress();
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("LoginWithSameGuestIPException");
		eb.setPriority("P0");
		eb.setPlayer(player);
		eb.setAction("LoginWithSameGuestIPException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
	}
	
	public static void updateLobbyAfterGameCompletionException(GameBean gameBean,TableBean tb)
	{
		String var = " team1player1 "+tb.getTeam1Player1()+"  team1player2 "+tb.getTeam1Player2()+"  team2player1 "+tb.getTeam2Player1()
				+" team2player2 "+tb.getTeam2Player2()+" team1Score "+tb.getTeam1Score()+" team2Score "+tb.getTeam2Score()
				+" spectatorsCount "+tb.getSpecators()+" isstarted "+tb.isStarted()+" host "+tb.getHost()
				+" isGameCompleted "+gameBean.isGameCompleted;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("UpdateLobbyAfterGameCompletionException");
		eb.setPriority("P0");
		eb.setPlayer(Constant.SYSTEM);
		eb.setAction("UpdateLobbyAfterGameCompletionException");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
	}
	
	public static void raiseEmailSendLog(int i)
	{
		String var = " The Mail is send  count "+i;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("raiseEmailSendLog");
		eb.setPriority("P0");
		eb.setPlayer(Constant.SYSTEM);
		eb.setAction("raiseEmailSendLog");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
	}
	
	public static void raiseEmailSendExc(int i, String err)
	{
		String var = " Exception while sending mail   Err "+err+"   "+i;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("raiseEmailSendExc");
		eb.setPriority("P0");
		eb.setPlayer(Constant.SYSTEM);
		eb.setAction("raiseEmailSendExc");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
	}
	
	public static void raiseGenderNullInLogin(String userName, int userId)
	{
		String var = " userName "+userName+" userId "+userId;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("raiseGenderNullInLogin");
		eb.setPriority("P0");
		eb.setPlayer(Constant.SYSTEM);
		eb.setAction("raiseGenderNullInLogin");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
	}
	
	public static void raiseGenderNullInSFSReady(String userName, int userId)
	{
		String var = " userName "+userName+" userId "+userId;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("raiseGenderNullInSFSReady");
		eb.setPriority("P0");
		eb.setPlayer(Constant.SYSTEM);
		eb.setAction("raiseGenderNullInSFSReady");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
	}
	
	public static void genderNotFoundDutTo_PFB_null(PlayerProfileBean pfb, String gender, String player, Integer playerId)
	{
		String var = " pfb "+pfb+" gender "+gender+ " player "+player+"  playerId "+playerId;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("genderNotFoundDutTo_PFB_null");
		eb.setPriority("P0");
		eb.setPlayer(Constant.SYSTEM);
		eb.setAction("genderNotFoundDutTo_PFB_null");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
	}
	
	public static void userObjNotAvailableInGetIPBasedFreeGamesCount(GameBean gameBean, String player, User user)
	{
		String var = " player "+player+"  user obj "+user;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("UserObjNotAvailableInGetIPBasedFreeGamesCount");
		eb.setPriority("P0");
		eb.setPlayer(Constant.SYSTEM);
		eb.setAction("UserObjNotAvailableInGetIPBasedFreeGamesCount");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
	}
	
	public static void userObjNotAvailableInupdateIPBasedFreeGamesCount(GameBean gameBean, String player, User user)
	{
		String var = " player "+player+"  user obj "+user;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("userObjNotAvailableInupdateIPBasedFreeGamesCount");
		eb.setPriority("P0");
		eb.setPlayer(Constant.SYSTEM);
		eb.setAction("userObjNotAvailableInupdateIPBasedFreeGamesCount");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
	}
	
	public static void userObjNotAvailableAtTheTimeofUpdatingGames(GameBean gameBean, String player, User user,
			int reduceCount, int freeGamesCount, int freeGamesWithIp, int paidGamesCount, boolean isPrivateTable)
	{
		String var = " player "+player+"  user obj "+user+" reduceCount "+reduceCount+"  freeGamesCount "+freeGamesCount
				+"  freeGamesWithIp "+freeGamesWithIp+"  paidGamesCount "+paidGamesCount+" isPrivateTable "+isPrivateTable;
		
		ExcBean eb = new ExcBean();
		eb.setExcMsg("userObjNotAvailableAtTheTimeofUpdatingGames");
		eb.setPriority("P0");
		eb.setPlayer(Constant.SYSTEM);
		eb.setAction("userObjNotAvailableAtTheTimeofUpdatingGames");
		eb.setVariable(var);
		
		Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, "");
		
		if(gameBean != null)
		{
			gameBean.addExc(eb);
		}
	}
}
