package src.baloot.exce;

import java.io.BufferedWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import src.baloot.constants.Constant;
import src.baloot.exce.beans.ExcBean;
import src.baloot.logs.ExcSummaryLog;
import src.baloot.utils.Apps;
import src.baloot.utils.Commands;


public class WriteExceptions implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	
	public void writeServerLogFile(String str[])throws Exception
	{
		String filePath = Apps.getZonePath()+"/"+Apps.getMonth()+"/"+Apps.getDayString()+"/"+"server_"+Apps.getDayString() + ".txt";
		Path path = Paths.get(filePath);		
		
 		File serverFile = new File(filePath);
 		
 		if(!serverFile.exists())
 		{
 			Files.createDirectories(path.getParent());
 			Files.createFile(path);
 		}

		FileWriter fileWriter = new FileWriter(serverFile, true); 				
		BufferedWriter bwServer = new BufferedWriter(fileWriter); 
			//bwServer.newLine();
		
		for(int i=0;i<str.length;i++)
		{
			bwServer.append(str[i]);
			bwServer.flush();
			bwServer.newLine();
		}
		
		// closing the file.
		bwServer.close();
		fileWriter.close();
	}
	
	public void writeClientLogFile(String str[])throws Exception
	{
		String filePath = Apps.getZonePath()+"/"+Apps.getMonth()+"/"+Apps.getDayString()+"/"+"client_"+Apps.getDayString() + ".txt";
		Path path = Paths.get(filePath);
		
		File clientFile = new File(filePath);
 		
 		if(!clientFile.exists())
 		{
 			Files.createDirectories(path.getParent());
 			Files.createFile(path);
 		}

		FileWriter fileWriter = new FileWriter(clientFile, true); 				
		BufferedWriter bwClient = new BufferedWriter(fileWriter);   
		//bwClient.newLine();
		
		for(int i=0;i<str.length;i++)
		{
			bwClient.append(str[i]);
			bwClient.flush();
			bwClient.newLine();
		}
		
		// closing the file.
		bwClient.close();
		fileWriter.close();
	}
	
	public void writeServerExceptionData(ExcBean eb, String path)
	{		
		String current_time = Apps.getDateString();
		
		String log = eb.getLogType()+" | "+eb.getPriority()+" | "+current_time+" | "+eb.getGameID()+" | "+eb.getPlayer()+" | "+"server"+" |"+eb.getExcMsg()+" | "+eb.getVariable()+" | "+path;
		
		if( Commands.appInstance.gameCV.isWriteServerExceptionns() && eb.getLogType().equals(Constant.EXC))
		{			
			Commands.appInstance.meLog.addServerLog(log);
		}
		
		if(eb.getLogType().equals(Constant.EXC) && eb.getExcMsg() != null)
		{
			ExcSummaryLog.incrementServerExcTypeCount(eb.getExcMsg());
		}
		
		if(eb.getPriority().equals("P0"))
		{
			Commands.appInstance.meLog.addServerP0Log(log);
		}
	}
}
