package src.baloot.exce;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import src.baloot.utils.Apps;

public class P0Exc implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	
	public static void writeServerP0Log(String str[])throws Exception
	{
		String logPath = Apps.getZonePath()+"/"+Apps.getMonth()+"/"+Apps.getDayString()+"/"+"server_P0_"+Apps.getDayString() + ".txt";
		Path path = Paths.get(logPath);		
		
 		File serverFile = new File(logPath);
 		
 		if(!serverFile.exists())
 		{
			Files.createDirectories(path.getParent());
			Files.createFile(path);
 		}

		FileWriter fileWriter = new FileWriter(serverFile, true); 				
		BufferedWriter serverP0Writer = new BufferedWriter(fileWriter); 
			
		for(int i=0;i<str.length;i++)
		{
			serverP0Writer.append(str[i]);
			serverP0Writer.flush();
			serverP0Writer.newLine();
		}
		
		serverP0Writer.close();
		fileWriter.close();
	}
	
	public static void writeClientP0Log(String str[])throws Exception
	{
		String logPath = Apps.getZonePath()+"/"+Apps.getMonth()+"/"+Apps.getDayString()+"/"+"client_P0_"+Apps.getDayString() + ".txt";
		Path path = Paths.get(logPath);		
		
 		File clientFile = new File(logPath);
 		
 		if(!clientFile.exists())
 		{
			Files.createDirectories(path.getParent()); 				
            Files.createFile(path);
 		}
  
		FileWriter fileWriter = new FileWriter(clientFile, true); 				
		BufferedWriter clientP0Writer = new BufferedWriter(fileWriter); 
			//clientP0Writer.newLine();
		
		for(int i=0;i<str.length;i++)
		{
			clientP0Writer.append(str[i]);
			clientP0Writer.flush();
			clientP0Writer.newLine();
		}
		
		clientP0Writer.close();
		fileWriter.close();
	}
}
