package src.baloot.exce;

import java.io.Serializable;

import src.baloot.exce.beans.ExcBean;
import src.baloot.utils.Commands;

public class PreCondition implements Serializable{

	private static final long serialVersionUID = 1L;
	private static AppException appErr;
	private static ErrorInfo errInfo;
	
	
	public static <T> T checkForAnyNotNull(T var, ExcBean eb) 
	{
		String errMessage = eb.getVariable()+" is NULL";
		
		try{
			
			var = checkNotNull(var, errMessage);			
		}
		catch (AppException e) 
		{
			if(Commands.appInstance.meLog.preExcData.isNewServerLog(eb.getPlayer(), errMessage))
			{
				eb.setExcMsg(errMessage);
				Commands.appInstance.meLog.wrExc.writeServerExceptionData(eb, e.errorInfoList.get(0).contextId);
			}
		}	
		
		return var;
	}
	
	private static <T> T checkNotNull(T reference, Object errorMessage) 
	 {
		 if (reference == null) 
		 {
			 appErr = new AppException();
			 errInfo = new ErrorInfo();
			 errInfo.errorMessage = errorMessage.toString();
				
			 appErr.addInfo(errInfo);
			 throw appErr;
		 }
		 return reference;
	  }

}
