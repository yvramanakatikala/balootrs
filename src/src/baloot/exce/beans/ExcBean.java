package src.baloot.exce.beans;

import java.io.Serializable;

import src.baloot.constants.Constant;

public class ExcBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String variable = "NA";
	private String logType = Constant.EXC;
	private String priority = "P4";
	private String player = "NA";
	private String gameID = "NA";
	private String msg = "NA";
	private String excMsg = "NA";
	private String action = "NA";
	private Integer ticket = 0;
	
	public String getVariable() {
		return variable;
	}
	public void setVariable(String variable) {
		this.variable = variable;
	}
	public String getLogType() {
		return logType;
	}
	public void setLogType(String logType) {
		this.logType = logType;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getPlayer() {
		return player;
	}
	public void setPlayer(String player) {
		this.player = player;
	}
	public String getGameID() {
		return gameID;
	}
	public void setGameID(String gameID) {
		this.gameID = gameID;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getExcMsg() {
		return excMsg;
	}
	public void setExcMsg(String excMsg) {
		this.excMsg = excMsg;
	}	
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Integer getTicket() {
		return ticket;
	}
	public void setTicket(Integer ticket) {
		this.ticket = ticket;
	}

	
}
