package src.baloot.exce.beans;

import java.io.Serializable;

public class LogBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String logType = null;
	private String priority = null;
	private String player = null;
	private String action = null;
	private String variables = null;
	private String from = "server";
	
	public void clear()
	{
		logType = null;
		priority = null;
		player = null;
		action = null;
		variables = null;
	}
	
	public void setValues(String logType, String priority, String player, String action, String variables)
	{
		clear();
		
		this.logType = logType;
		this.priority = priority;
		this.player = player;
		this.action = action;
		this.variables = variables;
	}

	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getPlayer() {
		return player;
	}

	public void setPlayer(String player) {
		this.player = player;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getVariables() {
		return variables;
	}

	public void setVariables(String variables) {
		this.variables = variables;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}
	
	

}
