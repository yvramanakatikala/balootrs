package src.baloot.exce;

import java.util.ArrayList;
import java.util.List;


public class AppException extends RuntimeException {
	 
	private static final long serialVersionUID = 1L;
	
	public List<ErrorInfo> errorInfoList = new ArrayList<ErrorInfo>();
	  
	  public ErrorInfo addInfo(ErrorInfo info)
	  {
	    this.errorInfoList.add(info);
	    return info;
	  }

	  public ErrorInfo addInfo()
	  {
	    ErrorInfo info = new ErrorInfo();
	    this.errorInfoList.add(info);
	    return info;
	  }

	  public List<ErrorInfo> getErrorInfoList() 
	  {
	    return errorInfoList;
	  }
	}
